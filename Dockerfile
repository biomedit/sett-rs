FROM gcr.io/distroless/static-debian12

ARG BINARY=/target/release/sett

COPY ${BINARY} /sett

ENTRYPOINT ["/sett"]
