# sshd container

A small container image for a sftp server with 2 factor auth.

## Server

```sh
podman build -t sshd-container .
podman run -it --rm -p 2222:2222 -e "AUTHORIZED_KEY=$(cat ~/.ssh/id_rsa.pub)" --name sshd sshd-container
```

## Client

Install oathtool:

```sh
apt-get install oathtool
```

Test if the server works:

```sh
ssh -vvv -p 2222 user@127.0.0.1
# or with sftp
sftp -vvv -P 2222 user@127.0.0.1
```

Test if sett-rs works:

Either specify an ssh key via env var `SSH_KEY`, or load a key into
the ssh agent. Then:

```sh
cargo run --example two_factor_auth -- user localhost:2222
# using a specific key
cargo run --example two_factor_auth -- user localhost:2222 -k ~/.ssh/id_rsa.pub
```

2nd factor:

```sh
oathtool -b --totp=sha1 <secret from server output>
```
