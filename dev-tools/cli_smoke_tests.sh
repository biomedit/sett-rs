#!/usr/bin/env bash

set -e # Exit immediately on error

S3_ENDPOINT="http://localhost:9000"
S3_BUCKET_NAME="prj"
S3_ACCESS_KEY="minioadmin"
S3_SECRET_KEY=$S3_ACCESS_KEY

SFTP_HOST="localhost"
SFTP_PORT="22"
SFTP_USER="sftp"
SFTP_KEY_PATH="$HOME/.ssh/sftp_ssh_testkey"

HELP="Runs the CLI smoke tests.

Requirements:
  - minio server running on '$S3_ENDPOINT', and a bucket '$S3_BUCKET_NAME'.
  - SFTP server running on '$SFTP_HOST:$SFTP_PORT'.

Usage: $(basename "$0") -d <WORKING-DIR> -e <EMAIL-ADDRESS-KEY> [--s3-endpoint <S3_ENDPOINT>] [--sftp-port <SFTP_PORT>] [-i <SSH-KEY-PATH>] <FILE>

Arguments:
  -e, --email    Email address associated with the OpenPGP key (sender and receiver).
  -d, --dir      Working directory path.
  -i, --ssh-key  SSH key (identity) path for SFTP server. Default is '$SFTP_KEY_PATH'.
  --s3-endpoint  S3 endpoint. Default is '$S3_ENDPOINT'.
  --sftp-port    SFTP server port. Default is '$SFTP_PORT'.
"

if [[ $# -lt 5 ]]; then
  echo "💥 Not enough arguments specified" >&2
  echo "$HELP"
  exit 0
fi

while [[ $# -gt 0 ]]; do
  case $1 in
    -d | --dir)
      if [ -z "$2" ]; then
        echo "💥 Missing working directory" >&2
        echo "$HELP"
        exit 1
      fi
      WORKING_DIR=$2
      shift 2
      if [[ -d "$WORKING_DIR" ]]; then
        OUTPUT_FILE="${WORKING_DIR%/}/package$RANDOM.zip"
      else
        echo "💥 Given working path '$WORKING_DIR' is NOT a directory."
        exit 1
      fi
      ;;
    -e | --email)
      if [ -z "$2" ]; then
        echo "💥 Missing email address" >&2
        echo "$HELP"
        exit 1
      fi
      PRIVATE_KEY_EMAIL=$2
      shift 2
      ;;
    -i | --ssh-key)
      if [ -z "$2" ]; then
        echo "💥 Missing SSH key" >&2
        echo "$HELP"
        exit 1
      fi
      SFTP_KEY_PATH=$2
      shift 2
      ;;
    --s3-endpoint)
      if [ -z "$2" ]; then
        echo "💥 Missing S3 endpoint" >&2
        echo "$HELP"
        exit 1
      fi
      S3_ENDPOINT=$2
      shift 2
      ;;
    --sftp-port)
      if [ -z "$2" ]; then
        echo "💥 Missing SFTP port" >&2
        echo "$HELP"
        exit 1
      fi
      SFTP_PORT=$2
      shift 2
      ;;
    -*)
      echo "💥 Invalid option: '$1'" >&2
      echo "$HELP"
      exit 1
      ;;
    *)
      FILE=$1
      shift
      ;;
  esac
done

if [ -z "$FILE" ]; then
  echo "💥 Missing file to package" >&2
  echo "$HELP"
  exit 1
fi

echo "👷Building the binary, might take some time (happens only once) ..."
cargo build --release --bin sett
CMD="./target/release/sett"

read -resp "Private OpenPGP key password: " SETT_OPENPGP_KEY_PWD
export SETT_OPENPGP_KEY_PWD
echo

echo "📦 Packaging given file '$FILE' into '$OUTPUT_FILE' ..."
$CMD encrypt local -r "$PRIVATE_KEY_EMAIL" -s "$PRIVATE_KEY_EMAIL" -o "$OUTPUT_FILE" "$FILE"
echo

echo "🛫 Transferring package '$OUTPUT_FILE' to S3 endpoint '$S3_ENDPOINT' ..."
$CMD transfer s3 -b "$S3_BUCKET_NAME" --access-key "$S3_ACCESS_KEY" --secret-key "$S3_SECRET_KEY" -e "$S3_ENDPOINT" "$OUTPUT_FILE"
echo

echo "🚢 Shipping package '$OUTPUT_FILE' to SFTP endpoint '$SFTP_HOST:$SFTP_PORT' ..."
$CMD transfer sftp -H "$SFTP_HOST" -P "$SFTP_PORT" -u "$SFTP_USER" --base-path upload --key-path "$SFTP_KEY_PATH" "$OUTPUT_FILE"
echo

echo "🔐 Directly encrypting '$FILE' to S3 endpoint '$S3_ENDPOINT' ..."
$CMD encrypt s3 -b "$S3_BUCKET_NAME" --access-key "$S3_ACCESS_KEY" --secret-key "$S3_SECRET_KEY" -e "$S3_ENDPOINT" -r "$PRIVATE_KEY_EMAIL" -s "$PRIVATE_KEY_EMAIL" "$FILE"
echo

echo "⏳ Directly encrypting '$FILE' to SFTP endpoint '$SFTP_HOST:$SFTP_PORT' ..."
$CMD encrypt sftp -H "$SFTP_HOST" -P "$SFTP_PORT" -u "$SFTP_USER" --base-path upload --key-path "$SFTP_KEY_PATH" -r "$PRIVATE_KEY_EMAIL" -s "$PRIVATE_KEY_EMAIL" "$FILE"
echo

echo "🔓 Decrypting local package '$OUTPUT_FILE' to '$WORKING_DIR'..."
$CMD decrypt local -o "$WORKING_DIR" "$OUTPUT_FILE"
echo

echo "🔓 Decrypting package from S3 endpoint '$S3_ENDPOINT' to '$WORKING_DIR'..."
$CMD decrypt s3 -b "$S3_BUCKET_NAME" --access-key "$S3_ACCESS_KEY" --secret-key "$S3_SECRET_KEY" -e "$S3_ENDPOINT" -o "$WORKING_DIR" "$(basename "$OUTPUT_FILE")"
