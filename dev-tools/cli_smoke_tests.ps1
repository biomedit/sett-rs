# PowerShell script to perform sett CLI smoke tests

param(
    [string]$WorkingDir,
    [string]$Email,
    [string]$FilePath,
    [string]$S3Endpoint = "http://localhost:9000",
    [string]$S3BucketName = "prj",
    [string]$S3AccessKey = "minioadmin",
    [string]$S3SecretKey = "minioadmin",
    [string]$SFTPHost = "localhost",
    [string]$SFTPPort = "22",
    [string]$SFTPUser = "sftp",
    [string]$SFTPKeyPath = "$HOME\.ssh\sftp_ssh_testkey"
)

function Show-Help {
    Write-Host @"
Runs the sett CLI smoke tests.

Requirements:
  - An S3-compatible server (e.g., MinIO) running at 'S3Endpoint', with a bucket named 'S3BucketName'.
  - An SFTP server running at 'SFTPHost:SFTPPort'.

Parameters:
  -WorkingDir      (Required) Output directory for test data.
  -Email           (Required) Email address of an OpenPGP key used for the test.
  -FilePath        (Required) Path to the input file/folder for the test.
  -S3Endpoint      (Optional) The S3-compatible server endpoint. Default: '${S3Endpoint}'.
  -S3BucketName    (Optional) Name of the S3 bucket to use. Default: '${S3BucketName}'.
  -S3AccessKey     (Optional) Access key for the S3 server. Default: '${S3AccessKey}'.
  -S3SecretKey     (Optional) Secret key for the S3 server. Default: '${S3SecretKey}'.
  -SFTPHost        (Optional) Hostname of the SFTP server. Default: '${SFTPHost}'.
  -SFTPPort        (Optional) Port for the SFTP server. Default: '${SFTPPort}'.
  -SFTPUser        (Optional) Username for the SFTP server. Default: '${SFTPUser}'.
  -SFTPKeyPath     (Optional) Path to the SSH private key for SFTP authentication. Default: '${SFTPKeyPath}'.

Usage:
  .\cli_smoke_tests.ps1 -WorkingDir <WORKING-DIR> -Email <EMAIL-ADDRESS> -FilePath <FILE-PATH>
      [-S3Endpoint <S3-ENDPOINT>] [-S3BucketName <S3-BUCKET-NAME>] [-S3AccessKey <S3-ACCESS-KEY>] 
      [-S3SecretKey <S3-SECRET-KEY>] [-SFTPHost <SFTP-HOST>] [-SFTPPort <SFTP-PORT>] 
      [-SFTPUser <SFTP-USER>] [-SFTPKeyPath <SSH-KEY-PATH>]
"@
}

# Validate required parameters
if (-not $WorkingDir -or -not $Email -or -not $FilePath) {
    Write-Error "Missing required arguments"
    Show-Help
    exit 1
}

# Verify working directory
if (-not (Test-Path $WorkingDir -PathType Container)) {
    Write-Error "Given working path '$WorkingDir' is NOT a directory."
    exit 1
}

$OutputFile = Join-Path $WorkingDir "package$((Get-Random)).zip"

# Run a command and handle errors
function Run-Command {
    param (
        [string]$Command,
        [string[]]$Arguments
    )
    try {
        & $Command @Arguments
    } catch {
        Write-Error "Command failed: $Command"
        Write-Error $_.Exception.Message
        if ($_.InvocationInfo.Line) {
            Write-Error "Error occurred at: $($_.InvocationInfo.Line)"
        }
        exit 1
    }
}

$Cmd = ".\target\release\sett"

# Prompt for OpenPGP key password
$OpenPGPKeyPwd = Read-Host -Prompt "Private OpenPGP key password" -AsSecureString
$Env:SETT_OPENPGP_KEY_PWD = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($OpenPGPKeyPwd))

try {
  Write-Host "Building the binary, might take some time (happens only once) ..."
  & cargo build --release --bin sett --no-default-features --features crypto-cng --features auth

  Write-Host "Packaging file $FilePath into $OutputFile ..."
  Run-Command $Cmd @("encrypt", "local", "-r", $Email, "-s", $Email, "-o", $OutputFile, $FilePath)

  Write-Host "Transferring package $OutputFile to S3 endpoint $S3Endpoint ..."
  Run-Command $Cmd @("transfer", "s3", "-b", $S3BucketName, "--access-key", $S3AccessKey, "--secret-key", $S3SecretKey, "-e", $S3Endpoint, $OutputFile)

  Write-Host "Shipping package $OutputFile to SFTP endpoint $SFTPHost (port: $SFTPPort) ..."
  Run-Command $Cmd @("transfer", "sftp", "-H", $SFTPHost, "-P", $SFTPPort, "-u", $SFTPUser, "--base-path", "upload", "--key-path", $SFTPKeyPath, $OutputFile)

  Write-Host "Directly encrypting $FilePath to S3 endpoint $S3Endpoint ..."
  Run-Command $Cmd @("encrypt", "s3", "-b", $S3BucketName, "--access-key", $S3AccessKey, "--secret-key", $S3SecretKey, "-e", $S3Endpoint, "-r", $Email, "-s", $Email, $FilePath)

  Write-Host "Directly encrypting $FilePath to SFTP endpoint $SFTPHost (port: $SFTPPort) ..."
  Run-Command $Cmd @("encrypt", "sftp", "-H", $SFTPHost, "-P", $SFTPPort, "-u", $SFTPUser, "--base-path", "upload", "--key-path", $SFTPKeyPath, "-r", $Email, "-s", $Email, $FilePath)

  Write-Host "Decrypting local package $OutputFile to $WorkingDir ..."
  Run-Command $Cmd @("decrypt", "local", "-o", $WorkingDir, $OutputFile)

  Write-Host "Decrypting package from S3 endpoint $S3Endpoint to $WorkingDir ..."
  Run-Command $Cmd @("decrypt", "s3", "-b", $S3BucketName, "--access-key", $S3AccessKey, "--secret-key", $S3SecretKey, "-e", $S3Endpoint, "-o", $WorkingDir, (Split-Path -Leaf $OutputFile))
} finally {
  Remove-Item Env:\SETT_OPENPGP_KEY_PWD -ErrorAction SilentlyContinue
}
