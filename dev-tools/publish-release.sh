#!/usr/bin/env sh

# Publishes artifacts to the GitLab registry and creates a new release.
#
# Note: This script is intended to be run from a GitLab CI job and expects the
# following environment variables to be set:
#
# - GITLAB_API_TOKEN: GitLab API token with API write access (maintainer level)
#   to the project (needed for updating the version snippet).
#
# The remaining variables are set by GitLab CI automatically.

set -e

prefix=$(echo "${CI_COMMIT_TAG}" | cut -d'/' -f1)
version=$(echo "${CI_COMMIT_TAG}" | cut -d'/' -f2)
registry_url="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${prefix}/${version}"

find_file() {
  find assets -type f -name "$1" -print
}

get_registry_url() {
  echo "${registry_url}/$(basename "$1")"
}

upload_files() {
  echo "Uploading files to ${registry_url}"
  for path in $1; do
    curl -sS --fail-with-body \
      --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
      --upload-file "${path}" "$(get_registry_url "$path")"
    echo "Uploaded ${path}"
  done
}

create_release() {
  # Creates a new release on GitLab with a permanent link for each asset
  # associated to the release. The permanent links have no version number in
  # the file names, so that the automatically generated links to the latest
  # version always point to the latest version.
  # Links end in `/sett-cli/<name of file>` for CLI binaries, and
  # `/sett-gui/<name of file>` for GUI binaries.
  #
  # Example of permanent link to the latest release.
  # * https://gitlab.com/biomedit/sett-rs/-/releases/permalink/latest/downloads/sett-cli/<name of file>

  echo "Creating GitLab release"
  data=""
  for path in $1; do
    versionless_name=$(basename "$path" | sed -r 's/[_-][0-9]+.[0-9]+.[0-9]+(-rc.[0-9]+)?//')
    prefix=$(
      case "$versionless_name" in
        "sett-gui"*) echo "sett-gui" ;;
        *) echo "sett-cli" ;;
      esac
    )
    data="${data}$(basename "$path") /$prefix/$versionless_name $(get_registry_url "$path");"
  done
  description="[CHANGELOG](https://${CI_SERVER_HOST}/${CI_PROJECT_PATH}/-/blob/${prefix}/${version}/${prefix}/CHANGELOG.md)"
  release=$(
    jq --null-input --compact-output --raw-output \
      '{
        "name": $name,
        "tag_name": env.CI_COMMIT_TAG,
        "description": $description,
        "assets": {
          "links": $links | split(";") | map(select(length > 1)) | map(split(" ")) | map(
            {
              "name": .[0],
              "filepath": .[1],
              "url": .[2],
              "link_type": "package"
            }
          )
        }
      }' \
      --arg name "Release ${CI_COMMIT_TAG}" \
      --arg description "$description" \
      --arg links "$data"
  )
  echo "$release" | curl --fail-with-body \
    --request POST \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases" \
    --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
    --header "Content-Type: application/json" \
    -d @-
}

if [ "${prefix}" = "sett-cli" ]; then
  echo "Publishing ${prefix} artifacts"
  files="\
    assets/sett-cli_${version}_x86_64-unknown-linux-gnu \
    assets/sett-cli_${version}_x86_64-unknown-linux-musl \
    assets/sett-cli_${version}_aarch64-unknown-linux-gnu \
    assets/sett-cli_${version}_aarch64-unknown-linux-musl \
    assets/sett-cli_${version}_x86_64-apple-darwin \
    assets/sett-cli_${version}_aarch64-apple-darwin \
    assets/sett-cli_${version}_x86_64-pc-windows-msvc.exe \
    assets/sett-cli_${version}*.deb"
  upload_files "$files"
  create_release "$files"

elif [ "${prefix}" = "sett-gui" ]; then
  echo "Publishing ${prefix} artifacts"
  snippet_id=$(sed -n 's|.*/snippets/\([0-9]*\).*|\1|p' <sett-gui/src-tauri/tauri.conf.json)

  macos_updater_path="assets/sett-gui.app.tar.gz"
  macos_dmg_path=$(find_file "sett-gui_${version}*.dmg")
  linux_updater_path=$(find_file "sett-gui_${version}*.AppImage.tar.gz")
  linux_appimage_path=$(find_file "sett-gui_${version}*.AppImage")
  linux_deb_path=$(find_file "sett-gui_${version}*.deb")
  linux_exec_path="assets/sett-gui_${version}_x86_64-unknown-linux-gnu"
  windows_updater_path=$(find_file "sett-gui_${version}*-setup.nsis.zip")
  windows_nsis_path=$(find_file "sett-gui_${version}*-setup.exe")
  windows_exec_path="assets/sett-gui_${version}_x86_64-pc-windows-msvc.exe"

  updater_files="$macos_updater_path $linux_updater_path $windows_updater_path"
  app_files="$linux_exec_path $linux_appimage_path $linux_deb_path $macos_dmg_path $windows_exec_path $windows_nsis_path"

  upload_files "$app_files $updater_files"

  echo "Updating updater version snippet (id: ${snippet_id})"
  snippet=$(
    jq --null-input --compact-output --raw-output \
      '{
        "title": "sett-gui updater",
        "description": "Metadata for the current sett-gui release",
        "visibility": "public",
        "files": [
          {
            "action": "update",
            "file_path": "version.json",
            "content": {
              "version": $version,
              "notes": "\nRelease highlights:\nhttps://biomedit.gitlab.io/blog/\n\nDetailed changelog:\nhttps://gitlab.com/biomedit/sett-rs/-/blob/main/sett-gui/CHANGELOG.md",
              "pub_date": now | todateiso8601,
              "platforms": {
                "darwin-aarch64": {"signature": $darwin_sig, "url": $darwin_url},
                "darwin-x86_64": {"signature": $darwin_sig, "url": $darwin_url},
                "linux-x86_64": {"signature": $linux_sig, "url": $linux_url},
                "windows-x86_64": {"signature": $windows_sig, "url": $windows_url},
              }
            } | tostring
          }
        ]
      }' \
      --arg version "${version}" \
      --arg darwin_sig "$(cat "${macos_updater_path}.sig")" \
      --arg darwin_url "$(get_registry_url "${macos_updater_path}")" \
      --arg linux_sig "$(cat "${linux_updater_path}.sig")" \
      --arg linux_url "$(get_registry_url "${linux_updater_path}")" \
      --arg windows_sig "$(cat "${windows_updater_path}.sig")" \
      --arg windows_url "$(get_registry_url "${windows_updater_path}")"
  )

  echo "$snippet" | curl --fail-with-body \
    --request PUT \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/snippets/${snippet_id}" \
    --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" \
    --header "Content-Type: application/json" \
    --data @-

  create_release "$app_files"

else
  echo "Unknown prefix ${prefix}"
  exit 1
fi
