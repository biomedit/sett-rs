import jsEslint from '@eslint/js';
import jsonEslint from 'eslint-plugin-jsonc';
import prettier from 'eslint-config-prettier';
import svelteEslint from 'eslint-plugin-svelte';
import tsEslint from 'typescript-eslint';

import prettierPluginRecommended from 'eslint-plugin-prettier/recommended';

// ESlint settings and options for all files.
const tsConfig = tsEslint.config({
  files: ['**/*.json', '**/*.js', '**/*.ts', '**/*.svelte'],
  extends: [
    jsEslint.configs.recommended,
    ...tsEslint.configs.strict,
    // Note: we could be stricter by using:
    // ...tsEslint.configs.strictTypeChecked,
    // ...tsEslint.configs.stylisticTypeChecked,
    prettier,
    prettierPluginRecommended,
  ],
  languageOptions: {
    parserOptions: {
      // 'true' indicates that each source file being linted should use
      // the nearest 'tsconfig.json' from its directory.
      projectService: true,
      // Not clear what `programs: null` does, but it is somehow needed.
      // https://typescript-eslint.io/packages/parser/#programs
      programs: null,
      extraFileExtensions: ['.svelte', '.json'],
      // tsconfigRootDir: import.meta.dirname,
    },
  },
  rules: {
    semi: ['warn', 'always'],
    quotes: [
      'warn',
      'single',
      { avoidEscape: true, allowTemplateLiterals: true },
    ],
    'linebreak-style': ['error', 'unix'],
    'no-cond-assign': ['error', 'always'],
    'no-console': 'error',
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/sort-type-constituents': 'error',
    // 'no-deprecated' can be removed if we ever use 'strictTypeChecked'.
    '@typescript-eslint/no-deprecated': 'error',
    'sort-imports': [
      'error',
      {
        ignoreCase: true,
        ignoreDeclarationSort: false,
        ignoreMemberSort: false,
        memberSyntaxSortOrder: ['none', 'all', 'multiple', 'single'],
        allowSeparatedGroups: true,
      },
    ],
    '@typescript-eslint/switch-exhaustiveness-check': 'error',
  },
});

// ESlint settings and options specific to Svelte files.
const svelteConfig = tsEslint.config({
  files: ['**/*.svelte'],
  extends: [
    ...svelteEslint.configs['flat/recommended'],
    ...svelteEslint.configs['flat/prettier'],
  ],
  languageOptions: {
    parserOptions: {
      parser: tsEslint.parser,
    },
  },
  rules: {
    'svelte/no-target-blank': 'error',
    'svelte/no-at-debug-tags': 'error',
    'svelte/no-reactive-functions': 'error',
    'svelte/no-reactive-literals': 'error',
  },
});

// ESlint settings and options specific to JSON files.
const jsonConfig = tsEslint.config({
  files: ['**/*.json'],
  ...jsonEslint.configs['flat/recommended-with-jsonc'],
});

// Set ESlint config values.
export default tsEslint.config(...tsConfig, ...svelteConfig, ...jsonConfig);
