use std::path::PathBuf;

use sett::{
    openpgp::certstore::{CertStore, CertStoreOpts},
    package::{Metadata, Package},
};

use crate::{cert::StoreLocation, error::Error, password::Secret};

/// S3 source for a data package.
#[derive(Debug, serde::Deserialize)]
#[serde(tag = "kind", rename_all = "camelCase")]
pub(crate) enum Source {
    Local {
        path: PathBuf,
    },
    #[serde(rename_all = "camelCase")]
    S3 {
        url: String,
        bucket: String,
        object_name: String,
        access_key: Box<Secret>,
        secret_key: Box<Secret>,
        session_token: Box<Secret>,
    },
    #[serde(rename_all = "camelCase")]
    S3Portal {
        transfer_id: u32,
        object_name: String,
        portal_url: String,
    },
}

impl Source {
    pub(crate) async fn open(
        &self,
        auth_state: &crate::auth::State,
    ) -> Result<Package<sett::package::state::Unverified>, Error> {
        match self {
            Source::Local { path } => Ok(Package::open(path).await?),
            Source::S3 {
                url,
                bucket,
                object_name: object,
                access_key,
                secret_key,
                session_token,
            } => {
                let client = sett::remote::s3::Client::builder()
                    .endpoint(Some(url))
                    .access_key(Some(access_key.clone().into_inner()))
                    .secret_key(Some(secret_key.clone().into_inner()))
                    .session_token(Some(session_token.clone().into_inner()))
                    .build()
                    .await?;
                Ok(Package::open_s3(&client, bucket.clone(), object.clone()).await?)
            }
            Source::S3Portal {
                transfer_id,
                object_name,
                portal_url,
            } => {
                let connection_details = sett::portal::Portal::new(portal_url)?
                    .get_s3_connection_details(
                        *transfer_id,
                        sett::portal::S3Permission::Read,
                        &auth_state.get_access_token().await?,
                    )
                    .await?;
                Ok(Package::open_s3(
                    &connection_details.build_client().await?,
                    connection_details.bucket,
                    object_name.clone(),
                )
                .await?)
            }
        }
    }
}

#[tauri::command]
pub(crate) async fn read_pkg_metadata(
    store: StoreLocation,
    source: Source,
    auth_state: tauri::State<'_, crate::auth::State>,
) -> Result<Metadata, Error> {
    // Verify the file has the correct structure for a data package
    // and that the metadata signature is valid. Then extract the metadata.
    Ok(verify_package(store, &source, &auth_state)
        .await?
        .metadata()
        .await
        .map_err(|e| format!("error reading package metadata - {e}"))?)
}

pub(crate) async fn verify_package(
    store: StoreLocation,
    source: &Source,
    auth_state: &crate::auth::State,
) -> Result<Package<sett::package::state::Verified>, crate::error::Error> {
    let cert_store = CertStore::open(&CertStoreOpts {
        read_only: true,
        location: Some(&store.public),
    })?;

    Ok(source
        .open(auth_state)
        .await?
        .verify(&cert_store)
        .await
        .map_err(|e| format!("error verifying package - {e}"))?)
}
