use std::path::PathBuf;

use chrono::{DateTime, Local};
use serde::{Deserialize, Serialize};
use sett::{
    openpgp::{
        cert::{AsciiArmored, Cert},
        certstore::{CertStore, CertStoreOpts},
        keyserver::{Keyserver, KeyserverEmailStatus},
        keystore::KeyStore,
    },
    portal,
};

use crate::{error::Error, password::Secret};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub(crate) struct StoreLocation {
    pub(crate) public: PathBuf,
    pub(crate) private: PathBuf,
}

#[derive(Serialize, Deserialize, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub(crate) enum CertType {
    Secret,
    Public,
}

impl From<sett::openpgp::cert::CertType> for CertType {
    fn from(val: sett::openpgp::cert::CertType) -> Self {
        match val {
            sett::openpgp::cert::CertType::Public => Self::Public,
            sett::openpgp::cert::CertType::Secret => Self::Secret,
        }
    }
}

impl From<CertType> for sett::openpgp::cert::CertType {
    fn from(val: CertType) -> Self {
        match val {
            CertType::Public => Self::Public,
            CertType::Secret => Self::Secret,
        }
    }
}

#[derive(Serialize, Deserialize)]
enum ValidityStatus {
    Valid,
    Revoked,
}

#[derive(Serialize, Deserialize, Copy, Clone)]
pub(super) enum ReasonForRevocation {
    Unspecified,
    Superseded,
    Compromised,
    Retired,
}

impl From<sett::openpgp::cert::ReasonForRevocation> for ReasonForRevocation {
    fn from(value: sett::openpgp::cert::ReasonForRevocation) -> Self {
        use sett::openpgp::cert::ReasonForRevocation::*;
        match value {
            KeySuperseded => Self::Superseded,
            KeyRetired => Self::Retired,
            UIDRetired => Self::Retired,
            KeyCompromised => Self::Compromised,
            Unspecified => Self::Unspecified,
            _ => Self::Unspecified,
        }
    }
}

impl From<ReasonForRevocation> for sett::openpgp::cert::ReasonForRevocation {
    fn from(value: ReasonForRevocation) -> Self {
        match value {
            ReasonForRevocation::Unspecified => Self::Unspecified,
            ReasonForRevocation::Superseded => Self::KeySuperseded,
            ReasonForRevocation::Compromised => Self::KeyCompromised,
            ReasonForRevocation::Retired => Self::KeyRetired,
        }
    }
}

#[derive(Serialize, Deserialize)]
struct Validity {
    status: ValidityStatus,
    reasons: Option<Vec<ReasonForRevocation>>,
    messages: Option<Vec<String>>,
}

impl From<sett::openpgp::cert::RevocationStatus> for Validity {
    fn from(revocation_status: sett::openpgp::cert::RevocationStatus) -> Self {
        match revocation_status {
            sett::openpgp::cert::RevocationStatus::Revoked(signatures) => {
                let (reasons, messages) = signatures
                    .into_iter()
                    .map(|sig| {
                        sig.reason_for_revocation().map_or(
                            (ReasonForRevocation::Unspecified, String::from("")),
                            |(r, m)| (r.into(), String::from_utf8_lossy(m).to_string()),
                        )
                    })
                    .unzip();
                Self {
                    status: ValidityStatus::Revoked,
                    reasons: Some(reasons),
                    messages: Some(messages),
                }
            }
            _ => Self {
                status: ValidityStatus::Valid,
                reasons: None,
                messages: None,
            },
        }
    }
}

#[derive(Serialize, Deserialize)]
enum ExpirationDateStatus {
    Valid,
    Expired,
}

#[derive(Serialize, Deserialize)]
#[serde(tag = "kind")]
enum ExpirationDate {
    At {
        date: String,
        status: ExpirationDateStatus,
    },
    Never,
    ReadError {
        message: String,
    },
}

impl From<Cert> for ExpirationDate {
    fn from(cert: Cert) -> Self {
        match cert.expiration_time() {
            Ok(option_time) => match option_time {
                Some(expiration_time) => Self::At {
                    status: if expiration_time > std::time::SystemTime::now() {
                        ExpirationDateStatus::Valid
                    } else {
                        ExpirationDateStatus::Expired
                    },
                    date: DateTime::<Local>::from(expiration_time).to_string(),
                },
                None => Self::Never {},
            },
            Err(msg) => Self::ReadError {
                message: msg.to_string(),
            },
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub(super) struct CertInfo {
    fingerprint: String,
    userid: Option<String>,
    cert_type: CertType,
    validity: Validity,
    expiration_date: ExpirationDate,
}

impl From<Cert> for CertInfo {
    fn from(cert: Cert) -> Self {
        Self {
            fingerprint: cert.fingerprint().to_string(),
            userid: cert.userids().into_iter().next(),
            cert_type: cert.cert_type().into(),
            validity: cert.revocation_status().into(),
            expiration_date: cert.into(),
        }
    }
}

impl From<sett::gnupg::CertInfo> for CertInfo {
    fn from(cert: sett::gnupg::CertInfo) -> Self {
        Self {
            fingerprint: cert.fingerprint,
            userid: cert.userid,
            cert_type: cert.cert_type.into(),
            validity: Validity {
                status: ValidityStatus::Valid,
                reasons: None,
                messages: None,
            },
            expiration_date: ExpirationDate::ReadError {
                message: String::from("Not needed"),
            },
        }
    }
}

#[tauri::command]
pub(super) async fn list_certs(store: StoreLocation) -> Result<Vec<CertInfo>, Error> {
    let cert_store = CertStore::open(&CertStoreOpts {
        read_only: true,
        location: Some(&store.public),
    })?;
    let keys = KeyStore::open(Some(&store.private))
        .await?
        .list()
        .await?
        .into_iter()
        .map(|key| key.fingerprint())
        .collect::<Vec<_>>();
    let mut certs = Vec::new();
    for cert in cert_store.certs() {
        let cert = match cert {
            Ok(cert) => cert,
            Err(e) => {
                tracing::warn!("Failed to read a key: {e}");
                continue;
            }
        };
        certs.push(if keys.contains(&cert.fingerprint()) {
            let mut cert_info: CertInfo = cert.into();
            cert_info.cert_type = CertType::Secret;
            cert_info
        } else {
            cert.into()
        });
    }
    certs.sort_unstable_by_key(|cert| {
        (
            cert.cert_type,
            cert.userid.as_ref().map(|x| x.to_lowercase()),
        )
    });
    Ok(certs)
}

#[tauri::command]
pub(super) async fn generate_cert(
    store: StoreLocation,
    userid: String,
    password: Secret,
    // Duration of certificate validity until expiration, in seconds.
    validity_period: Option<std::time::Duration>,
) -> Result<(CertInfo, String), Error> {
    let (cert, rev) = sett::openpgp::cert::CertBuilder::new()
        .add_userid(&userid)
        .set_password(Some(password.into_inner()))
        .set_validity_period(validity_period)
        .generate()?;
    KeyStore::open(Some(&store.private))
        .await?
        .import(cert.clone())
        .await?;
    CertStore::open(&CertStoreOpts {
        location: Some(&store.public),
        ..Default::default()
    })?
    .import(&cert)?;
    Ok((cert.into(), String::from_utf8(rev)?))
}

async fn import_cert_from_bytes<R: std::io::Read + Send + Sync>(
    store: &StoreLocation,
    data: R,
) -> Result<Vec<CertInfo>, Error> {
    let mut cert_store = CertStore::open(&CertStoreOpts {
        location: Some(&store.public),
        ..Default::default()
    })?;
    let mut key_store = KeyStore::open(Some(&store.private)).await?;
    let mut certs = Vec::new();
    for cert in sett::openpgp::cert::parse_certs(data)? {
        certs.push(cert_store.import(&cert)?.into());
        key_store.import(cert).await?;
    }
    Ok(certs)
}

#[tauri::command]
pub(super) async fn import_cert_from_string(
    store: StoreLocation,
    data: String,
) -> Result<Vec<CertInfo>, Error> {
    import_cert_from_bytes(&store, std::io::Cursor::new(&data)).await
}

#[tauri::command]
pub(super) async fn import_cert_from_file(
    store: StoreLocation,
    path: String,
) -> Result<Vec<CertInfo>, Error> {
    import_cert_from_bytes(&store, std::fs::File::open(path)?).await
}

#[tauri::command]
pub(super) async fn upload_cert_to_keyserver(
    store: StoreLocation,
    fingerprint: String,
    verify: bool,
) -> Result<String, Error> {
    let cert = load_cert(&store, &fingerprint, CertType::Public).await?;
    if let Some(email) = cert.get_primary_email()? {
        let keyserver = Keyserver::new()?;
        let response = keyserver.upload_cert(&cert).await?;
        if verify {
            return match response.status.get(&email) {
                Some(KeyserverEmailStatus::Unpublished) | Some(KeyserverEmailStatus::Pending) => {
                    keyserver.verify_cert(&response.token, &email).await?;
                    Ok("Key uploaded and verification requested.".into())
                }
                Some(KeyserverEmailStatus::Revoked) => {
                    Err("This key is revoked and cannot be used.".into())
                }
                Some(KeyserverEmailStatus::Published) => {
                    Err("This key is already verified.".into())
                }
                None => Err(format!("Couldn't find '{}' in keyserver's response.", email).into()),
            };
        }
        Ok("OpenPGP key successfully uploaded.".into())
    } else {
        Err("This key does not contain an email.".into())
    }
}

#[tauri::command]
pub(super) async fn list_gnupg_certs(cert_type: CertType) -> Result<Vec<CertInfo>, Error> {
    Ok(
        sett::gnupg::list_keys(cert_type.into(), None::<&std::path::Path>)?
            .into_iter()
            .map(Into::into)
            .collect(),
    )
}

#[tauri::command]
pub(super) async fn import_cert_from_gnupg(
    store: StoreLocation,
    identifier: String,
    cert_type: CertType,
    password: Option<String>,
) -> Result<(), Error> {
    let cert_type = cert_type.into();
    let exported = Cert::from_bytes(sett::gnupg::export_key(
        &identifier,
        cert_type,
        password.map(|p| p.into_bytes()).as_deref(),
        None::<&std::path::Path>,
    )?)?;
    CertStore::open(&CertStoreOpts {
        location: Some(&store.public),
        ..Default::default()
    })?
    .import(&exported)?;
    if let sett::openpgp::cert::CertType::Secret = cert_type {
        KeyStore::open(Some(&store.private))
            .await?
            .import(exported)
            .await?;
    }
    Ok(())
}

#[tauri::command]
pub(super) async fn import_cert_from_keyserver(
    store: StoreLocation,
    identifier: String,
) -> Result<CertInfo, Error> {
    let cert = Keyserver::new()?.get_cert(&identifier.parse()?).await?;
    let mut store = CertStore::open(&CertStoreOpts {
        location: Some(&store.public),
        ..Default::default()
    })?;
    Ok(store.import(&cert)?.into())
}

#[derive(Serialize)]
pub(crate) enum ApprovalStatus {
    Approved,
    Revoked,
    CertificateDeleted,
    Pending,
    CertificateRevoked,
    CertificateUnknown,
    Rejected,
}

#[derive(Serialize)]
pub(super) struct CertApprovalStatus {
    fingerprint: String,
    status: ApprovalStatus,
}

impl From<portal::ApprovalStatus> for ApprovalStatus {
    fn from(val: portal::ApprovalStatus) -> Self {
        use portal::ApprovalStatus::*;
        match val {
            Approved => Self::Approved,
            ApprovalRevoked => Self::Revoked,
            Deleted => Self::CertificateDeleted,
            Pending => Self::Pending,
            KeyRevoked => Self::CertificateRevoked,
            UnknownKey => Self::CertificateUnknown,
            Rejected => Self::Rejected,
        }
    }
}

impl From<portal::KeyStatus> for CertApprovalStatus {
    fn from(data: portal::KeyStatus) -> Self {
        Self {
            fingerprint: data.fingerprint,
            status: data.status.into(),
        }
    }
}

/// Retrieve the approval status of OpenPGP certificates specified via their
/// `fingerprints` from the portal.
#[tauri::command]
pub(super) async fn get_cert_approval_status_from_portal(
    portal_url: String,
    fingerprints: Vec<String>,
) -> Result<Vec<CertApprovalStatus>, Error> {
    Ok(portal::Portal::new(&portal_url)?
        .get_key_status(&fingerprints)
        .await?
        .into_iter()
        .map(Into::into)
        .collect())
}

/// Returns the default path of the certificate store on disk.
#[tauri::command]
pub(super) async fn get_default_certstore_path() -> Result<PathBuf, Error> {
    Ok(CertStore::default_location()?)
}

/// Returns the default path of the private key store on disk.
#[tauri::command]
pub(super) async fn get_default_keystore_path() -> Result<PathBuf, Error> {
    Ok(KeyStore::default_location()?)
}

/// Returns the path of the certificate identified by the given fingerprint.
#[tauri::command]
pub(super) async fn get_cert_path(
    store: StoreLocation,
    fingerprint: String,
) -> Result<String, Error> {
    Ok(CertStore::open(&CertStoreOpts {
        read_only: true,
        location: Some(&store.public),
    })?
    .get_cert_path(&fingerprint.parse()?)?
    .to_string_lossy()
    .to_string())
}

/// Deletes private keys associated with a certificate.
///
/// The certificate is identified by the given fingerprint.
#[tauri::command]
pub(super) async fn delete_private_key(
    store: StoreLocation,
    fingerprint: String,
) -> Result<(), Error> {
    use std::collections::BTreeSet;
    let cert_store = CertStore::open(&CertStoreOpts {
        read_only: true,
        location: Some(&store.public),
    })?;
    let cert = cert_store
        .get_cert(&fingerprint.parse()?)?
        .pop()
        .expect("there is exactly 1 element at this point");
    let mut key_store = KeyStore::open(Some(&store.private)).await?;
    let all_private_keys = key_store.list().await?;
    let cert_keys = cert.keys();
    let keys_for_deletion: Vec<_> =
        BTreeSet::from_iter(cert_keys.into_iter().map(|key| key.fingerprint()))
            .intersection(&BTreeSet::from_iter(
                all_private_keys.iter().map(|key| key.fingerprint()),
            ))
            .cloned()
            .collect();
    if keys_for_deletion.is_empty() {
        return Err(format!("No private keys found for {cert}").into());
    }
    for key in keys_for_deletion {
        key_store.delete_key(key).await?;
    }
    Ok(())
}

async fn load_cert(
    store: &StoreLocation,
    fingerprint: &str,
    cert_type: CertType,
) -> Result<Cert, Error> {
    let query = fingerprint.parse()?;
    let cert_store = CertStore::open(&CertStoreOpts {
        read_only: true,
        location: Some(&store.public),
    })?;
    let cert = cert_store
        .get_cert(&query)?
        .pop()
        .expect("`certs` should contain exactly 1 element at this point.");
    if let CertType::Secret = cert_type {
        let mut key_store = KeyStore::open(Some(&store.private)).await?;
        Ok(cert.update_secret_material(&mut key_store).await?)
    } else {
        Ok(cert)
    }
}

#[tauri::command]
pub(super) async fn export_cert(
    store: StoreLocation,
    fingerprint: String,
    cert_type: CertType,
) -> Result<String, Error> {
    let cert = load_cert(&store, &fingerprint, cert_type).await?;
    let exported = match cert_type {
        CertType::Secret => AsciiArmored::try_from(cert.secret()?),
        CertType::Public => AsciiArmored::try_from(cert.public()),
    }?;
    Ok(String::from_utf8(exported.into())?)
}

#[tauri::command]
pub(super) async fn generate_rev_sig(
    store: StoreLocation,
    fingerprint: String,
    reason: ReasonForRevocation,
    message: String,
    password: String,
) -> Result<String, Error> {
    let cert = load_cert(&store, &fingerprint, CertType::Secret).await?;
    let rev =
        cert.generate_rev_sig(reason.into(), message.as_bytes(), Some(password.as_bytes()))?;
    Ok(String::from_utf8(rev)?)
}

#[tauri::command]
pub(super) async fn revoke_cert(
    store: StoreLocation,
    fingerprint: String,
    rev: String,
) -> Result<(), Error> {
    let cert = load_cert(&store, &fingerprint, CertType::Secret).await?;
    let revoked = cert.revoke(std::io::Cursor::new(&rev))?;
    let mut cert_store = CertStore::open(&CertStoreOpts {
        location: Some(&store.public),
        ..Default::default()
    })?;
    cert_store.import(&revoked)?;
    Ok(())
}

/// Set or update the expiration date (validity) of an OpenPGP certificate.
///
/// The OpenPGP certificate is specified via the `fingerprint` of its primary
/// key. This function assumes that the primary key of the certificate
/// corresponds to the key that has "certification" capacity. This will almost
/// always be the case.
#[tauri::command]
pub(super) async fn set_expiration_date(
    store: StoreLocation,
    pass_state: tauri::State<'_, crate::password::State>,
    fingerprint: String,
    userid: String,
    validity_period: Option<std::time::Duration>,
) -> Result<(), Error> {
    let mut cert_store = CertStore::open(&CertStoreOpts {
        location: Some(&store.public),
        ..Default::default()
    })?;

    // Retrieve the secret part of the certificate's primary key. Multiple
    // keys can be returned in the case where the key is present in multiple
    // "backends" of the keystore, e.g. both in Sequoia store and GnuPG keyring.
    let private_keys = KeyStore::open(Some(&store.private))
        .await?
        .find_key(fingerprint.parse()?)
        .await?;

    let tx = pass_state.pass_tx.clone();
    let password = move |fingerprint: sett::openpgp::cert::Fingerprint| {
        let tx_clone = tx.clone();
        let userid_copy = userid.clone();
        async move {
            let (resp_tx, resp_rx) = tokio::sync::oneshot::channel();
            tx_clone
                .send((
                    // Primary and subkey fingerprint are the same because we
                    // here assume that the certification-capable key is
                    // always the primary key of the certificate.
                    sett::openpgp::crypto::PasswordHint {
                        fingerprint: fingerprint.clone(),
                        userid: Some(userid_copy),
                        fingerprint_primary: Some(fingerprint),
                    },
                    resp_tx,
                ))
                .await
                .expect("recipient is alive");
            resp_rx.await.expect("sender is alive")
        }
    };
    let mut errors = Vec::new();
    for mut key in private_keys {
        if let Err(e) = key.unlock(&password).await {
            errors.push(e.to_string());
            continue;
        }
        let cert = load_cert(&store, &fingerprint, CertType::Public)
            .await?
            .set_expiration_time(
                key,
                validity_period.map(|period| std::time::SystemTime::now() + period),
            )
            .await?;
        cert_store.import(&cert)?;
        return Ok(());
    }
    Err(format!(
        "Failed to update expiration date for key '{}': {}",
        fingerprint,
        errors.join(", ")
    )
    .into())
}
