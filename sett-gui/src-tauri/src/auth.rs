use std::{ops::Deref as _, sync::Arc, time::Duration};

use sett::auth::{AccessToken, Oidc, UserInfo, VerificationDetails};
use sett::portal::{response::DataTransfer, Portal};
use tokio::sync::{mpsc, oneshot, RwLock};
use tracing::{error, trace};

use crate::error::Error;

const BIOMEDIT_KEYCLOAK_URL: &str = "https://login.biomedit.ch/realms/biomedit";
const BIOMEDIT_KEYCLOAK_SHORT_URL: &str = "https://login.biomedit.ch";
const SETT_OIDC_CLIENT_ID: &str = "sett";
const ENV_SETT_OIDC_ISSUER_URL: &str = "SETT_OIDC_ISSUER_URL";

#[derive(Clone)]
pub(super) struct State {
    pub(super) oauth: Arc<RwLock<Option<OAuthSession>>>,
    pub(super) token_tx: mpsc::Sender<oneshot::Sender<Result<AccessToken, Error>>>,
}

impl State {
    // Get a portal access token. If needed, this token will be automatically
    // refreshed by the `token_daemon` of the application.
    pub(super) async fn get_access_token(&self) -> Result<AccessToken, Error> {
        let (tx, rx) = oneshot::channel();
        self.token_tx.send(tx).await?;
        let access_token = rx.await??;
        Ok(access_token)
    }
}

/// Access `token`s and OpenID Connect service provider `client` of
/// an OAuth session.
pub(super) struct OAuthSession {
    token: sett::auth::Token,
    client: sett::auth::Oidc,
}

pub(super) async fn token_daemon(
    state: State,
    mut rx: mpsc::Receiver<oneshot::Sender<Result<AccessToken, Error>>>,
    refresh_interval: Duration,
    window: tauri::Window,
) {
    macro_rules! emit_refresh_error {
        ($window:expr, $error:expr) => {{
            let message = "Token refresh failed";
            error!(error = ?$error, message);
            if $window
                .emit(
                    "auth-error",
                    AuthError::Logout {
                        details: format!("{} ({:?})", message, $error),
                    },
                )
                .is_err()
            {
                error!("Failed to emit token refreshed failure error.")
            }
        }};
    }
    // The main purpose of this task is keeping the oauth session alive by regularly using the
    // refresh token. As a side effect, both access and refresh tokens are refreshed.
    // The interval should be at least as long as the lifespan of the access token to minimize the
    // number of request made to the oauth server. However, it should be smaller than the session
    // expiration time (so the refresh token is valid).
    let state_clone = state.clone();
    let window_clone = window.clone();
    tokio::task::spawn(async move {
        let mut interval = tokio::time::interval(refresh_interval);
        loop {
            interval.tick().await;
            if state_clone.oauth.read().await.is_none() {
                continue;
            }
            let mut guard = state_clone.oauth.write().await;
            match refresh(guard.take()).await {
                Ok(oauth) => {
                    *guard = {
                        trace!("Successfully refreshed tokens");
                        Some(oauth)
                    }
                }
                Err(e) => {
                    emit_refresh_error!(&window_clone, e);
                }
            }
        }
    });
    // Listen to requests for an access token.
    // It the token is expired (or is about to expire soon) refresh it first.
    while let Some(tx) = rx.recv().await {
        let guard = state.oauth.read().await;
        let response = match guard.deref() {
            Some(oauth) => {
                // Refresh tokens if the access token is expired. Use 10 second margin to decrease
                // the chance of token expiring before being used.
                if oauth.token.expires_in().unwrap_or_default() < Duration::from_secs(10) {
                    trace!("Access token is expired, refreshing");
                    drop(guard);
                    let mut guard = state.oauth.write().await;
                    match refresh(guard.take()).await {
                        Ok(oauth) => {
                            trace!("Returning refreshed access token");
                            let access_token = oauth.token.access_token.clone();
                            *guard = Some(oauth);
                            Ok(access_token)
                        }
                        Err(e) => {
                            emit_refresh_error!(&window, e);
                            Err(e)
                        }
                    }
                } else {
                    trace!("Access token is alive, returning");
                    Ok(oauth.token.access_token.clone())
                }
            }
            None => {
                let message = "No access token available (unauthenticated)";
                error!(message);
                Err(message.into())
            }
        };
        if tx.send(response).is_err() {
            error!("Failed to send token (recipient dropped)");
        }
    }
}

#[derive(Clone, Debug, serde::Serialize)]
#[serde(tag = "state", rename_all(serialize = "camelCase"))]
enum AuthError {
    Logout { details: String },
}

/// Refreshes access and refresh tokens.
async fn refresh(oauth: Option<OAuthSession>) -> Result<OAuthSession, Error> {
    match oauth {
        Some(oauth) => match oauth.token.refresh_token {
            Some(refresh_token) => {
                trace!("Refreshing OAuth2 token");
                Ok(OAuthSession {
                    token: oauth.client.refresh(&refresh_token).await?,
                    client: oauth.client,
                })
            }
            None => {
                let message = "Unable to refresh tokens (refresh token missing)";
                error!(message);
                Err(message.into())
            }
        },
        None => {
            let message = "Unable to refresh tokens (unauthenticated)";
            error!(message);
            Err(message.into())
        }
    }
}

#[tauri::command]
pub(super) async fn login(
    state: tauri::State<'_, State>,
    window: tauri::Window,
    oidc_issuer: String,
) -> Result<(), Error> {
    let client = Oidc::new(SETT_OIDC_CLIENT_ID, oidc_issuer.parse()?).await?;
    let token = client
        .authenticate(move |details| {
            window.emit(
                "details",
                VerificationDetails {
                    user_code: details.user_code,
                    verification_uri: shorten_oidc_issuer_url(details.verification_uri),
                    verification_uri_complete: details
                        .verification_uri_complete
                        .map(shorten_oidc_issuer_url),
                },
            )?;
            Ok::<(), tauri::Error>(())
        })
        .await?;
    *state.oauth.write().await = Some(OAuthSession { token, client });
    Ok(())
}

/// Shorten OIDC issuer URL.
///
/// The URL is only shortened for the official BioMedIT issuer when the
/// corresponding environment variable is unset.
pub(crate) fn shorten_oidc_issuer_url(issuer: String) -> String {
    if std::env::var_os(ENV_SETT_OIDC_ISSUER_URL).is_none() {
        issuer.replace(BIOMEDIT_KEYCLOAK_URL, BIOMEDIT_KEYCLOAK_SHORT_URL)
    } else {
        issuer
    }
}

// This function is infallible, however Tauri requires using `Result`.
#[tauri::command]
pub(super) async fn logout(state: tauri::State<'_, State>) -> Result<(), Error> {
    *state.oauth.write().await = None;
    Ok(())
}

#[tauri::command]
pub(super) async fn fetch_user_info(
    state: tauri::State<'_, State>,
    oidc_issuer: String,
) -> Result<UserInfo, Error> {
    let (tx, rx) = oneshot::channel();
    state.token_tx.send(tx).await?;
    let access_token = rx.await??;
    Ok(Oidc::new(SETT_OIDC_CLIENT_ID, oidc_issuer.parse()?)
        .await?
        .user_info(&access_token)
        .await?)
}

// Makes an request to the `portal` API that returns the list of data transfers
// associated with the currently authenticated user.
//
// Notes:
// * This function requires the user to be authenticated with portal.
// * the `state` argument is automatically injected by Tauri when the function
//   is called. It does not need to be provided when invoking the function
//   from the frontend.
#[tauri::command]
pub(super) async fn fetch_data_transfers(
    portal_url: &str,
    state: tauri::State<'_, State>,
) -> Result<Vec<DataTransfer>, Error> {
    let access_token = state.get_access_token().await?;
    let portal = Portal::new(portal_url)?;
    Ok(portal.get_data_transfers(&access_token).await?)
}

#[tauri::command]
pub(super) fn open_url(url: &str) -> Result<(), Error> {
    Ok(webbrowser::open(url)?)
}
