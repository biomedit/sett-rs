use std::{
    collections::BTreeMap,
    sync::{Arc, Mutex},
};

use crate::error::Error;

type PasswordDb = Arc<Mutex<BTreeMap<String, sett::secret::Secret>>>;

#[derive(Clone)]
pub(crate) struct State {
    pub(crate) pass_db: PasswordDb,
    pub(crate) pass_tx: tokio::sync::mpsc::Sender<(
        sett::openpgp::crypto::PasswordHint,
        tokio::sync::oneshot::Sender<sett::secret::Secret>,
    )>,
}

#[derive(Clone, Debug)]
pub(crate) struct Secret(sett::secret::Secret);

impl Secret {
    pub(crate) fn into_inner(self) -> sett::secret::Secret {
        self.0
    }
}

impl<'de> serde::de::Deserialize<'de> for Secret {
    fn deserialize<D: serde::de::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        deserializer.deserialize_any(SecretVisitor)
    }
}

struct SecretVisitor;

impl serde::de::Visitor<'_> for SecretVisitor {
    type Value = Secret;
    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "a str or a byte array")
    }

    fn visit_str<E: serde::de::Error>(self, value: &str) -> Result<Self::Value, E> {
        Ok(Secret(value.into()))
    }

    fn visit_bytes<E: serde::de::Error>(self, value: &[u8]) -> Result<Self::Value, E> {
        Ok(Secret(value.into()))
    }
}

/// Hint for an OpenPGP secret key password.
#[derive(Clone, Debug, serde::Serialize)]
#[serde(rename_all(serialize = "camelCase"))]
struct PasswordHint<'a> {
    fingerprint: &'a str,
    userid: Option<&'a str>,
    fingerprint_primary: Option<&'a str>,
}

pub(crate) async fn password_daemon(
    window: tauri::Window,
    mut rx: tokio::sync::mpsc::Receiver<(
        sett::openpgp::crypto::PasswordHint,
        tokio::sync::oneshot::Sender<sett::secret::Secret>,
    )>,
    passwords: PasswordDb,
) {
    while let Some((hint, tx)) = rx.recv().await {
        let fingerprint = hint.fingerprint.to_string();
        let maybe_fingerprint_primary = hint.fingerprint_primary.map(|f| f.to_string());
        let maybe_password = passwords.lock().unwrap().get(&fingerprint).cloned();
        if let Some(password) = maybe_password {
            tx.send(password).expect("receiver should be alive");
        } else {
            if window
                .emit(
                    "password-prompt",
                    PasswordHint {
                        fingerprint: &fingerprint,
                        userid: hint.userid.as_deref(),
                        fingerprint_primary: maybe_fingerprint_primary.as_deref(),
                    },
                )
                .is_err()
            {
                tracing::error!("Failed to emit task event");
            };
            loop {
                tokio::time::sleep(std::time::Duration::from_millis(200)).await;
                if let Some(p) = passwords.lock().unwrap().remove(&fingerprint) {
                    tx.send(p.clone()).expect("receiver should be alive");
                    break;
                }
            }
        }
    }
}

/// Add a fingerprint:password pair to the CLI in-memory password database.
///
/// Note that the fingerprint to be added is the fingerprint of the
/// singing/encryption subkey associated with the password, and not the
/// fingerprint of the certificate's primary key.
#[tauri::command]
pub(crate) async fn add_password(
    state: tauri::State<'_, State>,
    fingerprint: String,
    password: Secret,
) -> Result<(), Error> {
    state.pass_db.lock()?.insert(fingerprint, password.0);
    Ok(())
}
