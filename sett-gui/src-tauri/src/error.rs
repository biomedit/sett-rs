use serde::Serialize;

#[derive(Debug, Serialize)]
pub(crate) struct Error(String);

impl<E> From<E> for Error
where
    E: std::fmt::Display,
{
    fn from(val: E) -> Self {
        Self(val.to_string())
    }
}
