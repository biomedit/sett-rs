pub(crate) fn init_log() -> tracing_appender::non_blocking::WorkerGuard {
    use tracing_subscriber::{layer::SubscriberExt as _, EnvFilter, Layer as _, Registry};
    let (non_blocking_file_writer, non_blocking_file_writer_guard) = tracing_appender::non_blocking(
        tracing_appender::rolling::RollingFileAppender::builder()
            .rotation(tracing_appender::rolling::Rotation::DAILY)
            .filename_suffix("sett-gui.jsonl")
            .build(get_log_dir())
            .expect("initializing rolling file appender failed"),
    );
    tracing::subscriber::set_global_default(
        Registry::default().with(
            tracing_subscriber::fmt::layer()
                .with_writer(non_blocking_file_writer)
                .json()
                .with_filter(EnvFilter::try_from_default_env().unwrap_or_else(|_| {
                    EnvFilter::try_new("info,sett=debug,sett_gui=debug").expect("valid directives")
                })),
        ),
    )
    .expect("subscriber is set only once");
    non_blocking_file_writer_guard
}

#[tauri::command]
pub(super) async fn log_dir() -> String {
    get_log_dir().to_string_lossy().into()
}

fn get_log_dir() -> std::path::PathBuf {
    dirs::data_dir()
        .expect("a valid path on all supported operating systems")
        .join("ch.biomedit.sett")
        .join("log")
}
