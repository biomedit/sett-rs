use std::path::PathBuf;

use serde::Serialize;
use tempfile::NamedTempFile;

use crate::error::Error;

#[tauri::command]
pub(super) async fn is_dir(path: String) -> bool {
    PathBuf::from(path).is_dir()
}

#[tauri::command]
pub(super) fn is_writeable(path: String) -> Result<bool, Error> {
    match NamedTempFile::new_in(PathBuf::from(path)) {
        Ok(marker_file) => {
            marker_file.close()?;
            Ok(true)
        }
        Err(_) => Ok(false),
    }
}

#[derive(Debug, Serialize)]
pub(super) struct FileStats {
    size: u64,
    count: u64,
}

#[tauri::command]
pub(super) fn stats(paths: Vec<String>) -> Result<FileStats, Error> {
    let mut size = 0;
    let mut count = 0;
    for entry in paths.iter().flat_map(walkdir::WalkDir::new) {
        let entry = entry?;
        if entry.file_type().is_file() {
            size += entry.metadata()?.len();
            count += 1;
        }
    }
    Ok(FileStats { size, count })
}
