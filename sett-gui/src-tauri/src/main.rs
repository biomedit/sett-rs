// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::sync::Arc;
use tauri::Manager as _;

mod auth;
mod cert;
mod error;
mod fs;
mod log;
mod package;
mod password;
mod task;

fn main() {
    #[cfg(not(windows))]
    {
        let _ = fix_path_env::fix();
    }
    let _log_guard = log::init_log();
    let (tx, rx) = tokio::sync::mpsc::channel(2);
    let pass_db = Default::default();
    let (token_tx, token_rx) = tokio::sync::mpsc::channel(10);
    let token_state = auth::State {
        oauth: Default::default(),
        token_tx,
    };
    let default = tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![
            auth::login,
            auth::logout,
            auth::fetch_user_info,
            auth::open_url,
            auth::fetch_data_transfers,
            cert::export_cert,
            cert::generate_cert,
            cert::generate_rev_sig,
            cert::get_cert_approval_status_from_portal,
            cert::get_default_certstore_path,
            cert::get_default_keystore_path,
            cert::get_cert_path,
            cert::delete_private_key,
            cert::import_cert_from_file,
            cert::import_cert_from_string,
            cert::import_cert_from_gnupg,
            cert::import_cert_from_keyserver,
            cert::list_certs,
            cert::list_gnupg_certs,
            package::read_pkg_metadata,
            cert::revoke_cert,
            cert::set_expiration_date,
            cert::upload_cert_to_keyserver,
            fs::is_dir,
            fs::is_writeable,
            fs::stats,
            password::add_password,
            log::log_dir,
            task::check_task,
            task::run_task,
        ])
        .manage(password::State {
            pass_tx: tx,
            pass_db: Arc::clone(&pass_db),
        })
        .manage(token_state.clone())
        .setup(|app| {
            let main_window = app.get_window("main").unwrap();
            tauri::async_runtime::spawn(password::password_daemon(
                main_window.clone(),
                rx,
                pass_db,
            ));
            tauri::async_runtime::spawn(auth::token_daemon(
                token_state,
                token_rx,
                std::time::Duration::from_secs(60 * 15),
                main_window,
            ));
            Ok(())
        });
    default
        .plugin(tauri_plugin_store::Builder::default().build())
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
