use std::{
    collections::BTreeMap,
    path::{Path, PathBuf},
    str::FromStr,
};

use chrono::Utc;
use serde::{Deserialize, Serialize};
use sett::auth::AccessToken;
use sett::{
    openpgp::{
        certstore::{CertStore, CertStoreOpts},
        keystore::KeyStore,
    },
    package::{Metadata, DATETIME_FORMAT},
    task::Mode,
};

use crate::error::Error;

#[derive(Clone, Serialize)]
#[serde(tag = "type", rename_all(serialize = "camelCase"))]
enum TaskStatus {
    Running { progress: f32 },
    Finished { destination: String },
    Error { message: String },
}

#[derive(Clone, Serialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub(super) struct TaskEvent {
    task_id: u32,
    status: TaskStatus,
}

#[derive(Debug, Deserialize)]
pub(super) struct Task {
    id: u32,
    kind: TaskKind,
}

#[derive(Debug, Deserialize)]
pub(super) enum TaskKind {
    Encrypt(EncryptOpts),
    Decrypt(DecryptOpts),
    Transfer(TransferOpts),
}

#[derive(Debug, Deserialize)]
pub(super) struct DecryptOpts {
    source: crate::package::Source,
    destination: PathBuf,
    store: crate::cert::StoreLocation,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all(deserialize = "camelCase"))]
pub(super) struct TransferOpts {
    package: PathBuf,
    destination: DestinationProps,
    verify: bool,
    portal_url: String,
    store: crate::cert::StoreLocation,
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
enum DestinationProps {
    Local(LocalProps),
    S3(S3PropsSource),
    Sftp(SftpProps),
}

#[derive(Debug, Deserialize)]
struct LocalProps(String);

#[derive(Debug, Deserialize)]
#[serde(rename_all(deserialize = "camelCase"))]
struct SftpProps {
    username: String,
    host: String,
    base_path: String,
    ssh_key: SSHKey,
}

fn get_sftp_client(props: &SftpProps) -> sett::remote::sftp::Client {
    let mut host = props.host.split(':');
    sett::remote::sftp::Client::builder()
        .host(
            host.next()
                .expect("the split iterator should always have at least one element"),
        )
        .port(
            host.next()
                .and_then(|s| u16::from_str(s).ok())
                .unwrap_or(22),
        )
        .username(&props.username)
        .key_path(props.ssh_key.location.as_ref())
        .key_password(props.ssh_key.password.as_deref())
        .build()
}

fn get_sftp_destination(props: &SftpProps) -> Result<sett::destination::Sftp, Error> {
    Ok(sett::destination::Sftp::new(
        get_sftp_client(props),
        &props.base_path,
    ))
}

#[derive(Debug, Deserialize)]
struct SSHKey {
    location: Option<String>,
    password: Option<String>,
}

/// Credentials for an S3 connection.
#[derive(Debug, Deserialize)]
#[serde(rename_all(deserialize = "camelCase"))]
struct S3Props {
    url: String,
    bucket: String,
    access_key: String,
    secret_key: String,
    session_token: String,
}

/// Arguments for when S3 credentials are to be retrieved from a remote
/// credential provider (e.g. portal). Only the data transfer Id associated
/// with the package is necessary in that case.
#[derive(Debug, Deserialize)]
#[serde(rename_all(deserialize = "camelCase"))]
struct S3RemoteProps {
    transfer_id: u32,
}

/// Source of S3 credentials.
#[derive(Debug, Deserialize)]
#[serde(untagged)]
enum S3PropsSource {
    /// S3 arguments are passed manually by the user.
    Manual(S3Props),
    /// S3 arguments are fetched from a remote credential provider.
    Remote(S3RemoteProps),
}

fn add_prefix_to_url_if_needed(url: &str) -> String {
    let prefix = if !url.starts_with("https://") && !url.starts_with("http://") {
        "https://"
    } else {
        ""
    };
    format!("{}{}", prefix, url)
}

async fn get_s3_client(props: &S3Props) -> Result<sett::remote::s3::Client, Error> {
    Ok(sett::remote::s3::Client::builder()
        .endpoint(Some(add_prefix_to_url_if_needed(&props.url)))
        .access_key(Some(props.access_key.as_str()))
        .secret_key(Some(props.secret_key.as_str()))
        .session_token(Some(props.session_token.as_str()))
        .build()
        .await?)
}

async fn get_s3_destination(props: &S3Props) -> Result<sett::destination::S3, Error> {
    Ok(sett::destination::S3::new(
        get_s3_client(props).await?,
        &props.bucket,
    ))
}

#[derive(Debug, Deserialize)]
#[serde(rename_all(deserialize = "camelCase"))]
pub(super) struct EncryptOpts {
    files: Vec<String>,
    signer: String,
    recipients: Vec<String>,
    transfer_id: Option<u32>,
    destination: DestinationProps,
    verify: bool,
    portal_url: String,
    extra_metadata: BTreeMap<String, String>,
    store: crate::cert::StoreLocation,
}

struct Progress<Cb: Fn(f32)> {
    current: u64,
    total: u64,
    percentage: u64,
    cb: Cb,
}

impl<Cb: Fn(f32)> Progress<Cb> {
    fn new(cb: Cb) -> Self {
        Self {
            current: 0,
            total: 0,
            percentage: 0,
            cb,
        }
    }
    pub fn update(&mut self) {
        let current_percentage = (100 * self.current).checked_div(self.total).unwrap_or(100);
        if current_percentage > self.percentage {
            self.percentage = current_percentage;
            (self.cb)(current_percentage as f32);
        }
    }
}

impl<Cb: Fn(f32)> sett::progress::ProgressDisplay for Progress<Cb> {
    fn set_completion_value(&mut self, len: u64) {
        self.total = len;
    }
    fn increment(&mut self, delta: u64) {
        self.current += delta;
        self.update();
    }
    fn finish(&mut self) {
        self.current = self.total;
        self.update();
    }
}

/// Run the specified sett workflow `task` in "Check" mode (dry run).
#[tauri::command]
pub(super) async fn check_task(
    task: TaskKind,
    pass_state: tauri::State<'_, crate::password::State>,
    auth_state: tauri::State<'_, crate::auth::State>,
) -> Result<(), Error> {
    let progress = None::<Progress<fn(f32)>>;
    match task {
        TaskKind::Encrypt(opts) => {
            encrypt(opts, Mode::Check, progress, pass_state, auth_state).await?
        }
        TaskKind::Decrypt(opts) => {
            decrypt(opts, Mode::Check, progress, pass_state, auth_state).await?
        }
        TaskKind::Transfer(opts) => transfer(opts, Mode::Check, progress, auth_state).await?,
    };
    Ok(())
}

/// Run the specified sett workflow `task`.
#[tauri::command]
pub(super) async fn run_task(
    task: Task,
    window: tauri::Window,
    pass_state: tauri::State<'_, crate::password::State>,
    auth_state: tauri::State<'_, crate::auth::State>,
) -> Result<(), Error> {
    macro_rules! emit_task_event {
        ($payload:expr, $window:expr) => {
            if $window.emit("task", $payload).is_err() {
                tracing::error!("Failed to emit task event");
            }
        };
    }
    macro_rules! emit_error {
        ($f:literal) => {
            |e| {
                emit_task_event!(
                    TaskEvent {
                        task_id: task.id,
                        status: TaskStatus::Error {
                            message: format!(concat!("{:", $f, "}"), e),
                        },
                    },
                    window
                );
                e
            }
        };
    }
    macro_rules! emit_finished {
        ($status:expr) => {
            emit_task_event!(
                TaskEvent {
                    task_id: task.id,
                    status: TaskStatus::Finished {
                        destination: match $status {
                            sett::task::Status::Completed { destination, .. } => destination,
                            sett::task::Status::Checked { .. } => unreachable!(),
                        }
                    },
                },
                window
            );
        };
    }

    let window_clone = window.clone();
    let progress = Some(Progress::new(move |progress| {
        emit_task_event!(
            TaskEvent {
                task_id: task.id,
                status: TaskStatus::Running { progress },
            },
            window_clone
        )
    }));
    match task.kind {
        TaskKind::Encrypt(opts) => {
            let status = encrypt(opts, Mode::Run, progress, pass_state, auth_state)
                .await
                .map_err(emit_error!("?"))?;
            emit_finished!(status);
            Ok(())
        }
        TaskKind::Decrypt(opts) => {
            let status = decrypt(opts, Mode::Run, progress, pass_state, auth_state)
                .await
                .map_err(emit_error!("?"))?;
            emit_finished!(status);
            Ok(())
        }
        TaskKind::Transfer(opts) => {
            let status = transfer(opts, Mode::Run, progress, auth_state)
                .await
                .map_err(emit_error!("?"))?;
            emit_finished!(status);
            Ok(())
        }
    }
}

async fn s3_dest_from_remote(
    transfer_id: u32,
    access_token: AccessToken,
    remote_url: String,
) -> Result<sett::destination::S3, Error> {
    let portal = sett::portal::Portal::new(remote_url)?;
    let connection_details = portal
        .get_s3_connection_details(
            transfer_id,
            sett::portal::S3Permission::Write,
            &access_token,
        )
        .await?;
    Ok(sett::destination::S3::new(
        connection_details.build_client().await?,
        connection_details.bucket,
    ))
}

async fn encrypt(
    opts: EncryptOpts,
    mode: Mode,
    progress: Option<Progress<impl Fn(f32) + Send + 'static>>,
    pass_state: tauri::State<'_, crate::password::State>,
    auth_state: tauri::State<'_, crate::auth::State>,
) -> Result<sett::task::Status, Error> {
    let signer = opts.signer.parse()?;
    let recipients = opts
        .recipients
        .iter()
        .map(|r| r.parse())
        .collect::<Result<_, _>>()?;
    let timestamp = Utc::now();
    let mut prefix = None;
    if opts.verify {
        let metadata = Metadata {
            sender: opts.signer,
            recipients: opts.recipients,
            timestamp,
            transfer_id: opts.transfer_id,
            extra: opts.extra_metadata.clone(),
            ..Default::default()
        };
        prefix = Some(verify_package(&metadata, "missing", &opts.portal_url).await?);
    }
    let tx = pass_state.pass_tx.clone();
    let password = move |hint: sett::openpgp::crypto::PasswordHint| {
        let tx_clone = tx.clone();
        async move {
            let (resp_tx, resp_rx) = tokio::sync::oneshot::channel();
            tx_clone
                .send((hint, resp_tx))
                .await
                .expect("recipient is alive");
            resp_rx.await.expect("sender is alive")
        }
    };
    let encrypt_opts = sett::encrypt::EncryptOpts {
        files: opts.files.iter().map(PathBuf::from).collect(),
        recipients,
        signer,
        cert_store: CertStore::open(&CertStoreOpts {
            read_only: true,
            location: Some(&opts.store.public),
        })?,
        key_store: KeyStore::open(Some(&opts.store.private)).await?,
        mode,
        purpose: None,
        extra_metadata: opts.extra_metadata,
        transfer_id: opts.transfer_id,
        compression_algorithm: Default::default(),
        password,
        progress,
        timestamp,
        prefix,
    };
    let destination = match opts.destination {
        DestinationProps::S3(S3PropsSource::Remote(props)) => s3_dest_from_remote(
            props.transfer_id,
            auth_state.get_access_token().await?,
            opts.portal_url,
        )
        .await?
        .into(),
        DestinationProps::S3(S3PropsSource::Manual(props)) => {
            get_s3_destination(&props).await?.into()
        }
        DestinationProps::Local(props) => {
            sett::destination::Local::new(props.0, None::<String>)?.into()
        }
        DestinationProps::Sftp(props) => get_sftp_destination(&props)?.into(),
    };
    Ok(sett::encrypt::encrypt(encrypt_opts, destination).await?)
}

async fn transfer(
    opts: TransferOpts,
    mode: Mode,
    progress: Option<Progress<impl Fn(f32) + Send + 'static>>,
    auth_state: tauri::State<'_, crate::auth::State>,
) -> Result<sett::task::Status, Error> {
    let package = crate::package::verify_package(
        opts.store,
        &crate::package::Source::Local {
            path: opts.package.clone(),
        },
        &auth_state,
    )
    .await?;
    if opts.verify {
        let file_name = opts
            .package
            .file_name()
            .ok_or("Failed to get file name")?
            .to_string_lossy();
        let metadata = package.metadata().await?;
        let project_code = verify_package(&metadata, &file_name, &opts.portal_url).await?;
        let file_name_expected = format!(
            "{}_{}.zip",
            project_code,
            metadata.timestamp.format(DATETIME_FORMAT)
        );
        if file_name != file_name_expected {
            return Err(Error::from(format!(
                "Package verification failed: invalid file name '{}' (expected '{}')",
                file_name, file_name_expected
            )));
        }
    }
    match opts.destination {
        DestinationProps::S3(S3PropsSource::Remote(props)) => {
            let s3dest = s3_dest_from_remote(
                props.transfer_id,
                auth_state.get_access_token().await?,
                opts.portal_url,
            )
            .await?;
            Ok(s3dest
                .client()
                .upload(&package, s3dest.bucket(), mode, progress)
                .await
                .map_err(|e| format!("{:#}", e))?)
        }
        DestinationProps::S3(S3PropsSource::Manual(props)) => {
            let s3client = get_s3_client(&props).await?;
            Ok(s3client
                .upload(&package, &props.bucket, mode, progress)
                .await?)
        }
        DestinationProps::Sftp(props) => Ok(sett::remote::sftp::upload(
            &package,
            &get_sftp_client(&props),
            Path::new(&props.base_path),
            mode,
            progress,
        )
        .await?),
        DestinationProps::Local(_) => Err(Error::from("Local destination is not supported")),
    }
}

async fn decrypt(
    opts: DecryptOpts,
    mode: Mode,
    progress: Option<Progress<impl Fn(f32) + Send + Sync + 'static>>,
    pass_state: tauri::State<'_, crate::password::State>,
    auth_state: tauri::State<'_, crate::auth::State>,
) -> Result<sett::task::Status, Error> {
    // handle PGP passwords
    let tx = pass_state.pass_tx.clone();
    let password = move |hint: sett::openpgp::crypto::PasswordHint| {
        let tx_clone = tx.clone();
        let (resp_tx, resp_rx) = tokio::sync::oneshot::channel();
        tx_clone
            .blocking_send((hint, resp_tx))
            .expect("recipient is alive");
        resp_rx.blocking_recv().expect("sender is alive")
    };

    let package = opts.source.open(&auth_state).await?;
    let cert_store = CertStore::open(&CertStoreOpts {
        read_only: true,
        location: Some(&opts.store.public),
    })?;
    Ok(sett::decrypt::decrypt(sett::decrypt::DecryptOpts {
        package,
        cert_store,
        key_store: KeyStore::open(Some(&opts.store.private)).await?,
        password,
        output: Some(opts.destination),
        decrypt_only: false,
        mode,
        progress,
    })
    .await?)
}

async fn verify_package(
    metadata: &Metadata,
    package_name: &str,
    portal_url: &str,
) -> Result<String, Error> {
    let portal = sett::portal::Portal::new(portal_url)?;
    let response = portal.check_package(metadata, package_name).await?;
    Ok(response.project_code)
}
