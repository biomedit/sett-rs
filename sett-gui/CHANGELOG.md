# Changelog

All notable changes to this project will be documented in this file.

## [5.6.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dgui%2F5%2E6%2E0) - 2025-02-18

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dgui%2F5%2E5%2E0...sett%2Dgui%2F5%2E6%2E0)


### ✨ Features

- **encrypt:** Show input file total size and count ([3ff4a5c](https://gitlab.com/biomedit/sett-rs/commit/3ff4a5cc413ec0f3e78075a96c2cf82d3b89a341)), Close #246

### 🐞 Bug Fixes

- **decrypt:** Request read permission when opening pkgs from auth S3 ([9541c71](https://gitlab.com/biomedit/sett-rs/commit/9541c710a8394f2df4b5b2404336bd6c63594b18)), Closes #409

### 🧹 Refactoring

- **task.rs:** Remove code duplication in decrypt task ([49fe8ea](https://gitlab.com/biomedit/sett-rs/commit/49fe8eae8b2863c5687e465418bcbc0d154fde27))
- Use the new `sett::auth::Oidc::new` API ([0511d68](https://gitlab.com/biomedit/sett-rs/commit/0511d6881a036627e08e6664376295c19e3f2c2e))

## [5.5.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dgui%2F5%2E5%2E0) - 2025-01-22

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dgui%2F5%2E4%2E0...sett%2Dgui%2F5%2E5%2E0)


### ✨ Features

- **gui/S3:** Remove paste credentials from clipboard button ([a2d6d78](https://gitlab.com/biomedit/sett-rs/commit/a2d6d780a68977f441668aa56bce0f1c5ba20713)), Close #396
- **auth:** Prevent unintended OIDC issuer URL shortening ([7f01d72](https://gitlab.com/biomedit/sett-rs/commit/7f01d72f94cb487d50875149bf84a4d09600be85))
- Change short URL for device authorization grant ([3853b40](https://gitlab.com/biomedit/sett-rs/commit/3853b406583034fcdee2754eb3f83971fbf34457))

### 🐞 Bug Fixes

- **decrypt:** Use "read" credentials for the S3 source ([736ec25](https://gitlab.com/biomedit/sett-rs/commit/736ec25a90b2cdc48622acd9a3c944abf19605ee)), Close #401

### 🧱 Build system and dependencies

- **npm:** Revert to the latest working esrap version ([40dd505](https://gitlab.com/biomedit/sett-rs/commit/40dd505140d22473ed8642acebd52dd3908e4f75))
- **sftp::upload:** Use the reference-based library API ([50bf080](https://gitlab.com/biomedit/sett-rs/commit/50bf080b79cfb85b5d27dc4281cf61841c6e524f))

## [5.4.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dgui%2F5%2E4%2E0) - 2024-12-17

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dgui%2F5%2E3%2E0...sett%2Dgui%2F5%2E4%2E0)


### ✨ Features

- **sett-rs/gui:** Implement decrypt method in authenticated mode ([41fb866](https://gitlab.com/biomedit/sett-rs/commit/41fb866b052a1f92c25cc662c5d5b0a55a3daa1c))
- **sett-rs/tauri:** Implement decrypt method in authenticated mode ([c42d783](https://gitlab.com/biomedit/sett-rs/commit/c42d78387df89fd577a94ee1f8034c753d6f046b))
- **sett-rs/tauri:** Add auth_state to open, decrypt and verify_package ([dfad97d](https://gitlab.com/biomedit/sett-rs/commit/dfad97da63957cb30456bf59d334f52e0b6e60e3))
- **package:** Include additional information in package details ([885f2d1](https://gitlab.com/biomedit/sett-rs/commit/885f2d1bce4db57b4fd025e3216dbca284f187d3)), Close #345
- Add decrypt portal subcommand ([e107fd9](https://gitlab.com/biomedit/sett-rs/commit/e107fd9f5d446485b2e57419bff68ca4b1e59525))

### 🐞 Bug Fixes

- **Encrypt:** Fix wrongly-shown warning message about non-authorized DTR ([978984d](https://gitlab.com/biomedit/sett-rs/commit/978984d67bbb52470d812972d02dd9bc26c5f6be)), Closes #379
- Move settingsStore.load() to svelte-level ([a3b99ab](https://gitlab.com/biomedit/sett-rs/commit/a3b99abdc9f2ddaa107d679f79ed99683f4f0e05))
- Prevent closing the password dialog ([ac65340](https://gitlab.com/biomedit/sett-rs/commit/ac65340bc5e21f6df2722e419a71e16bc55e67c8)), Close #358

### 🧱 Build system and dependencies

- Migrate to setts API relocations ([838da4d](https://gitlab.com/biomedit/sett-rs/commit/838da4d858c775463d535c486309169886def310))
- Migrate to sett's new certificate API ([744ad8f](https://gitlab.com/biomedit/sett-rs/commit/744ad8f9905815213867244fafdbbbe1440aeb3e))
- Bump MSRV to 1.83 ([712a9c0](https://gitlab.com/biomedit/sett-rs/commit/712a9c015862477a1893c712bd280992b57dd102))
- Update svelte to v5 ([0f7a37f](https://gitlab.com/biomedit/sett-rs/commit/0f7a37f9ce0bd3454c0f7c5072b976d3cedfa018)), Close #356

### 🧹 Refactoring

- Improve text regarding authenticated mode ([1048f5d](https://gitlab.com/biomedit/sett-rs/commit/1048f5d397947a6a995e01a629a2e6112de6abe3))
- Name pass_state and auth_state consistently ([b12cd73](https://gitlab.com/biomedit/sett-rs/commit/b12cd7372bedbc9e7859dde301b68b29c211f16f))
- **auth:** Use owned value in auth_handler callback ([4b14a8f](https://gitlab.com/biomedit/sett-rs/commit/4b14a8f2c5507ddf4e1db916fee83a640cd50564))
- **auth:** Replace anyhow errors by transparent error types ([bb41ca7](https://gitlab.com/biomedit/sett-rs/commit/bb41ca7748cbd15ec1fe3a5416a0b355e418c170))
- Update switch statements to work with new eslint ([64881c0](https://gitlab.com/biomedit/sett-rs/commit/64881c0a0e54d2f0843cf357069fb49ee3bd9d9b))
- **about:** Use snippet to reduce code duplication ([b51f86d](https://gitlab.com/biomedit/sett-rs/commit/b51f86d8695bd43252626d2dc6e4bd1617fd4641))
- **settings:** Use snippet to reduce code duplication ([dfb548c](https://gitlab.com/biomedit/sett-rs/commit/dfb548c1fcc1c79dee0d37a67f6a5d1883c71325))
- **package:** Improve typing for packages ([50bf106](https://gitlab.com/biomedit/sett-rs/commit/50bf1066c8f4df068e92d9208796c8dd396a2e19)), Close #346
- **package:** Make Package generic over PackageSource ([46d5c0f](https://gitlab.com/biomedit/sett-rs/commit/46d5c0f4f7fe48b6090fc565f51c5bb260a91e67))

### 🎨 Style

- **eslint:** Drop no-nested-ternary eslint rule ([037129e](https://gitlab.com/biomedit/sett-rs/commit/037129eda65930eabd7dd6a889d6f028e1c17d8b)), Close #350

## [5.3.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dgui%2F5%2E3%2E0) - 2024-10-18

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dgui%2F5%2E2%2E0...sett%2Dgui%2F5%2E3%2E0)


### ✨ Features

- **Decrypt:** Warn user when any of the keys used for decryption is about to expire ([000c879](https://gitlab.com/biomedit/sett-rs/commit/000c8798b05c7f6f2ced18606e75d1f7d910c2e3)), Close #341
- **Encrypt:** Warn user when the key used for encryption is about to expire ([239f4be](https://gitlab.com/biomedit/sett-rs/commit/239f4be7466b265b8be8c8507d5e8278746b0540))
- **Cert:** Add badge for keys that will expire soon ([2bd6b86](https://gitlab.com/biomedit/sett-rs/commit/2bd6b862e1b4da0efe2b30051bb962423df9e447))
- **decrypt:** Add direct decryption from s3 ([170d6f2](https://gitlab.com/biomedit/sett-rs/commit/170d6f23d4fa1ffbf62e8ca36776025c6f2c2bec)), Close #340
- **gui/transfer:** Prevent user from sending pkg without dtr ([0d4ed68](https://gitlab.com/biomedit/sett-rs/commit/0d4ed68df5732060ff2319e1ab8e8fb4233d1ea4)), closes #328
- **Cert:** Add support for changing expiration date ([4c8d0d0](https://gitlab.com/biomedit/sett-rs/commit/4c8d0d0421800792ca57b47576d7b40fd99764bf)), Close #335
- Add set_expiration_date command ([ea2425e](https://gitlab.com/biomedit/sett-rs/commit/ea2425e06c3c54e74ec347a68984f2480a0e1dc5))

### 🐞 Bug Fixes

- Improve key hints when asking for password ([1d331c6](https://gitlab.com/biomedit/sett-rs/commit/1d331c6e647952a74b113f5406585c637903211d))
- **s3:** Rename private to secret key ([9b76cfd](https://gitlab.com/biomedit/sett-rs/commit/9b76cfdd3ee2157e0368da7c6d1f3ce460ddf3d8))
- **PasswordDialogListener:** Fix typo ([df53454](https://gitlab.com/biomedit/sett-rs/commit/df5345496334297f1923c682a9daa41d13440f20))
- **keys:** Fix import from GnuPG ([e517a42](https://gitlab.com/biomedit/sett-rs/commit/e517a4274735d730db246473124fa8f5ce6a5071)), Close #338

### 👷 CI

- Add switch-exhaustiveness-check to eslint ([8d04ef5](https://gitlab.com/biomedit/sett-rs/commit/8d04ef57647c4a68335206b2389eeebfc767e712))

### 🧹 Refactoring

- Adapt expiration date component to sett library changes ([c39ac48](https://gitlab.com/biomedit/sett-rs/commit/c39ac487fb85d1fe3a04600280c79108d29b570e))
- **package:** Rename metadata interface ([a96064c](https://gitlab.com/biomedit/sett-rs/commit/a96064c1e14f1430f2b2dbda497bdb6976f96783))
- Merge encrypt and transfer stores ([a39078a](https://gitlab.com/biomedit/sett-rs/commit/a39078a2f217165b0ce3fc02172387e52a4d83fe))
- **cert:** Pass fingerprint to load_cert as a reference ([87ab3bc](https://gitlab.com/biomedit/sett-rs/commit/87ab3bc5522c573a586fa89822927236b42e43c1))
- **cert:** Improve typing of ExpirationDate ([9c1447e](https://gitlab.com/biomedit/sett-rs/commit/9c1447eaf3dedd4dc280c884dc1760989b62b8cd))
- **GenerateCertDialog:** Extract expiration date related logic ([b8a86e9](https://gitlab.com/biomedit/sett-rs/commit/b8a86e9c0e6c01e7c52193db36254b32467120f3))

## [5.2.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dgui%2F5%2E2%2E0) - 2024-09-18

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dgui%2F5%2E1%2E0...sett%2Dgui%2F5%2E2%2E0)


### ✨ Features

- Merge Encrypt and Transfer tabs into a single 'Encrypt and Transfer' tab ([9855e67](https://gitlab.com/biomedit/sett-rs/commit/9855e67b95541a4fd5b4a2744a65c213a4bc0904)), Closes #235
- **settings:** Add public and private keystore location ([8341065](https://gitlab.com/biomedit/sett-rs/commit/8341065edb599477b2c330136740bf297d69fec1))
- Add toggle for switching in authenticated mode ([c351a6b](https://gitlab.com/biomedit/sett-rs/commit/c351a6b26ff61f542e2fd7567c444ee63f2b6c31)), Close #283
- **Keys tab:** Add expiration date setting to OpenPGP certificate generation ([095fbbe](https://gitlab.com/biomedit/sett-rs/commit/095fbbef38cb817ae6a665d55e5fd921505b3776)), Closes #312
- Prompt for password only when needed ([f35e12e](https://gitlab.com/biomedit/sett-rs/commit/f35e12ee304d29b796571e9e0b087c9308885335)), Close #274, #275
- Add support for optional custom metadata ([4cb1ace](https://gitlab.com/biomedit/sett-rs/commit/4cb1acefb9e26a3cfc2f11bfc8ae16659cf8c78f)), Closes #309
- **frontend:** Update encrypt and decrypt tabs to support authenticated mode ([8a1724a](https://gitlab.com/biomedit/sett-rs/commit/8a1724aac2e9589f9d94b215f1a6967a930da25f)), Closes #255
- **backend:** Add support for authenticated encryption and transfer ([375c3d8](https://gitlab.com/biomedit/sett-rs/commit/375c3d8203ea6bb3d660dd4ae41d5f7787d54cdd))
- **auth:** Keep OAuth2 tokens alive ([3e0cebb](https://gitlab.com/biomedit/sett-rs/commit/3e0cebbd8ce7489e4fd6c7597cb4a8d862253ce1)), Close #282
- **log:** Generate structured, JSON-based logs ([3a6f2ca](https://gitlab.com/biomedit/sett-rs/commit/3a6f2cab8e79ff328429d8ec62b0a7d39410117a)), Close #291, #296
- **sett-gui/Cert:** Add label for keys containing the public part ([96caffe](https://gitlab.com/biomedit/sett-rs/commit/96caffef7357e840e4df683a431be668dce74a06)), Close #221
- Display user info when available ([e01f6b0](https://gitlab.com/biomedit/sett-rs/commit/e01f6b0b4dff4e8f0060c25699648df7f0b3a63e)), Closes #278
- Add device login in the GUI ([0aad9e9](https://gitlab.com/biomedit/sett-rs/commit/0aad9e912a49ab96bec16b03dff368758ca9b8a6)), Closes #257
- Add possibility to specify a setting for OIDC issuer ([f1ab321](https://gitlab.com/biomedit/sett-rs/commit/f1ab321714366635621b6df9481fdc50ef3611af))

### 🐞 Bug Fixes

- **TransferIdSelector:** Do not reset transferId value upon component reload ([eb42822](https://gitlab.com/biomedit/sett-rs/commit/eb42822b642ed15e5ce849c0896c74dd399aad8a))
- **dropzone:** Warn user when dropping incompatible files ([894b7c2](https://gitlab.com/biomedit/sett-rs/commit/894b7c271fe3903e328d233507449b3590219f57))
- **DropZone:** Button tooltip truncated to the left ([d066583](https://gitlab.com/biomedit/sett-rs/commit/d0665836197d95e24baa31bff9b202bcea566cef)), Closes #326
- **DropZone:** Remove the extra space when a data package is selected ([691a567](https://gitlab.com/biomedit/sett-rs/commit/691a567be8a140e74527564ff86b31fb8c0b5722)), Closes #326
- Fix auth mode ([1ced4f5](https://gitlab.com/biomedit/sett-rs/commit/1ced4f513cf00c8ae5811f222936dadc797b57d8))
- **Certs:** Use a keyed each block to display certs ([e815223](https://gitlab.com/biomedit/sett-rs/commit/e8152230f703111865f261d90c1e4d81eaad9ff1)), Closes #317
- Minor visual element alignment changes in RadioInput ([ad25b30](https://gitlab.com/biomedit/sett-rs/commit/ad25b3024fd3dac83c184092cb2fe5022e858fb9))
- Prevent Enter key press on transfer ID field from submitting form ([bc34880](https://gitlab.com/biomedit/sett-rs/commit/bc348806953fed0b4e9b59afb8502ec8919c084b))
- Include custom metadata when performing a test run ([005f3b1](https://gitlab.com/biomedit/sett-rs/commit/005f3b1b3235de604c61b85ad50bacb879403473))
- **Settings:** Hide display of oidcIssuerURL behind authEnabled flag ([9cc96d2](https://gitlab.com/biomedit/sett-rs/commit/9cc96d29e97a68c985dfce5d00cbaaee4375cbc3))
- Minor type annotation fix in destination.ts ([f5adc50](https://gitlab.com/biomedit/sett-rs/commit/f5adc50368adc2af1a4717b19d0f638fce9b790a))
- **encrypt:** Encrypt options can have custom metadata ([4941892](https://gitlab.com/biomedit/sett-rs/commit/49418924da4cea0c5602015980713c1ef17ac568))
- Remove dynamic imports ([ac9d011](https://gitlab.com/biomedit/sett-rs/commit/ac9d011d46203365acb067241cfa0038819a9ba2)), Close #300
- **package.json:** Version number ([364691b](https://gitlab.com/biomedit/sett-rs/commit/364691bec39a417bfefb5df5e22f6af43696bde4))

### 🚀 Performance

- **progress:** Remove atomics ([b9b868b](https://gitlab.com/biomedit/sett-rs/commit/b9b868b3563ee14399889cbc92db166705351d67))

### 🧱 Build system and dependencies

- Bump minimal tauri version ([20e1cf7](https://gitlab.com/biomedit/sett-rs/commit/20e1cf7183c021ee7d29029f8d43d5c17c6fce65))
- Make webbrowser crate optional ([fcb572c](https://gitlab.com/biomedit/sett-rs/commit/fcb572c21aa39bcec2f7539f1107c4404a9cffa0))
- Include sett-gui in the main workspace ([328bb08](https://gitlab.com/biomedit/sett-rs/commit/328bb0851741247e7e81b552322693e4f419359f)), Close #270

### 👷 CI

- Add linting of JSON files to eslint.config.js ([8d8778f](https://gitlab.com/biomedit/sett-rs/commit/8d8778fbf4a5ebec2b9c8bdbcb59fdff7b34879c))
- Include eslint.config.js in list of files linted by eslint ([6659d97](https://gitlab.com/biomedit/sett-rs/commit/6659d9707dc3ee74470559e6cdeac87413ff8939))
- Refactor eslint.config.js and add no-deprecated rule ([543dea1](https://gitlab.com/biomedit/sett-rs/commit/543dea129ed8bb870c0516e27559dfe6e335692b)), Closes #323

### 🧹 Refactoring

- **Encrypt:** Reduce code duplication ([793fe2f](https://gitlab.com/biomedit/sett-rs/commit/793fe2f1bec4908ee2a7e47fdd518294a36924ef)), Closes #331
- Use latest `sett` crate ([c8d0f8f](https://gitlab.com/biomedit/sett-rs/commit/c8d0f8f6933330ff3505281fa56a871b50526cb2))
- Make authenticated mode available by default, remove 'auth' flag ([e32b961](https://gitlab.com/biomedit/sett-rs/commit/e32b961209a4c2c17fba8276adf2663a9d995cc3)), Closes #318
- **metadata:** Rename metadata field `custom` to `extra` ([31b3926](https://gitlab.com/biomedit/sett-rs/commit/31b3926fa9ee01de950e9d64d77c3a3ef091c195)), Close #320
- Adapt to change in Cert::update_secret_material of sett ([28c84fd](https://gitlab.com/biomedit/sett-rs/commit/28c84fd187d4c0354ee8f42f5cd372e0e16350e3))
- Merge `cred` with `password` module ([84fa608](https://gitlab.com/biomedit/sett-rs/commit/84fa608762083b31671c6167390a371d392ebedf))
- **SecretInput:** Add optional `onInput` prop ([124ff3c](https://gitlab.com/biomedit/sett-rs/commit/124ff3c77f9d143c4fbd8e31040e57a62967ef93))
- Remove dependency on the log crate ([4bb51d4](https://gitlab.com/biomedit/sett-rs/commit/4bb51d4473f0242544904ab55b010a5f9f5d4119))
- Use the new `Package` and `s3` API ([88fc212](https://gitlab.com/biomedit/sett-rs/commit/88fc21222ba3c7d701ee442ee74b07f561653c23))
- **metadata:** Use fixed custom field format ([5da8d2d](https://gitlab.com/biomedit/sett-rs/commit/5da8d2df8573270e764445697b3738ec6d56cb75))
- Use builder for generating new certs ([f0ee14d](https://gitlab.com/biomedit/sett-rs/commit/f0ee14d1eb83260056222a96c8d4e2f045b28deb))
- Rename methods for encrypt Props from .reset to .clear ([3390b2a](https://gitlab.com/biomedit/sett-rs/commit/3390b2a36e1320a48a85c0434bc8cde36016fa9d))
- Reorder imports in App.svelte ([bd0d966](https://gitlab.com/biomedit/sett-rs/commit/bd0d96661174694e2288ebe5f6270c65848dc92e))
- **sett-gui:** Omit default values for flexbox properties ([4845301](https://gitlab.com/biomedit/sett-rs/commit/4845301096cdd57d1ac58e9e72518771e8c8a114))
- Be more concise in the layout and use dialog for login ([7e1250a](https://gitlab.com/biomedit/sett-rs/commit/7e1250a1e5485ba0a1964d53ec783850393e3ea6)), Closes #297
- Use latest `sett` crate API ([09f2faf](https://gitlab.com/biomedit/sett-rs/commit/09f2fafaed33acf92709058fd80f4e847a07e49d))
- Use the new download from keyserver API ([01f4b5f](https://gitlab.com/biomedit/sett-rs/commit/01f4b5faf6912742dad36ca40936db20cfb13a70))
- Update code for breaking changes in sett ([177bc87](https://gitlab.com/biomedit/sett-rs/commit/177bc8782fe70431a1afa6147e953ba0cb574147))
- Introduce dynamic imports for `NavItem` ([5178260](https://gitlab.com/biomedit/sett-rs/commit/51782601b2e11e10ffc5a25233626d6694fa1d52))

## [5.1.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dgui%2F5%2E1%2E0) - 2024-06-19

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dgui%2F5%2E0%2E0...sett%2Dgui%2F5%2E1%2E0)


### ✨ Features

- Improve OpenPGP key generation dialog validation ([53c0ffc](https://gitlab.com/biomedit/sett-rs/commit/53c0ffc47988f0ce804e0dd79b8131b259b336c2)), close #238
- Allow import of PGP key from clipboard content ([cdff7d7](https://gitlab.com/biomedit/sett-rs/commit/cdff7d7c5484d4ea1b928b304290f4eef83e40d8)), Closes #237
- Add possibility to remove pending tasks ([9d351be](https://gitlab.com/biomedit/sett-rs/commit/9d351be612acfd3e68106d2eb9b7bebf68546b45)), Closes #97
- Update Svelte components to use custom tooltips ([7456a42](https://gitlab.com/biomedit/sett-rs/commit/7456a42109cfd0a33298d816ec546c68260ef2d3)), Closes #228
- Add tooltip component ([f64dbaf](https://gitlab.com/biomedit/sett-rs/commit/f64dbafe5d62c0406319a9706e844d7e9f588c39))
- **encrypt:** Display warning icon when directory is read-only ([793764f](https://gitlab.com/biomedit/sett-rs/commit/793764f120b93c4f01cef390150cd09e473423e4)), Closes #209
- Add `Folder` icon to selected directories ([cafed8e](https://gitlab.com/biomedit/sett-rs/commit/cafed8ed0c8ea2288ff0b26fe5e8854aebf3c725)), Closes #183
- Make `onClear` optional and add possibility to specify an icon ([78f7190](https://gitlab.com/biomedit/sett-rs/commit/78f7190a51ab96adfdf38e55c7001c4ff15d108b))

### 🐞 Bug Fixes

- **styling:** Dark mode mouse-over colour is too bright ([5def63d](https://gitlab.com/biomedit/sett-rs/commit/5def63d72988aa7d46bf2fc66c802e561b95df5a)), Closes #267
- **CertItem:** Make the error dismissible, allowing a retry ([2861b6c](https://gitlab.com/biomedit/sett-rs/commit/2861b6c31093520bfc5154c16f91d374e79c8e33)), Closes #265
- `gpg` not found in PATH for productive builds ([c2c9f8a](https://gitlab.com/biomedit/sett-rs/commit/c2c9f8a9e2e2eab02e06373fece599b223bf85d3)), Closes #268
- Remove the clear and reveal password icons from windows ([eb761f5](https://gitlab.com/biomedit/sett-rs/commit/eb761f58a8e469028395e4ee347605e9cced8e46)), Closes #259

### 🚀 Performance

- Use the new, faster async api ([11121c1](https://gitlab.com/biomedit/sett-rs/commit/11121c10dbd048028b57e12d3d4e777cb0218992))

### 🧱 Build system and dependencies

- Update dependencies to solve RUSTSEC-2024-0344 ([f731562](https://gitlab.com/biomedit/sett-rs/commit/f731562eea1df42cd688baebc849a7ac677976e6))
- Try `"typescript-eslint": "8.0.0-alpha.10"` for fixing `eslint` v9 ([55b3ced](https://gitlab.com/biomedit/sett-rs/commit/55b3ced20cb3fcad2d580069154ecb07577fac3c))
- Bump MSRV to 1.75 ([3c0143e](https://gitlab.com/biomedit/sett-rs/commit/3c0143e14b8c426df917bb7cbbf22395a77f55b5))

### 👷 CI

- Standardize npm script names. Use run-s to run multiple scripts ([3b32d09](https://gitlab.com/biomedit/sett-rs/commit/3b32d099ae96aac1c10460914925a6b3e00f0443))
- Add `npm run check` script to run all checks ([e9e075c](https://gitlab.com/biomedit/sett-rs/commit/e9e075cde21ca144b242056b26817f1a0ea80917))

### 🧹 Refactoring

- **decrypt:** Use async decrypt ([69ba26d](https://gitlab.com/biomedit/sett-rs/commit/69ba26db3d9e22c56c484e50750a329ba8f443cb))
- Adapt decrypt options to the breaking change ([57ac573](https://gitlab.com/biomedit/sett-rs/commit/57ac57308e00e2c1ea10dec59e6c9384fa1d9bc1))
- Split Alert and Badge into separate components ([3439345](https://gitlab.com/biomedit/sett-rs/commit/343934524f727de226f518e77d2b6177ba924fae))
- Use a more complex object structure to store selected paths ([a0788a5](https://gitlab.com/biomedit/sett-rs/commit/a0788a547116412c720dfc74cc2097cfd208d2e8))
- Move `iconMap` to `util.ts` ([1a0ff32](https://gitlab.com/biomedit/sett-rs/commit/1a0ff322f7f3838658541956c733322baaa154d4))

## [5.0.0-rc.3](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dgui%2F5%2E0%2E0%2Drc%2E3) - 2024-04-18

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dgui%2F5%2E0%2E0%2Drc%2E2...sett%2Dgui%2F5%2E0%2E0%2Drc%2E3)


### ✨ Features

- Add tooltips to interactive components ([bb7e9b3](https://gitlab.com/biomedit/sett-rs/commit/bb7e9b3a1ede3d751a0358bce2908eea8b4aba1f)), Closes #229
- Add and harmonize button icons in keys tab ([a05bb55](https://gitlab.com/biomedit/sett-rs/commit/a05bb557d6f937461f0e21a3006f761989377bd1))
- Require pwd to be ge 10 chars when generating new PGP key ([0de6c14](https://gitlab.com/biomedit/sett-rs/commit/0de6c14dcaa54924ebc30e462bd42b98654438bb))
- Add input verification to ImportFromKeyserverDialog ([8d33323](https://gitlab.com/biomedit/sett-rs/commit/8d33323b1034b70b312429f8e53b330b03f95015))
- Add relevant content to the about section ([14a419a](https://gitlab.com/biomedit/sett-rs/commit/14a419acc53bdb26cd1f2bfa711ccacfe1be70b7)), Close #226
- **settings:** Display location of log file and keyserver in settings tab ([0846fe1](https://gitlab.com/biomedit/sett-rs/commit/0846fe177ee536bddb74d84ad52a5bfc752ca322)), Closes #222
- Make button text and size configurable in CopyToClipboard component ([7130013](https://gitlab.com/biomedit/sett-rs/commit/71300138dff08dc3fc3a1391455d0f0caed83c5d))
- Add optional comment field to generate key dialog ([4a0a9cf](https://gitlab.com/biomedit/sett-rs/commit/4a0a9cf283a683e0baab45de78944af751cb38bc)), Closes #215
- **cert:** Sort certificates alphabetically ([5c4203a](https://gitlab.com/biomedit/sett-rs/commit/5c4203a1fbd3ee5338363a45b510469a7b9bf493))
- **cert:** Use badge for representing Portal approval ([9ba28e3](https://gitlab.com/biomedit/sett-rs/commit/9ba28e31ccac8276c261b701a8b92d44e85ed0cd)), Close #153
- **about:** Add version number ([9e5862b](https://gitlab.com/biomedit/sett-rs/commit/9e5862bb719ae8f72e70b770a98256ef80fe51ec)), Close #191
- Verify package with Portal when the corresponding setting is enabled ([de605c0](https://gitlab.com/biomedit/sett-rs/commit/de605c0b66fc74b39eb41d77419e14c0190a4464))

### 🐞 Bug Fixes

- **sett-gui/button:** Change background color when button is disabled ([277f310](https://gitlab.com/biomedit/sett-rs/commit/277f3108153a081cdf0ea7f5c1ddbfa863f494e4)), Close #199
- **sett-gui/task:** Handle division by zero when updating progress bar ([eda5aff](https://gitlab.com/biomedit/sett-rs/commit/eda5affe4665307f248bfd395d212432188ae820)), Close #224
- Make email verification checkbox in UploadCertDialog work ([405874a](https://gitlab.com/biomedit/sett-rs/commit/405874a6b9c84e8569f94df63d0975ce2c160b5f))
- Show a complete destination url for all tasks ([937371e](https://gitlab.com/biomedit/sett-rs/commit/937371eb8fcba524ebabd8c6ae15bba43815d5c8)), Close #223
- **keyserver:** Remove option to import further keys after success import ([b7eaeb0](https://gitlab.com/biomedit/sett-rs/commit/b7eaeb03868c0b3ee654b241b8627083d069477d)), Close #218
- **revocation-dialog:** Improve message for unspecified reason ([0a90a04](https://gitlab.com/biomedit/sett-rs/commit/0a90a04ca3ab28dd34e60a20601959043628f964)), Close #220
- **cert:** Show upload to keyserver button only for private keys ([09a94a3](https://gitlab.com/biomedit/sett-rs/commit/09a94a3b99634b730f66003648082c01cf2a1552)), Close #219
- **css:** Switch warning color from yellow to orange ([73b7793](https://gitlab.com/biomedit/sett-rs/commit/73b77930bb712cf65a1a0a5199be57e8dfd1e56a))
- Correctly display key type in DeleteCertDialog ([ed84ba1](https://gitlab.com/biomedit/sett-rs/commit/ed84ba1b73e4709f4d00b97ae22ce9e3d1202c0d)), Closes #203
- **task:** Forget password after submitting a task ([55d4a38](https://gitlab.com/biomedit/sett-rs/commit/55d4a38f1768167e0ee19dec090e51b716fb95a9))
- **Chip:** Use the correct color for the chip button ([8877384](https://gitlab.com/biomedit/sett-rs/commit/88773844dd7a176da6b0788f45b508e222cb8100))
- **QuickSearch:** Make magnification glass icon visible also in the production build ([351b095](https://gitlab.com/biomedit/sett-rs/commit/351b095878ed8ed5ce2e68a12447de83fe01d043)), Close #216
- **settings:** Hide keyserver url ([cb8c498](https://gitlab.com/biomedit/sett-rs/commit/cb8c4980f8b6bebf8ef973137a24e0dad954b132))
- **Windows:** Enable overlay scrollbars ([845c096](https://gitlab.com/biomedit/sett-rs/commit/845c096b25b3a0681c45a7f3ef80db2adefe69da))
- **encrypt:** Require transfer id when verify package is enabled ([2d5345b](https://gitlab.com/biomedit/sett-rs/commit/2d5345b4907509b5c63e10c9269f57538bbe5d73))

### 🧱 Build system and dependencies

- Update Cargo lock ([fa43d0d](https://gitlab.com/biomedit/sett-rs/commit/fa43d0dfffc48e54c069b85a1934d6c8e8d3ab60))
- Add licenses for each crate ([be2ad30](https://gitlab.com/biomedit/sett-rs/commit/be2ad30accc4693eed5136be70892866aea682f0)), Close #217

### 👷 CI

- Switch to flat config file for eslint ([fa83820](https://gitlab.com/biomedit/sett-rs/commit/fa83820f82d3fcd0216321ffcf6ea68b8fefbdc1)), Closes #234
- **build/linux:** Bump base image to ubuntu 20.04 for gui .Appimage builds ([e85d3ed](https://gitlab.com/biomedit/sett-rs/commit/e85d3edffd10a9dfa7a845115951c2bb54755652)), Close #205
- Fix new security vulnerability ([2381703](https://gitlab.com/biomedit/sett-rs/commit/238170307a0446e71e14f812e4b246499bb8883b))

### 📝 Documentation

- Improve all project's `README.md` ([365085f](https://gitlab.com/biomedit/sett-rs/commit/365085f285cf23a85727a54977241c8f3970887f)), Close #177
- Use OpenPGP instead of PGP in functions documentation ([3b2e78a](https://gitlab.com/biomedit/sett-rs/commit/3b2e78ab765b580078abfc103091f9ab8dc15893))

### 🧹 Refactoring

- Sort imports in .ts and .svelte files ([280103f](https://gitlab.com/biomedit/sett-rs/commit/280103fb8b055ebfb4a716ee021ebf8022949e68))
- **cert:** Improve error message from read_pkg_metadata() ([88bc2d5](https://gitlab.com/biomedit/sett-rs/commit/88bc2d559da602ae6d0ce8bb0ffd7a43b273b780))
- **keyserver:** Use the new keyserver API ([03e1701](https://gitlab.com/biomedit/sett-rs/commit/03e1701d2e6b8c626d70d7b188d474f112abbfe0))
- **settings:** Use Svelte store for managing settings ([9c1c39e](https://gitlab.com/biomedit/sett-rs/commit/9c1c39e7cae1e6d9178b8bd91c3bfb5afcc17550))

## [5.0.0-rc.2](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dgui%2F5%2E0%2E0%2Drc%2E2) - 2024-03-05

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dgui%2F5%2E0%2E0%2Drc%2E1...sett%2Dgui%2F5%2E0%2E0%2Drc%2E2)


### ✨ Features

- Icrease default App size to 900 x 1040 px ([1bfee0e](https://gitlab.com/biomedit/sett-rs/commit/1bfee0e9a1856d62acfc2b9221618ac3de73110f))
- Add preflight checks ([43a364a](https://gitlab.com/biomedit/sett-rs/commit/43a364af31d905262756766dd0ea35691f0af583)), Close #113
- **gui:** Rename certificate(s) to key(s) when interacting with the user ([a534d6f](https://gitlab.com/biomedit/sett-rs/commit/a534d6fcbe13a8e98290a63e67ab42b58af50442)), Close #170
- Refactor pkg summary table to show user IDs, not fingerprints ([b45b7e0](https://gitlab.com/biomedit/sett-rs/commit/b45b7e08cc5c8e2a38c43a0e18ec7ba5909ac7e4)), Closes #133
- **settings:** Add reset settings button to GUI ([604e481](https://gitlab.com/biomedit/sett-rs/commit/604e481ce05abcacee7e3050fcaa905b038dff24)), Closes #180
- Use chips to improve UX in GUI ([e20a26d](https://gitlab.com/biomedit/sett-rs/commit/e20a26d93ce31d41b2087b2a34cd9530f84fd61e)), Closes #142
- **settings:** Add default output directory setting to GUI ([303246f](https://gitlab.com/biomedit/sett-rs/commit/303246fbe9f5e318d987f92724c05c89fa139d83))
- Specify `started`/`updated|finished` timestamps for each task ([502b0cc](https://gitlab.com/biomedit/sett-rs/commit/502b0cccd2273f8421608818bed53fecd3841f63)), Closes #114

### 🐞 Bug Fixes

- **crypt:** Improve error message when wrong password to decrypt data ([29e0d51](https://gitlab.com/biomedit/sett-rs/commit/29e0d51f7ff341034958cff3cf32a37427c6439c)), Closes #193
- **keys tab:** Rename Private Key badge to Private ([a475e6c](https://gitlab.com/biomedit/sett-rs/commit/a475e6c7d885a3ecb2f5c8bb109639090f925c87))
- **Toast:** Keep size of dismissal button constant ([18090ce](https://gitlab.com/biomedit/sett-rs/commit/18090ce8c0174281569aa4b3f5922491d903d925)), Closes #194
- **Chip:** Keep size of dismissal button constant ([edcc7e7](https://gitlab.com/biomedit/sett-rs/commit/edcc7e749627a8e8a9290a45c42f7ca4f6dc7248))
- Improve error message when parsing data packages ([b144daa](https://gitlab.com/biomedit/sett-rs/commit/b144daa69d50a8cb0bea0b2853122a44c394d254)), Closes #164
- **decrypt:** Fix bug in decryption with multiple recipients in GUI ([8db333d](https://gitlab.com/biomedit/sett-rs/commit/8db333df247c3aedacee77851277355848aeb154)), Closes #173
- `encrypt` does no longer work for S3 transfers ([fd9690e](https://gitlab.com/biomedit/sett-rs/commit/fd9690ec30b29188cdc1c31cf7d13d697e65517f)), Closes #179
- `upload` method not working for S3 destination ([ec4c75e](https://gitlab.com/biomedit/sett-rs/commit/ec4c75e7c9ef3cb4ebaaefe60d03ba2c73de9c63)), Closes #179
- **settings:** Correct async loading of settings in GUI ([ba32a53](https://gitlab.com/biomedit/sett-rs/commit/ba32a53339e8c14069631a77d4cf242e6bb7f238))
- Update styling of main workflow buttons ([479966e](https://gitlab.com/biomedit/sett-rs/commit/479966e297060b355882c49c9f348e38df3fae40)), Closes #174
- Correctly load default settings ([266299a](https://gitlab.com/biomedit/sett-rs/commit/266299a3c849684cfc648b41a75b5b79e14fac08))
- Make selecting destination compulsory in GUI ([8643689](https://gitlab.com/biomedit/sett-rs/commit/864368940c6353da11fd15341316400bb8599be5)), Closes #171
- Make DTR ID field more `numeric` and nicer to use ([3f4bb84](https://gitlab.com/biomedit/sett-rs/commit/3f4bb84f2d5c4b624f1fbf6c86c5d279e5290dc6))
- The whole key line is now clickable to open the details ([64f4810](https://gitlab.com/biomedit/sett-rs/commit/64f48106d916ce6eecb57f6412ff3ca930fcbd2e))
- **encrypt:** List both private and public certs as recipient choice ([58b33f7](https://gitlab.com/biomedit/sett-rs/commit/58b33f782cb74bd559b21d024c1354c4d11e0697)), Closes #163

### 🧱 Build system and dependencies

- Select build features in Cargo.toml ([f5e7ab5](https://gitlab.com/biomedit/sett-rs/commit/f5e7ab53cc3120445c9ee4f1b809310c1b16137f))
- Remove `serde_json` as it's only relevant as transitive dependency ([1a4e880](https://gitlab.com/biomedit/sett-rs/commit/1a4e880c25e29a003203f096fd7675db72fe6170))
- Update lock files ([8a8dd7e](https://gitlab.com/biomedit/sett-rs/commit/8a8dd7eb8cff0f2c4e833d604495ab2428fbf8dd))
- Fix openssl-sys compilation issue ([26ff364](https://gitlab.com/biomedit/sett-rs/commit/26ff36489cab8d2fc97da60ea38c4150f81e64c9))
- Bump MSRV to 1.74 ([f0c5743](https://gitlab.com/biomedit/sett-rs/commit/f0c574305a87e2fc74ec085d9a3c929c52463831))
- **lockfile:** Remove unused dependency ([d18ef21](https://gitlab.com/biomedit/sett-rs/commit/d18ef211accd6a41501bfb3ec3c3c0ced716f244))
- Increase MSRV from 1.70 to 1.72 ([694f68a](https://gitlab.com/biomedit/sett-rs/commit/694f68ab85d29b7256f2a378afa5c36b901d025f))

### 🧹 Refactoring

- Refactor DropZone component ([272a84b](https://gitlab.com/biomedit/sett-rs/commit/272a84b9f6c1ee08c9be097b3f11ea936ed2dfdd))
- `sessionToken` is NOT required ([7646bf6](https://gitlab.com/biomedit/sett-rs/commit/7646bf63b99436793d1ec8d8b57251e2df1d438f))
- Sort string-unions and import-members, add eslint rules ([90421c8](https://gitlab.com/biomedit/sett-rs/commit/90421c890290a174741e973869e4d3939fef3737))
- **PasswordDialog:** Change prop name from "value" to "password" ([53a8d05](https://gitlab.com/biomedit/sett-rs/commit/53a8d0535559e6748524204f810108b54f7d8559))
- Add ClearableItem component ([5f69352](https://gitlab.com/biomedit/sett-rs/commit/5f69352e645ca0a06a78ea7c7ca8b2f6527e288c))
- `CheckboxInput` and `RadioInput` added ([95b1f44](https://gitlab.com/biomedit/sett-rs/commit/95b1f4469124230e002c2f6997697adb11b8a0b5))
- Simplify the code for loading settings ([d2ad0ac](https://gitlab.com/biomedit/sett-rs/commit/d2ad0ac2c1a9a6e701bc74649d06d88b002ff9ac))
- Use already existing global CSS definition `nowrap` ([d73fb7a](https://gitlab.com/biomedit/sett-rs/commit/d73fb7aadfcd3b910438459fa55262ebb8967631))
- `event.target` can not be null ([7e72f8b](https://gitlab.com/biomedit/sett-rs/commit/7e72f8bb53ae4e268fef75912bacc5037aaceeb8))
- Improve tooltip for private badge ([81c286c](https://gitlab.com/biomedit/sett-rs/commit/81c286cea210039dc7ccefdd18be4812aebff00a)), Closes #172
- Use `cog` instead of `adjustments` icon for settings ([5f04ccf](https://gitlab.com/biomedit/sett-rs/commit/5f04ccf7ffc491551b1548ee88781479409b2aa3)), Closes #172
- Button `Paste from clipboard` and helper text were shifted ([58503e1](https://gitlab.com/biomedit/sett-rs/commit/58503e1e198aff38d6741bddc2d553100971907c))
- `local` destination no longer occupies so much space ([8e6b64d](https://gitlab.com/biomedit/sett-rs/commit/8e6b64d1a35246b9a544c70db75e72285997b1e8))
- **encrypt:** Use Button component to fix unwanted form submission ([f719f11](https://gitlab.com/biomedit/sett-rs/commit/f719f1184a54df317a59314d5ea1b09acd2361ea))
- Comply with eslint standards ([920bc2a](https://gitlab.com/biomedit/sett-rs/commit/920bc2aaedade9c2c82b4660cad1499623b2d343))

## [5.0.0-rc.1](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dgui%2F5%2E0%2E0%2Drc%2E1) - 2024-02-02

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dgui%2F5%2E0%2E0%2Drc%2E1...sett%2Dgui%2F5%2E0%2E0%2Drc%2E1)


### ✨ Features

- **error:** Display all unhandled errors as toasts ([db5307a](https://gitlab.com/biomedit/sett-rs/commit/db5307ac5c0d0b33909619ca1c187929e786157c)), Close #136
- Display an error when an invalid package is selected ([f474c62](https://gitlab.com/biomedit/sett-rs/commit/f474c6217d168cb0085e37cf4b7d391d024bce43)), Close #129
- **updater:** Set up auto-updater ([f3739a8](https://gitlab.com/biomedit/sett-rs/commit/f3739a89c3f84c5eda6b1e445c8a84e76eaf2d18)), Close #155
- Display all revocation reasons in GUI ([907b9e8](https://gitlab.com/biomedit/sett-rs/commit/907b9e8a0beed6d0bb023e29414b2945e3018666))
- **certificates tab:** Remove User ID info from certificate details drop-down table ([c939591](https://gitlab.com/biomedit/sett-rs/commit/c939591b2e6681183a018a7a2d6cebbd07766546))
- Display certificate revocation and expiry date ([c1d8b11](https://gitlab.com/biomedit/sett-rs/commit/c1d8b11f5428392f2e2a08eee7c7738369edd247)), Closes #130
- Add close button to ImportFromGnupgDialog ([b86934a](https://gitlab.com/biomedit/sett-rs/commit/b86934a6bd1ea1dd5673a36333ff609be3508e21))
- **settings:** Add settings page ([6d0f1f0](https://gitlab.com/biomedit/sett-rs/commit/6d0f1f00a1be969225ac84004ade6fc08eaf09ab)), close #84
- Improve GUI after new certificate generation ([ab1a102](https://gitlab.com/biomedit/sett-rs/commit/ab1a102c90133c2a2c7ad1f53cc499a8b111f010))
- Do not close dialog when click on backdrop ([786e19c](https://gitlab.com/biomedit/sett-rs/commit/786e19c9d8779e0800ede107eda8d869a048e581))
- Merge private and public certificates ([6c5bc61](https://gitlab.com/biomedit/sett-rs/commit/6c5bc61056b549de1347d1d1009b452573ca5895)), Closes #134
- Add action for uploading a certificate to keyserver ([9aa01a5](https://gitlab.com/biomedit/sett-rs/commit/9aa01a5bf7a941c35322e31ac26d666fd54deac3)), Closes #106
- **cert:** Add certificate revocation dialog ([666d381](https://gitlab.com/biomedit/sett-rs/commit/666d38109f35bc7c587409e6de942cabb2280b1e)), Close #111
- **cert:** Add create revocation certificate dialog ([a6fa608](https://gitlab.com/biomedit/sett-rs/commit/a6fa608420452d25e65f6d71e1cce06edf366056)), Close #110
- Add certificate deletion dialog ([603ec01](https://gitlab.com/biomedit/sett-rs/commit/603ec010b93f0f16ea40dc86f4ede6e1e02467db)), closes #112
- **cert:** Add certificate export functionality ([d82bed9](https://gitlab.com/biomedit/sett-rs/commit/d82bed9838ab279fde69971ee4e09c13b95cf9e1)), Close #108, #109
- Add general structure of cert management tab ([c7728d3](https://gitlab.com/biomedit/sett-rs/commit/c7728d32d41bb89a232a04a651b3406f8d7800e6)), Closes #81
- **cert.rs:** Add functions to retrieve cert approval and path ([1941d78](https://gitlab.com/biomedit/sett-rs/commit/1941d78f0dff81d1ce53e6855afea724d487639e))
- **certificate:** Add "Add" certificate button ([d5af9f8](https://gitlab.com/biomedit/sett-rs/commit/d5af9f8925bace144bfbeaa51b97180c251f5df4)), Close #95
- Add encryption page ([4a669b0](https://gitlab.com/biomedit/sett-rs/commit/4a669b0a4f27dfc224b93a62822df61df94aad46)), Close #82
- **transfer:** Add transfer page ([e65e832](https://gitlab.com/biomedit/sett-rs/commit/e65e8327d8c56ade0b481d3e435c6f83f49d6699)), Closes #83
- Add `Page.svelte` and `Section.svelte` ([2eb21ae](https://gitlab.com/biomedit/sett-rs/commit/2eb21ae3d08a346ba5b71a40e3c4f91925291387))
- **ui:** Make styling consistent ([49181ce](https://gitlab.com/biomedit/sett-rs/commit/49181ce2d913285454e58d18ca5bcb92d36cf5ad)), Close #88
- Add decrypt and tasks pages ([d95a638](https://gitlab.com/biomedit/sett-rs/commit/d95a638ad9401aba45636e0161231e6a6e3e6e79)), Close #80
- Add (skeleton) GUI ([f7e765c](https://gitlab.com/biomedit/sett-rs/commit/f7e765c39e3b8435961788e9fd0881bc2e147b43)), Close #79

### 🐞 Bug Fixes

- **task:** Send error events for all possible backend errors ([b73c52f](https://gitlab.com/biomedit/sett-rs/commit/b73c52f8268947f9a6587d750bec29f3ccefe245))
- `region` and `endpoint` are now optional ([07b8fe2](https://gitlab.com/biomedit/sett-rs/commit/07b8fe2322ac4d4714601b11f7052d1c4ce0d98b))
- Make certificate revocation revoke both public and secret certs ([7a4eb26](https://gitlab.com/biomedit/sett-rs/commit/7a4eb2603a9574b2003b89d1ac932fbbbdb994cf)), Closes #152
- Micro-typo ([28eca1a](https://gitlab.com/biomedit/sett-rs/commit/28eca1a86d045c33862ed43b2ff1d43e3f26fcc6))
- Make reveal password button non-tabable ([cf3bf65](https://gitlab.com/biomedit/sett-rs/commit/cf3bf654a5c62fddbd35284c1de79ff3c78af756))
- The imported file decides which type ([9682946](https://gitlab.com/biomedit/sett-rs/commit/968294626be2fff52ef447893b49a5f835411f76)), Closes #126
- **cert:** Make getCertPath return a placeholder string instead of Error ([4a8ed85](https://gitlab.com/biomedit/sett-rs/commit/4a8ed85acda2da36e60f15e2ca5b51e20a5ee4f7))
- Secret key input field of type password ([1c766f7](https://gitlab.com/biomedit/sett-rs/commit/1c766f772617a74da39c88a2678bf7660b1256e7)), closes #104
- **security:** Export only selected TAURI variables to Vite ([d35d447](https://gitlab.com/biomedit/sett-rs/commit/d35d447584453b22c2574e7b349c999df10a7915))
- **destination:** Fix "attempted to assign to readonly property" ([224bd7b](https://gitlab.com/biomedit/sett-rs/commit/224bd7b93da715dfbbd1dbe8ea0284e7c7f44d03))
- Improve the top-level UI layout ([34f67b9](https://gitlab.com/biomedit/sett-rs/commit/34f67b9611629c260cdeb39187a7ad921fe1855a))
- Show correct job names in notifications ([53d3b68](https://gitlab.com/biomedit/sett-rs/commit/53d3b68d89864c7e8040da2aff72e0ca774a100d))

### 🧱 Build system and dependencies

- **windows:** Use NSIS installer ([ae3c4b0](https://gitlab.com/biomedit/sett-rs/commit/ae3c4b07ef2d97b06874913f763a2e259115e877)), Close #168
- **windows:** Use crypto-cng backend ([5812a8a](https://gitlab.com/biomedit/sett-rs/commit/5812a8a0e52244a070364868d127dbf3a226ad1c))
- Add CHANGELOG ([7563cc1](https://gitlab.com/biomedit/sett-rs/commit/7563cc12239080bce389583826cb644ab5826e87))
- Bump MSRV to 1.70 ([94705b8](https://gitlab.com/biomedit/sett-rs/commit/94705b818bba589df4796a586acfdeab5cc9e22e))
- Fix minimal dependency versions ([2048c8c](https://gitlab.com/biomedit/sett-rs/commit/2048c8cb0f56a86a0c3fed88053090be78e0957a)), Close #131
- Update frontend dependencies ([383c605](https://gitlab.com/biomedit/sett-rs/commit/383c605cc55d236928f6c4eb0626582b6923038d))
- Update Vite to v5 ([8f14bf1](https://gitlab.com/biomedit/sett-rs/commit/8f14bf10d7e64c2ca28156ba89d3ebc138773b20))
- Add 'eslin:fix' script shortcut to package.json ([452b4fd](https://gitlab.com/biomedit/sett-rs/commit/452b4fd00d6bbbf06c7ed5f75a5031417c61aa7d))
- Remove duplicate app version ([03c837c](https://gitlab.com/biomedit/sett-rs/commit/03c837cc8ec70f8f54a2d4e27fc6d63a17c68656))
- **lint:** Add `rules` to linter ([50296b2](https://gitlab.com/biomedit/sett-rs/commit/50296b23a0b5cffe0b7bf664d9b7b906b3dfa865))
- **postcss:** Use postcss-preset-env ([4307e81](https://gitlab.com/biomedit/sett-rs/commit/4307e815fe2c89c0885b340933fd5e616bc2cd21))
- **sett-gui:** Pin exact dependency versions in package.json ([c132032](https://gitlab.com/biomedit/sett-rs/commit/c13203240353e854631af23f4264695aaedd3dff))

### 👷 CI

- **prettier:** Ignore CHANGELOG ([cb92883](https://gitlab.com/biomedit/sett-rs/commit/cb9288344bec8441fcbceea23daeccd3743789eb))

### 📝 Documentation

- Update dev installation instructions ([3317566](https://gitlab.com/biomedit/sett-rs/commit/33175660144dd1f13c756c4c6b5c3fee1887a046))
- Complete the authors list with additional contributors ([b884770](https://gitlab.com/biomedit/sett-rs/commit/b8847704b710c56462517a6d2a978c4d4bf11b84))

### 🧹 Refactoring

- Change the terms certificate to key and secret to private ([c77fed5](https://gitlab.com/biomedit/sett-rs/commit/c77fed50f5e4a1f9f0d0cc73c5e0fc918429e458)), Closes #156
- Style for radio and checkboxes ([560b051](https://gitlab.com/biomedit/sett-rs/commit/560b0511fe26d45296a3baba01e37d9fef154e22))
- Improve consistency ([22ac645](https://gitlab.com/biomedit/sett-rs/commit/22ac6457f2321838bd4cf4baf8db8c61b1e7d897))
- Improve button consitency ([1dd8345](https://gitlab.com/biomedit/sett-rs/commit/1dd83458f9399b7318d62465652547314c2e86f2))
- **certificates tab:** Change status icons to solid icons ([34ca101](https://gitlab.com/biomedit/sett-rs/commit/34ca1015e23954e9c9cae64065abb7ebc6f44a63))
- Use `sett::dpgk::Package` for data package verification ([298ce2d](https://gitlab.com/biomedit/sett-rs/commit/298ce2d8a418a2ea6b26e0abf1aa46816833fc82))
- Replace button with Button component in Cert component ([d6e4bbf](https://gitlab.com/biomedit/sett-rs/commit/d6e4bbfc4157b83a3b91742d8fdc3ddb5ab7cd18))
- Add drag-and-drop component ([6f2fecc](https://gitlab.com/biomedit/sett-rs/commit/6f2fecc07ec48b4f01c1858c521e80c61169a20b)), Closes #99
- **backend:** Exclude public certs that have a private counterpart ([1fc6ea5](https://gitlab.com/biomedit/sett-rs/commit/1fc6ea545cf1d0ca3568df55033fcbce8d6fcc4f))
- Set `height` for `ApprovalIcon` ([14240f1](https://gitlab.com/biomedit/sett-rs/commit/14240f1e37f7ed0fce6ed7510f4e2b7af2a7c687))
- Define a generic and default class for badges ([601989e](https://gitlab.com/biomedit/sett-rs/commit/601989e26eb3688632bc87ace4c1fb17b5ebdec7))
- Make success and error colors brighter in dark mode ([aadb9c7](https://gitlab.com/biomedit/sett-rs/commit/aadb9c782ed73fef84ea512a0401904e8a9fa9ca))
- Get rid of section numbering and make section headers a bit bolder ([983a7a7](https://gitlab.com/biomedit/sett-rs/commit/983a7a7ff8b44b4a5aedf91651784bdc658b12ce))
- Apply some UI improvements to import dialog window ([1a2eb5f](https://gitlab.com/biomedit/sett-rs/commit/1a2eb5f1b42d5ff0df736b59aaeccc20729ecc94))
- Add `direction` to dropdown ([b0525f6](https://gitlab.com/biomedit/sett-rs/commit/b0525f6406ac832f8ff8d7e4596eb70dd40bd01d))
- Keep the gap between the buttons the same (`0.5rem`) ([34ea299](https://gitlab.com/biomedit/sett-rs/commit/34ea299b307538ae50998bfe89f978e56331708d))
- Making all the `small` buttons the same height ([cac541c](https://gitlab.com/biomedit/sett-rs/commit/cac541c3b7dea9951c41ad2640e59d2edae5aeb2))
- Remove `certificate(s)` from button labels ([518db7b](https://gitlab.com/biomedit/sett-rs/commit/518db7bcc76505d20126ee43a64907c1de961998))
- Create component for SecretInput ([eed5e0d](https://gitlab.com/biomedit/sett-rs/commit/eed5e0d969ddf152908c7219c85c5cd23a613cd0))
- Use CopyToClipboard component accross entire app ([8f5382e](https://gitlab.com/biomedit/sett-rs/commit/8f5382ece088649c9f96285f320e0b5357d2b8e0)), Closes #124
- Use custom error type ([9025f5f](https://gitlab.com/biomedit/sett-rs/commit/9025f5f14a1b8d094435a6a87c4cd05381ca28a2)), Close #123
- Move copy-to-clipboard functionality to its own component ([5a19c1e](https://gitlab.com/biomedit/sett-rs/commit/5a19c1e6be5d7494993cf7058ac014f3eae9b7bc))
- Add copy-to-clipboard icons to Button component ([a4d1672](https://gitlab.com/biomedit/sett-rs/commit/a4d16726fd229710ca0f09f1ede7f97645658ef2))
- **gnupug:** Use CertType instead of String ([40537bb](https://gitlab.com/biomedit/sett-rs/commit/40537bbd5a4e3d669daa1a521a2bc66a19deace5))
- **destination:** Make all the labels bold ([f514fba](https://gitlab.com/biomedit/sett-rs/commit/f514fbac8bfbfaee91e162121f15fd774f066c0f))
- Improve responsiveness for `Destination` panel ([952ce40](https://gitlab.com/biomedit/sett-rs/commit/952ce40a2827c3a033afe0cf87f691a090ea713b))
- Remove `secure TLS connection` input field ([b5e55cd](https://gitlab.com/biomedit/sett-rs/commit/b5e55cd1990221c3821509c114a915831aa23d75))
- Create a dedicated password dialog component ([e319ed4](https://gitlab.com/biomedit/sett-rs/commit/e319ed4a1d70281c2ecefda4aa73d181029b8286))
- Make `selectPackage` and `processOpts` available to other widgets/contexts ([ec765b5](https://gitlab.com/biomedit/sett-rs/commit/ec765b52f3dfda3a782bb7ca5ab52c46f9435642))
- Use `CloudArrowUp` as transfer icon ([01a502d](https://gitlab.com/biomedit/sett-rs/commit/01a502d54bc9a3d036f8581430ddbab2cce1d465))
