import { type Component } from 'svelte';

export type TableRow = {
  key: string;
  value: string;
  tooltip?: string;
};

export type DropZoneButton = {
  Icon: Component;
  text: string;
  color: 'primary' | 'secondary' | 'transparent';
  size: 'large' | 'medium' | 'small' | 'x-small';
  selectItem: () => void;
  tooltip?: string;
};

// Location where tooltip gets displayed.
export type TooltipPosition = 'bottom' | 'right' | 'top';

// Direction of tooltip text.
export type TooltipDirection = 'center' | 'to-left' | 'to-right';
