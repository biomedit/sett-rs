import { homeDir } from '@tauri-apps/api/path';
import { invoke } from '@tauri-apps/api';
import { Store } from 'tauri-plugin-store-api';
import { writable } from 'svelte/store';

import { removeTrailingPathSep } from '../page/utils';

export const lastSelectedDir = writable<string>('');

export type Settings = {
  defaultOutputDir: string;
  portalURL: string;
  oidcIssuerURL: string;
  keyserverURL: string;
  verifyPackage: boolean;
  logDir: string;
  enableExtraMetadata: boolean;
  publicKeyStore: string;
  privateKeyStore: string;
};

const applicationStore = new Store('.settings.dat');

function initialSettings(): Settings {
  return {
    defaultOutputDir: '',
    portalURL: 'https://portal.dcc.sib.swiss',
    oidcIssuerURL: 'https://login.biomedit.ch/realms/biomedit',
    keyserverURL: 'https://keys.openpgp.org',
    verifyPackage: true,
    logDir: '',
    enableExtraMetadata: false,
    publicKeyStore: '',
    privateKeyStore: '',
  };
}

async function defaultSettings(): Promise<Settings> {
  let publicKeyStore = '';
  try {
    publicKeyStore = await invoke('get_default_certstore_path');
  } catch (e) {
    throw new Error(`Failed to load public key store path: ${e}`);
  }
  let privateKeyStore = '';
  try {
    privateKeyStore = await invoke('get_default_keystore_path');
  } catch (e) {
    throw new Error(`Failed to load private key store path: ${e}`);
  }
  return {
    ...initialSettings(),
    defaultOutputDir: removeTrailingPathSep(await homeDir()),
    logDir: await invoke('log_dir'),
    publicKeyStore,
    privateKeyStore,
  };
}

function createSettingsStore() {
  const { subscribe, set } = writable<Settings>(initialSettings());

  async function load() {
    set(
      await Promise.all([
        defaultSettings(),
        applicationStore.get<Settings>('settings'),
      ]).then(([defaults, fromStore]) => {
        return { ...defaults, ...fromStore };
      }),
    );
  }

  async function save(settings: Settings) {
    await applicationStore.set('settings', settings);
    await applicationStore.save();
    set(settings);
  }

  async function reset() {
    save(await defaultSettings());
  }

  return {
    subscribe,
    load,
    set: save,
    reset,
  };
}

export const settingsStore = createSettingsStore();
export const appName = 'sett';
