import { get, writable } from 'svelte/store';
import { invoke } from '@tauri-apps/api';

import { userInfo, userInfoAsString } from './auth';
import { settingsStore } from './settings';
import { type TableRow } from '../component/utils';

// Possible statuses for a data transfer request on Portal.
export enum DataTransferStatus {
  Initial = 'Initial',
  Authorized = 'Authorized',
  Unauthorized = 'Unauthorized',
  Expired = 'Expired',
}

// Metadata of a Data Transfer registered on Portal.
export interface DataTransfer {
  id: number;
  data_provider_name: string;
  project_name: string;
  status: DataTransferStatus;
}

// Svelte store for Data Transfer associated with a logged-in user.
function createDataTransferStore() {
  const { subscribe, set } = writable<DataTransfer[]>([]);

  async function refresh() {
    const portalUrl = get(settingsStore).portalURL;
    try {
      const dataTransfers = await invoke<DataTransfer[]>(
        'fetch_data_transfers',
        { portalUrl: portalUrl },
      );
      set(dataTransfers);
    } catch (e) {
      set([]);
      const user = get(userInfo);
      throw new Error(
        `unable to retrieve the list of data transfers associated with user
        ${userInfoAsString(user)} from '${portalUrl}'.\n\n
        Please try to sign-out/sign-in to the service. Alternatively, this
        error might be caused by a temporary downtime of '${portalUrl}'.
        Original error message: ${e as string}`,
      );
    }
  }

  return {
    subscribe,
    refresh,
  };
}

export const dataTransferStore = createDataTransferStore();

// Returns the attributes of a `DataTransfer` as data table.
export function dataTransferAsTable(dataTransfer: DataTransfer): TableRow[] {
  return [
    {
      key: 'Transfer ID',
      value: dataTransfer.id.toString(),
    },
    {
      key: 'Project name',
      value: dataTransfer.project_name,
    },
    {
      key: 'Data provider',
      value: dataTransfer.data_provider_name,
    },
    {
      key: 'Status',
      value: dataTransfer.status,
    },
  ];
}
