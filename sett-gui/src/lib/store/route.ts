import { writable } from 'svelte/store';

export enum Route {
  About = 'about',
  Certs = 'certs',
  Decrypt = 'decrypt',
  Profile = 'profile',
  Settings = 'settings',
  Tasks = 'tasks',
  Encrypt = 'encrypt',
}

export const route = writable(Route.Encrypt);
