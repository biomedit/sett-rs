import { derived, writable } from 'svelte/store';
import { homeDir } from '@tauri-apps/api/path';
import { open } from '@tauri-apps/api/dialog';

import { createLocalPackageStore, type Local } from './source';
import {
  createLocalProps,
  createS3Props,
  createSftpProps,
  type Destination,
  type DestinationKind,
} from './destination';
import type { CertInfo } from './cert';
import { invoke } from '@tauri-apps/api';
import type { Package } from './package';

// This tab can handle both "Encrypt and Transfer" and "Transfer" workflows.
// This variable tracks the mode that the tab is currently in.
export enum TabMode {
  Initial, // Default mode, before any file is added.
  Encrypt, // Entered when a user selects a regular file/directory.
  Transfer, // Entered when a user selects a sett data package.
}

export interface EncryptOpts {
  kind: 'encrypt';
  files: string[];
  signer: string;
  recipients: string[];
  transferId: number | null;
  destination: Destination | null;
  verifyPackage: boolean;
  portalUrl: string;
  extraMetadata: Map<string, string>;
}

export interface TransferOpts {
  kind: 'transfer';
  destination: Destination;
  package: Package<Local>;
  verifyPackage: boolean;
  portalUrl: string;
}

export type FilePath = {
  path: string;
  directory: boolean;
};

function createPathsStore() {
  const { subscribe, update } = writable<FilePath[]>([]);

  function add(newFiles: FilePath[]) {
    update((files) => {
      const newPaths = [
        ...files,
        ...newFiles.filter((file) => !files.some((f) => f.path === file.path)),
      ];
      newPaths.sort(
        (a, b) =>
          // Directories first, then sort by path
          Number(b.directory) - Number(a.directory) ||
          a.path.toLowerCase().localeCompare(b.path.toLowerCase()),
      );
      return newPaths;
    });
  }

  function remove(path: string) {
    update((files) => files.filter((file) => file.path !== path));
  }

  function clear() {
    update(() => []);
  }

  return {
    subscribe,
    add,
    remove,
    clear,
  };
}

function createRecipientsStore() {
  const { subscribe, update } = writable<CertInfo[]>([]);

  function add(recipient: CertInfo) {
    update((s) => [...new Set([...s, recipient])]);
  }

  function remove(recipient: CertInfo) {
    update((s) => s.filter((r) => r.fingerprint !== recipient.fingerprint));
  }

  function clear() {
    update(() => []);
  }

  return {
    subscribe,
    add,
    remove,
    clear,
  };
}

function createExtraMetadataStore() {
  const { subscribe, update } = writable<Map<string, string>>(new Map());

  function add(key: string, value: string) {
    update((s) => s.set(key, value));
  }

  function remove(key: string) {
    update((s) => {
      s.delete(key);
      return s;
    });
  }

  function clear() {
    update(() => new Map());
  }

  return {
    subscribe,
    add,
    remove,
    clear,
  };
}

// Note: we are using more granular stores here to limit the unwanted
// reactivity caused by updating a larger store object.
export const paths = createPathsStore();
export const fileStats = derived(paths, async ($paths) =>
  invoke<{ size: number; count: number }>('stats', {
    paths: $paths.map((p) => p.path),
  }),
);
export const signer = writable<CertInfo | null>(null);
export const recipients = createRecipientsStore();
export const transferId = writable<number | null>(null);
export const destinationKind = writable<DestinationKind>('local');
export const extraMetadata = createExtraMetadataStore();
export const transferPackageStore = createLocalPackageStore();

export async function selectPaths(directory: boolean) {
  const selected = await open({
    multiple: true,
    directory,
    defaultPath: await homeDir(),
  });
  if (selected !== null && Array.isArray(selected)) {
    paths.add(selected.map((path) => ({ path, directory }) as FilePath));
  }
}

export const destinationLocalProps = createLocalProps();
export const destinationS3Props = createS3Props();
export const destinationSftpProps = createSftpProps();
