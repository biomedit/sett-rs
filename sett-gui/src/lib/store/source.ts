import { get, writable } from 'svelte/store';
import { settingsStore } from './settings';

import {
  loadMetadata,
  type Package,
  type PackageWithoutMetadata,
} from './package';

export enum SourceKind {
  Local = 'local',
  S3 = 's3',
  S3Portal = 's3Portal',
}

export interface Local {
  kind: SourceKind.Local;
  path: string;
}

export interface S3 {
  kind: SourceKind.S3;
  url: string;
  bucket: string;
  objectName: string;
  accessKey: string;
  secretKey: string;
  sessionToken: string;
}
export interface S3Portal {
  kind: SourceKind.S3Portal;
  objectName: string;
  transferId: number;
  portalUrl: string;
}

export type PackageSource = Local | S3 | S3Portal;

export function sourceLocation(source: PackageSource) {
  // depending on the PackageSource returns a path
  if (source.kind === 'local') {
    return source.path;
  }
  if (source.kind === 's3Portal') {
    return source.objectName;
  }
  return `${source.url}/${source.bucket}/${source.objectName}`;
}

export function sourcePackageName(source: PackageSource) {
  if (source.kind === 'local') {
    return (
      (source.path.split('\\').pop() || source.path).split('/').pop() ||
      source.path
    );
  }
  return source.objectName;
}

// stores the source and metadata (if available) of a local sett package.
export function createLocalPackageStore() {
  const defaultPackage = (): PackageWithoutMetadata<Local> => ({
    source: { kind: SourceKind.Local, path: '' },
  });
  const { subscribe, set, update } = writable<
    Package<Local> | PackageWithoutMetadata<Local>
  >(defaultPackage());
  async function load(path: string) {
    const metadata = await loadMetadata({ kind: SourceKind.Local, path });
    set(
      metadata
        ? {
            source: { kind: SourceKind.Local, path },
            metadata,
          }
        : { source: { kind: SourceKind.Local, path } },
    );
  }
  return {
    subscribe,
    set,
    update,
    load,
    reset: () => set(defaultPackage()),
  };
}

// stores the source and metadata (if available) of a sett package on s3
export function createS3PackageStore() {
  const defaultPackage = (): PackageWithoutMetadata<S3> => ({
    source: {
      kind: SourceKind.S3,
      url: '',
      bucket: '',
      objectName: '',
      accessKey: '',
      secretKey: '',
      sessionToken: '',
    },
  });
  const { subscribe, set, update } = writable<
    Package<S3> | PackageWithoutMetadata<S3>
  >(defaultPackage());
  async function load(source: S3) {
    const metadata = await loadMetadata(source);
    set(
      metadata
        ? {
            source,
            metadata,
          }
        : { source },
    );
  }
  return {
    subscribe,
    set,
    update,
    load,
    reset: () => set(defaultPackage()),
  };
}

// Stores the source and metadata (if available) of a sett package on s3.
// Fallsback to the default portal URL in settingsStore if it was not
// provided.
export function createS3PortalPackageStore(portalUrl?: string) {
  const defaultPortalUrl = portalUrl || get(settingsStore).portalURL;
  const defaultPackage = (): PackageWithoutMetadata<S3Portal> => ({
    source: {
      kind: SourceKind.S3Portal,
      transferId: 0,
      objectName: '',
      portalUrl: defaultPortalUrl,
    },
  });
  const { subscribe, set, update } = writable<
    Package<S3Portal> | PackageWithoutMetadata<S3Portal>
  >(defaultPackage());
  async function load(source: S3Portal) {
    const metadata = await loadMetadata(source);
    set(
      metadata
        ? {
            source,
            metadata,
          }
        : { source },
    );
  }
  return {
    subscribe,
    set,
    update,
    load,
    reset: () => set(defaultPackage()),
  };
}
