import { writable, type Writable } from 'svelte/store';

// Arguments for S3 connections in unauthenticated mode: authentication
// credentials must be explicitly given.
export type S3Props = {
  url: string;
  bucket: string;
  accessKey: string;
  secretKey: string;
  sessionToken?: string;
};

// Arguments for S3 connections in authenticated mode: only the transfer Id
// value is passed, the actual credentials are auto-retrieved.
export type S3RemoteProps = {
  transferId: number;
};

export type SftpProps = {
  username: string;
  host: string;
  basePath: string;
  sshKey: SSHKey;
};

type SSHKey = {
  location: string;
  password: string;
};

export type LocalProps = string;
export type DestinationProps = LocalProps | S3Props | S3RemoteProps | SftpProps;
export type DestinationKind = 'local' | 's3' | 'sftp';
export type Destination = {
  kind: DestinationKind;
  props: DestinationProps;
};
export type ClearProps = { clear: () => void };

const clear =
  <T extends S3Props | SftpProps>(update: CallableFunction, empty: T) =>
  () =>
    update((s: T) => ({
      ...s,
      ...empty,
    }));

export const createS3Props = (): ClearProps & Writable<S3Props> => {
  const createEmpty = () => ({
    url: '',
    bucket: '',
    accessKey: '',
    secretKey: '',
    sessionToken: '',
  });
  const { subscribe, set, update } = writable<S3Props>(createEmpty());

  return {
    subscribe,
    set,
    update,
    clear: clear(update, createEmpty()),
  };
};

export const createSftpProps = (): ClearProps & Writable<SftpProps> => {
  const createEmpty = () => ({
    username: '',
    host: '',
    basePath: '',
    sshKey: {
      location: '',
      password: '',
    },
  });
  const { subscribe, set, update } = writable<SftpProps>(createEmpty());
  return {
    subscribe,
    update,
    set,
    clear: clear(update, createEmpty()),
  };
};

export const createLocalProps = (): ClearProps & Writable<LocalProps> => {
  const { subscribe, set, update } = writable<LocalProps>('');
  return {
    subscribe,
    update,
    set,
    clear: () => set(''),
  };
};
