import { writable } from 'svelte/store';

import {
  createLocalPackageStore,
  createS3PackageStore,
  type Local,
  type PackageSource,
  type S3,
  type S3Portal,
  SourceKind,
} from './source';
import { createLocalProps } from './destination';
import { type Package } from './package';

export interface DecryptOpts<S extends PackageSource = Local | S3 | S3Portal> {
  kind: 'decrypt';
  destination: string;
  package: Package<S>;
}

export const decryptSourceKind = writable<SourceKind>(SourceKind.Local);
export const decryptLocalPackage = createLocalPackageStore();
export const decryptS3Package = createS3PackageStore();
export const decryptDestination = createLocalProps();
