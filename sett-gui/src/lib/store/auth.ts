import { writable } from 'svelte/store';

export interface UserInfo {
  familyName?: string;
  givenName?: string;
  username?: string;
  email?: string;
}

export const userInfo = writable<UserInfo | null>(null);
export const authenticatedMode = writable(false);

export function userInfoAsString(user: UserInfo | null) {
  if (user) {
    return `${user.email || 'missing email'} (${user.username || 'missing username'})`;
  } else {
    return 'no user info';
  }
}
