import { get } from 'svelte/store';
import { invoke } from '@tauri-apps/api/tauri';

import { NotificationLevel, toasts } from './event';
import { type PackageSource, SourceKind, sourcePackageName } from './source';
import { settingsStore } from './settings';

export interface Metadata {
  recipients: string[];
  sender: string;
  timestamp: string;
  transfer_id: number | null;
  extra: { [key: string]: string };
}

export interface PackageWithoutMetadata<S extends PackageSource> {
  source: S;
}

export interface Package<S extends PackageSource>
  extends PackageWithoutMetadata<S> {
  metadata: Metadata;
}

export function hasMetadata<S extends PackageSource>(
  pkg: Package<S> | PackageWithoutMetadata<S>,
): pkg is Package<S> {
  return 'metadata' in pkg;
}

export async function isSettPackage(path: string) {
  if (!path.endsWith('.zip')) {
    return false;
  }
  const settings = get(settingsStore);
  try {
    await invoke('read_pkg_metadata', {
      store: {
        public: settings.publicKeyStore,
        private: settings.privateKeyStore,
      },
      source: { kind: SourceKind.Local, path },
    });
    return true;
  } catch {
    return false;
  }
}

export async function loadMetadata(
  source: PackageSource,
): Promise<Metadata | null> {
  const settings = get(settingsStore);
  try {
    return await invoke('read_pkg_metadata', {
      store: {
        public: settings.publicKeyStore,
        private: settings.privateKeyStore,
      },
      source,
    });
  } catch (e) {
    toasts.add({
      level: NotificationLevel.Error,
      message: `Failed to load data package (${sourcePackageName(
        source,
      )}): ${e}`,
      dismissible: true,
    });
    return null;
  }
}
