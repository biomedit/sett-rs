import { type Event, listen } from '@tauri-apps/api/event';
import { get } from 'svelte/store';
import { invoke } from '@tauri-apps/api';

import { authenticatedMode, userInfo } from './store/auth';
import { decryptLocalPackage, decryptSourceKind } from './store/decrypt';
import { fileDragStatus, NotificationLevel, toasts } from './store/event';
import { type FilePath, paths } from './store/encrypt';
import { Route, route } from './store/route';
import { runNextTask, type Task, tasks, TaskState } from './store/task';
import { isSettPackage } from './store/package';
import { SourceKind } from './store/source';
import { transferPackageStore } from './store/encrypt';

const NOTIFICATION_TIMEOUT = 10000;

async function detectPayload(payload: string[]) {
  const filePaths = await Promise.all(
    payload.map(async (path) => {
      const directory: boolean = await invoke('is_dir', {
        path: payload[0],
      });
      return { path, directory } as FilePath;
    }),
  );
  return filePaths;
}

// Adds files or directory paths to the list of file paths to encrypt/transfer.
// Clears any data package input that might be present.
async function addFiles(files: string[]) {
  transferPackageStore.reset();
  paths.add(await detectPayload(files));
}

// Adds files if they don't contain any sett packages.
async function addFilesIfNotSettPackages(files: string[]) {
  const is_package = await Promise.all(files.map(isSettPackage));
  const packages = files.filter((_, i) => is_package[i]);
  if (packages.length === 0) {
    await addFiles(files);
  } else if (packages.length === files.length) {
    toasts.add({
      level: NotificationLevel.Warning,
      message: 'Only one sett package can be added at a time',
      timeout: NOTIFICATION_TIMEOUT,
      dismissible: true,
    });
  } else {
    toasts.add({
      level: NotificationLevel.Warning,
      message: 'Regular files/directories and sett packages cannot be mixed',
      timeout: NOTIFICATION_TIMEOUT,
      dismissible: true,
    });
  }
}

export async function listenFileDrop() {
  await listen('tauri://file-drop', async (event: Event<[string]>) => {
    const currentRoute = get(route);
    fileDragStatus.set(false);
    switch (currentRoute) {
      case Route.Encrypt: {
        const files = event.payload;
        if (files.length > 1) {
          await addFilesIfNotSettPackages(files);
        } else if (files.length == 1) {
          const maybePackage = files[0];
          if (await isSettPackage(maybePackage)) {
            paths.clear();
            transferPackageStore.load(maybePackage);
          } else {
            await addFiles(files);
          }
        }
        break;
      }
      case Route.Decrypt: {
        if (get(decryptSourceKind) === SourceKind.Local) {
          event.payload
            .filter((f) => !f.endsWith('.zip'))
            .forEach((f) => {
              toasts.add({
                level: NotificationLevel.Error,
                message: `File ${
                  (f.split('\\').pop() || f).split('/').pop() || f
                } is not a sett data package (ZIP file)`,
                timeout: NOTIFICATION_TIMEOUT,
                dismissible: true,
              });
            });
          const files = event.payload.filter((f) => f.endsWith('.zip'));
          decryptLocalPackage.load(files[0]);
          if (files.length > 1) {
            toasts.add({
              level: NotificationLevel.Warning,
              message: `Adding multiple packages not allowed: only the first one was kept.`,
              timeout: NOTIFICATION_TIMEOUT,
              dismissible: true,
            });
          }
        }
        break;
      }
      case Route.About:
      case Route.Certs:
      case Route.Profile:
      case Route.Settings:
      case Route.Tasks:
        break;
    }
  });
}

type TaskStatusType = 'error' | 'finished' | 'running';

interface TaskEvent {
  taskId: number;
  status: {
    type: TaskStatusType;
    message?: string;
    progress?: number;
    destination?: string;
  };
}

export async function listenTaskEvent() {
  await listen('task', (event: Event<TaskEvent>) => {
    const taskKindToName = {
      encrypt: 'Encryption',
      decrypt: 'Decryption',
      transfer: 'Transfer',
    };
    const statusToName = {
      running: 'running',
      finished: 'finished',
      error: 'failed',
    };
    const statusToLevel = {
      running: NotificationLevel.Info,
      finished: NotificationLevel.Success,
      error: NotificationLevel.Error,
    };

    const addToast = (task: Task, status: TaskStatusType) => {
      toasts.add({
        level: statusToLevel[status],
        message: `${taskKindToName[task.opts.kind]} job ${
          statusToName[status]
        }`,
        timeout: NOTIFICATION_TIMEOUT,
        dismissible: true,
      });
    };

    const task = event.payload;
    switch (task.status.type) {
      case 'running':
        tasks.update((current) => {
          const idx = current.findIndex((t) => t.id === task.taskId);
          current[idx] = {
            ...current[idx],
            updated: new Date(),
            progress: task.status.progress!,
            state: TaskState.Running,
          };
          return current;
        });
        break;
      case 'finished':
        tasks.update((current) => {
          const idx = current.findIndex((t) => t.id === task.taskId);
          current[idx] = {
            ...current[idx],
            updated: new Date(),
            progress: 100,
            state: TaskState.Finished,
            destination: task.status.destination!,
          };
          addToast(current[idx], task.status.type);
          return current;
        });
        runNextTask();
        break;
      case 'error':
        tasks.update((current) => {
          const idx = current.findIndex((t) => t.id === task.taskId);
          current[idx] = {
            ...current[idx],
            updated: new Date(),
            state: TaskState.Error,
            message: task.status.message,
          };
          addToast(current[idx], task.status.type);
          return current;
        });
        runNextTask();
        break;
    }
  });
}

export function listenThemeChange() {
  const theme = window.matchMedia('(prefers-color-scheme: dark)');
  if (theme.matches) {
    document.documentElement.setAttribute('data-theme', 'dark');
  }
  const toggle = (e: MediaQueryListEvent) => {
    if (e.matches) {
      document.documentElement.setAttribute('data-theme', 'dark');
    } else {
      document.documentElement.removeAttribute('data-theme');
    }
  };
  theme.addEventListener('change', toggle);
}

export function listenUnhandledRejection() {
  window.onunhandledrejection = (event) => {
    toasts.add({
      level: NotificationLevel.Error,
      message: `${event.reason}`,
      dismissible: true,
    });
  };
}

type AuthError = {
  state: 'logout';
  details: string;
};

export async function listenAuthError() {
  await listen('auth-error', (event: Event<AuthError>) => {
    if (event.payload.state === 'logout') {
      userInfo.set(null);
      authenticatedMode.set(false);
    }
    toasts.add({
      level: NotificationLevel.Error,
      message: `${event.payload.details}`,
      dismissible: true,
    });
  });
}
