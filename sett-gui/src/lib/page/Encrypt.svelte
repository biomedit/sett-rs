<script lang="ts">
  import { authenticatedMode, userInfo, userInfoAsString } from '../store/auth';
  import { type CertInfo, certStore, CertType } from '../store/cert';
  import {
    type DataTransfer,
    dataTransferAsTable,
    DataTransferStatus,
    dataTransferStore,
  } from '../store/transfer';
  import {
    destinationKind,
    destinationLocalProps,
    destinationS3Props,
    destinationSftpProps,
    type EncryptOpts,
    extraMetadata,
    fileStats,
    paths,
    recipients,
    selectPaths,
    signer,
    TabMode,
    transferId,
    type TransferOpts,
    transferPackageStore,
  } from '../store/encrypt';
  import {
    hasMetadata,
    type Package,
    type PackageWithoutMetadata,
  } from '../store/package';
  import {
    isCertExpiring,
    pkgMetadataAsTable,
    selectPackage,
    toHumanReadableSize,
    warnCertExpiring,
  } from './utils';
  import { NotificationLevel, toasts } from '../store/event';
  import { type DestinationProps } from '../store/destination';
  import { type DropZoneButton } from '../component/utils';
  import { setContext } from 'svelte';
  import { settingsStore } from '../store/settings';
  import { tasks } from '../store/task';

  import Alert from '../component/Alert.svelte';
  import AuthViewToggle from '../component/AuthViewToggle.svelte';
  import ButtonWorkflow from '../component/ButtonWorkflow.svelte';
  import Chip from '../component/Chip.svelte';
  import Destination from '../component/Destination.svelte';
  import DocumentPlus from '../icon/DocumentPlus.svelte';
  import DropZone from '../component/DropZone.svelte';
  import ExtraMetadata from '../component/ExtraMetadata.svelte';
  import FolderPlus from '../icon/FolderPlus.svelte';
  import Lock from '../icon/Lock.svelte';
  import type { PackageSource } from '../store/source';
  import Page from '../component/Page.svelte';
  import QuickSearch from '../component/QuickSearch.svelte';
  import Section from '../component/Section.svelte';
  import Table from '../component/Table.svelte';
  import TransferIdSelector from '../component/TransferIdSelector.svelte';

  let submitDisabled = $state(false);

  // This tab can handle both "Encrypt and Transfer" and "Transfer" workflows.
  // This variable tracks the mode that the tab is currently in.
  const tabMode = $derived(
    $paths.length > 0
      ? TabMode.Encrypt
      : hasMetadata($transferPackageStore)
        ? TabMode.Transfer
        : TabMode.Initial,
  );
  // Package verification is mandatory in authenticated mode.
  const verifyPackage = $derived(
    $authenticatedMode || $settingsStore.verifyPackage,
  );

  // Widgets used only in encrypt mode. The widgets are shown one after the
  // other, except "Extra metadata" that can be shown at the same time than
  // "Data Transfer ID" when the latter is optional.
  const showSender = $derived(tabMode === TabMode.Encrypt);
  const showRecipients = $derived(showSender && $signer !== null);
  const showDataTransferId = $derived(showRecipients && $recipients.length > 0);
  // Indicates whether the user has either entered a data transfer ID, or
  // if this argument is not mandatory (when "Verify package" is off).
  const transferIdProvided = $derived(
    showDataTransferId && ($transferId !== null || !verifyPackage),
  );
  const showExtraMetadata = $derived(
    $settingsStore.enableExtraMetadata && transferIdProvided,
  );
  const dataTransfer = $derived(
    getDataTransferFromPackage($transferPackageStore, $dataTransferStore),
  );

  // Widgets used only in transfer mode.
  // In transfer mode, an error about unauthorized or missing DTR ID is shown
  // when either:
  // a) The transfer has not been authorized (authenticated mode).
  // b) "Verify packages" is enabled, but package has no transfer_id
  //    (non-authenticated mode)
  const showDtrError = $derived(
    tabMode === TabMode.Transfer && hasMetadata($transferPackageStore)
      ? $authenticatedMode
        ? dataTransfer?.status !== DataTransferStatus.Authorized
        : $settingsStore.verifyPackage &&
          $transferPackageStore.metadata.transfer_id === null
      : false,
  );

  // Widgets used in both encrypt and transfer modes.
  // Only show the destination selector when either:
  // * Tab is in Encrypt mode, and a DTR ID was provided.
  // * Tab is in Transfer mode, and no problem with DTR ID was detected.
  const showDestination = $derived(
    tabMode === TabMode.Transfer ? !showDtrError : transferIdProvided,
  );

  // In transfer mode, the selected destination should never be "local".
  $effect(() => {
    if (tabMode === TabMode.Transfer && $destinationKind === 'local') {
      $destinationKind = 's3';
    }
  });

  // Button to launch the main workflow. When destination is "local", it only
  // gets shown if the user selected an output directory.
  const showWorkflowButton = $derived(
    showDestination && ($destinationLocalProps || $destinationKind !== 'local'),
  );
  const workflowDescription = $derived(
    tabMode === TabMode.Encrypt
      ? `Encrypt${$destinationKind !== 'local' ? ' and transfer' : ''}`
      : 'Transfer',
  );

  function getDataTransferFromPackage<S extends PackageSource>(
    pkg: Package<S> | PackageWithoutMetadata<S>,
    transfers: DataTransfer[],
  ) {
    if (!hasMetadata(pkg) || pkg.metadata.transfer_id === null) {
      return null;
    }
    return (
      transfers.find((transfer) => transfer.id === pkg.metadata.transfer_id) ??
      null
    );
  }

  setContext('store', {
    localProps: destinationLocalProps,
    s3Props: destinationS3Props,
    sftpProps: destinationSftpProps,
  });

  async function handleSubmit(event: Event) {
    event.preventDefault();
    // Disable the main button (to launch workflow) again while the workflow
    // is being processed to avoid users submitting twice.
    submitDisabled = true;

    // Set arguments depending on the chosen workflow.
    let destinationProps: DestinationProps;
    switch ($destinationKind) {
      case 's3':
        if ($authenticatedMode) {
          // In authenticated mode, transfer `id` is necessarily set.
          destinationProps = {
            transferId:
              tabMode === TabMode.Encrypt ? $transferId! : dataTransfer!.id,
          };
        } else {
          destinationProps = $destinationS3Props;
        }
        break;
      case 'local':
        // Only reachable in the Encrypt workflow.
        destinationProps = $destinationLocalProps;
        break;
      case 'sftp':
        destinationProps = $destinationSftpProps;
        break;
    }

    const workflowOpts: EncryptOpts | TransferOpts =
      tabMode === TabMode.Transfer && hasMetadata($transferPackageStore)
        ? {
            kind: 'transfer',
            package: $transferPackageStore,
            destination: { kind: $destinationKind, props: destinationProps },
            verifyPackage: $settingsStore.verifyPackage,
            portalUrl: $settingsStore.portalURL,
          }
        : {
            kind: 'encrypt',
            files: $paths.map((p) => p.path),
            signer: $signer!.fingerprint,
            recipients: $recipients.map((r) => r.fingerprint),
            transferId: $transferId,
            destination: { kind: $destinationKind, props: destinationProps },
            verifyPackage,
            portalUrl: $settingsStore.portalURL,
            extraMetadata: $extraMetadata,
          };
    // Dry-run the workflow to check for potential input errors.
    try {
      await tasks.check(workflowOpts);
    } catch (e) {
      toasts.add({
        level: NotificationLevel.Error,
        message: e as string,
        timeout: 5000,
        dismissible: true,
      });
      submitDisabled = false;
      return;
    }

    // Run the workflow.
    tasks.add(workflowOpts);
    toasts.add({
      level: NotificationLevel.Info,
      message: workflowDescription + ' job submitted',
      timeout: 5000,
      dismissible: true,
    });

    const signerCert =
      $signer ??
      (hasMetadata($transferPackageStore)
        ? $certStore.find(
            (cert) =>
              cert.fingerprint === $transferPackageStore.metadata.sender,
          )
        : undefined);
    if (signerCert && isCertExpiring(signerCert)) {
      warnCertExpiring(signerCert);
    }

    // Reset the form.
    paths.clear();
    recipients.clear();
    $signer = null;
    $transferId = null;
    destinationLocalProps.clear();
    destinationS3Props.clear();
    destinationSftpProps.clear();
    extraMetadata.clear();
    transferPackageStore.reset();

    // Re-enable the main button (to launch workflow) again.
    submitDisabled = false;
  }

  // Return the list of certificates in the certStore as a list of objects
  // with a `label` and `value` attribute.
  // Optional arguments:
  // * `certType`: filter the certificates to keep only those matching the
  //   specified certificate type.
  // * `exclude`: filter the certificates to exclude those in the `exclude`
  //   list. This is used to exclude certificates that are already selected.
  function certsAsQuickSearchChoice(
    certStoreContent: CertInfo[],
    certType: CertType | null = null,
    exclude: CertInfo[] = [],
  ) {
    // Note: the content of the store is an argument of the function for
    // reactivity reasons (so that Svelte runs the function after the store
    // is populated).
    return certStoreContent
      .filter(
        (certInfo) =>
          (certType ? certInfo.certType === certType : true) &&
          !exclude.includes(certInfo),
      )
      .map((certInfo) => ({
        label: `${certInfo.userid || ''} (${certInfo.fingerprint})`,
        value: certInfo,
      }));
  }

  function removePath(path?: string) {
    if (path) {
      paths.remove(path);
    }
  }

  // Possible buttons to use in the DropZone component.
  const selectFileButton: DropZoneButton = {
    Icon: DocumentPlus,
    text: 'Files',
    color: 'secondary',
    size: 'small',
    tooltip: 'Select files to encrypt and transfer',
    selectItem: () => selectPaths(false),
  };
  const selectDirButton: DropZoneButton = {
    Icon: FolderPlus,
    text: 'Folders',
    color: 'secondary',
    size: 'small',
    tooltip: 'Select folders to encrypt and transfer',
    selectItem: () => selectPaths(true),
  };
  const selectPackageButton: DropZoneButton = {
    Icon: DocumentPlus,
    text: 'sett Package',
    color: 'secondary',
    size: 'small',
    tooltip: 'Select package to transfer',
    selectItem: () => selectPackage(transferPackageStore.load),
  };

  // Return the correct set of buttons for the DropZone component, depending on
  // the `TabMode` the application is currently in.
  function dropZoneButtonsByMode(mode: TabMode) {
    switch (mode) {
      case TabMode.Initial:
        // Initial state: all options are available and the user can add either
        // regular files/directories, or a data package.
        return [selectFileButton, selectDirButton, selectPackageButton];
      case TabMode.Encrypt:
        // Encrypt mode: only files or directories can be added.
        return [selectFileButton, selectDirButton];
      case TabMode.Transfer:
        // Transfer mode: no additional button is shown, as only 1 package can
        // be selected at a time.
        return [];
    }
  }

  // Return the correct helper text for the DropZone component.
  function dropZoneHelpTextByMode(mode: TabMode) {
    switch (mode) {
      case TabMode.Initial:
        return 'Drag and drop or click the buttons to add files, folders, or an existing data package.';
      case TabMode.Encrypt:
        return 'Drag and drop files or folders, or click the buttons to select one.';
      case TabMode.Transfer:
        return '';
    }
  }
</script>

<form onsubmit={handleSubmit}>
  <Page title="Encrypt and Transfer Data">
    {#snippet headerRight()}
      <div class="toggle">
        <!-- Authenticated mode is only possible when signed-in to portal. -->
        {#if $userInfo !== null}
          <AuthViewToggle bind:checked={$authenticatedMode} />
        {/if}
      </div>
    {/snippet}
    <Section title="Select data to encrypt and/or transfer">
      <DropZone
        buttons={dropZoneButtonsByMode(tabMode)}
        selectedItems={$paths}
        clearItem={tabMode === TabMode.Encrypt
          ? removePath
          : transferPackageStore.reset}
        helpText={dropZoneHelpTextByMode(tabMode)}
        dataAsTable={hasMetadata($transferPackageStore)
          ? pkgMetadataAsTable($transferPackageStore)
          : undefined}
      />
      {#if tabMode === TabMode.Encrypt && $paths.length}
        <div>
          {#await $fileStats}
            Calculating total file size...
          {:then value}
            Total size: {toHumanReadableSize(value.size)} (number of files: {value.count})
          {:catch error}
            Unable to calculate total file size: {error}
          {/await}
        </div>
      {/if}
    </Section>

    {#if tabMode === TabMode.Encrypt}
      <Section title="Select sender" hidden={!showSender}>
        {#if $signer === null}
          <QuickSearch
            items={certsAsQuickSearchChoice($certStore, CertType.Secret)}
            onselected={(selected) => ($signer = selected.value)}
          />
        {:else}
          <Chip
            tooltip={`fingerprint: ${$signer.fingerprint}`}
            onClear={() => ($signer = null)}
          >
            {$signer.userid}
          </Chip>
        {/if}
      </Section>

      <Section title="Select recipients" hidden={!showRecipients}>
        <QuickSearch
          items={certsAsQuickSearchChoice($certStore, null, $recipients)}
          onselected={(selected) => recipients.add(selected.value)}
        />
        {#if $recipients.length > 0}
          <div class="recipients">
            {#each $recipients as recipient (recipient.fingerprint)}
              <Chip
                tooltip={`fingerprint: ${recipient.fingerprint}`}
                onClear={() => recipients.remove(recipient)}
              >
                {recipient.userid}
              </Chip>
            {/each}
          </div>
        {/if}
      </Section>
    {/if}

    <Section
      title={'Data Transfer ID' + (verifyPackage ? '' : ' (optional)')}
      hidden={!showDataTransferId}
    >
      <TransferIdSelector
        authenticatedMode={$authenticatedMode}
        bind:transferId={$transferId}
      />
    </Section>
    <Section title="Data transfer info" hidden={!showDtrError}>
      {#if dataTransfer !== null}
        <div class="dtr-field-authenticated">
          <Chip>
            <Table data={dataTransferAsTable(dataTransfer)} />
          </Chip>
          {#if dataTransfer.status === DataTransferStatus.Initial}
            <Alert level={NotificationLevel.Info} roundCorners>
              <p>
                <strong>Please note:</strong> the data transfer associated with the
                selected data package has not been approved yet. Data transfers must
                be approved before data packages associated with them can be sent.
              </p>
            </Alert>
          {:else if dataTransfer.status === DataTransferStatus.Expired}
            <Alert level={NotificationLevel.Warning} roundCorners>
              <p>
                <strong>Warning:</strong> the data transfer associated with the selected
                data package has expired. Data packages can no longer be sent for
                this data transfer.
              </p>
            </Alert>
          {:else if dataTransfer.status === DataTransferStatus.Unauthorized}
            <Alert level={NotificationLevel.Warning} roundCorners>
              <p>
                <strong>Warning:</strong> the data transfer associated with the selected
                data package is not authorized. Data packages can only be sent for
                authorized data transfers.
              </p>
            </Alert>
          {/if}
        </div>
      {:else}
        <Alert level={NotificationLevel.Info} roundCorners>
          <p>
            <strong>Please note:</strong>
            {#if hasMetadata($transferPackageStore) && $transferPackageStore.metadata.transfer_id}
              the selected data package has a transfer ID value of '{$transferPackageStore
                .metadata.transfer_id}', which does not match any authorized
              data transfer.
            {:else}
              the selected data package does not have an associated data
              transfer ID and cannot be transferred while the 'verify package'
              setting is enabled.
            {/if}
            {#if $authenticatedMode}
              <br />
              In authenticated mode, data packages can only be sent if the data transfer
              is authorized and associated to the logged-in user: {userInfoAsString(
                $userInfo,
              )}
            {/if}
          </p>
        </Alert>
      {/if}
    </Section>

    {#if showExtraMetadata}
      <Section title="Extra Metadata (optional)">
        <ExtraMetadata
          metadataItems={$extraMetadata}
          addItem={extraMetadata.add}
          removeItem={extraMetadata.remove}
        />
      </Section>
    {/if}
    <Section title="Select destination" hidden={!showDestination}>
      <Destination
        authenticatedMode={$authenticatedMode &&
          ($transferId !== null || dataTransfer !== null)}
        destinations={tabMode === TabMode.Encrypt
          ? ['local', 's3', 'sftp']
          : ['s3', 'sftp']}
        value={$destinationKind}
        onchange={(destination) => ($destinationKind = destination)}
      />
    </Section>

    <ButtonWorkflow
      disabled={submitDisabled}
      hidden={!showWorkflowButton}
      Icon={Lock}
      text={workflowDescription + ' data'}
      tooltip={$destinationKind === 'local'
        ? 'Encrypt data and save it locally as a data package'
        : workflowDescription + ' data to the selected destination'}
    />
  </Page>
</form>

<style>
  .recipients {
    display: flex;
    flex-wrap: wrap;
    gap: 0.2em;
    margin-top: 0.7em;
  }

  .toggle {
    font-size: 1.5rem;
  }
</style>
