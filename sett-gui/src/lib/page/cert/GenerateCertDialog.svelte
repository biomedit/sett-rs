<script lang="ts">
  import {
    dateAsString,
    toDuration,
    tomorrow,
    toValidityPeriod,
    ValidityPresets,
  } from './datetime';
  import { type CertInfo } from '../../store/cert';
  import { invoke } from '@tauri-apps/api';
  import { NotificationLevel } from '../../store/event';
  import { settingsStore } from '../../store/settings';

  import Alert from '../../component/Alert.svelte';
  import Button from '../../component/Button.svelte';
  import Cog8Tooth from '../../icon/Cog8Tooth.svelte';
  import CopyToClipboard from '../../component/CopyToClipboard.svelte';
  import Dialog from '../../component/Dialog.svelte';
  import ExpirationDateInput from './ExpirationDateInput.svelte';
  import SecretInput from '../../component/SecretInput.svelte';
  import UploadCertDialog from './UploadCertDialog.svelte';

  interface Props {
    onclose: () => void;
    refreshCertStore: () => void;
  }

  const { onclose, refreshCertStore }: Props = $props();

  let name = $state('');
  let comment = $state('');
  let email = $state('');
  let password = $state('');
  let passwordRepeat = $state('');
  let validityPreset: ValidityPresets = $state(ValidityPresets.y3);
  let validityExpiryDate = $state(tomorrow());

  let dialogState:
    | 'confirm'
    | 'error'
    | 'input'
    | 'run'
    | 'success'
    | 'uploadCert' = $state('input');
  let cert: CertInfo | undefined = $state();
  let revocationSignature = $state('');
  let errorMsg = $state('');
  let showPassword = $state(false);
  let errorEmail = $state(false);
  let errorPasswordMatch = $state(false);
  let errorPasswordLength = $state(false);
  const validityPeriod = $derived(
    toValidityPeriod(validityPreset, validityExpiryDate),
  );
  const errorExpiration = $derived(
    validityPeriod !== null && validityPeriod <= 0,
  );
  const validationErrors: string[] = $derived([
    ...(errorEmail ? ['Invalid email address syntax'] : []),
    ...(errorPasswordLength
      ? ['Password must be at least 10 characters long']
      : []),
    ...(errorPasswordMatch ? ['Passwords do not match'] : []),
    ...(errorExpiration ? ['Expiry date must be in the future'] : []),
  ]);

  const disableNextButton = $derived(
    name.trim() === '' ||
      email.trim() === '' ||
      password === '' ||
      passwordRepeat === '' ||
      errorExpiration,
  );
  const userid = $derived(
    name + (comment ? ` (${comment})` : '') + (email ? ` <${email}>` : ''),
  );

  function validateEmail() {
    errorEmail =
      email.length > 0 &&
      !/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/.test(email.toLowerCase());
  }

  // Verify that the password is long enough and that it matches the repeated
  // version. To avoid displaying "obvious" error messages, empty values
  // for password of the repeat are not considered errors.
  function validatePassword() {
    if (password.length > 0 && passwordRepeat.length > 0) {
      errorPasswordMatch = password !== passwordRepeat;
      errorPasswordLength = password.length < 10;
    } else {
      errorPasswordMatch = false;
      errorPasswordLength = false;
    }
  }

  // Notes:
  // * fields that must only be non-empty are verified via disabling of the
  //   main button.
  // * Expiry date is verified on change, no need to verify it again.
  function validateInput() {
    name = name.trim();
    comment = comment.trim();
    validateEmail();
    validatePassword();
    if (
      errorEmail ||
      errorPasswordMatch ||
      errorPasswordLength ||
      errorExpiration ||
      name === ''
    ) {
      return 'input';
    }
    return 'confirm';
  }

  async function generateKey() {
    dialogState = 'run';
    try {
      [cert, revocationSignature] = await invoke<[CertInfo, string]>(
        'generate_cert',
        {
          store: {
            public: $settingsStore.publicKeyStore,
            private: $settingsStore.privateKeyStore,
          },
          userid: userid,
          password: password,
          validityPeriod: toDuration(validityPeriod),
        },
      );

      dialogState = 'success';
    } catch (e) {
      errorMsg = e as string;
      dialogState = 'error';
    }
    refreshCertStore();
  }
</script>

<Dialog
  {onclose}
  title="Generate OpenPGP key pair"
  closeOnBackdropClick={false}
>
  {#if dialogState === 'input'}
    <div class="alert">
      {#each validationErrors as e (e)}
        <Alert level={NotificationLevel.Warning}>{e}</Alert>
      {/each}
    </div>

    <div class="grid-layout">
      <label for="name" class="required">Name</label>
      <!-- svelte-ignore a11y_autofocus -->
      <input
        type="text"
        id="name"
        name="name"
        required
        autocapitalize="none"
        autocomplete="off"
        autocorrect="off"
        autofocus
        placeholder="First and last name of key owner"
        bind:value={name}
      />
      <label for="comment">Comment</label>
      <input
        type="text"
        id="comment"
        name="comment"
        autocapitalize="none"
        autocomplete="off"
        autocorrect="off"
        placeholder="Optional: additional info about key owner"
        bind:value={comment}
      />
      <label for="email" class="required">Email</label>
      <input
        type="text"
        id="email"
        name="email"
        required
        autocapitalize="none"
        autocomplete="off"
        autocorrect="off"
        placeholder="Email of key owner"
        bind:value={email}
        onblur={validateEmail}
        oninput={() => {
          // Emails cannot contain white spaces.
          email = email.trim();
          // Detect whether a wrong email was corrected.
          if (errorEmail) {
            validateEmail();
          }
        }}
      />
      <label for="password" class="required">Password</label>
      <SecretInput
        bind:userInput={password}
        bind:showPassword
        required
        id="password"
        minlength={10}
        placeholder="Password for private key - min 10 chars"
        oninput={errorPasswordLength || errorPasswordMatch
          ? validatePassword
          : undefined}
      />
      <label for="password-repeat" class="required">Confirm password</label>
      <SecretInput
        bind:userInput={passwordRepeat}
        bind:showPassword
        required
        id="password-repeat"
        oninput={errorPasswordMatch ? validatePassword : undefined}
      />

      <label for="expiry-date" class="required">Expiration date</label>
      <ExpirationDateInput bind:validityPreset bind:validityExpiryDate />
    </div>

    <div class="button-layout">
      <Button
        color="secondary"
        text="Cancel"
        tooltip="Close window without generating new key"
        onclick={onclose}
      />
      <Button
        color="primary"
        text="Next >"
        tooltip="Proceed with new key generation"
        onclick={() => (dialogState = validateInput())}
        disabled={disableNextButton}
      />
    </div>
  {:else if dialogState === 'confirm'}
    <p>
      Please <strong>verify the name, email and expiry date</strong> to be associated
      with the new key.
    </p>
    <div class="grid-layout-confirm">
      <span><strong>Key user ID:</strong></span>
      <span>{userid}</span>
      <span><strong>Expiry date:</strong></span>
      {#if validityPeriod !== null}
        <span>{dateAsString(new Date(Date.now() + validityPeriod))}</span>
      {:else}
        <span>key never expires</span>
      {/if}
    </div>
    <div class="button-layout">
      <Button
        text="Back"
        color="secondary"
        tooltip="Go back to data input form"
        tooltipDirection="to-right"
        onclick={() => (dialogState = 'input')}
      />
      <Button
        color="secondary"
        text="Cancel"
        onclick={onclose}
        tooltip="Close window without generating new key"
        tooltipDirection="center"
      />
      <Button
        color="primary"
        text="Generate key"
        Icon={Cog8Tooth}
        tooltip="Generate a new OpenPGP key with the provided data"
        onclick={generateKey}
      />
    </div>
  {:else if dialogState === 'run'}
    <p>Generating OpenPGP key pair...</p>
  {:else if dialogState === 'success'}
    <Alert level={NotificationLevel.Success}
      >Successfully created OpenPGP key pair</Alert
    >
    <p>
      In addition to your new public and private key pair, a revocation
      signature has been created (see below). A revocation signature allows you
      to invalidate your key at any time, i.e. to let other people know that you
      key has been compromised or is no longer in use.
    </p>
    <p>
      You should store your revocation signature in a safe location, e.g. a
      password manager. While it is possible to generate a revocation signature
      at a later point in time, this will need your private key as well as its
      password.
    </p>
    <pre class="signature">{revocationSignature}</pre>
    <span class="button-layout">
      <CopyToClipboard
        toCopy={revocationSignature}
        successMsg="Revocation signature copied to clipboard"
        tooltip="Copy revocation signature to clipboard"
      />
      <Button
        color="primary"
        text="Next >"
        tooltip="Proceed to key upload dialog"
        onclick={() => (dialogState = 'uploadCert')}
      />
    </span>
  {:else if dialogState === 'uploadCert' && cert}
    <UploadCertDialog {onclose} {cert} />
  {:else if dialogState === 'error'}
    <Alert level={NotificationLevel.Error}
      >Failed to generate OpenPGP key pair: {errorMsg}</Alert
    >
  {/if}
</Dialog>

<style>
  label {
    font-weight: bold;
  }

  p {
    margin: 1em 0;
  }

  .signature {
    padding: 1em;
    margin: 1em 0;
    background-color: var(--color-component-bg);
    overflow-x: auto;
    font-size: smaller;
    line-height: 1.2em;
  }

  .grid-layout {
    display: grid;
    align-items: center;
    grid-template-columns: 1fr 2.4fr;
    gap: 1em;
  }

  .grid-layout-confirm {
    display: grid;
    grid-template-columns: 6rem 2fr;
    gap: 0.2rem;
    margin: 0 0 2rem 0.5rem;
  }

  .alert {
    margin-bottom: 1em;
  }

  .button-layout {
    display: flex;
    justify-content: flex-end;
    gap: 0.5rem;
    margin-top: 2rem;
  }
</style>
