type Duration = {
  secs: number;
  nanos: number;
};

const daysPerTropicalYear = 365.2421897;
const millisecondsPerMinute = 60 * 1000;
const millisecondsPerHour = millisecondsPerMinute * 60;
const millisecondsPerDay = millisecondsPerHour * 24;
const millisecondsPerYear = Math.round(
  daysPerTropicalYear * millisecondsPerDay,
);
export const millisecondsPerMonth = Math.round(millisecondsPerYear / 12);
export enum ValidityPresets {
  m6 = '6 month',
  y1 = '1 year',
  y2 = '2 years',
  y3 = '3 years',
  y4 = '4 years',
  y5 = '5 years',
  never = 'never',
  custom = 'custom',
}

// String representation ("yyyy-mm-dd") of today's date at local time zone.
export function today() {
  return dateAsString(new Date());
}

// String representation ("yyyy-mm-dd") of tomorrow's date at local time zone.
export function tomorrow() {
  return dateAsString(new Date(Date.now() + millisecondsPerDay));
}

// String ("yyyy-mm-dd") representation of a `Date` object,
// in the local time zone.
export function dateAsString(date: Date) {
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, '0');
  const day = String(date.getDate()).padStart(2, '0');
  return `${year}-${month}-${day}`;
}

// Returns the duration in milliseconds between the current date and the
// expiration date specified by the user.
export function toValidityPeriod(preset: ValidityPresets, customDate: string) {
  switch (preset) {
    case ValidityPresets.m6:
      return Math.round(millisecondsPerYear / 2);
    case ValidityPresets.y1:
    case ValidityPresets.y2:
    case ValidityPresets.y3:
    case ValidityPresets.y4:
    case ValidityPresets.y5:
      return millisecondsPerYear * Number(preset.split(' ')[0]);
    case ValidityPresets.custom:
      return Date.parse(customDate) - Date.parse(today());
    case ValidityPresets.never:
      return null;
    default:
      throw new Error(`Unsupported PGP key expiration preset: ${preset}`);
  }
}

// "Normalizes" the validity period so that the expiry time always falls
// close to 01:00 in the user's local time zone. This avoids having
// a key that is e.g. valid in the morning, and expired in the after-noon.
//
// 1 additional minute is also added, because for an unknown reason the
// expiration date of a key created by the sequoia crate is 1 minute less
// than the requested value.
function normalizeValidityPeriod(validityPeriod: number) {
  const expiryDate = new Date(Date.now() + validityPeriod);
  return (
    validityPeriod -
    expiryDate.getHours() * millisecondsPerHour -
    expiryDate.getMinutes() * millisecondsPerMinute +
    millisecondsPerHour +
    millisecondsPerMinute
  );
}

export function toDuration(validityPeriod: number | null): Duration | null {
  return validityPeriod !== null
    ? {
        secs: Math.round(normalizeValidityPeriod(validityPeriod) / 1000),
        nanos: 0,
      }
    : null;
}

function sliceDatetime(datetime: string) {
  return {
    year: datetime.slice(0, 4),
    month: datetime.slice(5, 7),
    day: datetime.slice(8, 10),
  };
}

export function toHumanReadable(datetime: string) {
  const { year, month, day } = sliceDatetime(datetime);
  return `${day}.${month}.${year}`;
}

export function toMachineReadable(datetime: string) {
  const { year, month, day } = sliceDatetime(datetime);
  return `${year}-${month}-${day}`;
}
