<script lang="ts">
  import {
    ApprovalStatus,
    approvalStore,
    type CertInfo,
    CertType,
    type ExpirationDate,
    ExpirationDateKind,
    ExpirationDateStatus,
    isExpirationAt,
    ReasonForRevocation,
    type Validity,
    ValidityStatus,
  } from '../../store/cert';
  import { type TableRow } from '../../component/utils';
  import { toHumanReadable } from './datetime';

  import Badge from '../../component/Badge.svelte';
  import Button from '../../component/Button.svelte';
  import ChevronDown from '../../icon/ChevronDown.svelte';
  import ChevronUp from '../../icon/ChevronUp.svelte';
  import CreateRevocationDialog from './CreateRevocationDialog.svelte';
  import DeleteCertDialog from './DeleteCertDialog.svelte';
  import DeleteKeyDialog from './DeleteKeyDialog.svelte';
  import Dropdown from '../../component/Dropdown.svelte';
  import EllipsisHorizontal from '../../icon/EllipsisHorizontal.svelte';
  import ExportDialog from './Export.svelte';
  import { isCertExpiring } from '../utils';
  import RevokeDialog from './RevokeDialog.svelte';
  import SetExpirationDateDialog from './SetExpirationDateDialog.svelte';
  import Table from '../../component/Table.svelte';
  import UploadCertDialog from './UploadCertDialog.svelte';

  let collapsed = $state(true);
  let selected:
    | 'create_revocation'
    | 'delete_cert'
    | 'delete_key'
    | 'export_public'
    | 'export_secret'
    | 'none'
    | 'revoke'
    | 'set_expiration_date'
    | 'upload_keyserver' = $state('none');

  type CertBadgeName =
    | 'Approved'
    | 'Expired'
    | 'Expiring'
    | 'Private'
    | 'Public'
    | 'Revoked';

  const showDetails = (e: KeyboardEvent | MouseEvent) => {
    const el = e.target as HTMLElement;
    // The dropdown button should not expand/collapse the details view.
    if (el.closest('.dropdown')) {
      return;
    }
    collapsed = !collapsed;
  };
  const onclose = () => {
    selected = 'none';
  };

  interface Props {
    cert: CertInfo;
    refreshCertStore: () => void;
  }

  const { cert, refreshCertStore }: Props = $props();

  // Certificate approval status on Portal.
  const certApprovalStatus = $derived(
    $approvalStore.get(cert.fingerprint) ?? ApprovalStatus.Unknown,
  );

  const secretCertActions = [
    {
      label: 'Upload public key to keyserver',
      action: () => (selected = 'upload_keyserver'),
    },
    {
      label: 'Export public key',
      action: () => (selected = 'export_public'),
    },
    {
      label: 'Export public and private key',
      action: () => (selected = 'export_secret'),
    },
    {
      label: 'Create revocation signature',
      action: () => (selected = 'create_revocation'),
    },
    { label: 'Revoke', action: () => (selected = 'revoke') },
    {
      label: 'Delete',
      action: () => (selected = 'delete_key'),
    },
    {
      label: `${cert.expirationDate.kind === ExpirationDateKind.At ? 'Update' : 'Set'} expiration date`,
      action: () => (selected = 'set_expiration_date'),
    },
  ];
  const publicCertActions = [
    {
      label: 'Export public key',
      action: () => (selected = 'export_public'),
    },
    {
      label: 'Delete',
      action: () => (selected = 'delete_cert'),
    },
  ];

  function expirationDateAsString(expirationDate: ExpirationDate) {
    switch (expirationDate.kind) {
      case ExpirationDateKind.At:
        switch (expirationDate.status) {
          case ExpirationDateStatus.Valid:
            return 'Valid until ' + toHumanReadable(expirationDate.date);
          case ExpirationDateStatus.Expired:
            return (
              'Expired on ' +
              toHumanReadable(expirationDate.date) +
              ' (key should not be used for encrypting or signing)'
            );
        }
        break; // Unreachable line, needed for eslint.
      case ExpirationDateKind.Never:
        return 'Does not expire';
      case ExpirationDateKind.ReadError:
        return expirationDate.message;
    }
  }

  function revocationReasonAsString(reasonForRevocation: ReasonForRevocation) {
    switch (reasonForRevocation) {
      case ReasonForRevocation.Unspecified:
        return 'no reason specified';
      case ReasonForRevocation.Superseded:
        return 'key is superseded';
      case ReasonForRevocation.Compromised:
        return 'key is compromised';
      case ReasonForRevocation.Retired:
        return 'key is retired by owner';
    }
  }
  function validityAsString(validity: Validity) {
    if (validity.status === ValidityStatus.Valid) {
      return 'Valid';
    } else {
      return validity
        .reasons!.map(
          (reason, i) =>
            'Revoked - ' +
            revocationReasonAsString(reason) +
            (validity.messages![i] ? ' [' + validity.messages![i] + ']' : '') +
            ' (key should not be used for encrypting or signing)',
        )
        .join(', ');
    }
  }

  function approvalAsString(approvalStatus: ApprovalStatus) {
    switch (approvalStatus) {
      case ApprovalStatus.Approved:
        return 'Key is approved on Portal';
      case ApprovalStatus.CertificateUnknown:
        return 'Key Unknown [key is not registered on Portal]';
      case ApprovalStatus.CertificateDeleted:
        return 'Deleted [key was removed from keyserver by its owner]';
      case ApprovalStatus.CertificateRevoked:
        return 'Key Revoked [key was revoked by its owner]';
      case ApprovalStatus.PortalUnreachable:
        return (
          'Portal Unreachable [unable to retrieve status at this time ' +
          ' because Portal website is unreachable'
        );
      case ApprovalStatus.Revoked:
      case ApprovalStatus.Pending:
      case ApprovalStatus.Rejected:
      case ApprovalStatus.Unknown:
        return approvalStatus.valueOf();
    }
  }

  function certInfoAsTable(
    c: CertInfo,
    approvalStatus: ApprovalStatus,
  ): TableRow[] {
    return [
      { key: 'Fingerprint', value: c.fingerprint },
      {
        key: 'Expiration date',
        value: expirationDateAsString(c.expirationDate),
      },
      { key: 'Approval status', value: approvalAsString(approvalStatus) },
      { key: 'Revocation status', value: validityAsString(c.validity) },
    ];
  }

  function badgePosition(badgeName: CertBadgeName) {
    switch (badgeName) {
      case 'Public':
        return 1;
      case 'Private':
        return 2;
      case 'Expired':
      case 'Expiring':
        return (cert.certType == CertType.Secret ? 1 : 0) + 2;
      case 'Revoked':
        return (
          (cert.certType == CertType.Secret ? 1 : 0) +
          (isExpirationAt(cert.expirationDate) &&
          cert.expirationDate.status == ExpirationDateStatus.Expired
            ? 1
            : 0) +
          2
        );
      case 'Approved':
        return (
          (cert.certType == CertType.Secret ? 1 : 0) +
          (isExpirationAt(cert.expirationDate) &&
          cert.expirationDate.status == ExpirationDateStatus.Expired
            ? 1
            : 0) +
          (cert.validity.status == ValidityStatus.Revoked ? 1 : 0) +
          2
        );
    }
  }

  function getTooltipDirection(badgeName: CertBadgeName) {
    const badgeRank = badgePosition(badgeName);
    const textLength = cert.userid
      ? cert.userid.length
      : cert.fingerprint.length;
    switch (badgeRank) {
      case 1:
        return textLength <= 60 ? 'to-right' : 'to-left';
      case 2:
        return textLength <= 50 ? 'to-right' : 'to-left';
      default:
        return 'to-left';
    }
  }
</script>

<div
  class="cert-item"
  onclick={showDetails}
  onkeydown={(e) => {
    if (e.key === 'Enter') {
      showDetails(e);
    }
  }}
  aria-labelledby="certificate-details"
  role="button"
  tabindex="0"
>
  <div class="cert-item">
    <span>{cert.userid ?? cert.fingerprint}</span>
    <div class="badges">
      <Badge
        text="Public"
        color="blue"
        tooltip="Key contains public part"
        tooltipPosition="bottom"
        tooltipDirection={getTooltipDirection('Public')}
      />
      {#if cert.certType === CertType.Secret}
        <Badge
          text="Private"
          color="blue"
          tooltip="Key contains private part"
          tooltipPosition="bottom"
          tooltipDirection={getTooltipDirection('Private')}
        />
      {/if}
      {#if cert.expirationDate.kind === ExpirationDateKind.At}
        {#if cert.expirationDate.status == ExpirationDateStatus.Expired}
          <Badge
            text="Expired"
            color="orange"
            tooltip="Key should not be used for encrypting or signing"
            tooltipPosition="bottom"
            tooltipDirection={getTooltipDirection('Expired')}
          />
        {:else if isCertExpiring(cert)}
          <Badge
            text="Expiring"
            color="orange"
            tooltip="Key will expire soon"
            tooltipPosition="bottom"
            tooltipDirection={getTooltipDirection('Expiring')}
          />
        {/if}
      {/if}
      {#if cert.validity.status == ValidityStatus.Revoked}
        <Badge
          text="Revoked"
          color="red"
          tooltip="Key should not be used for encrypting or signing"
          tooltipPosition="bottom"
          tooltipDirection={getTooltipDirection('Revoked')}
        />
      {/if}
      {#if certApprovalStatus == ApprovalStatus.Approved}
        <Badge
          text="Approved"
          color="green"
          tooltip="Key is approved on Portal"
          tooltipPosition="bottom"
          tooltipDirection={getTooltipDirection('Approved')}
        />
      {/if}
    </div>
  </div>
  <div class="cert-options">
    <Button
      Icon={collapsed ? ChevronDown : ChevronUp}
      size="small"
      tooltip={collapsed ? 'Show key details' : 'Hide key details'}
    />
    <div class="dropdown">
      <Dropdown
        direction="right"
        Icon={EllipsisHorizontal}
        actions={cert.certType === CertType.Secret
          ? secretCertActions
          : publicCertActions}
        tooltip="Show additional tasks available for this OpenPGP key"
      />
    </div>
  </div>
</div>

<!-- Certificate details: this section appears if collapsed is False. -->
<div class:collapsed class="cert-details">
  <Table data={certInfoAsTable(cert, certApprovalStatus)} />
</div>

{#if selected === 'export_secret'}
  <ExportDialog
    fingerprint={cert.fingerprint}
    certType={CertType.Secret}
    {onclose}
  />
{:else if selected === 'export_public'}
  <ExportDialog
    fingerprint={cert.fingerprint}
    certType={CertType.Public}
    {onclose}
  />
{:else if selected === 'delete_cert'}
  <DeleteCertDialog {onclose} {cert} />
{:else if selected === 'delete_key'}
  <DeleteKeyDialog {onclose} {cert} {refreshCertStore} />
{:else if selected === 'create_revocation'}
  <CreateRevocationDialog {onclose} {cert} />
{:else if selected === 'upload_keyserver'}
  <UploadCertDialog {onclose} {cert} />
{:else if selected === 'set_expiration_date'}
  <SetExpirationDateDialog {onclose} {cert} {refreshCertStore} />
{:else if selected === 'revoke'}
  <RevokeDialog {onclose} {cert} {refreshCertStore} />
{/if}

<style>
  .cert-item {
    display: flex;
    gap: 0.8rem;
    align-items: center;
  }

  .badges {
    display: flex;
    gap: 0.5rem;
  }

  .cert-options {
    display: flex;
    margin-left: auto;
  }

  .collapsed {
    display: none;
  }

  .cert-details {
    margin: 0 0 1em 0.5em;
  }
</style>
