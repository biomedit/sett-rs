import { homeDir, sep } from '@tauri-apps/api/path';
import { get } from 'svelte/store';
import { invoke } from '@tauri-apps/api';
import { open } from '@tauri-apps/api/dialog';

import {
  type CertInfo,
  certStore,
  CertType,
  ExpirationDateKind,
  ExpirationDateStatus,
  isExpirationAt,
} from '../store/cert';
import {
  millisecondsPerMonth,
  toHumanReadable,
  toMachineReadable,
} from './cert/datetime';
import { NotificationLevel, toasts } from '../store/event';
import { type PackageSource, sourceLocation } from '../store/source';
import { type Package } from '../store/package';
import { type TableRow } from '../component/utils';

export const selectDirectory = async (callback: (selected: string) => void) => {
  const selected = await open({
    multiple: false,
    defaultPath: await homeDir(),
    directory: true,
  });
  if (selected !== null && !Array.isArray(selected)) {
    callback(selected);
  }
};

export const isWriteable = async (path: string): Promise<boolean> =>
  await invoke('is_writeable', { path });

export const selectPackage = async (callback: (selected: string) => void) => {
  const selected = await open({
    multiple: false,
    defaultPath: await homeDir(),
    filters: [
      {
        name: 'zip',
        extensions: ['zip'],
      },
    ],
  });
  if (selected !== null && !Array.isArray(selected)) {
    callback(selected);
  }
};

// Retrieve the user IDs associated with the specified OpenPGP fingerprints.
// Note: using the get(store) function is not ideal in terms of overhead, and
// therefore this functions takes an array of fingerprints (rather than a
// single fingerprint) in order to minimize the number of times the function
// needs to be called.
function userIDsFromFingerprints(fingerprints: string[]) {
  const certInfos = get(certStore);
  return fingerprints.map((fingerprint) => {
    if (!fingerprint) {
      return 'unknown [no metadata]';
    }
    for (const cert of certInfos) {
      if (cert.fingerprint.toUpperCase() === fingerprint.toUpperCase()) {
        return cert.userid || `key has no user ID [${fingerprint}]`;
      }
    }
    return `unknown key [${fingerprint}]`;
  });
}

// Return an array that represents a "Table" where each row has 3 fields:
// * row description: either "sender" or "recipient(s)".
// * user ID: user ID of the OpenPGP key.
// * tooltip: tooltip to display for the row, in this case it's the key's
//   fingerprint.
export function senderAndRecipientsAsTable(
  sender: string,
  recipients: string[],
): TableRow[] {
  const fingerprints = [sender, ...recipients];
  const userIDs = userIDsFromFingerprints(fingerprints);
  const tooltips = fingerprints.map((x) => `key fingerprint: ${x}`);
  return [
    {
      key: 'Sender',
      value: userIDs[0],
      tooltip: tooltips[0],
    },
    {
      key: recipients.length == 1 ? 'Recipient' : 'Recipients',
      value: userIDs[1],
      tooltip: tooltips[1],
    },
    ...userIDs
      .slice(2)
      .map((x, index) => ({ key: '', value: x, tooltip: tooltips[index + 2] })),
  ];
}

// Formats metadata of a data package as an array representing a "Table".
export function pkgMetadataAsTable<S extends PackageSource>(
  opts: Package<S>,
): TableRow[] {
  return [
    ...(opts.source
      ? [{ key: 'Location', value: sourceLocation(opts.source) }]
      : []),
    ...senderAndRecipientsAsTable(
      opts.metadata.sender,
      opts.metadata.recipients,
    ),
    { key: 'Timestamp', value: opts.metadata.timestamp },
    ...(opts.metadata.transfer_id
      ? [{ key: 'Transfer ID', value: opts.metadata.transfer_id.toString() }]
      : []),
    ...Object.entries(opts.metadata.extra).map(([key, value]) => ({
      key,
      value,
    })),
  ];
}

export const removeTrailingPathSep = (path: string) =>
  path.endsWith(sep) ? path.slice(0, -sep.length) : path;

export function basename(path: string) {
  return path.substring(path.lastIndexOf(sep) + 1);
}

export const certTypeAsString = (certType: CertType) =>
  certType === CertType.Public ? 'public' : 'private';

export const isCertExpiring = (cert: CertInfo): boolean => {
  return (
    cert.expirationDate.kind === ExpirationDateKind.At &&
    cert.expirationDate.status === ExpirationDateStatus.Valid &&
    Date.parse(toMachineReadable(cert.expirationDate.date)) <
      Date.now() + 3 * millisecondsPerMonth
  );
};

export const warnCertExpiring = (cert: CertInfo): void => {
  if (isExpirationAt(cert.expirationDate)) {
    toasts.add({
      level: NotificationLevel.Warning,
      message: `Key ${cert.userid} will expire soon: ${toHumanReadable(cert.expirationDate.date)}`,
      timeout: 5000,
      dismissible: true,
    });
  }
};

export function assert(condition: unknown, msg?: string): asserts condition {
  if (!condition) {
    throw new Error(msg);
  }
}

export function toHumanReadableSize(size: number): string {
  const exp = 1024;
  const suffix = ['', 'k', 'M', 'G', 'T', 'P', 'E'];
  const index = size && Math.floor(Math.log(size) / Math.log(exp));
  const precision = index && 2;
  return `${(size / Math.pow(exp, index)).toFixed(precision)} ${suffix[index]}B`;
}
