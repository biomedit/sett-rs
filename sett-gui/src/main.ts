import 'modern-normalize/modern-normalize.css';
import './css/app.css';

import { mount } from 'svelte';

import App from './App.svelte';

const app = mount(App, {
  target: document.getElementById('app')!,
});

export default app;
