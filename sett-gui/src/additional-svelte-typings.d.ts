// Add "ref" as an optional attribute to HTML elements of this project.
// The addition of a "ref" attributed is needed in Tooltip.svelte.
declare namespace svelteHTML {
  interface HTMLAttributes {
    ref?: string;
  }
}
