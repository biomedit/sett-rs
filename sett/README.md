# sett

Main library of **sett**.

Detailed documentation can be found at <https://docs.rs/sett>.

## Usage

By default `sett` uses `openssl` as cryptographic backend, however,
it's possible to select a different backend at the compile time: disable
default features (`default-features = false`) and enable the
`crypto-nettle` or `crypto-cng` feature. Available backends:

- `openssl` (default)
- `nettle`
- `cng` Cryptography API: Next Generation (CNG), **Windows**-only.

### Library

Add `sett` to dependencies in project's `Cargo.toml`.

```toml
[dependencies]
sett = "*"
```
