# Changelog

All notable changes to this project will be documented in this file.

## [0.3.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2F0%2E3%2E0) - 2024-09-17

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2F0%2E2%2E2...sett%2F0%2E3%2E0)

### ⚠ BREAKING CHANGES

- Rename `dpkg` module and metadata struct.
- Extract remotes from destination.
- Sftp upload is async now.
- `KeyStore` and `CertStore` APIs changed.
- Use a callback instead of a value for password in encrypt/decrypt.
- `Package` methods are now `async`.
- S3 operations are now available under `Client`.
- Replace `Cert` constructor with a builder.
- Removed `max_cpu` field from `EncryptOpts`.
- Use `mut` ref in Progress trait.
- Removed optional keyserver use from certificate store.
  Importing certificates from the keyserver is now a two step process:
  get a certificate from the keyserver using the `Keyserver` client,
  then import it into the certificate store.
- Move into the `openpgp` submodule.
- Remove sequoia `Password` type from public API.
- Split `certstore` into smaller modules.
- Major changes to the certificate management API.
  Encrypt and decrypt workflows now use stores (public and private) for
  loading certificates and secret key material instead of directly
  requiring serialized certificates and TSKs.
- `decrypt` is now `async`.
- Remove `max_cpu` field from `DecryptOpts`.
- Keyserver interaction is now done via the `Keyserver` struct.
- Move `get_key_status` into `impl Portal`.
- `EncryptOpts` has two new fields: `timestamp` and `prefix`.
  Those values are used when generating the package name.
- Added `Stdout` variant to `Destination`.
  Additionally, `LocalOpts` changed. It now hold a path to the base
  directory where a data package is created and an optional package
  name.
- Remove `filename` from `SftpOpts`.
- Use the DateTime type for the metadata timestamp.
- Changed signatures for top-level tasks (encrypt, decrypt, transfer).
- `region` and `endpoint` are now optional.
  When `endpoint` is not specified, then the official public AWS
  endpoint will be used.
- Remove `SimpleLogger`.
- `DecryptOpts` now requires `Package` instead of `AsRef<Path>`
  for specifying the data package for decryption.
- `keyserver::upload_cert` and `keyserver::verify_cert` are now async functions.
  Their blocking versions have moved to `keyserver::blocking`.
- This change reduces the leakage of Sequoia types into
  the public API of sett.
- `portal::get_key_status` is now an `async` function.
  The blocking version was moved to `portal::blocking::get_key_status`.
- Export requires an optional password.
  Additionally, there is only one certificate listing function that
  returns a CertInfo object instead of a String.
- This change reduces the leakage of Sequoia types into
  the public API of sett. Please note that the cleanup in incomplete, so
  expect further changes in the future.
  This change also consolidates the Cert-specific functionality as `impl`
  for that type.
- Functionality intended for internal use is now private.
- Remove `certstore migrate` command
- Split `certstore import` command into subcommands
- The following functions now take a `CertType` instead
  of a boolean as input argument: `CertStore.import_cert()`, and
  in the python bindings: `gnupg_extract_cert()`, `gnupg_list_certs()`
  and `gnupg_list_certs_with_userid()`.
- `crypt` and `error` modules are no longer public
- Heavily refactor parameters for encryption and destination.
  - CLI: Rename `Sftp` property `path` to `base_path`.
  - CLI: Remove `Encrypt` property `output`. You should now use destination
    specific properties.
  - Python bindings: Rename `SftpOpts` property `destination_dir` to
    `base_path`.
  - Python bindings: Remove `EncryptOpts` property `output`. You should now use
    destination specific properties.
  - Python bindings: Remove `S3Opts` from `encrypt` argument `remote`'s typing
    alternatives.
  - encrypt: Move `Destination` to a separate `destination` module.
  - encrypt: Remove `EncryptOpts` property `output`. You should now use
    destination specific properties.
  - transfer: Rename module to `destination`.
  - transfer/s3: Remove `S3Opts` generic `P`.
  - transfer/s3: Remove `S3Opts` property `files`. You should now pass it
    directly to the `upload` function.
  - transfer/s3: Remove `S3Opts` property `progress`. You should now pass it
    directly to the `upload` function.
  - transfer/s3: Remove `upload` generic `P`.
  - transfer/sftp: Rename `Remote` to `SftpOpts`.
  - transfer/sftp: Remove `SftpOpts` generic `Cb`.
  - transfer/sftp: Rename `SftpOpts` property `path` to `base_path`.
  - transfer/sftp: Change the type of `SftpOpts` property `base_path` from `str`
    to `PathBuf`.
  - transfer/sftp: Remove `SftpOpts` implementation for `connect`. You should
    now use the module's level `connect` function.
- Group encryption and decryption option into objects.
  - `encrypt` function signature has changed.
  - `decrypt` function signature has changed.
- Compression is now defined by both an algorithm and a level.
- `EncryptOpts` field types changed.
- `sftp_upload` replaced by more general `transfer`.
- `utils::Progress` is now a trait.
  Progress bar implementations should now implement the `Progress` trait.
- `anyhow::Error` replaces `sett::error::Error`

### ✨ Features

- **security:** Encrypt s3 and sftp secrets in memory ([33295a6](https://gitlab.com/biomedit/sett-rs/commit/33295a6682c4806907720593a6322ff17986139c))
- **log:** Improve logging ([14108df](https://gitlab.com/biomedit/sett-rs/commit/14108df1b618c0942d8f27f7734e06d7e1a15154)), Close #322
- **remote:** Extract remotes from destination ([07d25f8](https://gitlab.com/biomedit/sett-rs/commit/07d25f88a75b91a0c352c2b724bc7ceb142118f9))
- **destination:** Minimize options necessary for client creation ([bf03eea](https://gitlab.com/biomedit/sett-rs/commit/bf03eeafedc7c15ef899919a1a4a71cfe445f169))
- **task:** Include package metadata in status ([0a449fc](https://gitlab.com/biomedit/sett-rs/commit/0a449fc4483f03291c52f41744e89a42c5b869cb)), ⚠ BREAKING CHANGE
- **keystore:** Allow using custom location ([e14aa62](https://gitlab.com/biomedit/sett-rs/commit/e14aa6284b479091dafe7ce237d85d42eecb312a)), Close #293, ⚠ BREAKING CHANGE
- Use `sequoia-keystore` for the keystore implementation ([77b943f](https://gitlab.com/biomedit/sett-rs/commit/77b943fe25c76185cf758ebe593476f4fb372e65)), Close #290, ⚠ BREAKING CHANGE
- Add inspect subcommand ([935e9cd](https://gitlab.com/biomedit/sett-rs/commit/935e9cd33c46df990d596d5a58ea955d8dbfc9ac)), Close #299
- **decrypt:** Decrypt packages directly from s3 ([7c57c24](https://gitlab.com/biomedit/sett-rs/commit/7c57c2406c40b9ac09d19ccec86011ab7affe12d)), Close #197, ⚠ BREAKING CHANGE
- **s3:** Add S3 `Client` ([cca1fb2](https://gitlab.com/biomedit/sett-rs/commit/cca1fb2616298d99e919fc1604929c71274dec68)), ⚠ BREAKING CHANGE
- **cert:** Use a cert builder for generating new certs ([09c95b9](https://gitlab.com/biomedit/sett-rs/commit/09c95b90ca29df89d9fbb5654f1beb85441ea3ac)), ⚠ BREAKING CHANGE
- **auth.rs:** Derive Debug trait for AccessToken struct ([8a9200c](https://gitlab.com/biomedit/sett-rs/commit/8a9200c6d1c12def80c39f9ee77461812dc938b3))
- **response.rs:** Derive Serialize trait for DataTransfer struct ([94f1f55](https://gitlab.com/biomedit/sett-rs/commit/94f1f553586d4942520bedcbfbc92628a89331dd))
- **dpkg:** Add support for metadata custom fields ([0af069e](https://gitlab.com/biomedit/sett-rs/commit/0af069e6e6fb8bcff8d6a06dbe913e59691c2560)), Closes #292
- **auth:** Include access token lifespan ([b32d43b](https://gitlab.com/biomedit/sett-rs/commit/b32d43bfbc060ab86bc6f97752106f86106822b6))
- Display user info when available ([e01f6b0](https://gitlab.com/biomedit/sett-rs/commit/e01f6b0b4dff4e8f0060c25699648df7f0b3a63e)), Closes #278
- Fetch user info from appropriate endpoint ([2f50f8f](https://gitlab.com/biomedit/sett-rs/commit/2f50f8f2917ab5b70e800b12d1abc1e29bb235e5))
- **keyserver:** Explicitly pull certificates from keyserver ([372784e](https://gitlab.com/biomedit/sett-rs/commit/372784e99d84ab40fbda905bfba0b07cb5c36bcc)), ⚠ BREAKING CHANGE
- **keystore:** Add keystore automated migration ([83001ab](https://gitlab.com/biomedit/sett-rs/commit/83001abb6eb377f6f0b82addfac4bce86c3b22e0))
- Use sequoia-keystore for managing secret key material ([e465282](https://gitlab.com/biomedit/sett-rs/commit/e4652824af0506e1cf4cc8363f9e77a622cbb673)), Close #230, #149, ⚠ BREAKING CHANGE
- **decrypt:** Turn decrypt into async ([d939f76](https://gitlab.com/biomedit/sett-rs/commit/d939f76381cb71ed979d03d0c553621b0a604dfa)), ⚠ BREAKING CHANGE
- **sett-cli:** Improve user interaction for authentication process ([b8ee701](https://gitlab.com/biomedit/sett-rs/commit/b8ee7010b8aa177ef9a9f4ad72c77506a2fcb14f)), Close #252
- **encrypt:** Verify whether local destination is read-only ([c880014](https://gitlab.com/biomedit/sett-rs/commit/c880014fd696a950496099f26f9c4f29af731e12)), Closes #17
- **sett-cli:** Add portal subcommand and allow fetching data transfers from cli ([e17ed73](https://gitlab.com/biomedit/sett-rs/commit/e17ed737f0193e5ae7dea8d881c4da9baf6d518b)), Close #144
- **sett/portal:** Fetch the list of data transfers ([730b6c6](https://gitlab.com/biomedit/sett-rs/commit/730b6c68c9d70b242793839ef0c9a5eed3573df0))
- **s3:** Add http proxy support for AWS SDK ([0fcb574](https://gitlab.com/biomedit/sett-rs/commit/0fcb574c9831aa4e81e4ee94198d08b9280366e0)), Close #231
- Add OAuth2/OIDC-based authentication ([1f41f43](https://gitlab.com/biomedit/sett-rs/commit/1f41f4339f59bb0aa52085a5978c83f41862bca4)), Close #143, #145
- Use platform's native certificates in https clients ([ead0259](https://gitlab.com/biomedit/sett-rs/commit/ead02590c96920d671bbcd21404f88a3ecc1a442)), ⚠ BREAKING CHANGE, Close #213
- **portal:** Add package verification ([c2e8a8c](https://gitlab.com/biomedit/sett-rs/commit/c2e8a8ce2b03d0abb5eb50670171ab88bafb01f1)), Close #195
- **encrypt:** Add customization for generating package name ([12e75a3](https://gitlab.com/biomedit/sett-rs/commit/12e75a3001027c275e606752462acf01d3d799bd)), ⚠ BREAKING CHANGE
- **destination:** Extract stdout into its own destination variant ([ae50942](https://gitlab.com/biomedit/sett-rs/commit/ae509426750540e605e191e6e9ce0ac66c16a1ac)), ⚠ BREAKING CHANGE
- **dpkg:** Use the DateTime type for the metadata timestamp ([fe3229e](https://gitlab.com/biomedit/sett-rs/commit/fe3229e76cb4aabcf24d30c22f4aad4836c9a790))
- Remove blocking API ([5a91c5b](https://gitlab.com/biomedit/sett-rs/commit/5a91c5b0aca56a08457e140605636a961bc2b949)), Close #192
- Add preflight checks ([43a364a](https://gitlab.com/biomedit/sett-rs/commit/43a364af31d905262756766dd0ea35691f0af583)), Close #113
- Add "check" option to all top-level tasks and a common return type ([c8e5008](https://gitlab.com/biomedit/sett-rs/commit/c8e50084b869ea2f909bf701842ea3e5627ede07)), ⚠ BREAKING CHANGE
- **dpkg:** Add format check to package verification ([3d05172](https://gitlab.com/biomedit/sett-rs/commit/3d051721c5cbac6cbb9689993808e22d7ed8bdf3))
- **zip:** Add support for windows-generated zip files ([c35088c](https://gitlab.com/biomedit/sett-rs/commit/c35088c330e01e3171a29a3055024c8708c7859d)), Closes #190
- Make `region` and `endpoint` optional ([97174a8](https://gitlab.com/biomedit/sett-rs/commit/97174a864a4dcc42ed224f2df275ef2c1c7f867b)), ⚠ BREAKING CHANGE
- Use `tracing` crate for instrumentation ([d323647](https://gitlab.com/biomedit/sett-rs/commit/d323647f27796fede4c89e59263b003070bb12e2)), ⚠ BREAKING CHANGE, Close #146
- Display certificate revocation and expiry date ([c1d8b11](https://gitlab.com/biomedit/sett-rs/commit/c1d8b11f5428392f2e2a08eee7c7738369edd247)), Closes #130
- **sett-cli:** Move the CLI code into a separate crate ([c0d436b](https://gitlab.com/biomedit/sett-rs/commit/c0d436b21cbe75f879f1537fe2c52c431537f234)), Close #148
- **dpkg:** Add Package type for improved security and ergonomics ([92d6373](https://gitlab.com/biomedit/sett-rs/commit/92d637361678df35e60bce455152e42d0131b721)), Close #118, ⚠ BREAKING CHANGE
- **certstore:** Search also by key fingerprints in get_cert_by_fingerprint ([ff6c898](https://gitlab.com/biomedit/sett-rs/commit/ff6c8988efb69f7d7c6f19caa59b8cca14d84807))
- **keyserver:** Split `upload_cert` and `verify_cert` into async and blocking versions ([4f7a690](https://gitlab.com/biomedit/sett-rs/commit/4f7a6903cf20c1d0bdc3e376cf6e3d642fdafc41)), ⚠ BREAKING CHANGE
- **portal:** Split 'portal::get_key_status' into async and blocking versions ([d2cd723](https://gitlab.com/biomedit/sett-rs/commit/d2cd723d73de03c18f4b0ff7991e2855d6359436)), ⚠ BREAKING CHANGE
- **certstore:** Use sett Cert instead of Sequoia Cert ([a5adfc0](https://gitlab.com/biomedit/sett-rs/commit/a5adfc08ca9dcd4a4d7e7e37ad61e4e098dd223b)), ⚠ BREAKING CHANGE
- **destination:** Use `Cow` for destination fields ([3dcd4e7](https://gitlab.com/biomedit/sett-rs/commit/3dcd4e7e49fb1e56d7c473284f612766e44156b2)), ⚠ BREAKING CHANGE
- **python-bindings:** Add verify_metadata_signature function to python bindings ([c059434](https://gitlab.com/biomedit/sett-rs/commit/c0594345f9aff0f96596305fedf66f568a1a17bc)), Closes #92
- **dpkg:** Add reading metadata from data package file ([2f19cf8](https://gitlab.com/biomedit/sett-rs/commit/2f19cf88b5de86e3f6f99cbb674e67385f6a99cd))
- **certstore:** Merge migrate into import and split it into subcommands ([8707b5e](https://gitlab.com/biomedit/sett-rs/commit/8707b5e85ff006488d6a8db319df2eec1825b64d)), ⚠ BREAKING CHANGE, Close #77
- **gnupg:** Add option to list keys with user ID ([f96e5ce](https://gitlab.com/biomedit/sett-rs/commit/f96e5cee430a935ceb2238b5b63ce4b0fd74008f))
- Add support for encryption streaming directly to S3 object ([2c9a3a1](https://gitlab.com/biomedit/sett-rs/commit/2c9a3a1b6e79551fa77335c35cdcfb6491b65e3c)), Closes #62
- **sett/certstore:** Support uploading certificates to the keyserver ([b370013](https://gitlab.com/biomedit/sett-rs/commit/b370013d6aa0c6535b1c8ecca31d0465d23a430d)), Close #71
- **sett/certstore:** Support importing certificates from the keyserver ([01edf96](https://gitlab.com/biomedit/sett-rs/commit/01edf960bf640905add68161adad58ff13d8c7c6))
- **sett/certstore:** Support adding a keyserver as one of the certstore backends ([402a0bc](https://gitlab.com/biomedit/sett-rs/commit/402a0bc167f5eec6b377a7fd093ca21064a5367c))
- **gpg-import:** Implement the possibility to import keys directly from GPG keyring ([897733c](https://gitlab.com/biomedit/sett-rs/commit/897733ce405a32bb4271284f82bb2a072281d626)), Closes #72
- **cli:** Add support for multiple data recipients ([64069ee](https://gitlab.com/biomedit/sett-rs/commit/64069ee673e59031d89a7ce001a5d8113b4bf280)), Closes #70
- **decrypt:** Decrypt should also work with the certificate store ([5780489](https://gitlab.com/biomedit/sett-rs/commit/57804892c842aec70f2675008b43d9c987022961)), Closes #64
- **encrypt:** Encrypt should also work with the certificate store ([bbea2dd](https://gitlab.com/biomedit/sett-rs/commit/bbea2dd7a8b941708e5e3adafc9b1731b18bcd33)), Closes #64
- **encrypt:** Add stdout as a possible output for the encrypted package ([92f1eb2](https://gitlab.com/biomedit/sett-rs/commit/92f1eb28d0787c33a9c9ef962db1dc6037c487dd))
- **encrypt:** Remove `Seek` bound from encryption writer ([b129579](https://gitlab.com/biomedit/sett-rs/commit/b129579f976653b09ab46eae06b682bb893986a8)), Close #69
- **cli:** Add OpenPGP certificate revocation subcommand ([9f3ca04](https://gitlab.com/biomedit/sett-rs/commit/9f3ca04b9e0ef01e00f5be3d82bd71d49c7e40be)), Close #51
- **cli:** Add OpenPGP certificate generation subcommand ([562fa24](https://gitlab.com/biomedit/sett-rs/commit/562fa246741a1f1d37821f1e6d68360e7e715d4b)), Close #55
- **cli:** Add emoji support ([4d238a5](https://gitlab.com/biomedit/sett-rs/commit/4d238a51ab2f9d0af86b7676cec38642df19f934)), Close #52
- Add keystore implementation ([bf8b544](https://gitlab.com/biomedit/sett-rs/commit/bf8b544b7c6bdcb6a871ce3f977aa35ae0e54813)), Closes #34
- **s3:** Add support for STS `session_token` ([86d5927](https://gitlab.com/biomedit/sett-rs/commit/86d5927f9348d3fbe28ffea9fff77589a2cc5963)), Closes #47
- **portal:** Add API to query portal ([79d1cf9](https://gitlab.com/biomedit/sett-rs/commit/79d1cf9c3fbd6ccafd59220815cc02031b986f4c)), Closes #29
- **cli:** Make the CLI arguments more consistent ([0dcdb84](https://gitlab.com/biomedit/sett-rs/commit/0dcdb84f8e4ff1d568c4ad12fd4249a009f4b5e1)), ⚠ BREAKING CHANGE
- **s3:** Add support for s3 data transfers ([987ad22](https://gitlab.com/biomedit/sett-rs/commit/987ad22893949604152af1742eacc4f1bac27f71)), ⚠ BREAKING CHANGE
- Replace a concrete implementation of progress with a trait ([9879642](https://gitlab.com/biomedit/sett-rs/commit/9879642e6488a4cc7701c03cb87610b1b9d4a562)), ⚠ BREAKING CHANGE
- **cli:** Add an initial version of sett binary ([db12bfb](https://gitlab.com/biomedit/sett-rs/commit/db12bfb34c366df497a1156f4ac0668ba471ed13))
- **sftp::upload:** Accept `AsRef<Path>` instead of `&str` ([8d7cba8](https://gitlab.com/biomedit/sett-rs/commit/8d7cba8dbe3b0ca0ab8f512bae52e3fda32a47aa))
- **error:** Use `anyhow` for error handling ([71124fd](https://gitlab.com/biomedit/sett-rs/commit/71124fdc15c426c05d7c5fed104a292dd194ef77)), ⚠ BREAKING CHANGE

### 🐞 Bug Fixes

- **filesystem:** Find device for a given path by comparing extended paths ([f6ca568](https://gitlab.com/biomedit/sett-rs/commit/f6ca568d483011d177cabac7c6af1b8eb0bdbabd)), Close #337
- **PkgMetadata:** Bump metadata default version ([3256324](https://gitlab.com/biomedit/sett-rs/commit/32563241b15d5fd64bc26f674a491f00ec764bce)), Close #314
- **build:** `test-lib-windows` ([8aa8dce](https://gitlab.com/biomedit/sett-rs/commit/8aa8dce38ef44b333a803d213cd4c376883a7e38))
- **CLI:** Remove the annoying warning when making a S3 transfer ([3af8fff](https://gitlab.com/biomedit/sett-rs/commit/3af8fffa15086a0cb924fb9d7697092ef9719691))
- Use only self-signed user IDs ([1ad3759](https://gitlab.com/biomedit/sett-rs/commit/1ad3759b45329bf52496a506f6765d64191fdeac)), Closes #273
- **progress:** Use mut ref in Progress trait ([8fe1887](https://gitlab.com/biomedit/sett-rs/commit/8fe1887ca7a9fdf7cf229c9352710b7162b390b8)), Close #189
- Remove sequoia `Password` type from public API ([87e5696](https://gitlab.com/biomedit/sett-rs/commit/87e5696c0dc45ce01f72c898906eaee9b3a4fa74))
- **portal:** Use the latest Portal API schema ([bafe826](https://gitlab.com/biomedit/sett-rs/commit/bafe826247f86678da6a0ed22cf940c704c6c017)), Close #261
- **sett/portal:** Deal with Portal breaking changes ([f0bc3aa](https://gitlab.com/biomedit/sett-rs/commit/f0bc3aac8f97b17c924a536ae0c9d969c3c943ac))
- Unify destination urls across tasks ([cfe1d18](https://gitlab.com/biomedit/sett-rs/commit/cfe1d1867715d4676892421c20947d00ef7e6085))
- **checksum:** Ignore case differences in sha256sum verification ([554d940](https://gitlab.com/biomedit/sett-rs/commit/554d940020b7bbc5878abd830132bd933277074d))
- **sftp:** Remove `filename` from `SftpOpts` ([1a45292](https://gitlab.com/biomedit/sett-rs/commit/1a452924c7cd4d17b03c255c9060c64e94470898))
- **crypt:** Improve error message when wrong password to decrypt data ([29e0d51](https://gitlab.com/biomedit/sett-rs/commit/29e0d51f7ff341034958cff3cf32a37427c6439c)), Closes #193
- `encrypt` does no longer work for S3 transfers ([fd9690e](https://gitlab.com/biomedit/sett-rs/commit/fd9690ec30b29188cdc1c31cf7d13d697e65517f)), Closes #179
- `upload` method not working for S3 destination ([ec4c75e](https://gitlab.com/biomedit/sett-rs/commit/ec4c75e7c9ef3cb4ebaaefe60d03ba2c73de9c63)), Closes #179
- `put_object` does not work for small packages ([821a363](https://gitlab.com/biomedit/sett-rs/commit/821a3630a685d26fdaa9f0d2ce4a1923da1646c3))
- **package:** Show missing file name in parsing error ([b951e92](https://gitlab.com/biomedit/sett-rs/commit/b951e92f230c9f04b1914815c43885c21ac14cf4))
- **gnupg:** Allow using password for exporting secret certificates ([217ab29](https://gitlab.com/biomedit/sett-rs/commit/217ab29aaf4a31f539b03c470feb545ebeb4e8bc)), ⚠ BREAKING CHANGE
- Limit the publicly visible interface ([520fefb](https://gitlab.com/biomedit/sett-rs/commit/520fefb47570957786dd4e6832035ea1f2e38ce1)), ⚠ BREAKING CHANGE
- **certstore:** Use CertType enum for import_cert argument ([41f7b2b](https://gitlab.com/biomedit/sett-rs/commit/41f7b2b861e06c77d95707bc4ad8efff702191ed)), ⚠ BREAKING CHANGE, Closes #86
- **import:** Add support for multi-certificate imports ([a1fdd69](https://gitlab.com/biomedit/sett-rs/commit/a1fdd690c3bb2ce31581c9386763e28a97b547a7)), Closes #76
- **encrypt:** Delete incomplete .zip file upon workflow failure ([671d1d6](https://gitlab.com/biomedit/sett-rs/commit/671d1d65b956a78eee2b900bd5ed7d0cf8142951)), Closes #66
- **encrypt:** Use certificate fingerprint in package metadata ([57bd336](https://gitlab.com/biomedit/sett-rs/commit/57bd336c0477f699ac307572b41f2abcd92d6c56)), ⚠ BREAKING CHANGE
- Support packages exceeding 4GB ([e85d396](https://gitlab.com/biomedit/sett-rs/commit/e85d396201526e62fad02a4030aeb0c191cc8ef3)), Close #61
- **s3:** Simplify s3 object upload structure ([9d223f8](https://gitlab.com/biomedit/sett-rs/commit/9d223f8ac1463a1a97afc00974c66228e56c6259)), Close #49
- **s3:** Handle AWS S3 endpoints ([8a08a16](https://gitlab.com/biomedit/sett-rs/commit/8a08a1619812f4ed9659254fd787794e8cab93d8))
- **checksum:** Use POSIX-style path representation also on Windows ([9219eae](https://gitlab.com/biomedit/sett-rs/commit/9219eaec5f850e558eaca76a293348e1d158063f)), Close #33

### 🚀 Performance

- Compute all checksums in parallel ([822fda6](https://gitlab.com/biomedit/sett-rs/commit/822fda68b796bdf2aac5df7d87a0a431d0249578)), Close #280, ⚠ BREAKING CHANGE
- **decrypt:** Decrypt, checksum, and write in parallel ([7f97eee](https://gitlab.com/biomedit/sett-rs/commit/7f97eee263742d4418d76510adea13c88bcefaee)), Close #185, #186, ⚠ BREAKING CHANGE
- **s3:** Upload chunks in parallel ([ee0a37d](https://gitlab.com/biomedit/sett-rs/commit/ee0a37dc0bfb79ff2fd6b643e269294bebaec30e))
- Improve performance of encryption and transfer ([da088f7](https://gitlab.com/biomedit/sett-rs/commit/da088f755fb95c9fe36ab48e133169ce7349285a)), Close #158, #159, #188, ⚠ BREAKING CHANGE
- **sftp:** Improve speed of sftp uploads ([6b448d8](https://gitlab.com/biomedit/sett-rs/commit/6b448d85ae9f0b3e59e94a2b45deae126f8d681f)), Close #204
- **checksum/to_hex_string:** Allocate only a single String ([1011b40](https://gitlab.com/biomedit/sett-rs/commit/1011b402584be3787c69417c5f17c131223b70c0))
- Add zstandard (zstd) as a new compression algorithm ([fcac89d](https://gitlab.com/biomedit/sett-rs/commit/fcac89d1623eea441187c482f8030b7c7736e952)), ⚠ BREAKING CHANGE, Close #58
- **encrypt:** Run encryption and compression in parallel ([80ccded](https://gitlab.com/biomedit/sett-rs/commit/80ccded60d3454e000e0e2a0fc4ec401daca0dde)), Close #15

### 🧱 Build system and dependencies

- Include sett-gui in the main workspace ([328bb08](https://gitlab.com/biomedit/sett-rs/commit/328bb0851741247e7e81b552322693e4f419359f)), Close #270
- Add licenses for each crate ([be2ad30](https://gitlab.com/biomedit/sett-rs/commit/be2ad30accc4693eed5136be70892866aea682f0)), Close #217
- Unpin `cc` dependency ([fc41487](https://gitlab.com/biomedit/sett-rs/commit/fc414877302eb1b959595c9b02cef1a6f6eca681))
- Fix openssl-sys compilation issue ([26ff364](https://gitlab.com/biomedit/sett-rs/commit/26ff36489cab8d2fc97da60ea38c4150f81e64c9))
- Increase MSRV from 1.70 to 1.72 ([694f68a](https://gitlab.com/biomedit/sett-rs/commit/694f68ab85d29b7256f2a378afa5c36b901d025f))
- Fix minimal dependency versions ([2048c8c](https://gitlab.com/biomedit/sett-rs/commit/2048c8cb0f56a86a0c3fed88053090be78e0957a)), Close #131
- Use openssl as the default backend ([0c3d3da](https://gitlab.com/biomedit/sett-rs/commit/0c3d3dad7740a7ec2d1b7bba757ea23ddf67e649)), Close #57
- Decouple sett and sett-rs release schedule ([0a24dc2](https://gitlab.com/biomedit/sett-rs/commit/0a24dc2c26566207c75657633c8c96648977cf30)), Close #48
- **sequoia:** Add optional openssl backend ([68c8c76](https://gitlab.com/biomedit/sett-rs/commit/68c8c76ed5eb4c4dc923afa980449c3f03b2ad66))

### 👷 CI

- Trigger warnings for missing docstrings in the main library ([19f1428](https://gitlab.com/biomedit/sett-rs/commit/19f14286bead8f71cba7d248d6dd12bd9393e2ec))

### 📝 Documentation

- Update docstrings in certstore and keystore ([3fce0c3](https://gitlab.com/biomedit/sett-rs/commit/3fce0c3b7a235a4dcc5ecfd669e4b72befb7d16c))
- **sett:** Add docstrings for pub interfaces ([dfdca88](https://gitlab.com/biomedit/sett-rs/commit/dfdca881aaa80df4a6d0bc004d77b5d97363bcf4))
- Improve all project's `README.md` ([365085f](https://gitlab.com/biomedit/sett-rs/commit/365085f285cf23a85727a54977241c8f3970887f)), Close #177
- Add/correct the different `README.md`'s ([d8d39d5](https://gitlab.com/biomedit/sett-rs/commit/d8d39d580ea310db3e9fa0746c1dc83071ccda70))
- Improve functions doc comments ([9a960f8](https://gitlab.com/biomedit/sett-rs/commit/9a960f87d5a92fb8f1a618f1978ce8bbe881cafa))
- **certstore:** Document CertStoreOpts ([cdbb144](https://gitlab.com/biomedit/sett-rs/commit/cdbb144808ae9ecddfe5b3853025e53222a2cc81))
- Improve description of CLI arguments for S3 options ([673e8f5](https://gitlab.com/biomedit/sett-rs/commit/673e8f573b55c622b4d78000a8a46ef761f87309))
- Update readme ([f9e16d4](https://gitlab.com/biomedit/sett-rs/commit/f9e16d4438a3bb99363bf7fe8574a9fde6e6e8ba))

### 🧹 Refactoring

- **package:** Rename module and metadata ([2cfc3a4](https://gitlab.com/biomedit/sett-rs/commit/2cfc3a4267109c9be30ac536904f9aa199f5a0cb))
- **metadata:** Rename metadata field `custom` to `extra` ([31b3926](https://gitlab.com/biomedit/sett-rs/commit/31b3926fa9ee01de950e9d64d77c3a3ef091c195)), Close #320
- **cert:** Return error when Cert::update_secret_material fails ([6d36f6b](https://gitlab.com/biomedit/sett-rs/commit/6d36f6b44350e94617a7066ac017ec1fc284eff1)), Closes #321
- Standardize comments line length to 80 chars ([4375aea](https://gitlab.com/biomedit/sett-rs/commit/4375aea0410d6a8abaf03f64779a8d02bf1f093e))
- **auth:** Remove `profile` scope ([ab462ad](https://gitlab.com/biomedit/sett-rs/commit/ab462ada5c0bfe0010de8e80589dec699257600f))
- Remove checksum module ([a3340e6](https://gitlab.com/biomedit/sett-rs/commit/a3340e67ae53a8df860dc89c85bd42bfb9be5949))
- **decrypt:** Remove unnecessary Mutex ([2a5bbaa](https://gitlab.com/biomedit/sett-rs/commit/2a5bbaa5464b192c7c103194ec92c85836517529))
- Use sequoia for computing sha256 hashes ([1195e4b](https://gitlab.com/biomedit/sett-rs/commit/1195e4b2511837b1b3d804471eb98783f2c971f2)), Close #276
- **keyserver:** Move into the `openpgp` submodule ([259aa8d](https://gitlab.com/biomedit/sett-rs/commit/259aa8d1aadefb70532e9562b7c8bd3bbaa2b1c2))
- Split `certstore` into smaller modules ([f28cc4c](https://gitlab.com/biomedit/sett-rs/commit/f28cc4c77e458e13e64c34e790a17725e88b26f8))
- Move the `crypt` module to `openpgp::crypto` ([e20ac52](https://gitlab.com/biomedit/sett-rs/commit/e20ac52416c8059bb1b7bde1697db5e47d397cbe))
- Make `auth.rs` usable in the context of Tauri ([dbbcc57](https://gitlab.com/biomedit/sett-rs/commit/dbbcc570848c7e87d083a02a6e2617facdcba157))
- **ThreadWriter:** Use `bytes` crate for internal buffer ([55ecc97](https://gitlab.com/biomedit/sett-rs/commit/55ecc9737a68ddf7a2e94d642587403421000da7))
- **thread_writer:** Replace crossbeam_channel with std::sync:mpsc ([5e3e528](https://gitlab.com/biomedit/sett-rs/commit/5e3e528ffdb204862287b388dd1f1d41910855e0))
- **sett/portal:** Move response and payload to separate files ([54e9802](https://gitlab.com/biomedit/sett-rs/commit/54e98020723695b30512aaf2002c21575962f763))
- Fix typo in s3.rs ([0db504a](https://gitlab.com/biomedit/sett-rs/commit/0db504a199e82120b846075fd7cf74ba451f0727))
- **portal:** Move `get_key_status` into `impl Portal` ([ffdfabe](https://gitlab.com/biomedit/sett-rs/commit/ffdfabe135c3e1f5cb9e914d1b762c521f81a4b5))
- Fix small linter complaint in tests of checksum.rs ([220324c](https://gitlab.com/biomedit/sett-rs/commit/220324c7b2b153ac1c6ca542caa14ec83fc2d68a))
- **zip:** Use an internal implementation of ZIP reader ([d094616](https://gitlab.com/biomedit/sett-rs/commit/d0946164a6bf2c0c60429e2a1ed6463797067176)), Close #147
- **decrypt:** Remove code duplication in checking if progress is used ([7a78acb](https://gitlab.com/biomedit/sett-rs/commit/7a78acb005ff79cc81cc5f9e3cea97e264fc5b6c))
- Change the terms certificate to key and secret to private ([c77fed5](https://gitlab.com/biomedit/sett-rs/commit/c77fed50f5e4a1f9f0d0cc73c5e0fc918429e458)), Closes #156
- Use official `aws-sdk-rust` ([27a49a7](https://gitlab.com/biomedit/sett-rs/commit/27a49a776283f692fb644de5de42f6c25b0fd915)), Closes #38
- Adapt certstore to openpgp-cert-d 3.1 ([b08395f](https://gitlab.com/biomedit/sett-rs/commit/b08395f0ee74cd1b7fe428b4c3cb79d7e53c805d))
- Move `Helper` to the `crypt` module ([e1b1e4e](https://gitlab.com/biomedit/sett-rs/commit/e1b1e4eddcd48bf3a1d0c241d2d935c7a32483f5))
- **certstore:** Use internal revocation types ([91dc01b](https://gitlab.com/biomedit/sett-rs/commit/91dc01bd0c726766d721930a13a01e72aab26462)), ⚠ BREAKING CHANGE
- **sett:** Update code for changes in sequoia-openpgp 1.17 ([90d8dd4](https://gitlab.com/biomedit/sett-rs/commit/90d8dd4eab6e3d3f065757457ebdd9031659381b))
- **cmd:** Standardize shared arguments struct naming ([89775b0](https://gitlab.com/biomedit/sett-rs/commit/89775b0ac313c2009672ac2031e5f336e7db5218))
- **decrypt:** Make error message on failed decryption more explicit ([c9ba385](https://gitlab.com/biomedit/sett-rs/commit/c9ba385047ecfcbd6b2165ec55682a3c25e16031))
- **gnupg:** Add generic function to run gpg commands ([77fd1d9](https://gitlab.com/biomedit/sett-rs/commit/77fd1d9f2c5b6611a70ec57e46b6a5865da3b663))
- **gnupg:** Rename module gpg to gnupg ([6886ef2](https://gitlab.com/biomedit/sett-rs/commit/6886ef2f8f48c3037308aaf984b047988e6edabd))
- Rename option to specify GPG dir from homedir to gpghome ([232435d](https://gitlab.com/biomedit/sett-rs/commit/232435d9793d4a4a856949752163b2f11f4ded9d))
- **gpg:** Improve error message in export_key ([2d8b87a](https://gitlab.com/biomedit/sett-rs/commit/2d8b87a55355f2b9eff5b4783ef4d6d5ad16629b))
- **encrypt:** Refactor and add comments ([3249e05](https://gitlab.com/biomedit/sett-rs/commit/3249e05cae65c484df571a124c4a20e8e75c2c13))
- **certstore:** Removed error handling based on error message ([952ee85](https://gitlab.com/biomedit/sett-rs/commit/952ee85e10a0075bf2f0220cd96f2a03d504b076)), Closes #56
- **sett/examples:** Remove unused imports ([db3319e](https://gitlab.com/biomedit/sett-rs/commit/db3319e303df917f84789e5d4a6404e3fa04e2b4))
- Improve naming, separation of concerns and add local as a destination variant ([fee95f7](https://gitlab.com/biomedit/sett-rs/commit/fee95f79c4cb883d41a01e8774f2d4c071d437a6)), ⚠ BREAKING CHANGE
- **sett-rs:** Group encrypt and decrypt parameters into opts objects ([d61858c](https://gitlab.com/biomedit/sett-rs/commit/d61858cdc2d90a1bbee9447f1bf7f8fcf1a36e11)), Close #43, ⚠ BREAKING CHANGE

### 🎨 Style

- Fix clippy warnings (rust 1.67.0) ([39409bc](https://gitlab.com/biomedit/sett-rs/commit/39409bcb2593c475659995b9e746cbe8aa7cc178))

### ✅ Test

- **basic:** Create a package, then decrypt it ([5de9cb3](https://gitlab.com/biomedit/sett-rs/commit/5de9cb3a14dcf4feefcf662e0b32c7611295557c))
- **certstore:** Add unit tests for certstore module ([bc49b9f](https://gitlab.com/biomedit/sett-rs/commit/bc49b9f639807402e62b05617b6adcfb0fd71347)), Closes #53
- Fix `get_common_path` test when running on Windows ([720f38b](https://gitlab.com/biomedit/sett-rs/commit/720f38b373bbc84e0a2db6c785299718906d94c9))
- **examples:** Add transfer example ([def2dd4](https://gitlab.com/biomedit/sett-rs/commit/def2dd4a28df9ee9fedbf6ef1e5faccce797b3ae))

### [0.2.2](https://gitlab.com/biomedit/sett-rs/compare/0.2.1...0.2.2) (2022-11-23)

### Features

- add python submodule to provide improved python bindings ([f1d3223](https://gitlab.com/biomedit/sett-rs/commit/f1d3223c08b93b1d0b1b25097ff12caddb0c34c1)), closes [#23](https://gitlab.com/biomedit/sett-rs/issues/23)
- **encrypt:** allow encrypting directly to a remote destination ([d88a214](https://gitlab.com/biomedit/sett-rs/commit/d88a214b0789d3418d2c0f004b5fe0912cccdfc4)), closes [#22](https://gitlab.com/biomedit/sett-rs/issues/22)
- **sett-rs:** add OpenPGP cert/key reader ([3bf27dd](https://gitlab.com/biomedit/sett-rs/commit/3bf27dd02776321b64527875be67f3ed3ee16988)), closes [#21](https://gitlab.com/biomedit/sett-rs/issues/21)

### Bug Fixes

- **encrypt:** use the first/primary key for signing if multiple are provided ([c2c7300](https://gitlab.com/biomedit/sett-rs/commit/c2c73000426fe26430369f16255b35f61a176e59)), closes [#24](https://gitlab.com/biomedit/sett-rs/issues/24)
- **filesystem:** available space computation should work on each OS ([287bcfe](https://gitlab.com/biomedit/sett-rs/commit/287bcfe3267eaf770c858596039db47c3343d67f))

### [0.2.1](https://gitlab.com/biomedit/sett-rs/compare/0.2.0...0.2.1) (2022-09-30)

### Features

- add decrypt workflow ([01dc4a8](https://gitlab.com/biomedit/sett-rs/commit/01dc4a805fd52f14b2d937fd9ce90b972f9c286a)), closes [#14](https://gitlab.com/biomedit/sett-rs/issues/14)
- add encryption workflow ([320be33](https://gitlab.com/biomedit/sett-rs/commit/320be33ad56485adbfc6d57685de3be05e4b8e0c))
- **sett/encrypt:** use in-memory encryption for passwords ([643ad39](https://gitlab.com/biomedit/sett-rs/commit/643ad3939b493f2c947dbd19d2e0ddb8d0be9b69))

### Bug Fixes

- rename functions to remove clippy warnings ([dacb8e6](https://gitlab.com/biomedit/sett-rs/commit/dacb8e6268d54fe0bcafc0fb9775b1fe902d08d3))

## [0.2.0](https://gitlab.com/biomedit/sett-rs/compare/0.1.3...0.2.0) (2021-12-09)

### ⚠ BREAKING CHANGES

- **lib:** sftp_upload now requires progress argument to be callable

### Features

- Add library stub ([7560ab4](https://gitlab.com/biomedit/sett-rs/commit/7560ab4add87db737089731b418f722a46fdb811))
- **sftp:** Support for 2 factor authentication ([d83b160](https://gitlab.com/biomedit/sett-rs/commit/d83b16060811b179d9d0f4c1d524ee3567c23f98))

- **lib:** Change progress callback from python object to python callable ([a2a8e38](https://gitlab.com/biomedit/sett-rs/commit/a2a8e3863d83a209004f8187e2aeb87998382999))

### [0.1.3](https://gitlab.com/biomedit/sett-rs/compare/0.1.2...0.1.3) (2021-10-13)

### [0.1.2](https://gitlab.com/biomedit/sett-rs/compare/0.1.1...0.1.2) (2021-08-11)

### Features

- **sftp:** make upload buffer size configurable ([07e594b](https://gitlab.com/biomedit/sett-rs/commit/07e594b95452d0097f4ce780bd5d83577c230b7b))

### [0.1.1](https://gitlab.com/biomedit/sett-rs/compare/0.1.0...0.1.1) (2021-08-04)

### Features

- **sftp:** log uploaded files size ([2cd9d11](https://gitlab.com/biomedit/sett-rs/commit/2cd9d11b9167a03646b8c2097f5ffe5cbd873141))

### Bug Fixes

- remove needless_borrow reported by clippy ([3236462](https://gitlab.com/biomedit/sett-rs/commit/32364628a498a928f06ff6513d92882ad8772e82))
- replace deprecated macro text_signature with new pyo3(text_signature) ([fabefb0](https://gitlab.com/biomedit/sett-rs/commit/fabefb03cacb423c11dc7cdb0ec4424031266bf4))

## 0.1.0 (2021-08-03)

### Bug Fixes

- **sftp:** add upload progress ([d2576e4](https://gitlab.com/biomedit/sett-rs/commit/d2576e412daffb4ea76e1be0bde6142dcba79ea6))
