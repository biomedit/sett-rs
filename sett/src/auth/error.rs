//! Error types occurring during authentication

pub use openidconnect;
pub use reqwest;

type OpenIdRequestCoreError = openidconnect::RequestTokenError<
    reqwest::Error,
    openidconnect::StandardErrorResponse<openidconnect::core::CoreErrorResponseType>,
>;
type OpenIdRequestDeviceError = openidconnect::RequestTokenError<
    reqwest::Error,
    openidconnect::StandardErrorResponse<openidconnect::DeviceCodeErrorResponseType>,
>;
type OpenIdUserInfoError = openidconnect::UserInfoError<reqwest::Error>;
type OpenIdConfigurationError = openidconnect::ConfigurationError;

/// Error generated during openid provider metadata discovery
pub type OpenIdDiscoveryError = openidconnect::DiscoveryError<reqwest::Error>;

/// Error occurring when trying to parse an url
#[derive(Debug)]
pub struct UrlParseError(pub String);

impl std::fmt::Display for UrlParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}
impl core::error::Error for UrlParseError {}

/// Error occurring during client configuration
#[derive(Debug)]
pub enum ConfigError {
    /// OpenID provider metadata discovery error
    OpenIdDiscovery(OpenIdDiscoveryError),
    /// HTTP client error
    HttpClient(reqwest::Error),
}

impl core::error::Error for ConfigError {}

impl From<OpenIdDiscoveryError> for ConfigError {
    fn from(value: OpenIdDiscoveryError) -> Self {
        Self::OpenIdDiscovery(value)
    }
}

impl From<reqwest::Error> for ConfigError {
    fn from(value: reqwest::Error) -> Self {
        Self::HttpClient(value)
    }
}

impl std::fmt::Display for ConfigError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::OpenIdDiscovery(e) => e.fmt(f),
            Self::HttpClient(e) => {
                write!(f, "Unable to crate an HTTP client: {e:?}")
            }
        }
    }
}

/// Error occuring during authentication
#[derive(Debug)]
pub enum AuthError<E> {
    /// Error caused by an auth handler
    Handler(E),
    /// Auth fails during configuration of device code exchange
    OpenIdConfig(OpenIdConfigurationError),
    /// Auth fails during device code exchange (request)
    OpenIdRequestCore(OpenIdRequestCoreError),
    /// Auth fails during device access token exchange (request)
    OpenIdRequestDevice(OpenIdRequestDeviceError),
}

impl<E> std::fmt::Display for AuthError<E>
where
    E: std::fmt::Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Handler(e) => e.fmt(f),
            Self::OpenIdConfig(e) => e.fmt(f),
            Self::OpenIdRequestCore(e) => e.fmt(f),
            Self::OpenIdRequestDevice(e) => e.fmt(f),
        }
    }
}
impl<E> core::error::Error for AuthError<E> where E: std::fmt::Debug + std::fmt::Display {}
impl<E> From<OpenIdConfigurationError> for AuthError<E> {
    fn from(value: OpenIdConfigurationError) -> Self {
        Self::OpenIdConfig(value)
    }
}
impl<E> From<OpenIdRequestCoreError> for AuthError<E> {
    fn from(value: OpenIdRequestCoreError) -> Self {
        Self::OpenIdRequestCore(value)
    }
}
impl<E> From<OpenIdRequestDeviceError> for AuthError<E> {
    fn from(value: OpenIdRequestDeviceError) -> Self {
        Self::OpenIdRequestDevice(value)
    }
}

#[derive(Debug)]
/// Error occurring when retrieving user info
pub enum UserInfoError {
    /// Retrieval of user info fails during preparation of the request
    Config(OpenIdConfigurationError),
    /// Error while sending the request
    UserInfo(OpenIdUserInfoError),
}
impl std::fmt::Display for UserInfoError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::UserInfo(e) => {
                write!(f, "Failed requesting user info: {e:?}")
            }
            Self::Config(e) => {
                write!(f, "No user info endpoint: {e:?}")
            }
        }
    }
}
impl core::error::Error for UserInfoError {}
impl From<OpenIdConfigurationError> for UserInfoError {
    fn from(value: OpenIdConfigurationError) -> Self {
        Self::Config(value)
    }
}
impl From<OpenIdUserInfoError> for UserInfoError {
    fn from(value: OpenIdUserInfoError) -> Self {
        Self::UserInfo(value)
    }
}

#[derive(Debug)]
/// Error occuring when refreshing a token
pub enum RefreshTokenError {
    /// Token refresh fails during preparation of the request
    Config(OpenIdConfigurationError),
    /// OpenId request error
    RequestCore(OpenIdRequestCoreError),
}

impl std::fmt::Display for RefreshTokenError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Config(e) => {
                write!(f, "No token refresh endpoint: {e:?}")
            }
            Self::RequestCore(e) => e.fmt(f),
        }
    }
}
impl core::error::Error for RefreshTokenError {}
impl From<OpenIdConfigurationError> for RefreshTokenError {
    fn from(value: OpenIdConfigurationError) -> Self {
        Self::Config(value)
    }
}
impl From<OpenIdRequestCoreError> for RefreshTokenError {
    fn from(value: OpenIdRequestCoreError) -> Self {
        Self::RequestCore(value)
    }
}
