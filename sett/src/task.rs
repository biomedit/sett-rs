//! Task module
//!
//! Shared types for the top-level tasks: encrypt, decrypt, transfer.

/// Task execution mode
#[derive(Debug, Default, Copy, Clone)]
pub enum Mode {
    /// Check the task input without running the full task.
    ///
    /// The check should be quick to perform and should not modify any data.
    Check,
    /// Run the task but stop on any errors or warnings.
    #[default]
    Run,
    /// Run the task and ignore any warnings, but stop on errors.
    Force,
}

/// Task success status
#[derive(Clone, Debug)]
pub enum Status {
    /// Successfully checked task input without running the full task.
    Checked {
        /// Destination for the current task.
        destination: String,
        /// Size of the source file(s) or package for the current task.
        source_size: u64,
    },
    /// Task successfully completed.
    Completed {
        /// Destination for the current task.
        destination: String,
        /// Size of the source file(s) or package for the current task.
        source_size: u64,
        /// Size of the destination package for the current task.
        destination_size: u64,
        /// Package metadata.
        metadata: crate::package::Metadata,
    },
}
