//! Error namespace

#[derive(Debug)]
pub enum ReaderError {
    Io(tokio::io::Error),
    Zip(ZipError),
    S3(S3Error),
}

impl std::fmt::Display for ReaderError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Reader error")
    }
}

impl std::error::Error for ReaderError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Io(source) => Some(source),
            Self::Zip(source) => Some(source),
            Self::S3(source) => Some(source),
        }
    }
}

impl From<S3Error> for ReaderError {
    fn from(value: S3Error) -> Self {
        Self::S3(value)
    }
}
impl From<ZipError> for ReaderError {
    fn from(value: ZipError) -> Self {
        Self::Zip(value)
    }
}
impl From<std::io::Error> for ReaderError {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}

#[derive(Debug)]
pub enum FileReaderError {
    Io(tokio::io::Error),
    Zip(ZipError),
    S3(S3Error),
    NotFound(String),
    InvalidCompressionMethod,
}

impl std::fmt::Display for FileReaderError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::NotFound(path) => {
                write!(f, "File reader error: archive member not found: {path}")
            }
            Self::InvalidCompressionMethod => {
                write!(f, "only uncompressed files are supported")
            }
            _ => write!(f, "File reader error"),
        }
    }
}

impl std::error::Error for FileReaderError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Io(source) => Some(source),
            Self::Zip(source) => Some(source),
            Self::S3(source) => Some(source),
            _ => None,
        }
    }
}

impl From<S3Error> for FileReaderError {
    fn from(value: S3Error) -> Self {
        Self::S3(value)
    }
}
impl From<ZipError> for FileReaderError {
    fn from(value: ZipError) -> Self {
        Self::Zip(value)
    }
}
impl From<std::io::Error> for FileReaderError {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}

#[derive(Debug)]
pub enum S3Error {
    Io(tokio::io::Error),
    Get(crate::remote::s3::error::get::Error),
    ObjectTooSmall,
    MissingSizeInfo,
    GetObject(crate::remote::s3::error::S3GetObjectError),
    ReadStream(crate::remote::s3::error::S3ByteStreamError),
}

impl std::fmt::Display for S3Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::ObjectTooSmall => write!(
                f,
                "Error while opening zip reader: s3 object size is too small"
            ),
            Self::MissingSizeInfo => write!(
                f,
                "Error while opening zip reader: unable to fetch s3 object size"
            ),
            Self::ReadStream(_) => write!(
                f,
                "Error while opening zip reader: error while reading from stream"
            ),
            _ => write!(f, "Error while opening zip reader"),
        }
    }
}

impl std::error::Error for S3Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Io(source) => Some(source),
            Self::Get(source) => Some(source),
            Self::ObjectTooSmall => None,
            Self::MissingSizeInfo => None,
            Self::GetObject(source) => Some(source),
            Self::ReadStream(source) => Some(source),
        }
    }
}

impl From<tokio::io::Error> for S3Error {
    fn from(value: tokio::io::Error) -> Self {
        Self::Io(value)
    }
}

impl From<crate::remote::s3::error::get::Error> for S3Error {
    fn from(value: crate::remote::s3::error::get::Error) -> Self {
        Self::Get(value)
    }
}

impl From<crate::remote::s3::error::S3GetObjectError> for S3Error {
    fn from(value: crate::remote::s3::error::S3GetObjectError) -> Self {
        Self::GetObject(value)
    }
}

impl From<crate::remote::s3::error::S3ByteStreamError> for S3Error {
    fn from(value: crate::remote::s3::error::S3ByteStreamError) -> Self {
        Self::ReadStream(value)
    }
}

#[derive(Debug)]
pub enum ZipError {
    InvalidOffset,
    SeekCentralDirectory(SeekHeaderError),
    ParseCentralDirectory(HeaderParseError),
    Io(std::io::Error),
    UnimplementedMultiDisk,
}

impl std::fmt::Display for ZipError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidOffset => write!(
                f,
                "Error parsing zip file: zip offset is before the buffer start"
            ),
            Self::UnimplementedMultiDisk => {
                write!(f, "multi-disk zip archives are not supported")
            }
            _ => write!(f, "Error parsing zip file: zip"),
        }
    }
}

impl std::error::Error for ZipError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Io(source) => Some(source),
            Self::SeekCentralDirectory(source) => Some(source),
            Self::ParseCentralDirectory(source) => Some(source),
            _ => None,
        }
    }
}

impl From<std::io::Error> for ZipError {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}
impl From<SeekHeaderError> for ZipError {
    fn from(value: SeekHeaderError) -> Self {
        Self::SeekCentralDirectory(value)
    }
}
impl From<HeaderParseError> for ZipError {
    fn from(value: HeaderParseError) -> Self {
        Self::ParseCentralDirectory(value)
    }
}

#[derive(Debug)]
pub enum SeekHeaderError {
    NotFound(&'static str),
    Seek(std::io::Error),
}

impl std::fmt::Display for SeekHeaderError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::NotFound(message) => message.fmt(f),
            _ => write!(f, "Zip/Zip64 central directory header error"),
        }
    }
}

impl std::error::Error for SeekHeaderError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Seek(source) => Some(source),
            _ => None,
        }
    }
}

impl From<std::io::Error> for SeekHeaderError {
    fn from(value: std::io::Error) -> Self {
        Self::Seek(value)
    }
}

#[derive(Debug)]
pub enum HeaderParseError {
    InvalidSignature,
    InvalidExtraField,
    InvalidInput,
    Io(std::io::Error),
    MissingZipInfo(&'static str),
}

impl std::fmt::Display for HeaderParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidSignature => write!(f, "invalid zip central directory header"),
            Self::InvalidExtraField => write!(
                f,
                "invalid zip64 central directory header: missing or \
                incomplete 'extra field' section"
            ),
            Self::InvalidInput => write!(f, "only uncompressed files are supported"),
            Self::MissingZipInfo(msg) => msg.fmt(f),
            _ => write!(f, "Zip/Zip64 central directory header parse error"),
        }
    }
}

impl std::error::Error for HeaderParseError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Io(source) => Some(source),
            _ => None,
        }
    }
}

impl From<std::io::Error> for HeaderParseError {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}
