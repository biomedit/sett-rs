use std::io;

use super::{error, spec, ZipFile};
use crate::package::Source;

#[derive(Clone, Debug)]
pub(crate) struct ZipReader {
    files: Vec<ZipFile>,
    // Offset at which the ZIP archive starts in the original file
    // (ZIP file may be prepended with arbitrary data).
    zip_offset: u64,
    pub(crate) source: Source,
}

impl ZipReader {
    pub(crate) async fn open(source: Source) -> Result<Self, error::ReaderError> {
        match source {
            Source::Local(path) => {
                let mut reader = io::BufReader::new(std::fs::File::open(&path)?);
                let (files, zip_offset) = parse_zip_central_directory(&mut reader, 0)?;
                Ok(Self {
                    files,
                    zip_offset,
                    source: Source::Local(path),
                })
            }
            Source::S3(source) => {
                let metadata = source
                    .client
                    .inner
                    .head_object()
                    .bucket(&source.bucket)
                    .key(&source.object)
                    .send()
                    .await
                    .map_err(crate::remote::s3::error::get::Error)
                    .map_err(error::S3Error::Get)?;
                let object_size = metadata
                    .content_length()
                    .ok_or(error::S3Error::MissingSizeInfo)?;
                if object_size == 0 {
                    return Err(error::S3Error::ObjectTooSmall.into());
                }
                let object_size = object_size as u64;
                // Pull at most 1MB from the end of the file.
                // It should be sufficient for parsing ZIP central directory.
                let bytes_to_pull = std::cmp::min(1 << 20, object_size);
                let buffer_offset = object_size - bytes_to_pull;
                let data = source
                    .client
                    .inner
                    .get_object()
                    .bucket(&source.bucket)
                    .key(&source.object)
                    // bytes positions are inclusive so we need to subtract 1
                    .range(format!("bytes={}-{}", buffer_offset, object_size - 1))
                    .send()
                    .await
                    .map_err(error::S3Error::GetObject)?
                    .body
                    .collect()
                    .await
                    .map_err(error::S3Error::ReadStream)?
                    .into_bytes();
                let mut reader = io::Cursor::new(data);
                let (files, zip_offset) = parse_zip_central_directory(&mut reader, buffer_offset)?;
                Ok(Self {
                    files,
                    zip_offset,
                    source: Source::S3(source),
                })
            }
        }
    }

    /// Reads a file from the ZIP archive.
    ///
    /// Returns a tuple of a reader and a file size.
    pub(crate) async fn get_file_reader(
        &self,
        name: &str,
    ) -> Result<(Box<dyn tokio::io::AsyncBufRead + Unpin + Sync + Send>, u64), error::FileReaderError>
    {
        use tokio::io::{AsyncReadExt as _, AsyncSeekExt as _};
        let file_info = self
            .files
            .iter()
            .find(|f| f.name == name)
            .ok_or_else(|| error::FileReaderError::NotFound(name.to_string()))?;
        if file_info.compression_method != 0 {
            return Err(error::FileReaderError::InvalidCompressionMethod);
        }
        let offset = file_info.offset + self.zip_offset;
        match &self.source {
            Source::Local(path) => {
                // Read the local file header to find where the file contents starts
                let mut reader = tokio::io::BufReader::new(tokio::fs::File::open(path).await?);
                reader.seek(io::SeekFrom::Start(offset)).await?;
                let local_header = spec::LocalFileHeader::parse(&mut reader)
                    .await
                    .map_err(error::ZipError::ParseCentralDirectory)?;
                let offset = offset + local_header.header_size as u64;
                // Seek the beginning of the file's data
                reader.seek(io::SeekFrom::Start(offset)).await?;
                Ok((Box::new(reader.take(file_info.size)), file_info.size))
            }
            Source::S3(s3_source) => {
                // Read the local file header to find where the file contents starts
                let object = s3_source
                    .client
                    .inner
                    .get_object()
                    .bucket(&s3_source.bucket)
                    .key(&s3_source.object)
                    // 1kB should be enough to get the complete ZIP local file header
                    .range(format!("bytes={}-{}", offset, offset + 1024))
                    .send()
                    .await
                    .map_err(error::S3Error::GetObject)?;
                let mut reader = object.body.into_async_read();
                let local_header = spec::LocalFileHeader::parse(&mut reader)
                    .await
                    .map_err(error::ZipError::ParseCentralDirectory)?;
                // Read the file contents
                let object = s3_source
                    .client
                    .inner
                    .get_object()
                    .bucket(&s3_source.bucket)
                    .key(&s3_source.object)
                    .range(format!(
                        "bytes={}-{}",
                        offset + local_header.header_size as u64,
                        // bytes positions are inclusive so we need to subtract 1
                        offset + local_header.header_size as u64 + file_info.size - 1,
                    ))
                    .send()
                    .await
                    .map_err(error::S3Error::GetObject)?;
                Ok((Box::new(object.body.into_async_read()), file_info.size))
            }
        }
    }

    /// Returns the list of files present in the Zip archive.
    pub(crate) fn file_names(&self) -> impl Iterator<Item = &str> {
        self.files.iter().map(|f| f.name.as_str())
    }
}

/// Finds and parses ZIP central directory
///
/// The provided `reader` doesn't need to contain the entire ZIP archive.
/// It can be limited only to the end part of the archive (containing all
/// ZIP data structures that follow the last data file). In such a case
/// `buffer_offset` must specify reader's position relative to the beginning
/// of the ZIP file.
fn parse_zip_central_directory<R: io::Read + io::Seek>(
    reader: &mut R,
    buffer_offset: u64,
) -> Result<(Vec<ZipFile>, u64), error::ZipError> {
    let with_buffer_offset = |offset: u64| {
        offset
            .checked_sub(buffer_offset)
            .ok_or(error::ZipError::InvalidOffset)
    };

    spec::CentralDirectoryEnd::find(reader)?;
    let cde = spec::CentralDirectoryEnd::parse(reader)?;

    // If present the Zip64 central directory end locator is at 20 bytes
    // before the central directory end.
    reader.seek(io::SeekFrom::End(
        -((cde.size() + spec::Zip64CentralDirectoryEndLocator::SIZE) as i64),
    ))?;
    let (number_of_records, central_directory_offset, zip_offset) = if let Some(zip64_end) =
        spec::Zip64CentralDirectoryEndLocator::parse(reader)?
    {
        reader.seek(io::SeekFrom::Start(with_buffer_offset(
            zip64_end.zip64_central_directory_end_offset,
        )?))?;
        let zip64cde_actual_position =
            spec::Zip64CentralDirectoryEnd::find(reader)? + buffer_offset;
        let zip64cde = spec::Zip64CentralDirectoryEnd::parse(reader)?;
        if zip64cde.disk_number_of_records != zip64cde.total_number_of_records {
            return Err(error::ZipError::UnimplementedMultiDisk);
        }
        (
            zip64cde.total_number_of_records,
            zip64cde.central_directory_offset,
            zip64cde_actual_position - zip64_end.zip64_central_directory_end_offset,
        )
    } else {
        reader.seek(io::SeekFrom::Start(with_buffer_offset(
            cde.central_directory_offset as u64,
        )?))?;
        let central_directory_actual_offset = spec::CentralDirectoryHeader::find(reader)?;
        (
            cde.total_number_of_records as u64,
            cde.central_directory_offset as u64,
            central_directory_actual_offset + buffer_offset - (cde.central_directory_offset as u64),
        )
    };
    reader.seek(io::SeekFrom::Start(with_buffer_offset(
        zip_offset + central_directory_offset,
    )?))?;
    let mut files = Vec::with_capacity(number_of_records as usize);
    for _ in 0..number_of_records {
        let header = spec::CentralDirectoryHeader::parse(reader)?;
        files.push(ZipFile {
            name: header.name.into_owned(),
            offset: header.offset,
            size: header.size,
            hasher: crc32fast::Hasher::new(),
            crc32: header.crc32,
            flags: header.flags,
            external_attributes: header.external_attributes,
            timestamp: header.modified,
            compression_method: header.compression_method,
        });
    }
    Ok((files, zip_offset))
}
