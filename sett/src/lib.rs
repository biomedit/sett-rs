//! Library for Secure Encryption and Transfer Tool (sett).
#![warn(missing_docs)]

#[cfg(feature = "auth")]
pub mod auth;
pub mod decrypt;
pub mod destination;
pub mod encrypt;
mod filesystem;
pub mod gnupg;
mod io;
pub mod openpgp;
pub mod package;
pub mod portal;
pub mod progress;
pub mod remote;
pub mod secret;
pub mod task;
pub mod utils;
mod zip;
