//! Backends for remote storage.

pub mod s3;
pub mod sftp;
