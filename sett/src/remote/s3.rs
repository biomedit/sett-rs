//! Destination-specific logic for destination type S3.

pub mod error;

use std::{path::Path, sync::Arc};

use aws_config::{
    environment::EnvironmentVariableRegionProvider,
    meta::{credentials::CredentialsProviderChain, region::RegionProviderChain},
    profile::ProfileFileRegionProvider,
};
use aws_sdk_s3::{
    config::Credentials,
    primitives::ByteStream,
    types::{CompletedMultipartUpload, CompletedPart},
};
use aws_types::region::Region;
use bytes::BytesMut;
use tokio::{
    io::AsyncReadExt,
    sync::{mpsc, Semaphore},
};
use tracing::{debug, info, instrument, trace};

use crate::{
    progress::ProgressDisplay,
    secret::Secret,
    task::{Mode, Status},
    utils::to_human_readable_size,
};

// 4 * 8MB (4 * 8_388_608) = 32 MB (33_554_432)
const MINIMUM_CHUNK_SIZE: usize = 4 << 23;
const MAX_CHUNKS: u32 = 10000;

/// Options required to create an S3 client.
#[derive(Clone, Debug, Default)]
pub struct ClientBuilder {
    /// Endpoint URL
    endpoint: Option<String>,
    /// Region
    region: Option<String>,
    /// Profile
    profile: Option<String>,
    /// Access key ID
    access_key: Option<Secret>,
    /// Secret access key
    secret_key: Option<Secret>,
    /// Session token applications use for authentication
    session_token: Option<Secret>,
}

macro_rules! setter {
    ($name:ident, $type:ident, $doc:literal) => {
        #[doc = $doc]
        pub fn $name(mut self, $name: Option<impl Into<$type>>) -> Self {
            self.$name = $name.map(Into::into);
            self
        }
    };
}

impl ClientBuilder {
    /// Builds a new S3 [`Client`].
    pub async fn build(&self) -> Result<Client, error::ClientError> {
        Client::new(self).await
    }

    setter!(endpoint, String, "Sets the endpoint URL.");
    setter!(region, String, "Sets the region.");
    setter!(profile, String, "Sets the profile.");
    setter!(access_key, Secret, "Sets the access key ID.");
    setter!(secret_key, Secret, "Sets the secret access key.");
    setter!(session_token, Secret, "Sets the session token.");
}

/// S3 client
#[derive(Clone)]
pub struct Client {
    pub(crate) inner: aws_sdk_s3::Client,
    /// Endpoint URL
    pub(crate) endpoint: String,
}

impl std::fmt::Debug for Client {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Client")
            .field("endpoint", &self.endpoint)
            .finish()
    }
}

impl Client {
    /// Creates a new client builder.
    pub fn builder() -> ClientBuilder {
        ClientBuilder::default()
    }

    /// Builds a new S3 client from `S3Opts`.
    async fn new(opts: &ClientBuilder) -> Result<Self, error::ClientError> {
        let region = opts.region.as_deref().map(|s| Region::new(s.to_string()));
        let region_provider = RegionProviderChain::first_try(region)
            .or_else(EnvironmentVariableRegionProvider::new())
            .or_else(ProfileFileRegionProvider::new())
            .or_else("us-east-1");
        let credentials_provider =
            if let (Some(access_key), Some(secret_key)) = (&opts.access_key, &opts.secret_key) {
                CredentialsProviderChain::first_try(
                    "opts",
                    Credentials::from_keys(
                        access_key.reveal("access key")?,
                        secret_key.reveal("secret key")?,
                        opts.session_token
                            .as_ref()
                            .map(|s| s.reveal("session token"))
                            .transpose()?,
                    ),
                )
            } else {
                CredentialsProviderChain::default_provider().await
            };
        let mut config_loader = aws_config::from_env();
        config_loader = if let Some(profile) = opts.profile.as_deref() {
            config_loader.profile_name(profile)
        } else {
            config_loader
                .region(region_provider)
                .credentials_provider(credentials_provider)
        };
        if let Some(endpoint) = opts.endpoint.as_deref() {
            config_loader = config_loader.endpoint_url(endpoint);
        }
        let sdk_config = config_loader.load().await;
        let s3_config_builder =
            aws_sdk_s3::config::Builder::from(&sdk_config).force_path_style(true);
        let s3_config = if let Some(proxy_url) = proxy::get_url_from_env() {
            debug!(proxy_url, "Proxy URL found");
            s3_config_builder.http_client(proxy::build_http_client(&proxy_url)?)
        } else {
            s3_config_builder
        }
        .build();
        Ok(Self {
            inner: aws_sdk_s3::Client::from_conf(s3_config),
            endpoint: sdk_config.endpoint_url().unwrap_or_default().into(),
        })
    }

    /// Upload a package to an S3 object store.
    #[instrument(skip(progress), err(Debug, level=tracing::Level::ERROR))]
    pub async fn upload(
        &self,
        package: &crate::package::Package<crate::package::state::Verified>,
        bucket: &str,
        mode: Mode,
        progress: Option<impl ProgressDisplay + Send + 'static>,
    ) -> Result<Status, error::UploadError> {
        let metadata = package.metadata().await?;
        let path = package.path()?.to_path_buf();
        let object_name = package.name();
        let source_size = tokio::fs::metadata(&path).await?.len();
        let destination = [self.endpoint.as_str(), bucket, object_name].join("/");
        let status = if let crate::task::Mode::Check = mode {
            debug!(
                destination,
                source_size, "Checked {object_name} for transfer into bucket {bucket}",
            );
            Status::Checked {
                destination,
                source_size,
            }
        } else {
            let (tx, rx) = mpsc::channel(2);
            let handle =
                tokio::task::spawn(async move { read_chunks_from_file(&path, tx, progress).await });
            self.put_object(bucket, object_name, rx).await?;
            handle.await??;
            info!(
                destination,
                source_size,
                destination_size = source_size,
                metadata = metadata.to_json_or_debug(),
                "Successfully transferred {object_name} into bucket {bucket}",
            );
            Status::Completed {
                destination,
                source_size,
                destination_size: source_size,
                metadata,
            }
        };
        Ok(status)
    }

    #[instrument(skip(channel))]
    pub(crate) async fn put_object(
        &self,
        bucket: &str,
        key: &str,
        mut channel: mpsc::Receiver<BytesMut>,
    ) -> Result<(), error::put::Error> {
        trace!("Transferring '{}' to bucket '{}'", key, bucket);

        let log_completion = |size| {
            debug!(
                "Successfully transferred '{}' to bucket '{}' (size: {})",
                key,
                bucket,
                to_human_readable_size(size)
            );
        };

        let mut counter: u64 = 0;
        let mut part_number: i32 = 0;
        let mut upload_id = String::new();
        // Limit the number of concurrent uploads to 4.
        let semaphore = Arc::new(Semaphore::new(4));
        let mut join_handles = Vec::new();

        while let Some(chunk) = channel.recv().await {
            if part_number == 0 {
                // It's a single part upload.
                // We assume that the first chunk is completely filled for multipart
                // uploads.
                let chunk_len = chunk.len();
                if chunk_len < chunk.capacity() {
                    self.inner
                        .put_object()
                        .bucket(bucket)
                        .key(key)
                        .body(ByteStream::from(chunk.freeze()))
                        .send()
                        .await
                        .map_err(error::put::Error::from)?;
                    log_completion(chunk_len as u64);
                    return Ok(());
                // It's a multipart upload.
                } else {
                    self.inner
                        .create_multipart_upload()
                        .bucket(bucket)
                        .key(key)
                        .send()
                        .await
                        .map_err(error::put::Error::from)?
                        .upload_id()
                        .ok_or(error::put::Error::FetchMultipartId)?
                        .clone_into(&mut upload_id);
                }
            }
            // Part numbering starts at 1
            part_number += 1;
            counter += chunk.len() as u64;

            let permit = semaphore
                .clone()
                .acquire_owned()
                .await
                .map_err(error::put::Error::from);
            let upload_id = upload_id.clone();
            let key = key.to_string();
            let bucket = bucket.to_string();
            let client = self.inner.clone();
            let join_handle = tokio::task::spawn(async move {
                let upload_part_res = client
                    .upload_part()
                    .key(key)
                    .bucket(bucket)
                    .upload_id(upload_id)
                    .body(ByteStream::from(chunk.freeze()))
                    .part_number(part_number)
                    .send()
                    .await?;
                let completed_part = CompletedPart::builder()
                    .e_tag(
                        upload_part_res
                            .e_tag
                            .ok_or(error::put::Error::FetchEntityTag)?,
                    )
                    .part_number(part_number)
                    .build();
                drop(permit);
                Ok(completed_part)
            })
                as tokio::task::JoinHandle<Result<_, error::put::Error>>;
            join_handles.push(join_handle);
        }

        let mut upload_parts = Vec::with_capacity(join_handles.len());
        for join_handle in join_handles {
            upload_parts.push(join_handle.await??);
        }
        if upload_parts.is_empty() {
            return Err(error::put::Error::EmptyUpload);
        }

        let completed_multipart_upload = CompletedMultipartUpload::builder()
            .set_parts(Some(upload_parts))
            .build();
        self.inner
            .complete_multipart_upload()
            .bucket(bucket)
            .key(key)
            .multipart_upload(completed_multipart_upload)
            .upload_id(upload_id)
            .send()
            .await?;

        log_completion(counter);
        Ok(())
    }
}

async fn read_chunks_from_file(
    path: &Path,
    sink: mpsc::Sender<BytesMut>,
    progress: Option<impl ProgressDisplay>,
) -> Result<(), error::ReadChunksError> {
    let mut file = tokio::fs::File::open(path).await?;
    let input_size = file.metadata().await?.len();
    let chunk_size = compute_chunk_size(input_size);
    let mut buffer = BytesMut::with_capacity(chunk_size);
    let mut task = progress.map(|p| p.start(input_size));
    loop {
        let n = file.read_buf(&mut buffer).await?;
        if n == 0 || buffer.len() >= chunk_size {
            sink.send(std::mem::replace(
                &mut buffer,
                BytesMut::with_capacity(chunk_size),
            ))
            .await?;
        }
        if n == 0 {
            break;
        }
        if let Some(t) = &mut task {
            t.increment(n as u64);
        }
    }
    Ok(())
}

mod proxy {
    use aws_sdk_s3::config::SharedHttpClient;
    use aws_smithy_runtime::client::http::hyper_014::HyperClientBuilder;
    use hyper_proxy::{Intercept, Proxy, ProxyConnector};
    use hyper_rustls::ConfigBuilderExt;

    pub(super) fn get_url_from_env() -> Option<String> {
        let mut proxy_url = None;
        for name in [
            "HTTP_PROXY",
            "HTTPS_PROXY",
            "ALL_PROXY",
            "http_proxy",
            "https_proxy",
            "all_proxy",
        ] {
            if let Ok(val) = std::env::var(name) {
                proxy_url = Some(val);
                break;
            }
        }
        proxy_url
    }

    pub mod error {
        #[derive(Debug)]
        pub enum ConnectionError {
            UrlParse(String),
            Connection(std::io::Error),
        }

        impl std::fmt::Display for ConnectionError {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                match self {
                    Self::UrlParse(message) => write!(f, "{message}"),
                    Self::Connection(e) => write!(f, "{e}"),
                }
            }
        }

        impl std::error::Error for ConnectionError {
            fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
                match self {
                    Self::UrlParse(_) => None,
                    Self::Connection(source) => Some(source),
                }
            }
        }

        impl From<std::io::Error> for ConnectionError {
            fn from(value: std::io::Error) -> Self {
                Self::Connection(value)
            }
        }
    }

    pub(super) fn build_http_client(url: &str) -> Result<SharedHttpClient, error::ConnectionError> {
        // Connector code is taken from the AWS SDK.
        // https://github.com/awslabs/aws-sdk-rust/blob/45ba2a808bb01f4875229acae73ca994bd75177e
        // /sdk/aws-smithy-runtime/src/client/http/hyper_014.rs#L53
        let connector = hyper_rustls::HttpsConnectorBuilder::new()
            .with_tls_config(
                rustls::ClientConfig::builder()
                    .with_cipher_suites(&[
                        // TLS1.3 suites
                        rustls::cipher_suite::TLS13_AES_256_GCM_SHA384,
                        rustls::cipher_suite::TLS13_AES_128_GCM_SHA256,
                        // TLS1.2 suites
                        rustls::cipher_suite::TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
                        rustls::cipher_suite::TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        rustls::cipher_suite::TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
                        rustls::cipher_suite::TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        rustls::cipher_suite::TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256,
                    ])
                    .with_safe_default_kx_groups()
                    .with_safe_default_protocol_versions()
                    .expect("Error with the TLS configuration")
                    .with_native_roots()
                    .with_no_client_auth(),
            )
            .https_or_http()
            .enable_http1()
            .enable_http2()
            .build();
        let proxy_url = url
            .parse()
            .map_err(|e| error::ConnectionError::UrlParse(format!("{e}")))?;
        let proxy = ProxyConnector::from_proxy(connector, Proxy::new(Intercept::All, proxy_url))?;
        Ok(HyperClientBuilder::new().build(proxy))
    }
}

pub(crate) fn compute_chunk_size(total_size: u64) -> usize {
    std::cmp::max(
        (total_size as f64 / MAX_CHUNKS as f64).ceil() as usize,
        MINIMUM_CHUNK_SIZE,
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_compute_chunk_size() {
        let max_size_before_chunk_size = MAX_CHUNKS as u64 * MINIMUM_CHUNK_SIZE as u64;
        assert_eq!(compute_chunk_size(0), MINIMUM_CHUNK_SIZE);
        assert_eq!(compute_chunk_size(1), MINIMUM_CHUNK_SIZE);
        assert_eq!(
            compute_chunk_size(max_size_before_chunk_size),
            MINIMUM_CHUNK_SIZE
        );
        assert_eq!(
            compute_chunk_size(max_size_before_chunk_size + 1),
            MINIMUM_CHUNK_SIZE + 1
        );
    }
}
