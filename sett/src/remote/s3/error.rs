//! Error types for s3 operations

pub use aws_sdk_s3;

use crate::secret::SecretCorruptionError;

/// Reexports for foreign error types
mod aws {
    pub use super::aws_sdk_s3::{
        error::SdkError,
        operation::{
            complete_multipart_upload::CompleteMultipartUploadError,
            create_multipart_upload::CreateMultipartUploadError, get_object::GetObjectError,
            head_object::HeadObjectError, put_object::PutObjectError, upload_part::UploadPartError,
        },
        primitives::ByteStreamError,
    };
}

/// S3 get object error alias
pub type S3GetObjectError = aws::SdkError<aws::GetObjectError>;
/// S3 byte stream error alias
pub type S3ByteStreamError = aws::ByteStreamError;

/// Error occurring when setting up a new S3 client.
#[derive(Debug)]
pub enum ClientError {
    /// Credentials decoding error.
    CredentialsCorruption(SecretCorruptionError),
    /// Setup of the proxy error.
    Proxy(super::proxy::error::ConnectionError),
}

impl std::fmt::Display for ClientError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::CredentialsCorruption(_) => write!(f, "Credentials corruption"),
            Self::Proxy(_) => write!(f, "Failed to setup the proxy"),
        }
    }
}
impl std::error::Error for ClientError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::CredentialsCorruption(source) => Some(source),
            Self::Proxy(source) => Some(source),
        }
    }
}

impl From<SecretCorruptionError> for ClientError {
    fn from(value: SecretCorruptionError) -> Self {
        Self::CredentialsCorruption(value)
    }
}
impl From<super::proxy::error::ConnectionError> for ClientError {
    fn from(value: super::proxy::error::ConnectionError) -> Self {
        Self::Proxy(value)
    }
}

/// Error occurring while reading chunks from a stream
#[derive(Debug)]
pub enum ReadChunksError {
    /// IO error
    Io(tokio::io::Error),
    /// Error while transferring bytes
    Send(tokio::sync::mpsc::error::SendError<super::BytesMut>),
}

impl std::fmt::Display for ReadChunksError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Error while reading chunks")
    }
}

impl std::error::Error for ReadChunksError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Io(source) => Some(source),
            Self::Send(source) => Some(source),
        }
    }
}

impl From<tokio::io::Error> for ReadChunksError {
    fn from(value: tokio::io::Error) -> Self {
        Self::Io(value)
    }
}

impl From<tokio::sync::mpsc::error::SendError<super::BytesMut>> for ReadChunksError {
    fn from(value: tokio::sync::mpsc::error::SendError<super::BytesMut>) -> Self {
        Self::Send(value)
    }
}

/// Error occurring while uploading
#[derive(Debug)]
pub enum UploadError {
    /// IO error
    Io(tokio::io::Error),
    /// Invalid package
    InvalidPackage(crate::package::error::MetadataError),
    /// Wrong remote type
    InvalidRemotePath(crate::package::error::AttributeError),
    /// Error during put
    PutObject(put::Error),
    /// Error while reading chunks
    ReadChunks(ReadChunksError),
    /// Async task join error
    AsyncTask(tokio::task::JoinError),
}

impl std::fmt::Display for UploadError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Error while reading chunks")
    }
}

impl std::error::Error for UploadError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Io(source) => Some(source),
            Self::InvalidPackage(source) => Some(source),
            Self::InvalidRemotePath(source) => Some(source),
            Self::PutObject(source) => Some(source),
            Self::ReadChunks(source) => Some(source),
            Self::AsyncTask(source) => Some(source),
        }
    }
}

impl From<tokio::io::Error> for UploadError {
    fn from(value: tokio::io::Error) -> Self {
        Self::Io(value)
    }
}
impl From<crate::package::error::MetadataError> for UploadError {
    fn from(value: crate::package::error::MetadataError) -> Self {
        Self::InvalidPackage(value)
    }
}
impl From<crate::package::error::AttributeError> for UploadError {
    fn from(value: crate::package::error::AttributeError) -> Self {
        Self::InvalidRemotePath(value)
    }
}
impl From<put::Error> for UploadError {
    fn from(value: put::Error) -> Self {
        Self::PutObject(value)
    }
}
impl From<ReadChunksError> for UploadError {
    fn from(value: ReadChunksError) -> Self {
        Self::ReadChunks(value)
    }
}
impl From<tokio::task::JoinError> for UploadError {
    fn from(value: tokio::task::JoinError) -> Self {
        Self::AsyncTask(value)
    }
}

/// Namespace for [put::Error]
pub mod put {
    use super::aws;

    /// Error type for s3 put operations
    #[non_exhaustive]
    #[derive(Debug)]
    pub enum Error {
        /// Error during put
        Put(aws::SdkError<aws::PutObjectError>),
        /// Error during multipart put
        PutMultipart(aws::SdkError<aws::CreateMultipartUploadError>),
        /// Error while uploading a part
        UploadPart(aws::SdkError<aws::UploadPartError>),
        /// Fetching multipart id fails
        FetchMultipartId,
        /// Fetching entity tag fails
        FetchEntityTag,
        /// When nothing has been uploaded
        EmptyUpload,
        /// Completing multipart upload fails
        CompleteMultipartUpload(aws::SdkError<aws::CompleteMultipartUploadError>),
        /// Semaphore acquisition error
        Semaphore(tokio::sync::AcquireError),
        /// Async task join error
        Join(tokio::task::JoinError),
    }
    impl std::fmt::Display for Error {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                Self::Put(source) => std::fmt::Display::fmt(&super::Error(source), f),
                Self::PutMultipart(source) => std::fmt::Display::fmt(&super::Error(source), f),
                Self::FetchMultipartId => write!(f, "Multipart upload ID could not be fetched"),
                Self::UploadPart(source) => source.fmt(f),
                Self::Semaphore(_) => write!(f, "Semaphore error"),
                Self::EmptyUpload => write!(f, "No parts have been uploaded"),
                Self::FetchEntityTag => write!(
                    f,
                    "Could not retrieve the entity tag for the uploaded object"
                ),
                Self::CompleteMultipartUpload(source) => source.fmt(f),
                Self::Join(source) => source.fmt(f),
            }
        }
    }

    impl From<aws::SdkError<aws::PutObjectError>> for Error {
        fn from(value: aws::SdkError<aws::PutObjectError>) -> Self {
            Self::Put(value)
        }
    }

    impl From<aws::SdkError<aws::UploadPartError>> for Error {
        fn from(value: aws::SdkError<aws::UploadPartError>) -> Self {
            Self::UploadPart(value)
        }
    }

    impl From<aws::SdkError<aws::CreateMultipartUploadError>> for Error {
        fn from(value: aws::SdkError<aws::CreateMultipartUploadError>) -> Self {
            Self::PutMultipart(value)
        }
    }

    impl From<tokio::sync::AcquireError> for Error {
        fn from(value: tokio::sync::AcquireError) -> Self {
            Self::Semaphore(value)
        }
    }

    impl From<aws::SdkError<aws::CompleteMultipartUploadError>> for Error {
        fn from(value: aws::SdkError<aws::CompleteMultipartUploadError>) -> Self {
            Self::CompleteMultipartUpload(value)
        }
    }

    impl From<tokio::task::JoinError> for Error {
        fn from(value: tokio::task::JoinError) -> Self {
            Self::Join(value)
        }
    }

    impl std::error::Error for Error {
        fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
            match self {
                Self::Put(source) => Some(source),
                Self::PutMultipart(source) => Some(source),
                Self::UploadPart(source) => Some(source),
                Self::FetchMultipartId => None,
                Self::Semaphore(source) => Some(source),
                Self::EmptyUpload => None,
                Self::FetchEntityTag => None,
                Self::CompleteMultipartUpload(source) => Some(source),
                Self::Join(source) => Some(source),
            }
        }
    }
}

/// Namespace for [get::Error]
pub mod get {
    use super::aws;

    /// Error type for s3 fetch operations
    #[non_exhaustive]
    #[derive(Debug)]
    pub struct Error(pub aws::SdkError<aws::HeadObjectError>);
    impl std::fmt::Display for Error {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            std::fmt::Display::fmt(&super::Error(self.source()), f)
        }
    }

    impl Error {
        /// Access the source of the error
        pub fn source(&self) -> &aws::SdkError<aws::HeadObjectError> {
            &self.0
        }
    }

    impl From<aws::SdkError<aws::HeadObjectError>> for Error {
        fn from(value: aws::SdkError<aws::HeadObjectError>) -> Self {
            Self(value)
        }
    }

    impl std::error::Error for Error {
        fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
            Some(&self.0)
        }
    }
}

/// Generic errors for getting / putting an s3 object.
#[derive(Debug)]
struct Error<E>(E);

impl<E> std::fmt::Display for Error<&aws::SdkError<E>>
where
    E: ExtractMessage + MainMessage,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.0 {
            aws::SdkError::DispatchFailure(e) => {
                if let Some(connector_error) = e.as_connector_error() {
                    use std::error::Error;
                    if let Some(source) = connector_error.source() {
                        return write!(f, "could not connect to s3 store: {source}");
                    }
                }
            }
            aws::SdkError::ServiceError(e) => {
                if let Some(description) = e.err().extract_message() {
                    return write!(f, "{}: {description}", E::MAIN_MESSAGE);
                }
                if let Some(description) = e.raw().headers().get("x-minio-error-desc") {
                    return write!(f, "{}: {description}", E::MAIN_MESSAGE);
                }
            }
            _ => {}
        }
        write!(f, "{}: {}", E::MAIN_MESSAGE, self.0)
    }
}

trait MainMessage {
    const MAIN_MESSAGE: &'static str;
}

impl MainMessage for aws::HeadObjectError {
    const MAIN_MESSAGE: &'static str = "unable to access s3 object";
}

impl MainMessage for aws::PutObjectError {
    const MAIN_MESSAGE: &'static str = "unable to put s3 object";
}

impl MainMessage for aws::CreateMultipartUploadError {
    const MAIN_MESSAGE: &'static str = "unable to put s3 object";
}

trait ExtractMessage {
    fn extract_message(&self) -> Option<&str>;
}

impl ExtractMessage for aws::HeadObjectError {
    fn extract_message(&self) -> Option<&str> {
        self.meta().message()
    }
}

impl ExtractMessage for aws::PutObjectError {
    fn extract_message(&self) -> Option<&str> {
        self.meta().message()
    }
}

impl ExtractMessage for aws::CreateMultipartUploadError {
    fn extract_message(&self) -> Option<&str> {
        self.meta().message()
    }
}
