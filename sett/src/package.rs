//! Types and constants related to the data package format

use std::{
    collections::BTreeMap,
    path::{Path, PathBuf},
    str::FromStr,
};

use chrono::{DateTime, Utc};
use serde::{de::Visitor, Deserialize, Serialize, Serializer};
use tokio::io::AsyncReadExt as _;
use tracing::instrument;

/// Date format used for generating the default data package file name.
pub const DATETIME_FORMAT: &str = "%Y%m%dT%H%M%S";
/// Directory where decrypted and decompressed files are stored.
pub const CONTENT_FOLDER: &str = "content";
/// File containing checksums of individual input files.
pub const CHECKSUM_FILE: &str = "checksum.sha256";
/// Archive file containing all input files.
pub const DATA_FILE: &str = "data.tar.gz";
/// Encrypted archive file.
pub const DATA_FILE_ENCRYPTED: &str = "data.tar.gz.gpg";
/// File containing package metadata.
pub const METADATA_FILE: &str = "metadata.json";
/// Detached signature of the metadata file.
pub const METADATA_SIG_FILE: &str = "metadata.json.sig";

/// Data package verification state.
pub mod state {
    /// A marker type for a data package that has been verified.
    #[derive(Debug, Clone)]
    pub struct Verified;
    /// A marker type for a data package that has not been verified.
    #[derive(Debug, Clone)]
    pub struct Unverified;

    /// This is a sealed trait, it cannot be implemented outside of this crate.
    pub trait State: sealed::Sealed {}
    impl State for Unverified {}
    impl State for Verified {}

    mod sealed {
        pub trait Sealed {}
        impl Sealed for super::Unverified {}
        impl Sealed for super::Verified {}
    }
}

#[derive(Debug, Clone)]
pub(crate) enum Source {
    Local(PathBuf),
    S3(S3Source),
}

#[derive(Debug, Clone)]
pub(crate) struct S3Source {
    pub(crate) client: crate::remote::s3::Client,
    pub(crate) bucket: String,
    pub(crate) object: String,
}

/// A type for securely working with the data package format.
///
/// A data package is a ZIP file containing the following files:
/// - `metadata.json`: metadata about the package
/// - `metadata.json.sig`: detached signature of the metadata file
/// - `data.tar.gz.gpg`: encrypted archive of the input files
///
/// This type is generic over the verification state of the package.  A
/// package that has been verified is in the [`Verified`] state, and a
/// package that has not been verified is in the [`Unverified`] state.
/// [`Package::verify`] can be used to transition a package from the
/// [`Unverified`] state to the [`Verified`] state.
///
/// [`Verified`]: state::Verified
/// [`Unverified`]: state::Unverified
#[derive(Clone)]
pub struct Package<State: state::State = state::Unverified> {
    state: std::marker::PhantomData<State>,
    zip: crate::zip::ZipReader,
    name: String,
}

impl<State: state::State> std::fmt::Debug for Package<State> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Package")
            .field("name", &self.name)
            .field("source", &self.zip.source)
            .finish()
    }
}

/// Error namespace
pub mod error {
    use crate::openpgp::error::PgpError;
    use crate::zip::error::FileReaderError;

    /// Error occurring when trying to get the path of a remote source
    #[derive(Debug)]
    pub struct AttributeError;
    impl std::fmt::Display for AttributeError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "package path is not available for non-local sources")
        }
    }
    impl std::error::Error for AttributeError {}

    /// I/O error including info about the involved file name
    #[derive(Debug)]
    pub struct FileIoError<S, E> {
        /// file name
        pub file: S,
        /// Lower level cause
        pub source: E,
    }

    impl<E: std::fmt::Display, S: std::fmt::Display> std::fmt::Display for FileIoError<S, E> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{}: {}", self.file, self.source)
        }
    }
    impl<S: std::fmt::Display + std::fmt::Debug, E: std::fmt::Display + std::fmt::Debug>
        std::error::Error for FileIoError<S, E>
    {
    }

    /// Error related to package metadata
    #[derive(Debug)]
    pub enum MetadataError {
        /// I/O errors
        Io(FileIoError<&'static str, std::io::Error>),
        /// Reader error
        Reader(FileIoError<&'static str, FileReaderError>),
        /// json deserialization error
        Deserialize(serde_json::Error),
    }

    impl std::fmt::Display for MetadataError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "Metadata error")
        }
    }

    impl std::error::Error for MetadataError {
        fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
            match self {
                Self::Io(source) => Some(source),
                Self::Reader(source) => Some(source),
                Self::Deserialize(source) => Some(source),
            }
        }
    }
    impl From<FileIoError<&'static str, std::io::Error>> for MetadataError {
        fn from(value: FileIoError<&'static str, std::io::Error>) -> Self {
            Self::Io(value)
        }
    }
    impl From<FileIoError<&'static str, FileReaderError>> for MetadataError {
        fn from(value: FileIoError<&'static str, FileReaderError>) -> Self {
            Self::Reader(value)
        }
    }
    impl From<serde_json::Error> for MetadataError {
        fn from(value: serde_json::Error) -> Self {
            Self::Deserialize(value)
        }
    }

    /// Error related to package metadata
    #[derive(Debug)]
    pub enum PackageOpenError {
        /// I/O errors
        Io(FileIoError<String, std::io::Error>),
        /// Reader error
        Reader(crate::zip::error::ReaderError),
    }

    impl std::fmt::Display for PackageOpenError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "Metadata error")
        }
    }

    impl std::error::Error for PackageOpenError {
        fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
            match self {
                Self::Io(source) => Some(source),
                Self::Reader(source) => Some(source),
            }
        }
    }
    impl From<FileIoError<String, std::io::Error>> for PackageOpenError {
        fn from(value: FileIoError<String, std::io::Error>) -> Self {
            Self::Io(value)
        }
    }
    impl From<crate::zip::error::ReaderError> for PackageOpenError {
        fn from(value: crate::zip::error::ReaderError) -> Self {
            Self::Reader(value)
        }
    }

    /// Error occurring when deserializing a purpose
    #[derive(Debug)]
    pub struct InvalidPurposeError(pub String);
    impl std::fmt::Display for InvalidPurposeError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "Invalid purpose: {}", self.0)
        }
    }
    impl std::error::Error for InvalidPurposeError {}

    /// Error occurring when verifying a package
    #[derive(Debug)]
    pub enum VerificationError {
        /// Package contains unexpected files
        UnexpectedFiles(&'static [&'static str]),
        /// The package is missing some required files
        MissingFiles(Vec<String>, &'static [&'static str]),
        /// Invalid metadata
        Metadata(MetadataError),
        /// Signature verification error
        Signature(PgpError),
    }
    impl std::fmt::Display for VerificationError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            let error_msg_base = |expected: &[&str]| {
                format!(
                    "A valid data package must contain exactly the following {} files: {}",
                    expected.len(),
                    expected.join(", ")
                )
            };
            match self {
                Self::UnexpectedFiles(expected) => write!(
                    f,
                    "invalid data package. Zip archive contains unexpected \
                    files. {}.",
                    error_msg_base(expected)
                ),
                Self::MissingFiles(files, expected) => write!(
                    f,
                    "invalid data package. Zip archive is missing the following \
            files: {}. {}.",
                    files.join(", "),
                    error_msg_base(expected)
                ),
                _ => write!(f, "Package verification error"),
            }
        }
    }
    impl std::error::Error for VerificationError {
        fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
            match self {
                Self::Metadata(source) => Some(source),
                Self::Signature(source) => Some(source),
                _ => None,
            }
        }
    }
    impl From<MetadataError> for VerificationError {
        fn from(value: MetadataError) -> Self {
            Self::Metadata(value)
        }
    }
    impl From<PgpError> for VerificationError {
        fn from(value: PgpError) -> Self {
            Self::Signature(value)
        }
    }
}

impl<State: state::State> Package<State> {
    pub(crate) fn name(&self) -> &str {
        &self.name
    }

    pub(crate) fn path(&self) -> Result<&Path, error::AttributeError> {
        if let Source::Local(path) = &self.zip.source {
            Ok(path)
        } else {
            Err(error::AttributeError)
        }
    }
}

impl<S: state::State> Package<S> {
    async fn read_inner(&self, file: &'static str) -> Result<Vec<u8>, error::MetadataError> {
        let (mut reader, size) = self
            .zip
            .get_file_reader(file)
            .await
            .map_err(|source| error::FileIoError { file, source })?;
        let mut buffer = Vec::with_capacity(size as usize);
        reader
            .read_to_end(&mut buffer)
            .await
            .map_err(|source| error::FileIoError { file, source })?;
        Ok(buffer)
    }

    async fn read_metadata(&self) -> Result<Metadata, error::MetadataError> {
        let buf = self.read_inner(METADATA_FILE).await?;
        Ok(serde_json::from_slice(&buf)?)
    }
}

impl Package<state::Unverified> {
    /// Opens a data package from the local file system.
    #[instrument(fields(path = %path.as_ref().display()), err(Debug, level=tracing::Level::ERROR))]
    pub async fn open(path: impl AsRef<Path>) -> Result<Self, error::PackageOpenError> {
        let path = path.as_ref();
        if !path.exists() {
            return Err(error::PackageOpenError::Io(error::FileIoError {
                file: path.to_string_lossy().into(),
                source: std::io::Error::from(std::io::ErrorKind::NotFound),
            }));
        }
        let path = path.canonicalize().map_err(|source| error::FileIoError {
            source,
            file: String::from(path.to_string_lossy()),
        })?;
        let name = path
            .file_name()
            .ok_or(error::PackageOpenError::Io(error::FileIoError {
                source: std::io::Error::from(std::io::ErrorKind::IsADirectory),
                file: path.to_string_lossy().to_string(),
            }))?
            .to_string_lossy()
            .to_string();
        Ok(Package {
            state: Default::default(),
            name,
            zip: crate::zip::ZipReader::open(Source::Local(path)).await?,
        })
    }

    /// Opens a data package from an object store.
    #[instrument(err(Debug, level=tracing::Level::ERROR))]
    pub async fn open_s3(
        client: &crate::remote::s3::Client,
        bucket: String,
        object: String,
    ) -> Result<Self, crate::zip::error::ReaderError> {
        let source = Source::S3(S3Source {
            client: client.clone(),
            bucket,
            object: object.clone(),
        });
        Ok(Package {
            state: Default::default(),
            zip: crate::zip::ZipReader::open(source).await?,
            name: object,
        })
    }

    /// Verifies the Zip archive has the correct structure for a data package.
    ///
    /// A data package must only contain exactly the expected files.
    fn verify_format(&self) -> Result<(), error::VerificationError> {
        const EXPECTED_FILES: &[&str] = &[DATA_FILE_ENCRYPTED, METADATA_FILE, METADATA_SIG_FILE];
        // Search the .zip archive for the expected files.
        let mut actual_files = Vec::new();
        for file_name in self.zip.file_names() {
            if EXPECTED_FILES.contains(&file_name) {
                actual_files.push(file_name);
            } else {
                return Err(error::VerificationError::UnexpectedFiles(EXPECTED_FILES));
            }
        }

        // If exactly all expected files are present, verification is complete.
        if actual_files.len() == EXPECTED_FILES.len() {
            return Ok(());
        }

        Err(error::VerificationError::MissingFiles(
            EXPECTED_FILES
                .iter()
                .filter(|f| !actual_files.contains(f))
                .map(|f| f.to_string())
                .collect::<Vec<_>>(),
            EXPECTED_FILES,
        ))
    }

    /// Verify the signature of a data package.
    ///
    /// Verifies the signature of the metadata file contained in the data
    /// package. If the signature is valid, the function returns a [`Package`]
    /// in [`Verified`] state. Otherwise, the function returns an error.
    ///
    /// [`Verified`]: state::Verified
    pub async fn verify(
        self,
        cert_store: &crate::openpgp::certstore::CertStore<'_>,
    ) -> Result<Package<state::Verified>, error::VerificationError> {
        self.verify_format()?;
        crate::openpgp::cert::verify_file_signature(
            &self.read_inner(METADATA_FILE).await?,
            &self.read_inner(METADATA_SIG_FILE).await?,
            cert_store,
        )?;
        Ok(Package {
            state: Default::default(),
            zip: self.zip,
            name: self.name,
        })
    }

    /// Reads metadata from a data package file without verification.
    ///
    /// Use this method only for low-level inspection. Prefer using the
    /// `metadata` method from [`Package<state::Verified>`].
    pub async fn metadata_unverified(&self) -> Result<Metadata, error::MetadataError> {
        self.read_metadata().await
    }
}

impl Package<state::Verified> {
    /// Reads metadata from a data package file.
    pub async fn metadata(&self) -> Result<Metadata, error::MetadataError> {
        self.read_metadata().await
    }

    /// Reads the encrypted data file from a data package.
    ///
    /// Returns a reader for the encrypted data file and its size.
    pub(crate) async fn data(
        &self,
    ) -> Result<
        (Box<dyn tokio::io::AsyncBufRead + Unpin + Sync + Send>, u64),
        crate::zip::error::FileReaderError,
    > {
        self.zip.get_file_reader(DATA_FILE_ENCRYPTED).await
    }
}

/// Package Metadata struct
#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct Metadata {
    /// Fingerprint of the data package sender.
    pub sender: String,
    /// Data package recipients fingerprints.
    pub recipients: Vec<String>,
    /// Checksum of the encrypted data file.
    pub checksum: String,
    /// Creation time of the data package.
    ///
    /// (De)serialized using the RFC 3339 format.
    pub timestamp: DateTime<Utc>,
    /// Metadata version.
    #[serde(default = "default_version")]
    pub version: String,
    /// Algorithm used to compute the checksum of the encrypted data file.
    #[serde(default)]
    pub checksum_algorithm: ChecksumAlgorithm,
    #[serde(default)]
    /// Algorithm used to compress input data files.
    pub compression_algorithm: CompressionAlgorithm,
    #[serde(default)]
    /// Data transfer ID (DTR).
    pub transfer_id: Option<u32>,
    #[serde(default)]
    /// Data package purpose.
    pub purpose: Option<Purpose>,
    #[serde(default)]
    /// Extra metadata key-value fields.
    pub extra: BTreeMap<String, String>,
}

impl Metadata {
    /// Serializes the metadata to JSON or a debug string if serialization fails.
    ///
    /// Note, it's very unlikely for serialization to fail.
    pub(crate) fn to_json_or_debug(&self) -> String {
        serde_json::to_string(self).unwrap_or_else(|_| format!("{:?}", self))
    }
}

/// Possible checksum algorithms for the encrypted data file.
#[derive(Deserialize, Serialize, Debug, Default, Clone, Copy, PartialEq, Eq)]
pub enum ChecksumAlgorithm {
    /// sha256
    #[default]
    SHA256,
}

/// Possible compression algorithms for data.
///
/// Note: compression is applied before encryption.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CompressionAlgorithm {
    /// No compression
    Stored,
    /// Use gzip compression (level 1-9)
    Gzip(Option<u32>),
    /// Use zstandard compression (level 1-21)
    Zstandard(Option<i32>),
}

impl Default for CompressionAlgorithm {
    fn default() -> Self {
        Self::Zstandard(None)
    }
}

impl Serialize for CompressionAlgorithm {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match *self {
            CompressionAlgorithm::Stored => {
                serializer.serialize_unit_variant("CompressionAlgorithm", 0, "stored")
            }
            CompressionAlgorithm::Gzip(_) => {
                serializer.serialize_unit_variant("CompressionAlgorithm", 1, "gzip")
            }
            CompressionAlgorithm::Zstandard(_) => {
                serializer.serialize_unit_variant("CompressionAlgorithm", 2, "zstandard")
            }
        }
    }
}

impl<'de> Deserialize<'de> for CompressionAlgorithm {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct CompressionAlgorithmVisitor;

        impl Visitor<'_> for CompressionAlgorithmVisitor {
            type Value = CompressionAlgorithm;

            fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                formatter.write_str("one of `stored`, `gzip`, `zstandard`")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                match v.to_lowercase().as_str() {
                    "stored" => Ok(CompressionAlgorithm::Stored),
                    "gzip" => Ok(CompressionAlgorithm::Gzip(None)),
                    "zstandard" => Ok(CompressionAlgorithm::Zstandard(None)),
                    _ => Err(E::custom(format!("unknown variant `{}`", v))),
                }
            }
        }
        deserializer.deserialize_str(CompressionAlgorithmVisitor {})
    }
}

/// Returns the current default metadata version.
pub fn default_version() -> String {
    "0.7.2".into()
}

/// Data package purpose determines if data is meant for testing or production.
#[derive(Copy, Clone, Deserialize, Serialize, Debug)]
pub enum Purpose {
    /// For sensitive data
    PRODUCTION,
    /// Only for testing
    TEST,
}

impl FromStr for Purpose {
    type Err = error::InvalidPurposeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "production" => Ok(Self::PRODUCTION),
            "test" => Ok(Self::TEST),
            _ => Err(error::InvalidPurposeError(s.to_string())),
        }
    }
}

/// Generates the default name for the data package based on a timestamp.
pub(crate) fn generate_package_name(timestamp: &DateTime<Utc>, prefix: Option<&str>) -> String {
    let ts = timestamp.format(DATETIME_FORMAT);
    if let Some(prefix) = prefix {
        format!("{prefix}_{ts}.zip")
    } else {
        format!("{ts}.zip")
    }
}
