//! Portal API response types.

use serde::Deserialize;

#[cfg(feature = "auth")]
use serde::Serialize;

#[derive(Debug, Clone, PartialEq, Eq, Deserialize)]
/// Check package response from the Portal API.
pub struct CheckPackage {
    /// The project code of the package.
    ///
    /// It is derived from the `transfer_id` field of the package metadata.
    pub project_code: String,
}

#[derive(Deserialize, Debug)]
/// Error response from the Portal API when checking a package.
pub(super) struct PortalError {
    /// The error message.
    pub(super) detail: String,
}

#[cfg(feature = "auth")]
#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
/// Connection details (location and credentials) for an S3 instance.
pub struct S3ConnectionDetails {
    /// Access key
    pub access_key_id: String,
    /// Secret key
    pub secret_access_key: String,
    /// Session token
    pub session_token: String,
    /// S3 instance URL
    pub endpoint: String,
    /// Bucket name
    pub bucket: String,
}

#[cfg(feature = "auth")]
impl S3ConnectionDetails {
    /// Builds [Client](`crate::remote::s3::Client`) from connection details.
    pub async fn build_client(
        &self,
    ) -> Result<crate::remote::s3::Client, crate::remote::s3::error::ClientError> {
        crate::remote::s3::Client::builder()
            .endpoint(Some(self.endpoint.trim_end_matches('/')))
            .access_key(Some(self.access_key_id.as_str()))
            .secret_key(Some(self.secret_access_key.as_str()))
            .session_token(Some(self.session_token.as_str()))
            .build()
            .await
    }
}

#[cfg(feature = "auth")]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize, Serialize)]
#[serde(rename_all(deserialize = "SCREAMING_SNAKE_CASE"))]
/// Possible statuses for a data transfer request on Portal.
pub enum DataTransferStatus {
    /// The data transfer is undergoing the approval process.
    Initial,
    /// The data transfer has been approved by all parties and ready to be
    /// performed.
    Authorized,
    /// The data transfer has been rejected by at least one the parties and will
    /// not be performed.
    Unauthorized,
    /// The data transfer is expired and cannot be performed.
    Expired,
}

#[cfg(feature = "auth")]
#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
/// Portal's representation of data transfer request.
pub struct DataTransfer {
    /// Unique ID.
    pub id: u32,
    /// Name of the data provider sending the data.
    pub data_provider_name: String,
    /// Name of the project receiving the data.
    pub project_name: String,
    /// Current status of the data transfer request.
    pub status: DataTransferStatus,
}
