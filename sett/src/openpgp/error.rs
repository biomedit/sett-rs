//! Error namespace

/// Wrapper for sequoia_openpgp's opaque error type
#[derive(Debug)]
pub struct PgpError(pub String);
impl std::fmt::Display for PgpError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}
impl std::error::Error for PgpError {}

impl PgpError {
    pub(crate) fn from(error: impl std::fmt::Display) -> Self {
        Self(format!("{error}"))
    }
}

/// Error for opengpg related code
#[derive(Debug)]
pub enum Error {
    /// pgp error
    Pgp(PgpError),
    /// Async task join error
    AsyncTask(tokio::task::JoinError),
    /// IO error
    IO(std::io::Error),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Pgp(e) => e.fmt(f),
            Self::AsyncTask(e) => e.fmt(f),
            Self::IO(e) => e.fmt(f),
        }
    }
}
impl std::error::Error for Error {}

impl From<PgpError> for Error {
    fn from(value: PgpError) -> Self {
        Self::Pgp(value)
    }
}

impl From<tokio::task::JoinError> for Error {
    fn from(value: tokio::task::JoinError) -> Self {
        Self::AsyncTask(value)
    }
}

impl From<std::io::Error> for Error {
    fn from(value: std::io::Error) -> Self {
        Self::IO(value)
    }
}
