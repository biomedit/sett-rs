//! OpenPGP certificates

use super::error;
use sequoia_openpgp::{
    cert::amalgamation::ValidateAmalgamation as _,
    parse::Parse as _,
    policy::StandardPolicy,
    serialize::{Serialize as _, SerializeInto as _},
};
use tracing::warn;

/// Internal representation of the supported cipher suites.
#[derive(Default, Copy, Clone)]
pub enum CipherSuite {
    #[default]
    /// Elliptic-curve cryptography with 256-bit keys.
    Cv25519,
    /// RSA cryptography with 4096-bit keys.
    RSA4k,
}

/// A unique identifier for OpenPGP certificates and keys.
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Fingerprint(pub(crate) sequoia_openpgp::Fingerprint);

impl Fingerprint {
    /// Returns the fingerprint as a hexadecimal string.
    ///
    /// This representation uses only uppercase characters without spaces.
    pub fn to_hex(&self) -> String {
        self.0.to_hex()
    }
}

impl std::fmt::Display for Fingerprint {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::str::FromStr for Fingerprint {
    type Err = error::PgpError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(s.parse().map_err(error::PgpError::from)?))
    }
}

impl std::fmt::Debug for Fingerprint {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.0)
    }
}

/// OpenPGP certificate.
#[derive(Clone, Debug, PartialEq)]
pub struct Cert(pub(crate) sequoia_openpgp::cert::Cert);

impl Cert {
    /// Returns the fingerprint of the certificate.
    ///
    /// The fingerprint is a unique identifier for the certificate.
    /// It equal to the fingerprint of the primary key.
    pub fn fingerprint(&self) -> Fingerprint {
        Fingerprint(self.0.fingerprint())
    }

    /// Returns the valid keys of the certificate.
    pub fn keys(&self) -> Vec<Key> {
        self.0
            .with_policy(&StandardPolicy::new(), None)
            .map_or(vec![], |valid_cert| {
                valid_cert
                    .keys()
                    .map(|key| Key {
                        fingerprint: Fingerprint(key.fingerprint()),
                    })
                    .collect()
            })
    }

    /// Returns the valid user IDs of the certificate.
    pub fn userids(&self) -> Vec<String> {
        // Only consider valid certificates, i.e. certificates that are
        // correctly self-signed.
        self.0
            .with_policy(&StandardPolicy::new(), None)
            .map_or(vec![], |valid_cert| {
                valid_cert
                    .userids()
                    .map(|uid| uid.userid().to_string())
                    .collect()
            })
    }

    /// Verifies that the certificate has at least one valid self-signature.
    pub(super) fn validate(&self) -> Result<(), error::PgpError> {
        self.0
            .with_policy(&StandardPolicy::new(), None)
            .map_err(|e| {
                error::PgpError(format!(
                    "Invalid key: no valid self-signature was found. {e}"
                ))
            })?;
        Ok(())
    }

    /// Returns certificate's expiration time.
    ///
    /// Returns `None` if the certificate does not have an expiration.
    pub fn expiration_time(&self) -> Result<Option<std::time::SystemTime>, error::PgpError> {
        Ok(self
            .0
            .primary_key()
            .with_policy(&StandardPolicy::new(), None)
            .map_err(error::PgpError::from)?
            .key_expiration_time())
    }

    /// Sets the certificate's expiration time.
    ///
    /// The expiration time is set for the primary key, all subkeys, and all user IDs.
    pub async fn set_expiration_time(
        mut self,
        signer: crate::openpgp::keystore::Key,
        expiration_time: Option<std::time::SystemTime>,
    ) -> Result<Self, error::Error> {
        use sequoia_openpgp::packet::signature::SignatureBuilder;
        let mut signer = signer.inner;
        tokio::task::spawn_blocking(move || -> Result<_, error::Error> {
            let now = std::time::SystemTime::now();
            let policy = StandardPolicy::new();
            let mut signatures = Vec::new();
            // Subkeys
            for key in self.0.keys().subkeys() {
                signatures.push(
                    key.bind(
                        &mut signer,
                        &self.0,
                        SignatureBuilder::from(
                            key.binding_signature(&policy, now)
                                .or_else(|_| {
                                    key.self_signatures()
                                        .next()
                                        .ok_or(error::PgpError::from("no subkey binding signature"))
                                })?
                                .clone(),
                        )
                        .set_signature_creation_time(now)
                        .map_err(error::PgpError::from)?
                        .set_key_expiration_time(key.key(), expiration_time)
                        .map_err(error::PgpError::from)?,
                    )
                    .map_err(error::PgpError::from)?,
                );
            }
            // Primary key
            signatures.push(
                SignatureBuilder::from(
                    self.0
                        .primary_key()
                        .binding_signature(&policy, now)
                        .or_else(|_| {
                            self.0
                                .primary_key()
                                .self_signatures()
                                .next()
                                .ok_or(error::PgpError::from("no primary key signature"))
                        })?
                        .clone(),
                )
                .set_type(sequoia_openpgp::types::SignatureType::DirectKey)
                .set_signature_creation_time(now)
                .map_err(error::PgpError::from)?
                .set_key_expiration_time(self.0.primary_key().key(), expiration_time)
                .map_err(error::PgpError::from)?
                .sign_direct_key(&mut signer, None)
                .map_err(error::PgpError::from)?,
            );
            // User IDs
            for uid in self.0.userids() {
                signatures.push(
                    uid.bind(
                        &mut signer,
                        &self.0,
                        SignatureBuilder::from(
                            uid.binding_signature(&policy, now)
                                .or_else(|_| {
                                    uid.self_signatures().next().ok_or(error::PgpError::from(
                                        "no user ID binding signature",
                                    ))
                                })?
                                .clone(),
                        )
                        .set_signature_creation_time(now)
                        .map_err(error::PgpError::from)?
                        .set_key_expiration_time(self.0.primary_key().key(), expiration_time)
                        .map_err(error::PgpError::from)?,
                    )
                    .map_err(error::PgpError::from)?,
                );
            }
            self.0 = self
                .0
                .insert_packets(signatures)
                .map_err(error::PgpError::from)?;
            Ok(self)
        })
        .await?
    }

    /// Return the first certificate found in the input bytes.
    pub fn from_bytes(data: impl AsRef<[u8]>) -> Result<Self, super::error::PgpError> {
        Ok(Self(
            sequoia_openpgp::cert::Cert::from_bytes(data.as_ref())
                .map_err(super::error::PgpError::from)?,
        ))
    }

    /// Return the certificate type.
    pub fn cert_type(&self) -> CertType {
        if self.0.is_tsk() {
            CertType::Secret
        } else {
            CertType::Public
        }
    }

    /// Check if the certificate's primary key is encrypted.
    pub fn is_encrypted(&self) -> Result<bool, error::PgpError> {
        Ok(self
            .0
            .primary_key()
            .key()
            .clone()
            .parts_into_secret()
            .map_err(error::PgpError::from)?
            .secret()
            .is_encrypted())
    }

    /// Return the revocation status of the certificate.
    pub fn revocation_status(&self) -> RevocationStatus {
        use sequoia_openpgp::types::RevocationStatus::*;
        match self.0.revocation_status(&StandardPolicy::new(), None) {
            Revoked(s) => {
                RevocationStatus::Revoked(s.into_iter().map(|s| Signature(s.clone())).collect())
            }
            CouldBe(s) => {
                RevocationStatus::CouldBe(s.into_iter().map(|s| Signature(s.clone())).collect())
            }
            NotAsFarAsWeKnow => RevocationStatus::NotAsFarAsWeKnow,
        }
    }

    /// Generate revocation signature.
    ///
    /// Use the certificate's primary key to generate a revocation signature.
    pub fn generate_rev_sig(
        &self,
        code: ReasonForRevocation,
        reason: &[u8],
        password: Option<&[u8]>,
    ) -> Result<Vec<u8>, error::PgpError> {
        if !self.0.is_tsk() {
            return Err(error::PgpError::from("Secret material is missing, cannot generate a revocation signature for a public key."));
        }
        let secret = self
            .0
            .primary_key()
            .key()
            .clone()
            .parts_into_secret()
            .map_err(error::PgpError::from)?;
        let mut signer = if secret.secret().is_encrypted() {
            if let Some(password) = password {
                secret
                    .decrypt_secret(&password.into())
                    .map_err(|_| error::PgpError::from("Wrong password"))?
                    .into_keypair()
                    .map_err(error::PgpError::from)?
            } else {
                return Err(error::PgpError::from(
                    "The OpenPGP key is encrypted, but no password was provided.",
                ));
            }
        } else {
            secret.into_keypair().map_err(error::PgpError::from)?
        };
        serialize_revocation_signature(
            &self.0,
            self.0
                .revoke(&mut signer, code.into_sequoia(), reason)
                .map_err(error::PgpError::from)?,
        )
    }

    /// Revoke the certificate.
    pub fn revoke<R: std::io::Read + Send + Sync>(
        &self,
        signature: R,
    ) -> Result<Self, error::PgpError> {
        let mut revocations = Vec::new();
        let mut ppr = sequoia_openpgp::parse::PacketParserBuilder::from_reader(signature)
            .map_err(error::PgpError::from)?
            .buffer_unread_content()
            .build()
            .map_err(error::PgpError::from)?;
        while let sequoia_openpgp::parse::PacketParserResult::Some(pp) = ppr {
            let (packet, next_ppr) = pp.recurse().map_err(error::PgpError::from)?;
            ppr = next_ppr;

            if let sequoia_openpgp::Packet::Signature(sig) = packet {
                if matches!(
                    sig.typ(),
                    sequoia_openpgp::types::SignatureType::CertificationRevocation
                        | sequoia_openpgp::types::SignatureType::KeyRevocation
                        | sequoia_openpgp::types::SignatureType::SubkeyRevocation
                ) {
                    revocations.push(sig);
                }
            }
        }
        if revocations.is_empty() {
            return Err(error::PgpError::from("No revocation signature found"));
        }
        Ok(Self(
            self.0
                .clone()
                .insert_packets(revocations)
                .map_err(error::PgpError::from)?,
        ))
    }

    /// Updates the certificate with the secret material from the specified key
    /// store.
    ///
    /// Returns the certificate enriched with the secret material, or an error
    /// if the secret material for the primary key or any subkey cannot be
    /// found in the specified keystore.
    pub async fn update_secret_material(
        self,
        key_store: &mut crate::openpgp::keystore::KeyStore,
    ) -> Result<Cert, error::PgpError> {
        let mut packets: Vec<sequoia_openpgp::Packet> = Vec::new();
        let primary_fingerprint = self.0.primary_key().fingerprint();

        let primary_or_subkey_text = |fingerprint: &sequoia_openpgp::Fingerprint| {
            if fingerprint == &primary_fingerprint {
                "".to_string()
            } else {
                format!(" the subkey '{}' of", fingerprint)
            }
        };

        // For each key (primary and all subkeys) of the certificate, retrieve
        // the secret material associated with it from the specified keystore
        // (keystores contain the key's secret material).
        for fingerprint in self.0.keys().map(|key| key.fingerprint()) {
            // Verify that secret material for the key exists in the keystore.
            if key_store
                .inner
                .find_key_async(fingerprint.clone().into())
                .await
                .map_err(error::PgpError::from)?
                .is_empty()
            {
                return Err(error::PgpError(format!(
                    "No secret material was found in the keystore for{primary_fingerprint} \
                the certificate '{}'",
                    primary_or_subkey_text(&fingerprint)
                )));
            }

            // Get the key's secret material.
            let secret_key = key_store.export(&fingerprint).await.map_err(|_| {
                error::PgpError(format!(
                    "Secret material for{} the certificate '{primary_fingerprint}' \
                cannot be exported from the keystore",
                    primary_or_subkey_text(&fingerprint)
                ))
            })?;
            if fingerprint == primary_fingerprint {
                packets.push(secret_key.role_into_primary().into())
            } else {
                packets.push(secret_key.role_into_subordinate().into())
            }
        }

        // Return a certificate based on the original certificate, enriched
        // with the secret material retrieved for all primary/sub-keys.
        let (cert, _modified) = self
            .0
            .insert_packets2(packets)
            .map_err(error::PgpError::from)?;
        Ok(Cert(cert))
    }

    /// Retrieves the email of the certificate's primary user ID.
    pub fn get_primary_email(&self) -> Result<Option<String>, error::PgpError> {
        self.0
            .with_policy(&StandardPolicy::new(), None)
            .map_err(error::PgpError::from)?
            .primary_userid()
            .map_err(error::PgpError::from)?
            .email2()
            .map_err(error::PgpError::from)
            .map(|s| s.map(String::from))
    }

    /// Return a view to the public part of the key for export
    pub fn public(&self) -> CertDataView<&sequoia_openpgp::cert::Cert> {
        CertDataView(&self.0)
    }
    /// Return a view to the secret part of the key for export
    pub fn secret(&self) -> Result<CertDataView<SecretCertDataView<'_>>, error::PgpError> {
        if !self.0.is_tsk() {
            return Err(error::PgpError::from("Secret material is missing, cannot generate a revocation signature for a public key."));
        }
        Ok(CertDataView(SecretCertDataView(&self.0)))
    }
}

impl std::fmt::Display for Cert {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let user_id = self
            .0
            .with_policy(&StandardPolicy::new(), None)
            .map_or_else(
                |_| String::from("Invalid key: no valid self-signature was found"),
                |valid_cert| {
                    valid_cert.primary_userid().map_or_else(
                        |_| String::from("Missing or no valid user ID"),
                        |user_id| user_id.userid().to_string(),
                    )
                },
            );
        // `Name <email> [fingerprint]`
        write!(f, "{} [{}]", user_id, self.fingerprint().to_hex())
    }
}

/// View to either the public or the secret part of a certificate.
///
/// The view can be converted to [`Vec<u8>`] or [AsciiArmored] via [TryFrom] and
/// serde serialized in ascii armored format.
#[derive(Debug, Clone, Copy)]
pub struct CertDataView<T>(T);

/// Ascii armored export format for a [CertDataView] via [TryFrom].
#[derive(Debug)]
pub struct AsciiArmored(pub(crate) Vec<u8>);

impl AsRef<[u8]> for AsciiArmored {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

impl std::ops::Deref for AsciiArmored {
    type Target = [u8];
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<AsciiArmored> for Vec<u8> {
    fn from(value: AsciiArmored) -> Self {
        value.0
    }
}

/// Helper type to work around the lack of Copy of [sequoia_openpgp::serialize::TSK].
#[derive(Debug, Copy, Clone)]
pub struct SecretCertDataView<'a>(&'a sequoia_openpgp::cert::Cert);

impl TryFrom<CertDataView<&sequoia_openpgp::cert::Cert>> for Vec<u8> {
    type Error = super::error::PgpError;
    fn try_from(value: CertDataView<&sequoia_openpgp::cert::Cert>) -> Result<Self, Self::Error> {
        value.0.to_vec().map_err(Self::Error::from)
    }
}
impl TryFrom<CertDataView<SecretCertDataView<'_>>> for Vec<u8> {
    type Error = super::error::PgpError;
    fn try_from(value: CertDataView<SecretCertDataView<'_>>) -> Result<Self, Self::Error> {
        value.0 .0.as_tsk().to_vec().map_err(Self::Error::from)
    }
}

impl TryFrom<CertDataView<&sequoia_openpgp::cert::Cert>> for AsciiArmored {
    type Error = super::error::PgpError;
    fn try_from(value: CertDataView<&sequoia_openpgp::cert::Cert>) -> Result<Self, Self::Error> {
        Ok(Self(value.0.armored().to_vec().map_err(Self::Error::from)?))
    }
}

impl TryFrom<CertDataView<SecretCertDataView<'_>>> for AsciiArmored {
    type Error = super::error::PgpError;
    fn try_from(value: CertDataView<SecretCertDataView<'_>>) -> Result<Self, Self::Error> {
        Ok(Self(
            value
                .0
                 .0
                .as_tsk()
                .armored()
                .to_vec()
                .map_err(Self::Error::from)?,
        ))
    }
}

impl<T> serde::Serialize for CertDataView<T>
where
    AsciiArmored: TryFrom<CertDataView<T>>,
    <AsciiArmored as TryFrom<CertDataView<T>>>::Error: std::fmt::Display,
    CertDataView<T>: Copy,
{
    fn serialize<S>(&self, serializer: S) -> std::result::Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        use serde::ser::Error;
        let bytes = AsciiArmored::try_from(*self).map_err(Error::custom)?;
        let decoded = std::str::from_utf8(&bytes).map_err(Error::custom)?;
        serializer.collect_str(decoded)
    }
}

/// Builder for creating OpenPGP certificates.
pub struct CertBuilder<'a> {
    builder: sequoia_openpgp::cert::CertBuilder<'a>,
}

impl Default for CertBuilder<'_> {
    fn default() -> Self {
        Self {
            builder: sequoia_openpgp::cert::CertBuilder::new()
                .set_cipher_suite(sequoia_openpgp::cert::CipherSuite::Cv25519),
        }
    }
}

impl CertBuilder<'_> {
    /// Creates a new `CertBuilder`.
    pub fn new() -> Self {
        Self::default()
    }

    /// Adds a user ID to the certificate.
    pub fn add_userid(mut self, uid: &str) -> Self {
        self.builder = self.builder.add_userid(uid);
        self
    }

    /// Sets the certificate's validity period.
    pub fn set_validity_period(
        mut self,
        validity_period: impl Into<Option<std::time::Duration>>,
    ) -> Self {
        self.builder = self.builder.set_validity_period(validity_period);
        self
    }

    /// Sets the certificate's cipher suite.
    pub fn set_cipher_suite(mut self, cipher: CipherSuite) -> Self {
        self.builder = self.builder.set_cipher_suite(match cipher {
            CipherSuite::Cv25519 => sequoia_openpgp::cert::CipherSuite::Cv25519,
            CipherSuite::RSA4k => sequoia_openpgp::cert::CipherSuite::RSA4k,
        });
        self
    }

    /// Sets the password for encrypting the secret key material.
    pub fn set_password(mut self, password: Option<crate::secret::Secret>) -> Self {
        self.builder = self
            .builder
            .set_password(password.map(|p| p.as_inner().to_owned()));
        self
    }

    /// Generates an OpenPGP certificate and a revocation signature.
    pub fn generate(self) -> Result<(Cert, Vec<u8>), error::PgpError> {
        let (cert, rev) = self
            .builder
            .add_authentication_subkey()
            .add_signing_subkey()
            .add_subkey(
                sequoia_openpgp::types::KeyFlags::empty()
                    .set_transport_encryption()
                    .set_storage_encryption(),
                None,
                None,
            )
            .generate()
            .map_err(error::PgpError::from)?;
        let rev_sig = serialize_revocation_signature(&cert, rev)?;
        Ok((Cert(cert), rev_sig))
    }
}

fn serialize_revocation_signature(
    cert: &sequoia_openpgp::Cert,
    rev: sequoia_openpgp::packet::Signature,
) -> Result<Vec<u8>, error::PgpError> {
    let mut rev_serialized = Vec::new();
    let mut writer = sequoia_openpgp::armor::Writer::with_headers(
        &mut rev_serialized,
        sequoia_openpgp::armor::Kind::Signature,
        std::iter::once(("Comment", "Revocation certificate for")).chain(
            cert.armor_headers()
                .iter()
                .map(|value| ("Comment", value.as_str())),
        ),
    )
    .map_err(error::PgpError::from)?;
    sequoia_openpgp::packet::Packet::Signature(rev)
        .serialize(&mut writer)
        .map_err(error::PgpError::from)?;
    writer.finalize().map_err(error::PgpError::from)?;
    Ok(rev_serialized)
}

/// Reads OpenPGP certificates a reader.
///
/// Input data may contain multiple certificates. The parser will return
/// an error if the data contains invalid certificates.
pub fn parse_certs<R: std::io::Read + Send + Sync>(data: R) -> Result<Vec<Cert>, error::PgpError> {
    sequoia_openpgp::cert::CertParser::from(
        sequoia_openpgp::parse::PacketParser::from_reader(data).map_err(error::PgpError::from)?,
    )
    .map(|c| Ok(Cert(c.map_err(error::PgpError::from)?)))
    .collect()
}

/// Enum indicating whether an OpenPGP certificate stores only public material,
/// or if it also contains secret material.
#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub enum CertType {
    /// Certificate contains only public material.
    #[default]
    Public,
    /// Certificate contains both public and secret material.
    Secret,
}

// Allows to print a `CertType` variant as a string.
impl std::fmt::Display for CertType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            CertType::Public => write!(f, "public"),
            CertType::Secret => write!(f, "private"),
        }
    }
}

#[derive(Debug, Clone)]
/// Internal representation of an OpenPGP signature.
pub struct Signature(sequoia_openpgp::packet::Signature);

impl Signature {
    /// Return the reason for the revocation of the signature.
    pub fn reason_for_revocation(&self) -> Option<(ReasonForRevocation, &[u8])> {
        self.0
            .reason_for_revocation()
            .map(|(code, msg)| (ReasonForRevocation::from_sequoia(code), msg))
    }
}

#[derive(Debug, Clone)]
/// Internal representation of an OpenPGP key revocation status.
pub enum RevocationStatus {
    /// Key is revoked.
    Revoked(Vec<Signature>),
    /// There is a revocation certificate from a possible designated
    /// revoker.
    CouldBe(Vec<Signature>),
    /// Key does not appear to be revoked.
    NotAsFarAsWeKnow,
}

#[derive(Debug, Clone, PartialEq)]
#[non_exhaustive]
/// Describes the reason for a revocation.
/// This enum cannot be exhaustively matched to allow future extensions.
pub enum ReasonForRevocation {
    /// None of the other reasons apply. This can be used can be used when
    /// creating a revocation signature in advance.
    Unspecified,
    /// The private key has been replaced by a new one.
    KeySuperseded,
    /// The private key material (may) have been compromised.
    KeyCompromised,
    /// This public key should not be used anymore and there is no
    /// replacement key.
    KeyRetired,
    /// User ID information is no longer valid (cert revocations).
    UIDRetired,
    /// Private reason identifier.
    Private(u8),
    /// Unknown reason identifier.
    Unknown(u8),
}

impl std::fmt::Display for ReasonForRevocation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.clone().into_sequoia())
    }
}

impl ReasonForRevocation {
    pub(super) fn into_sequoia(self) -> sequoia_openpgp::types::ReasonForRevocation {
        use sequoia_openpgp::types::ReasonForRevocation::*;
        match self {
            ReasonForRevocation::Unspecified => Unspecified,
            ReasonForRevocation::KeySuperseded => KeySuperseded,
            ReasonForRevocation::KeyCompromised => KeyCompromised,
            ReasonForRevocation::KeyRetired => KeyRetired,
            ReasonForRevocation::UIDRetired => UIDRetired,
            ReasonForRevocation::Private(v) => Private(v),
            ReasonForRevocation::Unknown(v) => Unknown(v),
        }
    }

    pub(super) fn from_sequoia(val: sequoia_openpgp::types::ReasonForRevocation) -> Self {
        use sequoia_openpgp::types::ReasonForRevocation::*;
        match val {
            Unspecified => Self::Unspecified,
            KeySuperseded => Self::KeySuperseded,
            KeyCompromised => Self::KeyCompromised,
            KeyRetired => Self::KeyRetired,
            UIDRetired => Self::UIDRetired,
            Private(v) => Self::Private(v),
            Unknown(v) => Self::Unknown(v),
            // TODO: currently it's unreachable, but it might change in the
            // future if Sequoia extends the enum.
            _ => todo!(),
        }
    }
}

/// OpenPGP key.
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Key {
    pub(super) fingerprint: Fingerprint,
}

impl Key {
    /// Returns the fingerprint of the key.
    ///
    /// The fingerprint is a unique identifier for the key.
    pub fn fingerprint(&self) -> Fingerprint {
        self.fingerprint.clone()
    }
}

/// Warns if the certificate expires in the next 90 days.
pub(crate) fn warn_if_cert_expires_soon(cert: &sequoia_openpgp::Cert) {
    use chrono::{DateTime, Duration, Utc};
    match cert.with_policy(&sequoia_openpgp::policy::StandardPolicy::new(), None) {
        Ok(cert) => {
            if let Some(expiration_time) = cert.primary_key().key_expiration_time() {
                let expiration_time = DateTime::<Utc>::from(expiration_time);
                if expiration_time < Utc::now() + Duration::days(90) {
                    warn!(
                        "Certificate {}[{}] expires on {}, consider renewing it",
                        cert.userids()
                            .next()
                            .map_or_else(String::new, |uid| format!("{} ", uid.userid())),
                        cert.fingerprint(),
                        expiration_time
                    );
                }
            }
        }
        Err(e) => {
            warn!(
                "Failed to check expiration time of certificate {}: {}",
                cert, e
            );
        }
    }
}

/// Verify the signature of a file.
pub(crate) fn verify_file_signature(
    body: &[u8],
    signature: &[u8],
    cert_store: &super::certstore::CertStore<'_>,
) -> Result<(), super::error::PgpError> {
    use sequoia_openpgp::parse::{stream::DetachedVerifierBuilder, Parse};
    DetachedVerifierBuilder::from_bytes(signature)
        .map_err(super::error::PgpError::from)?
        .with_policy(
            &sequoia_openpgp::policy::StandardPolicy::new(),
            None,
            crate::openpgp::crypto::VerificationHelper { cert_store },
        )
        .map_err(error::PgpError::from)?
        .verify_bytes(body)
        .map_err(error::PgpError::from)
}

#[cfg(test)]
mod tests {
    use sequoia_openpgp::{cert::CertBuilder, packet::UserID};

    use super::*;

    const VALID_USER_ID1: &str = "chuck norris <chuck@roundhouse.org>";
    const VALID_USER_ID2: &str = "cn <cn@roundhouse.org>";
    const INVALID_USER_ID: &str = "sgt hartman <invalid@roundhouse.org>";

    #[test]
    fn test_userids() {
        // Test that a Cert with a single valid UserID returns a single value.
        let (cert, _sig) = CertBuilder::general_purpose(None, Some(VALID_USER_ID1))
            .generate()
            .unwrap();
        assert_eq!(cert.userids().len(), 1);
        assert_eq!(Cert(cert).userids(), vec![VALID_USER_ID1]);

        // Test that a Cert with 2 valid UserIDs returns a vector of 2 values.
        let (cert, _sig) = CertBuilder::general_purpose(None, Some(VALID_USER_ID1))
            .add_userid(VALID_USER_ID2)
            .generate()
            .unwrap();
        assert_eq!(cert.userids().len(), 2);
        assert_eq!(Cert(cert).userids(), vec![VALID_USER_ID1, VALID_USER_ID2]);

        // Test that a Cert with no UserID returns an empty vector.
        let (cert, _sig) = CertBuilder::general_purpose(None, None::<UserID>)
            .generate()
            .unwrap();
        let empty_vec: Vec<String> = Vec::new();
        assert_eq!(cert.userids().len(), 0);
        assert_eq!(Cert(cert).userids(), empty_vec);

        // Test that a Cert with an invalid (non-self-signed) UserID returns
        // an empty vector.
        let (cert, _sig) = CertBuilder::general_purpose(None, None::<UserID>)
            .generate()
            .unwrap();
        let invalid_user: UserID = INVALID_USER_ID.into();
        let cert = cert.insert_packets(invalid_user).unwrap();
        assert_eq!(cert.userids().len(), 1);
        assert_eq!(Cert(cert).userids(), empty_vec);

        // Test that a Cert with one valid and one invalid (non-self-signed)
        // UserID returns only the valid UserID.
        let (cert, _sig) = CertBuilder::general_purpose(None, Some(VALID_USER_ID1))
            .generate()
            .unwrap();
        let invalid_user: UserID = INVALID_USER_ID.into();
        let cert = cert.insert_packets(invalid_user).unwrap();
        assert_eq!(cert.userids().len(), 2);
        assert_eq!(Cert(cert).userids(), vec![VALID_USER_ID1]);

        // Test that a Cert with only an invalid UserID returns an empty vector.
        let future_time = std::time::SystemTime::now() + std::time::Duration::from_secs(60);
        let (cert, _) = CertBuilder::new()
            .set_creation_time(future_time)
            .add_userid(INVALID_USER_ID)
            .generate()
            .unwrap();
        assert_eq!(cert.userids().len(), 1);
        assert_eq!(Cert(cert).userids(), empty_vec);
    }

    const PUB: &[u8] =
        include_bytes!("../../tests/data/B2E961753ECE0B345E718E74BA6F29C998DDD9BF.pub");
    const SEC: &[u8] =
        include_bytes!("../../tests/data/B2E961753ECE0B345E718E74BA6F29C998DDD9BF.sec");
    #[test]
    fn test_export_public_cert_to_bytes_works() {
        let cert = Cert::from_bytes(PUB).unwrap();
        let exported_cert: Vec<u8> = cert.public().try_into().unwrap();
        let reimported_cert = Cert::from_bytes(&exported_cert).unwrap();
        assert_eq!(reimported_cert, cert);
    }

    #[test]
    fn test_export_public_cert_to_ascii_armored_works() {
        let cert = Cert::from_bytes(PUB).unwrap();
        let exported_cert = serde_json::to_string(&cert.public()).unwrap();
        let reimported_cert =
            Cert::from_bytes(serde_json::from_str::<String>(&exported_cert).unwrap()).unwrap();
        assert_eq!(reimported_cert, cert);
    }

    #[test]
    fn test_export_secret_cert_to_bytes_works() {
        let cert = Cert::from_bytes(SEC).unwrap();
        let exported_cert: Vec<u8> = cert.secret().unwrap().try_into().unwrap();
        let reimported_cert = Cert::from_bytes(&exported_cert).unwrap();
        assert_eq!(reimported_cert, cert);
    }

    #[test]
    fn test_export_secret_cert_to_ascii_armored_works() {
        let cert = Cert::from_bytes(SEC).unwrap();
        let exported_cert = serde_json::to_string(&cert.secret().unwrap()).unwrap();
        let reimported_cert =
            Cert::from_bytes(serde_json::from_str::<String>(&exported_cert).unwrap()).unwrap();
        assert_eq!(reimported_cert, cert);
    }

    #[test]
    fn test_export_public_part_of_cert_to_bytes_strips_secret_part() {
        let public = Cert::from_bytes(PUB).unwrap();
        let cert = Cert::from_bytes(SEC).unwrap();
        let exported_cert: Vec<u8> = cert.public().try_into().unwrap();
        let reimported_cert = Cert::from_bytes(&exported_cert).unwrap();
        assert_eq!(reimported_cert, public);
    }

    #[test]
    fn test_export_public_part_of_cert_to_ascii_armored_strips_secret_part() {
        let public = Cert::from_bytes(PUB).unwrap();
        let cert = Cert::from_bytes(SEC).unwrap();
        let exported_cert = serde_json::to_string(&cert.public()).unwrap();
        let reimported_cert =
            Cert::from_bytes(serde_json::from_str::<String>(&exported_cert).unwrap()).unwrap();
        assert_eq!(reimported_cert, public);
    }

    #[test]
    fn test_export_secret_part_of_public_cert_fails() {
        let public = Cert::from_bytes(PUB).unwrap();
        assert!(public.secret().is_err());
    }
}
