//! Utilities for OpenPGP operations.

use super::error;
use tracing::{debug, trace, warn};

use super::{
    cert::{AsciiArmored, Fingerprint},
    certstore::CertStore,
    keystore::KeyStore,
};

/// Hint about an OpenPGP key or subkey for which a password is required.
///
/// This struct stores information about a secret key or subkey that can be
/// used to help users identify a key for which a password is being requested.
///
/// As signing and decryption operations are often carried-out with a subkey
/// rather than with the certificate's primary key - but users are generally
/// more familiar with the fingerprint of their primary key - the fingerprint
/// of the certificate's primary key can optionally be included via the
/// `fingerprint_primary` field.
///
/// The reason why `userid` and `fingerprint_primary` are optional is for
/// edge-cases where a signing/decryption subkey is not associated
/// with a primary key in the user's local environment.
#[derive(Clone, Debug)]
pub struct PasswordHint {
    /// Fingerprint of the key or subkey for which the password is required.
    pub fingerprint: super::cert::Fingerprint,
    /// UserID of the associated certificate.
    pub userid: Option<String>,
    /// Fingerprint of the associated certificate's primary key.
    pub fingerprint_primary: Option<super::cert::Fingerprint>,
}

/// Error type to forward to sequoia API
#[derive(Debug)]
struct ErrorForward<S>(S);

impl<S: std::fmt::Display> std::fmt::Display for ErrorForward<S> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl<S: std::fmt::Debug + std::fmt::Display> core::error::Error for ErrorForward<S> {}

/// Creates a detached OpenPGP signature.
pub(crate) fn sign_detached<T: sequoia_openpgp::crypto::Signer + Send + Sync>(
    data: &[u8],
    cert: T,
) -> Result<AsciiArmored, error::PgpError> {
    use sequoia_openpgp::serialize::stream::{Armorer, Message, Signer};
    use std::io::Write as _;
    let mut sink = vec![];
    let message = Armorer::new(Message::new(&mut sink))
        .kind(sequoia_openpgp::armor::Kind::Signature)
        .build()
        .map_err(error::PgpError::from)?;
    let mut message = Signer::new(message, cert)
        .detached()
        .build()
        .map_err(error::PgpError::from)?;
    message.write_all(data).map_err(error::PgpError::from)?;
    message.finalize().map_err(error::PgpError::from)?;

    Ok(AsciiArmored(sink))
}

/// Returns encryption-capable keys.
pub(crate) fn get_encryption_keys<T: sequoia_openpgp::policy::Policy>(
    certs: &[sequoia_openpgp::Cert],
    policy: &T,
) -> Result<
    Vec<
        sequoia_openpgp::packet::Key<
            sequoia_openpgp::packet::key::PublicParts,
            sequoia_openpgp::packet::key::UnspecifiedRole,
        >,
    >,
    error::PgpError,
> {
    let mut recipient_subkeys = Vec::new();
    for cert in certs {
        // Make sure we add at least one subkey from every certificate.
        let mut found_one = false;
        for ka in cert
            .keys()
            .with_policy(policy, None)
            .supported()
            .alive()
            .revoked(false)
            .for_transport_encryption()
        {
            recipient_subkeys.push(ka.key().clone());
            found_one = true;
        }

        if !found_one {
            return Err(error::PgpError(format!(
                "No suitable encryption subkey for {cert}"
            )));
        }
    }
    Ok(recipient_subkeys)
}

#[tracing::instrument(skip_all, fields(cert=%cert.fingerprint()))]
pub(crate) async fn get_signing_capable_key<F, Fut>(
    cert: &sequoia_openpgp::Cert,
    policy: &impl sequoia_openpgp::policy::Policy,
    key_store: &mut KeyStore,
    password: F,
) -> Result<sequoia_keystore::Key, error::PgpError>
where
    F: Fn(PasswordHint) -> Fut,
    Fut: std::future::Future<Output = crate::secret::Secret>,
{
    let cert = cert
        .with_policy(policy, None)
        .map_err(error::PgpError::from)?;
    let signing_capable_keys = cert
        .keys()
        .alive()
        .revoked(false)
        .for_signing()
        .map(|key| key.fingerprint().into())
        .collect::<Vec<_>>();
    if signing_capable_keys.is_empty() {
        return Err(error::PgpError(format!(
            "No signing-capable subkey found for the provided certificate: {cert}"
        )));
    }

    let (keys, _) = key_store
        .inner
        .find_keys_async(&signing_capable_keys)
        .await
        .map_err(error::PgpError::from)?;
    if keys.is_empty() {
        return Err(error::PgpError(format!(
            "No signing key found for the provided fingerprint: {}",
            cert.fingerprint()
        )));
    }

    let mut errors = Vec::new();
    for mut key in keys.into_iter() {
        let hint = PasswordHint {
            fingerprint: Fingerprint(key.fingerprint()),
            userid: cert.userids().next().map(|uid| uid.to_string()),
            fingerprint_primary: Some(Fingerprint(cert.fingerprint())),
        };
        let key_description = hint_to_string(&hint);
        trace!("Attempting to unlock key: {key_description}");

        match key.locked_async().await {
            Ok(sequoia_keystore::Protection::Unlocked) => {
                trace!("Key is already unlocked");
                return Ok(key);
            }
            Ok(sequoia_keystore::Protection::Password(_)) => {
                if let Ok(()) = key
                    .unlock_async(password(hint).await.as_inner().clone())
                    .await
                {
                    trace!("Key unlocked with the provided password");
                    return Ok(key);
                } else {
                    let err_msg =
                        format!("The provided password failed to unlock key: {key_description}");
                    debug!(err_msg);
                    errors.push(err_msg);
                }
            }
            Ok(_) => {
                return {
                    trace!("Key is externally protected");
                    Ok(key)
                }
            }
            Err(e) => {
                warn!("Failed to check lock status of key: {key_description}. Reason: {e}");
                errors.push(e.to_string());
            }
        }
    }
    Err(error::PgpError(if !errors.is_empty() {
        errors.join(", ")
    } else {
        format!("Unable to unlock private key: {cert}")
    }))
}

pub(crate) struct VerificationHelper<'cert_store, 'cert_store_ref> {
    pub(crate) cert_store: &'cert_store_ref CertStore<'cert_store>,
}

pub(crate) struct DecryptionHelper<'cert_store, 'cert_store_ref, 'key_store_ref, F> {
    pub(crate) cert_store: &'cert_store_ref CertStore<'cert_store>,
    pub(crate) key_store: &'key_store_ref mut KeyStore,
    pub(crate) password: F,
}

impl sequoia_openpgp::parse::stream::VerificationHelper for VerificationHelper<'_, '_> {
    fn get_certs(
        &mut self,
        ids: &[sequoia_openpgp::KeyHandle],
    ) -> sequoia_openpgp::Result<Vec<sequoia_openpgp::Cert>> {
        get_certs(self.cert_store, ids)
    }

    fn check(
        &mut self,
        structure: sequoia_openpgp::parse::stream::MessageStructure,
    ) -> sequoia_openpgp::Result<()> {
        check(structure)
    }
}

impl<F> sequoia_openpgp::parse::stream::VerificationHelper for DecryptionHelper<'_, '_, '_, F> {
    fn get_certs(
        &mut self,
        ids: &[sequoia_openpgp::KeyHandle],
    ) -> sequoia_openpgp::Result<Vec<sequoia_openpgp::Cert>> {
        get_certs(self.cert_store, ids)
    }

    fn check(
        &mut self,
        structure: sequoia_openpgp::parse::stream::MessageStructure,
    ) -> sequoia_openpgp::Result<()> {
        check(structure)
    }
}

fn check(
    structure: sequoia_openpgp::parse::stream::MessageStructure,
) -> sequoia_openpgp::Result<()> {
    use sequoia_openpgp::parse::stream::MessageLayer;
    for layer in structure.into_iter() {
        match layer {
            MessageLayer::Compression { algo } => trace!("Data compressed using {}", algo),
            MessageLayer::Encryption {
                sym_algo,
                aead_algo,
            } => match aead_algo {
                Some(aead_algo) => {
                    trace!(
                        "Data encrypted and protected using {}/{}",
                        sym_algo,
                        aead_algo
                    )
                }
                None => trace!("Data encrypted using {}", sym_algo),
            },
            MessageLayer::SignatureGroup { ref results } => {
                if !results.iter().any(|r| r.is_ok()) {
                    return Err(ErrorForward("No valid signature").into());
                }
            }
        }
    }
    Ok(())
}

fn get_certs(
    cert_store: &CertStore<'_>,
    ids: &[sequoia_openpgp::KeyHandle],
) -> sequoia_openpgp::Result<Vec<sequoia_openpgp::Cert>> {
    Ok(ids
        .iter()
        .flat_map(|key_handle| {
            cert_store
                .get_certs_by_key_handle(key_handle)
                .unwrap_or_else(|e| {
                    tracing::warn!(?e);
                    Vec::new()
                })
        })
        .collect())
}

impl<F> sequoia_openpgp::parse::stream::DecryptionHelper for DecryptionHelper<'_, '_, '_, F>
where
    F: Fn(PasswordHint) -> crate::secret::Secret,
{
    #[tracing::instrument(skip_all)]
    fn decrypt<D>(
        &mut self,
        pkesks: &[sequoia_openpgp::packet::PKESK],
        _skesks: &[sequoia_openpgp::packet::SKESK],
        sym_algo: Option<sequoia_openpgp::types::SymmetricAlgorithm>,
        mut decrypt: D,
    ) -> sequoia_openpgp::Result<Option<sequoia_openpgp::Fingerprint>>
    where
        D: FnMut(
            sequoia_openpgp::types::SymmetricAlgorithm,
            &sequoia_openpgp::crypto::SessionKey,
        ) -> bool,
    {
        let mut errors = Vec::new();
        match self.key_store.inner.decrypt(pkesks) {
            Ok((_i, fp, sym_algo, sk)) => {
                trace!(fingerprint=%fp, "Decrypted with an unlocked key");
                if decrypt(sym_algo, &sk) {
                    tracing::debug!("Decrypted data with key {fp}");
                    return Ok(Some(fp));
                }
            }
            Err(err) => {
                trace!("Unlocking decryption key");
                match err.downcast() {
                    Ok(sequoia_keystore::Error::InaccessibleDecryptionKey(keys)) => {
                        for key_status in keys.into_iter() {
                            let pkesk = key_status.pkesk().clone();
                            let mut key = key_status.into_key();
                            let protection = key.locked();
                            match protection {
                                Ok(sequoia_keystore::Protection::Password(_)) => {
                                    // Retrieve the user ID and primary
                                    // fingerprint of the certificate
                                    // associated with the decryption key.
                                    let fingerprint = Fingerprint(key.fingerprint());
                                    let hint = if let Ok(cert) =
                                        self.cert_store.get_cert_by_fingerprint(&fingerprint)
                                    {
                                        super::cert::warn_if_cert_expires_soon(&cert.0);
                                        PasswordHint {
                                            fingerprint,
                                            userid: cert.userids().first().cloned(),
                                            fingerprint_primary: Some(cert.fingerprint()),
                                        }
                                    } else {
                                        PasswordHint {
                                            fingerprint,
                                            userid: None,
                                            fingerprint_primary: None,
                                        }
                                    };
                                    let key_description = hint_to_string(&hint);

                                    // Try to unlock the decryption subkey.
                                    // The password is retrieved via a call to
                                    // the "password callback", which can e.g.
                                    // prompt the user to enter a password.
                                    if let Ok(()) =
                                        key.unlock((self.password)(hint).as_inner().clone())
                                    {
                                        let (sym_algo, sk) = pkesk
                                            .decrypt(&mut key, sym_algo)
                                            .ok_or(error::PgpError::from("failed"))?;
                                        if decrypt(sym_algo, &sk) {
                                            let fp = key.fingerprint();
                                            tracing::debug!(
                                                "Decrypted data with key: {key_description}"
                                            );
                                            return Ok(Some(fp));
                                        }
                                    } else {
                                        let err_msg = format!(
                                            "The provided password failed to unlock key: {key_description}"
                                        );
                                        debug!(err_msg);
                                        errors.push(err_msg);
                                    }
                                }
                                Ok(p) => {
                                    let err_msg =
                                        format!("Unsupported key protection method {p:?}");
                                    debug!(err_msg);
                                    errors.push(err_msg);
                                }
                                Err(e) => {
                                    let err_msg = e.to_string();
                                    debug!(err_msg);
                                    errors.push(err_msg);
                                }
                            }
                        }
                    }
                    Ok(err) => {
                        let err_msg = format!("Unsupported key unlock operation ({err})");
                        debug!(err_msg);
                        errors.push(err_msg);
                    }
                    Err(err) => {
                        let err_msg = format!("Failed to decrypt using the keystore ({err})");
                        debug!(err_msg);
                        errors.push(err_msg);
                    }
                }
            }
        }
        // If this point is reached, no key to decrypt the data could be found
        // or successfully unlocked with the provided password.
        Err(ErrorForward(if !errors.is_empty() {
            errors.join(", ")
        } else {
            format!(
                "Unable to find any suitable private key for decryption (expected one of: {:?})",
                pkesks
                    .iter()
                    .map(|pkesk| pkesk.recipient().to_hex())
                    .collect::<Vec<_>>()
                    .join(", ")
            )
        })
        .into())
    }
}

/// Formats the password `hint` of an OpenPGP key as a string.
pub(super) fn hint_to_string(hint: &PasswordHint) -> String {
    if let Some(fingerprint_primary) = hint.fingerprint_primary.as_ref() {
        let userid = hint.userid.as_deref().unwrap_or("--missing user ID--");
        if fingerprint_primary == &hint.fingerprint {
            // Key is a primary key.
            format!("{userid} {fingerprint_primary}")
        } else {
            // Key is a subkey: both the primary and the subkey's fingerprints
            // are included in the string to help better identify the key.
            format!(
                "{} {} (subkey {})",
                userid, fingerprint_primary, hint.fingerprint
            )
        }
    } else {
        format!("{} (key is not part of a certificate)", hint.fingerprint)
    }
}
