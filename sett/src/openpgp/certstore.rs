//! OpenPGP certificate store

use super::error;
use std::{
    borrow::Cow,
    path::{Path, PathBuf},
};

use sequoia_cert_store::{Store as _, StoreUpdate as _};

use super::cert::{Cert, Fingerprint};

#[derive(Clone, Debug, Default)]
/// Options required to instantiate a new certificate store.
pub struct CertStoreOpts<'a> {
    /// Path of the store on disk.
    ///
    /// If `None`, the default, platform-specific, location is used.
    pub location: Option<&'a Path>,
    /// Open the store in read-only mode.
    pub read_only: bool,
}

/// Store for OpenPGP certificates with public material only, i.e. public keys
/// and user IDs.
pub struct CertStore<'store> {
    /// Sequoia store for OpenPGP certificates with public material only.
    inner: sequoia_cert_store::CertStore<'store>,
    // TODO: this field can be replaced by a method that return the path
    // of the CertD certificate store after the next release of pgp-cert-d.
    path: PathBuf,
}

impl<'store> CertStore<'store> {
    /// Environment variable to override the default cert store location.
    const ENV: &'static str = "PGP_CERT_D";

    /// Default directory for the public certificate store.
    ///
    /// https://www.ietf.org/archive/id/draft-nwjw-openpgp-cert-d-00.html
    const DEFAULT_DIR: &'static str = "pgp.cert.d";

    /// Returns the default path for the certificate store.
    pub fn default_location() -> Result<std::path::PathBuf, error::Error> {
        Ok(dirs::data_dir()
            .ok_or(std::io::Error::new(
                std::io::ErrorKind::Unsupported,
                "Unsupported platform: 'data dir' is not defined.",
            ))?
            .join(Self::DEFAULT_DIR))
    }

    /// Open a certificate store from disk.
    ///
    /// If no path is provided, the certificate store will be created at the
    /// default, platform-specific, location.
    pub fn open(opts: &CertStoreOpts<'_>) -> Result<CertStore<'store>, error::Error> {
        let store_path = if let Some(p) = opts.location {
            p.into()
        } else if let Some(p) = std::env::var_os(Self::ENV) {
            p.into()
        } else {
            Self::default_location()?
        };
        if !store_path.is_dir() {
            std::fs::create_dir_all(&store_path)?;
        }
        let store = if opts.read_only {
            sequoia_cert_store::CertStore::open_readonly(&store_path)
                .map_err(error::PgpError::from)?
        } else {
            sequoia_cert_store::CertStore::open(&store_path).map_err(error::PgpError::from)?
        };
        Ok(Self {
            inner: store,
            path: store_path,
        })
    }

    /// Opens an in-memory certificate store.
    pub fn open_ephemeral() -> CertStore<'store> {
        Self {
            inner: sequoia_cert_store::CertStore::empty(),
            path: PathBuf::new(),
        }
    }

    /// Returns OpenPGP certificate file location on disk.
    ///
    /// * The certificate is specified via the fingerprint of the primary key.
    /// * If the certificate is not present in the store, an error is returned.
    ///
    /// Certificates are stored as files under the path:
    ///  * `/path/of/store/<first 2 chars of fingerprint>/<remainder of fingerprint>`.
    ///  * Example: `~/.local/share/pgp.cert.d/1e/a0292ecbf2457cadae20e2b94fa6a56d9fa1fb`.
    pub fn get_cert_path(&self, fingerprint: &Fingerprint) -> Result<PathBuf, error::PgpError> {
        // Make sure the certificate is present in the store.
        if let Err(err_msg) = self.get_cert_by_fingerprint(fingerprint) {
            return Err(error::PgpError(format!("Cannot get key path: {err_msg}")));
        }

        // Return path of certificate.
        let fingerprint_as_string = fingerprint.0.to_hex().to_ascii_lowercase();
        Ok(self
            .path
            .join(&fingerprint_as_string[..2])
            .join(&fingerprint_as_string[2..]))
    }

    /// Add a public certificate (i.e. with no secret material) to the public
    /// store. If a certificate with secret material is passed, the secret
    /// material is stripped away and only the public part of the certificate
    /// is stored.
    /// If a certificate with the same fingerprint is already present, it gets
    /// updated with the new info (if any) present in the certificate being
    /// added.
    fn add_pub_cert(&mut self, cert: &Cert) -> Result<Cert, error::PgpError> {
        let imported_cert = self
            .inner
            .update_by(
                std::sync::Arc::new(sequoia_cert_store::LazyCert::from_cert(
                    cert.0.clone().strip_secret_key_material(),
                )),
                &(),
            )
            .map_err(error::PgpError::from)?
            .to_cert()
            .map_err(error::PgpError::from)?
            .clone();

        // Make sure any secret material has been stripped from the certificate.
        assert!(!imported_cert.is_tsk());
        Ok(Cert(imported_cert))
    }

    /// Add an OpenPGP certificate to a local store.
    ///
    /// Only the public part of the certificate is stored if the store.
    /// If a certificate with the same fingerprint is already present, it
    ///   gets updated with the new material (if any) present in the
    ///   certificate being added.
    pub fn import(&mut self, cert: &Cert) -> Result<Cert, error::PgpError> {
        cert.validate()?;
        self.add_pub_cert(cert)
    }

    /// Retrieves an OpenPGP certificate by its fingerprint.
    ///
    /// If no certificates matching the specified `fingerprint` is found,
    /// an error is returned.
    ///
    /// # Arguments
    ///
    /// * `fingerprint`: `Fingerprint` of the certificate (or any of its
    ///    subkeys) to retrieve.
    pub(crate) fn get_cert_by_fingerprint(
        &self,
        fingerprint: &Fingerprint,
    ) -> Result<Cert, error::PgpError> {
        let key_handle = sequoia_openpgp::KeyHandle::Fingerprint(fingerprint.0.clone());
        if let Ok(certs) = self.inner.lookup_by_cert_or_subkey(&key_handle) {
            if certs.len() == 1 {
                return Ok(Cert(
                    certs[0].to_cert().map_err(error::PgpError::from)?.clone(),
                ));
            }
            if certs.len() > 1 {
                return Err(error::PgpError(format!(
                    "Multiple keys with the same fingerprint \
                    '{fingerprint}' were found."
                )));
            }
        }
        Err(error::PgpError(format!(
            "No key matching the specified fingerprint \
                    '{fingerprint}' was found."
        )))
    }

    /// Retrieve an OpenPGP certificate from the store by email.
    ///
    /// Search is case-insensitive.
    ///
    /// Returns an error if no certificate matching the specified email is
    /// found.
    fn get_cert_by_email(&self, email: &str) -> Result<Vec<Cert>, error::PgpError> {
        // This implemenation is the same as
        // sequoia_cert_store::Store::lookup_by_email, but
        // case-insensitive.
        let email = sequoia_cert_store::email_to_userid(email)
            .map_err(error::PgpError::from)?
            .email_normalized()
            .map_err(error::PgpError::from)?
            .expect("have one");
        self.inner
            .select_userid(
                sequoia_cert_store::store::UserIDQueryParams::new()
                    .set_email(true)
                    .set_anchor_start(true)
                    .set_anchor_end(true)
                    .set_ignore_case(true),
                &email,
            )
            .map_err(|_| {
                error::PgpError(format!(
                    "No public key matching the email '{email}' was found"
                ))
            })?
            .iter()
            .map(|lazy_cert| {
                Ok(Cert(
                    lazy_cert.to_cert().map_err(error::PgpError::from)?.clone(),
                ))
            })
            .collect()
    }

    /// Retrieve OpenPGP certificates from the `CertStore` by searching for a
    /// `query_term` that can be either a fingerprint or an email address.
    /// * The function returns a `Vec` of certificates, because there can be
    ///   multiple certificates with the same email in the store.
    /// * If no certificates matching the specified `query_term` is found,
    ///   an error is returned.
    ///
    /// # Arguments
    ///
    /// * `query_term`: fingerprint or an email address of the certificate to
    ///   retrieve. Must be passed as a `QueryTerm` variant.
    pub fn get_cert(&self, query_term: &QueryTerm) -> Result<Vec<Cert>, error::PgpError> {
        match query_term {
            QueryTerm::Fingerprint(f) => Ok(vec![self.get_cert_by_fingerprint(f)?]),
            QueryTerm::Email(email) => self.get_cert_by_email(email),
        }
    }

    /// Retrieve all certificates from the store.
    pub fn certs<'a>(&'a self) -> Box<dyn Iterator<Item = Result<Cert, error::PgpError>> + 'a> {
        Box::new(self.inner.certs().map(|lazy_cert| {
            Ok(Cert(
                lazy_cert.to_cert().map_err(error::PgpError::from)?.clone(),
            ))
        }))
    }

    pub(crate) fn get_certs_by_key_handle(
        &self,
        key_handle: &sequoia_openpgp::KeyHandle,
    ) -> Result<Vec<sequoia_openpgp::Cert>, error::PgpError> {
        self.inner
            .lookup_by_cert_or_subkey(key_handle)
            .map_err(error::PgpError::from)?
            .into_iter()
            .map(|lazy_cert| Ok(lazy_cert.to_cert().map_err(error::PgpError::from)?.clone()))
            .collect()
    }
}

/// Search term to use when retrieving an OpenPGP certificate from a
/// `CertStore` by either email or fingerprint.
#[derive(Debug, Clone, PartialEq)]
pub enum QueryTerm<'a> {
    /// Fingerprint associated with the certificate to retrieve.
    Fingerprint(Cow<'a, Fingerprint>),
    /// Email associated with the user ID of the certificate to retrieve.
    Email(Cow<'a, str>),
}

impl std::str::FromStr for QueryTerm<'_> {
    type Err = error::PgpError;

    /// Converts the input string `s` to a `QueryTerm` Enum.
    /// * If `s` is a valid OpenPGP Fingerprint, return the `Fingerprint` variant.
    /// * If `s` is a valid email, return the `Email` variant.
    /// * Otherwise return an error.
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Ok(fingerprint) = s.parse() {
            Ok(Self::Fingerprint(Cow::Owned(fingerprint)))
        } else if is_valid_email(s) {
            Ok(Self::Email(Cow::Owned(s.into())))
        } else {
            Err(error::PgpError(format!(
                "Invalid query term: '{s}' is not a valid fingerprint or email."
            )))
        }
    }
}

/// Test if the specified string `s` is a valid email.
fn is_valid_email(s: &str) -> bool {
    // Note: the "local-part" of email addresses allows a lot of special
    // characters and edge-cases. As such, it's better to keep the check
    // minimal.
    let email_regex =
        regex::Regex::new(r"^[^.-].*[^.]@([[:alnum:]]+([\-\.]{1}[[:alnum:]]+)*\.[a-zA-Z]+)")
            .expect("regexp pattern should never fail because it's hard-coded");
    email_regex.is_match(s)
}

#[cfg(test)]
mod tests {
    use crate::openpgp::cert::{CertType, ReasonForRevocation, RevocationStatus};

    use super::*;

    fn generate_tmp_certstore<'a>() -> Result<CertStore<'a>, error::Error> {
        let tmp_dir = tempfile::tempdir()?.into_path();
        CertStore::open(&CertStoreOpts {
            location: Some(&tmp_dir.join(CertStore::DEFAULT_DIR)),
            ..Default::default()
        })
    }

    #[test]
    fn default_certstore_location() {
        let default_dir = "pgp.cert.d";
        let default_location = dirs::data_dir().unwrap().join(default_dir);
        assert_eq!(CertStore::default_location().unwrap(), default_location);
    }

    #[test]
    fn custom_certstore_location() {
        let tmp_dir = tempfile::tempdir().unwrap().into_path();
        let store = CertStore::open(&CertStoreOpts {
            location: Some(&tmp_dir),
            ..Default::default()
        })
        .unwrap();
        assert_eq!(store.path, tmp_dir);
    }

    #[test]
    fn certstore_location_from_env() {
        let tmp_dir = tempfile::tempdir().unwrap().into_path();
        std::env::set_var(CertStore::ENV, tmp_dir.clone());
        let store = CertStore::open(&Default::default()).unwrap();
        assert_eq!(store.path, tmp_dir);
    }

    #[test]
    fn open_certstore() {
        // Creating a new certstore should work.
        let store = generate_tmp_certstore().unwrap();
        assert!(store.path.is_dir());
    }

    // Test adding certificates to the store.
    #[test]
    fn import_cert() -> Result<(), error::Error> {
        let mut store = generate_tmp_certstore()?;

        macro_rules! assert_import {
            ($file_name:expr) => {
                // Add cert to store.
                let cert = store.import(&Cert::from_bytes(include_bytes!(concat!(
                    "../../tests/data/",
                    $file_name
                )))?)?;
                // Make sure secret certificates are stripped from their secret
                // material.
                assert_eq!(cert.0.is_tsk(), false);
                // Make sure the certificate file is stored at the expected
                // place.
                assert!(store.get_cert_path(&cert.fingerprint())?.is_file());
                // Make sure cert can be retrieved from the store.
                let fingerprint = &$file_name[..40];
                assert_eq!(
                    store
                        .get_cert_by_fingerprint(&fingerprint.parse()?,)?
                        .fingerprint()
                        .to_string(),
                    fingerprint,
                );
            };
        }

        // Adding a certificate with only public material should pass.
        assert_import!("1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub");
        assert_eq!(store.certs().count(), 1);

        // Adding a certificate with secret material should pass.
        assert_import!("B2E961753ECE0B345E718E74BA6F29C998DDD9BF.sec");
        assert_eq!(store.certs().count(), 2);

        // Adding the same certificate again should update the existing one,
        // i.e. the number of certificates should not increase.
        assert_import!("B2E961753ECE0B345E718E74BA6F29C998DDD9BF.sec");
        assert_eq!(store.certs().count(), 2);
        // Adding a different certificate should increase the number of certs.
        assert_import!("C0621CB3669020CC31050A361956EC38A96CA852.sec");
        assert_eq!(store.certs().count(), 3);
        Ok(())
    }

    // Test that a certificate can be revoked. This also tests that adding
    // content to a (in this case the revocation signature) is done properly.
    #[test]
    fn revoke_cert() -> Result<(), error::Error> {
        let mut store = generate_tmp_certstore()?;
        macro_rules! assert_revoke {
            ($fingerprint:expr, $revocation_message:expr) => {
                let fingerprint = $fingerprint.parse().unwrap();
                // Add the certificate to the store.
                store.import(&Cert::from_bytes(include_bytes!(concat!(
                    "../../tests/data/",
                    $fingerprint,
                    ".pub"
                )))?)?;
                // Make sure the certificate is originally not revoked.
                let cert = store.get_cert_by_fingerprint(&fingerprint)?;
                assert!(matches!(
                    cert.revocation_status(),
                    RevocationStatus::NotAsFarAsWeKnow
                ));
                // Revoke the certificate and add the revoked version of the
                // certificate to the store.
                store.import(&cert.revoke(std::io::Cursor::new(&include_bytes!(concat!(
                    "../../tests/data/",
                    $fingerprint,
                    ".rev"
                ))))?)?;
                // Make sure the certificate in the store is now revoked.
                let revoked_cert = store.get_cert_by_fingerprint(&fingerprint)?;
                assert!(matches!(
                    revoked_cert.revocation_status(),
                    RevocationStatus::Revoked(_)
                ));
                if let RevocationStatus::Revoked(sig) = revoked_cert.revocation_status() {
                    assert_eq!(sig.len(), 1);
                    let (reason, message) = sig[0]
                        .reason_for_revocation()
                        .expect("This signature has a revocation reason.");
                    assert_eq!(reason, ReasonForRevocation::KeyCompromised);
                    assert_eq!(message, $revocation_message.as_bytes())
                }
            };
        }
        assert_revoke!(
            "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB",
            "CN does not compromise"
        );
        assert_revoke!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF",
            "CN does not remember passwords, passwords remember him!"
        );
        Ok(())
    }

    // Test that certificates can be retrieved from the CertStore.
    #[test]
    fn get_cert() -> Result<(), error::Error> {
        let mut store = generate_tmp_certstore()?;

        // Contains only public material
        let cert_1 = store.import(&Cert::from_bytes(include_bytes!(
            "../../tests/data/1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub"
        ))?)?;
        // Contains both public and secret material.
        let cert_2 = store.import(&Cert::from_bytes(include_bytes!(
            "../../tests/data/B2E961753ECE0B345E718E74BA6F29C998DDD9BF.sec"
        ))?)?;

        // Retrieving all certificates should yield the correct number of certs.
        assert_eq!(store.certs().count(), 2);

        // Retrieving certificates by their fingerprint should work.
        assert_eq!(
            store.get_cert_by_fingerprint(&cert_1.fingerprint())?,
            cert_1
        );
        assert_eq!(
            store.get_cert_by_fingerprint(&cert_2.fingerprint())?,
            cert_2
        );
        let cert1_vec = vec![cert_1];
        let cert2_vec = vec![cert_2];

        // Retrieving certificates by their email should work.
        assert_eq!(
            &store.get_cert_by_email("chuck.norris@roundhouse.org")?,
            &cert1_vec
        );
        assert_eq!(
            &store.get_cert_by_email("chuck.norris@roundhouse.swiss")?,
            &cert2_vec
        );
        assert_eq!(
            &store.get_cert_by_email("Chuck.Norris@roundhouse.org")?,
            &cert1_vec
        );

        // Retrieving certificates by QueryTerm (fingerprint or email) should
        // work.
        assert_eq!(
            &store.get_cert(&QueryTerm::Email("chuck.norris@roundhouse.org".into()),)?,
            &cert1_vec
        );
        assert_eq!(
            &store.get_cert(&QueryTerm::Fingerprint(Cow::Owned(
                "B2E961753ECE0B345E718E74BA6F29C998DDD9BF".parse().unwrap()
            )),)?,
            &cert2_vec
        );

        // Trying to retrieve a non-existing certificate by its fingerprint or
        // email should fail.
        assert!(store.get_cert_by_email("chuck.norris@missing.com").is_err());
        Ok(())
    }

    #[test]
    fn queryterm_from_str() {
        use std::str::FromStr;
        // Passing a valid fingerprint should work.
        assert_eq!(
            QueryTerm::from_str("B2E961753ECE0B345E718E74BA6F29C998DDD9BF").unwrap(),
            QueryTerm::Fingerprint(Cow::Owned(
                "B2E961753ECE0B345E718E74BA6F29C998DDD9BF".parse().unwrap()
            ))
        );
        // Passing a valid email should work.
        assert_eq!(
            QueryTerm::from_str("chuck.norris@roundhouse.gov").unwrap(),
            QueryTerm::Email(Cow::Owned("chuck.norris@roundhouse.gov".into()))
        );
        // Passing non-valid fingerprints or emails should fail.
        assert!(QueryTerm::from_str("not_a_fingerprint_nor_an_email").is_err());
    }

    #[test]
    fn validate_email() -> Result<(), error::PgpError> {
        // Test valid values.
        assert!(is_valid_email("chuck.norris@roundhouse.org"));
        assert!(is_valid_email("chuck@roundhouse.org"));
        assert!(is_valid_email("chuck_norris+@round-house.org.com"));
        assert!(is_valid_email("chûéèà_k.ñörrís+@example.org"));
        assert!(is_valid_email("CHUCK.NORRIS@EXAMPLE.ORG"));

        // Test invalid values.
        assert!(!is_valid_email(""));
        assert!(!is_valid_email(".chuck.norris@roundhouse.org"));
        assert!(!is_valid_email("-chuck.norris@roundhouse.org"));
        assert!(!is_valid_email("chuck.@roundhouse.org"));
        assert!(!is_valid_email("chuck@roundhouse."));
        assert!(!is_valid_email("chuck"));
        assert!(!is_valid_email("chuck.norris"));
        assert!(!is_valid_email("chuck@"));
        assert!(!is_valid_email("chuck@.org"));
        assert!(!is_valid_email("chuck.@roundhouse.oo7"));
        Ok(())
    }

    #[test]
    fn cert_type_to_string() -> Result<(), error::PgpError> {
        // Verify that variants of CertType are correctly converted to
        // string.
        assert_eq!(format!("{}", CertType::Public), "public");
        assert_eq!(format!("{}", CertType::Secret), "private");
        Ok(())
    }
}
