use sett::openpgp::{
    cert::{Cert, CertType},
    keystore::KeyStore,
};

#[tokio::test]
async fn export_secret() {
    let mut key_store = KeyStore::open_ephemeral().await.unwrap();
    let pub_cert_ref = Cert::from_bytes(include_bytes!(
        "./data/1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub"
    ))
    .unwrap();
    assert!(matches!(pub_cert_ref.cert_type(), CertType::Public));

    // When the key store is empty, an error should be returned.
    let updated_cert = pub_cert_ref
        .clone()
        .update_secret_material(&mut key_store)
        .await;
    assert!(updated_cert.is_err());
    assert!(updated_cert
        .unwrap_err()
        .to_string()
        .starts_with("No secret material was found "));

    // After importing the secret material to the key store the certificate
    // should be updated.
    let sec_cert_ref = Cert::from_bytes(include_bytes!(
        "./data/1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.sec"
    ))
    .unwrap();
    key_store.import(sec_cert_ref.clone()).await.unwrap();
    let updated_cert = pub_cert_ref
        .clone()
        .update_secret_material(&mut key_store)
        .await
        .unwrap();
    assert!(matches!(updated_cert.cert_type(), CertType::Secret));

    // Note: since Sequoia certs check equality without considering the secret
    // material, certs are here serialized to TSKs (Transferable Secret Keys)
    // in order to compare them.
    // https://docs.rs/sequoia-openpgp/1.20.0/sequoia_openpgp/struct.Cert.html#a-note-on-equality.
    assert_eq!(
        Vec::<u8>::try_from(updated_cert.secret().unwrap()).unwrap(),
        Vec::<u8>::try_from(sec_cert_ref.secret().unwrap()).unwrap()
    );
}
