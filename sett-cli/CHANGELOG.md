# Changelog

All notable changes to this project will be documented in this file.

## [5.6.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dcli%2F5%2E6%2E0) - 2025-02-18

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dcli%2F5%2E5%2E0...sett%2Dcli%2F5%2E6%2E0)


### ✨ Features

- **tui:** Use `enter` for form submission ([4722849](https://gitlab.com/biomedit/sett-rs/commit/472284921542b79b468b584c404fed55343ec53f))
- **tui::decrypt:** Use tabs in decrypt source selection dialog ([22b479e](https://gitlab.com/biomedit/sett-rs/commit/22b479eecefff21813f9e9b23cb4e9be6a10ec90)), Close #397
- **tui::decrypt:** Add authenticated mode ([f47a0a5](https://gitlab.com/biomedit/sett-rs/commit/f47a0a5d03516458c887e455c9c4a2ed15bbdadf)), Close #389

### 🐞 Bug Fixes

- **tui:** Handle UTF-8 characters ([0d2f867](https://gitlab.com/biomedit/sett-rs/commit/0d2f867188dbe67ea7d0408f1c3b9a0253439db8)), Close #410
- **encrypt:** Make positional argument required ([a6ce492](https://gitlab.com/biomedit/sett-rs/commit/a6ce4928e51a02eac4ce7d8868df035819e97e8d))

### 🧹 Refactoring

- Use the new `sett::auth::Oidc::new` API ([0511d68](https://gitlab.com/biomedit/sett-rs/commit/0511d6881a036627e08e6664376295c19e3f2c2e))

## [5.5.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dcli%2F5%2E5%2E0) - 2025-01-22

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dcli%2F5%2E4%2E0...sett%2Dcli%2F5%2E5%2E0)


### ✨ Features

- **tut::encrypt:** Add authenticated mode ([f3f6033](https://gitlab.com/biomedit/sett-rs/commit/f3f60334ece1f49c6fdac0d57437ef9682329b7b)), Close #388
- **tui::decrypt:** Show package location ([8e43806](https://gitlab.com/biomedit/sett-rs/commit/8e4380664609cdae556a96bbe275c721ef41a18b))
- **tui::decrypt:** Add direct decryption from s3 ([ed32858](https://gitlab.com/biomedit/sett-rs/commit/ed32858e454a4ac68d491e016c4001602e46649e)), Close #394
- **tui::decrypt:** Use TAB to interact with the UI ([21ce89d](https://gitlab.com/biomedit/sett-rs/commit/21ce89dc77488689458a0cccc7746849d24b583e)), Close #387
- **tui:** Add encrypt/send functionality ([361ecb7](https://gitlab.com/biomedit/sett-rs/commit/361ecb715c710b9be974964ea2d95524371c40ba)), Close #362
- **tui::widget::file:** Add more information and allow multiple selection ([098b28c](https://gitlab.com/biomedit/sett-rs/commit/098b28cf1c6ffd5a0a1f4c75a584d6a132bb5f76))
- **auth:** Prevent unintended OIDC issuer URL shortening ([7f01d72](https://gitlab.com/biomedit/sett-rs/commit/7f01d72f94cb487d50875149bf84a4d09600be85))
- Change short URL for device authorization grant ([3853b40](https://gitlab.com/biomedit/sett-rs/commit/3853b406583034fcdee2754eb3f83971fbf34457))

### 🐞 Bug Fixes

- **decrypt:** Use "read" credentials for the S3 source ([736ec25](https://gitlab.com/biomedit/sett-rs/commit/736ec25a90b2cdc48622acd9a3c944abf19605ee)), Close #401
- **tui::encrypt:** Fix typo in transfer id component ([c44ed65](https://gitlab.com/biomedit/sett-rs/commit/c44ed65b06467fac50406c8adda0a29a17ed2e95))
- **tui::progress:** Reset progress before starting a new job ([de26ebd](https://gitlab.com/biomedit/sett-rs/commit/de26ebdb8bc141423d1f17de52b72d53cdded65d))
- **tui::encrypt:** Use better layout constraints ([2a82852](https://gitlab.com/biomedit/sett-rs/commit/2a828521720ac9baa01c5a9e807199eabdb1aff5))
- **tui:** Exit app only on esc and ctrl+c ([03fe9da](https://gitlab.com/biomedit/sett-rs/commit/03fe9da5822715726e92221c1861de59b4b6ed73))
- **tui::widget::progress:** Unify colors ([d5947ee](https://gitlab.com/biomedit/sett-rs/commit/d5947eeccbe1ce68f734d7e8e308192e7f8ee043))
- **tui:** Decrease progress bar refresh rate ([8b800c5](https://gitlab.com/biomedit/sett-rs/commit/8b800c55e9e1990a100b2af9988c4b05b4049c37))

### 🧱 Build system and dependencies

- Remove `anyhow` from workspace dependencies ([ec616c0](https://gitlab.com/biomedit/sett-rs/commit/ec616c0f768c532e59038b9938d631953bcada45))
- Remove `tui` feature flag ([b0a2a60](https://gitlab.com/biomedit/sett-rs/commit/b0a2a609006c08632edcbc66502b477e091d94c0)), Close #392
- **sftp::upload:** Use the reference-based library API ([50bf080](https://gitlab.com/biomedit/sett-rs/commit/50bf080b79cfb85b5d27dc4281cf61841c6e524f))

### 🧹 Refactoring

- **tui::widget::field::Field:** Add default implementations ([62260e3](https://gitlab.com/biomedit/sett-rs/commit/62260e3b7c4de2d33b0b383f002e375c6683a79b))
- Move `get_portal_client` to `crate::util` ([4b9de8f](https://gitlab.com/biomedit/sett-rs/commit/4b9de8fc221c6481968c09011389fb1e924098b6))
- **tui::form:** Extract Form into a separate module ([325c895](https://gitlab.com/biomedit/sett-rs/commit/325c89592a1448b22358b51dda9734918f474ff4))
- **tui::cert:** Extract cert rendering into a widget ([11744d5](https://gitlab.com/biomedit/sett-rs/commit/11744d5b095b5584be3cb296910d20533186fc5b))

## [5.4.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dcli%2F5%2E4%2E0) - 2024-12-17

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dcli%2F5%2E3%2E0...sett%2Dcli%2F5%2E4%2E0)


### ✨ Features

- **tui:** Add user authentication ([c7bf685](https://gitlab.com/biomedit/sett-rs/commit/c7bf6851d1c91568265f068e395c2ea08a714116)), Close #363
- **tui:** Add initial implementation of Terminal User Interface ([51cd90e](https://gitlab.com/biomedit/sett-rs/commit/51cd90e6f9365608427c6b83d049429bd7bbe110)), Close #303
- Add decrypt portal subcommand ([e107fd9](https://gitlab.com/biomedit/sett-rs/commit/e107fd9f5d446485b2e57419bff68ca4b1e59525))

### 🐞 Bug Fixes

- **auth:** Hide all auth-related code behind the `auth` flag ([8b0f763](https://gitlab.com/biomedit/sett-rs/commit/8b0f7635a872e418d3fa558c499dd380ba2a1187))

### 🧱 Build system and dependencies

- Migrate to setts API relocations ([d40ac1d](https://gitlab.com/biomedit/sett-rs/commit/d40ac1d61f1c0efe18eca8d219e43902ea5892c7))
- Migrate to sett's new certificate API ([c10eddc](https://gitlab.com/biomedit/sett-rs/commit/c10eddcd23d605a71da262cda1e7c20345af3d10))
- Migrate to the new sett error API in sett::portal ([7bd1696](https://gitlab.com/biomedit/sett-rs/commit/7bd1696d0cbbf2d131cb5818ade2c51cba835447))
- Migrate to the new sett error API in remote::s3 ([5b91982](https://gitlab.com/biomedit/sett-rs/commit/5b919824e03742700fcc7b4f8618b6d5356c126e))
- Migrate to the new sett API for sftp 2 factor callbacks ([d75baa2](https://gitlab.com/biomedit/sett-rs/commit/d75baa2692b7c4cb6dfbc47bf2e0e95723899e70))

### 📝 Documentation

- Fix warnings ([62f33bc](https://gitlab.com/biomedit/sett-rs/commit/62f33bc7edb46c9bcbda8e58f044cf89d3186143))

### 🧹 Refactoring

- **auth:** Use owned value in auth_handler callback ([4b14a8f](https://gitlab.com/biomedit/sett-rs/commit/4b14a8f2c5507ddf4e1db916fee83a640cd50564))
- **log:** Make console logging optional ([fbceebd](https://gitlab.com/biomedit/sett-rs/commit/fbceebd8f073e9ae242f209cad3c54b2614b4fa5))
- Fix clippy warnings ([639737c](https://gitlab.com/biomedit/sett-rs/commit/639737c775f92ba5887ddcdec583ca085d2ddcf3))
- **auth:** Replace anyhow errors by transparent error types ([bb41ca7](https://gitlab.com/biomedit/sett-rs/commit/bb41ca7748cbd15ec1fe3a5416a0b355e418c170))
- Get rid of unneccessary macros ([6058836](https://gitlab.com/biomedit/sett-rs/commit/605883600fc70fc6611871d4668dd214ebbd1713))

## [5.3.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dcli%2F5%2E3%2E0) - 2024-10-18

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dcli%2F5%2E2%2E0...sett%2Dcli%2F5%2E3%2E0)


### ✨ Features

- **keys:** Use more granular arguments for specifying User ID ([00dff26](https://gitlab.com/biomedit/sett-rs/commit/00dff262766306f2185a001022913533774966d4)), Close #349
- **keys:** Add subcommand for changing expiration time ([ca86077](https://gitlab.com/biomedit/sett-rs/commit/ca8607775d67ecb753e871ef27086bdfdfecca6b)), Close #288

### 🐞 Bug Fixes

- Improve key hints when asking for password ([1d331c6](https://gitlab.com/biomedit/sett-rs/commit/1d331c6e647952a74b113f5406585c637903211d))

### 🧹 Refactoring

- **decrypt:** Get rid of the decrypt! macro ([ec2c3c9](https://gitlab.com/biomedit/sett-rs/commit/ec2c3c9005a0955c9112efa0e8216ef0b0d9acc6))

## [5.2.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dcli%2F5%2E2%2E0) - 2024-09-17

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dcli%2F5%2E1%2E0...sett%2Dcli%2F5%2E2%2E0)


### ✨ Features

- Prompt for password only when needed ([f35e12e](https://gitlab.com/biomedit/sett-rs/commit/f35e12ee304d29b796571e9e0b087c9308885335)), Close #274, #275
- Add inspect subcommand ([935e9cd](https://gitlab.com/biomedit/sett-rs/commit/935e9cd33c46df990d596d5a58ea955d8dbfc9ac)), Close #299
- **decrypt:** Add decryption from s3 ([3f139dd](https://gitlab.com/biomedit/sett-rs/commit/3f139ddb432521c10b8930b5d6b56648b0e1bf24))
- **keys:** Allow specifying certificate expiration time ([a27f675](https://gitlab.com/biomedit/sett-rs/commit/a27f6754d4708a1751f9f804e21dad1aab3cebf3)), Close #151
- **encrypt:** Add new arg for custom metadata ([a5d245e](https://gitlab.com/biomedit/sett-rs/commit/a5d245ec43f2ad68a37bd7367842070928199bbd))

### 🐞 Bug Fixes

- Remove unused `--password` flag ([3f6d1ce](https://gitlab.com/biomedit/sett-rs/commit/3f6d1cec71053c5f7763812609dbe10c4c9d8a84)), Close #329
- **keys:** Fix name of a conditionally required flag ([06c00b2](https://gitlab.com/biomedit/sett-rs/commit/06c00b28072b22cc8be6788bdd750c7ec5a16e5c))
- **log:** Use a better log file name format and extension ([a541ff7](https://gitlab.com/biomedit/sett-rs/commit/a541ff7481fc485ba155ca6d15e312ca30099685))
- **log:** Use default debug level only on the sett crates ([d590991](https://gitlab.com/biomedit/sett-rs/commit/d590991f8debe1f488211a4e65e1de247250f4a2))
- **keys:** Add logging ([942037c](https://gitlab.com/biomedit/sett-rs/commit/942037c0f46f7d55cd77a5c3a769b2ebaafa7cb3))

### 🧱 Build system and dependencies

- Move tracing crates to the top-level Cargo.toml ([78746d4](https://gitlab.com/biomedit/sett-rs/commit/78746d4da8a28a7c8863a082a8e00f3d4bab075a))

### 📝 Documentation

- Wrap comment line length at 80 chars ([176fa1f](https://gitlab.com/biomedit/sett-rs/commit/176fa1fbab8c12f7afc22f58ca70d48a1cbf62da))
- **CLI:** Improve help message for `session_token` and `output` ([d5ad6ba](https://gitlab.com/biomedit/sett-rs/commit/d5ad6bac6ff78b420810d90904003b4b5ddae72d))

### 🧹 Refactoring

- Use latest `sett` crate ([c8d0f8f](https://gitlab.com/biomedit/sett-rs/commit/c8d0f8f6933330ff3505281fa56a871b50526cb2))
- Enable 'auth' feature by default ([f7ad5d6](https://gitlab.com/biomedit/sett-rs/commit/f7ad5d63c2c45226436e4eda0eff8fa8ffc45690))
- **metadata:** Rename metadata field `custom` to `extra` ([31b3926](https://gitlab.com/biomedit/sett-rs/commit/31b3926fa9ee01de950e9d64d77c3a3ef091c195)), Close #320
- Use the new `KeyStore` and `CertStore` API ([715e12f](https://gitlab.com/biomedit/sett-rs/commit/715e12fbae8127d52968433624d825e830d09d3f))
- Adapt to change in Cert::update_secret_material of sett ([28c84fd](https://gitlab.com/biomedit/sett-rs/commit/28c84fd187d4c0354ee8f42f5cd372e0e16350e3))
- **metadata:** Use fixed custom field format ([5da8d2d](https://gitlab.com/biomedit/sett-rs/commit/5da8d2df8573270e764445697b3738ec6d56cb75))
- Use latest `sett` crate API ([09f2faf](https://gitlab.com/biomedit/sett-rs/commit/09f2fafaed33acf92709058fd80f4e847a07e49d))
- Use the new Progress interface ([b179803](https://gitlab.com/biomedit/sett-rs/commit/b1798038277c38162931add1c713831c4a25dc02))
- Use the new download from keyserver API ([01f4b5f](https://gitlab.com/biomedit/sett-rs/commit/01f4b5faf6912742dad36ca40936db20cfb13a70))
- Update code for breaking changes in sett ([177bc87](https://gitlab.com/biomedit/sett-rs/commit/177bc8782fe70431a1afa6147e953ba0cb574147))

## [5.1.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dcli%2F5%2E1%2E0) - 2024-06-19

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dcli%2F5%2E0%2E0...sett%2Dcli%2F5%2E1%2E0)


### ✨ Features

- Add a filter option to sett keys list ([f95a3aa](https://gitlab.com/biomedit/sett-rs/commit/f95a3aa443f319e7eb818b23243fa080c80b9f53))
- **sett-cli:** Improve user interaction for authentication process ([b8ee701](https://gitlab.com/biomedit/sett-rs/commit/b8ee7010b8aa177ef9a9f4ad72c77506a2fcb14f)), Close #252
- **sett-cli:** Add portal subcommand and allow fetching data transfers from cli ([e17ed73](https://gitlab.com/biomedit/sett-rs/commit/e17ed737f0193e5ae7dea8d881c4da9baf6d518b)), Close #144

### 🐞 Bug Fixes

- **portal:** Use the latest Portal API schema ([bafe826](https://gitlab.com/biomedit/sett-rs/commit/bafe826247f86678da6a0ed22cf940c704c6c017)), Close #261

### 🚀 Performance

- Use the new, faster async api ([11121c1](https://gitlab.com/biomedit/sett-rs/commit/11121c10dbd048028b57e12d3d4e777cb0218992))

### 🧹 Refactoring

- **decrypt:** Use async decrypt ([69ba26d](https://gitlab.com/biomedit/sett-rs/commit/69ba26db3d9e22c56c484e50750a329ba8f443cb))
- Adapt decrypt options to the breaking change ([57ac573](https://gitlab.com/biomedit/sett-rs/commit/57ac57308e00e2c1ea10dec59e6c9384fa1d9bc1))

## [5.0.0-rc.3](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dcli%2F5%2E0%2E0%2Drc%2E3) - 2024-04-18

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dcli%2F5%2E0%2E0%2Drc%2E2...sett%2Dcli%2F5%2E0%2E0%2Drc%2E3)


### ✨ Features

- Try loading OpenPGP key password from environment ([aa09878](https://gitlab.com/biomedit/sett-rs/commit/aa09878f858000e71fec2ba074b94d942d085c50)), Close #239
- **s3:** Add subcommands that fetch s3 credentials directly from Portal ([377db90](https://gitlab.com/biomedit/sett-rs/commit/377db90a4c54da577d40650581c88a5e995ef7d8))
- Simplify CLI for encrypt and decrypt commands ([ea2e375](https://gitlab.com/biomedit/sett-rs/commit/ea2e3756bd597bdd58c9a102fcd5f83249ad88e1)), Closes #198
- Add optional package verification with Portal ([71ae066](https://gitlab.com/biomedit/sett-rs/commit/71ae0667ba38d7deebe29e57085b63c4431e841d))

### 🐞 Bug Fixes

- Remove disturbing trailing `/` if any from endpoint ([391506a](https://gitlab.com/biomedit/sett-rs/commit/391506ad960f11fb4b567546a7a7b521d5bfbecf)), Closes #211

### 🧱 Build system and dependencies

- Add licenses for each crate ([be2ad30](https://gitlab.com/biomedit/sett-rs/commit/be2ad30accc4693eed5136be70892866aea682f0)), Close #217

### 📝 Documentation

- Improve all project's `README.md` ([365085f](https://gitlab.com/biomedit/sett-rs/commit/365085f285cf23a85727a54977241c8f3970887f)), Close #177
- Add/correct the different `README.md`'s ([d8d39d5](https://gitlab.com/biomedit/sett-rs/commit/d8d39d580ea310db3e9fa0746c1dc83071ccda70))

### 🧹 Refactoring

- **keyserver:** Use the new keyserver API ([03e1701](https://gitlab.com/biomedit/sett-rs/commit/03e1701d2e6b8c626d70d7b188d474f112abbfe0))

## [5.0.0-rc.2](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dcli%2F5%2E0%2E0%2Drc%2E2) - 2024-03-05

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dcli%2F5%2E0%2E0%2Drc%2E1...sett%2Dcli%2F5%2E0%2E0%2Drc%2E2)


### ✨ Features

- Add "--check" to all tasks and report the final output ([5c98865](https://gitlab.com/biomedit/sett-rs/commit/5c98865f170277d1444d9ea0b00939036f67c2c0)), Close #157
- **cli:** Rename certificate(s) to key(s) when interacting with the user ([3331b44](https://gitlab.com/biomedit/sett-rs/commit/3331b4443864d9943effd33c8ac17d40dbd96701))

### 🐞 Bug Fixes

- Reduce the default logging verbosity to warning ([e6aa990](https://gitlab.com/biomedit/sett-rs/commit/e6aa990a7fd1ba0ad72fb61e57cec20b18b60263)), Close #157
- `upload` method not working for S3 destination ([ec4c75e](https://gitlab.com/biomedit/sett-rs/commit/ec4c75e7c9ef3cb4ebaaefe60d03ba2c73de9c63)), Closes #179

### 🧹 Refactoring

- Use async API ([a41698c](https://gitlab.com/biomedit/sett-rs/commit/a41698c74b48346e93a92705f3a52c54a0fc406a))

## [5.0.0-rc.1](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dcli%2F5%2E0%2E0%2Drc%2E1) - 2024-02-01

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dcli%2F5%2E0%2E0%2Drc%2E1...sett%2Dcli%2F5%2E0%2E0%2Drc%2E1)


### ✨ Features

- Add expiration date to output of 'keys list' command ([841c67c](https://gitlab.com/biomedit/sett-rs/commit/841c67c404dd36cbbdd9dd599de1f936518ba9e5)), Closes #154
- Reduce log output generated by `aws_smithy_runtime` ([2a8b8e2](https://gitlab.com/biomedit/sett-rs/commit/2a8b8e2d00a3ca59abab67f71adb0f0a70c79a9e))
- Use tracing for collecting logs ([9b8b606](https://gitlab.com/biomedit/sett-rs/commit/9b8b606cb4b4947c7d172a0b0bb30fc163abb87e))
- **sett-cli:** Move the CLI code into a separate crate ([c0d436b](https://gitlab.com/biomedit/sett-rs/commit/c0d436b21cbe75f879f1537fe2c52c431537f234)), Close #148

### 🐞 Bug Fixes

- `region` and `endpoint` are now optional ([07b8fe2](https://gitlab.com/biomedit/sett-rs/commit/07b8fe2322ac4d4714601b11f7052d1c4ce0d98b))
- Make certificate revocation revoke both public and secret certs ([7a4eb26](https://gitlab.com/biomedit/sett-rs/commit/7a4eb2603a9574b2003b89d1ac932fbbbdb994cf)), Closes #152

### 🧱 Build system and dependencies

- Add CHANGELOG ([e45d448](https://gitlab.com/biomedit/sett-rs/commit/e45d44812ad0fd988bb4de2ceb517d12e77ecd38))

### 🧹 Refactoring

- Change the terms certificate to key and secret to private ([c77fed5](https://gitlab.com/biomedit/sett-rs/commit/c77fed50f5e4a1f9f0d0cc73c5e0fc918429e458)), Closes #156
