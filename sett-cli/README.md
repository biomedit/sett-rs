# sett CLI

Build an optimized `sett` CLI binary:

```shell
cargo build --release
```

Run `../target/release/sett` to see a help message:

```shell
sett command line tool

Usage: sett <COMMAND>

Commands:
  encrypt   Encrypt data package
  decrypt   Decrypt data package
  transfer  Transfer data package
  keys      OpenPGP key management
  help      Print this message or the help of the given subcommand(s)

Options:
  -h, --help     Print help
  -V, --version  Print version
```

Note: For the **Windows** targets it's easier to compile with the `crypto-cng`
backend:

```shell
cargo build --release --no-default-features --features=crypto-cng
```
