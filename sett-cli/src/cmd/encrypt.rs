use std::{
    path::{Path, PathBuf},
    str::FromStr,
};

use anyhow::{ensure, Context, Result};
use chrono::Utc;
use clap::{Args, Subcommand, ValueEnum};
use sett::{destination::Destination, task::Mode};

use crate::util::get_portal_client;

/// Parse a single key-value pair
fn parse_key_val<T: FromStr, U: FromStr>(input: &str) -> Result<(T, U)>
where
    <T as FromStr>::Err: std::error::Error + Send + Sync + 'static,
    <U as FromStr>::Err: std::error::Error + Send + Sync + 'static,
{
    let idx = input.trim().find('=').with_context(|| {
        format!(
            "invalid extra metadata argument: no `=` found in `{}` (format must be NAME=VALUE)",
            input
        )
    })?;
    Ok((input[..idx].parse()?, input[idx + 1..].parse()?))
}

#[derive(Subcommand)]
pub(crate) enum Encrypt {
    /// Encrypt and store data in the local file system.
    Local(Local),
    /// Encrypt and send data directly to an S3 bucket.
    S3(S3),
    #[cfg(feature = "auth")]
    /// Encrypt and send data directly to an S3 bucket with credentials
    /// fetched from Portal.
    S3Portal(S3Portal),
    /// Encrypt and send data directly to an SFTP server.
    Sftp(Sftp),
}

#[derive(Clone, Copy, ValueEnum, Debug)]
pub enum CompressionAlgorithm {
    /// No compression.
    Stored,
    /// Use gzip compression.
    Gzip,
    /// Use zstandard compression.
    Zstandard,
}

#[derive(Args)]
pub(crate) struct Local {
    #[command(flatten)]
    encryption: EncryptionArgs,

    /// Path or name of the encrypted data package. If not specified, the
    /// output is sent to stdout, which is useful for piping to another
    /// command.
    #[arg(short, long)]
    output: Option<PathBuf>,
}

#[derive(Args)]
pub(crate) struct S3 {
    #[command(flatten)]
    encryption: EncryptionArgs,

    #[command(flatten)]
    s3: super::common::S3Args,
}

#[cfg(feature = "auth")]
#[derive(Args)]
pub(crate) struct S3Portal {
    #[command(flatten)]
    encryption: EncryptionArgs,
}

#[derive(Args)]
pub(crate) struct Sftp {
    #[command(flatten)]
    encryption: EncryptionArgs,

    #[command(flatten)]
    sftp: super::common::SftpArgs,
}

#[derive(Args)]
struct EncryptionArgs {
    #[command(flatten)]
    display: super::common::DisplayArgs,

    #[command(flatten)]
    mode: super::common::ModeArgs,

    /// Signer OpenPGP key identifier.
    ///
    /// Fingerprint or email address of the private key with which to sign the
    /// data (usually this is the key of the data sender). The signer's key
    /// must be present in the local keystore. Mutually exclusive with
    /// `--signer-path`.
    #[arg(
        short,
        long,
        conflicts_with = "signer_path",
        required_unless_present = "signer_path",
        value_name = "SIGNER_KEY"
    )]
    signer: Option<String>,

    /// Signer OpenPGP key file path.
    ///
    /// Path of file containing the data signer's private key. This argument
    /// is an alternative to `--signer`, allowing to specify the signer's key
    /// as a file on disk. Mutually exclusive with `--signer`.
    #[arg(short = 'S', long, required_unless_present = "signer")]
    pub(super) signer_path: Option<PathBuf>,

    /// Recipient OpenPGP key identifier.
    ///
    /// Fingerprint or email of the public key for which to encrypt the data.
    /// More than one recipient can be specified by passing this argument
    /// multiple times. The recipient's key must be present in the local
    /// keystore. At least one of `--recipient` or `--recipient-path` must
    /// be given.
    #[arg(
        short,
        long,
        required_unless_present = "recipient_path",
        value_name = "RECIPIENT_KEY"
    )]
    recipient: Option<Vec<String>>,

    /// Recipient OpenPGP key file path.
    ///
    /// Path of file containing a public key for which to encrypt the data.
    /// This argument is an alternative to `--recipient`, allowing to specify a
    /// recipient's key as a file on disk.
    /// More than one recipient can be specified by passing this argument
    /// multiple times. At least one of `--recipient` or `--recipient-path`
    /// must be given.
    #[arg(short = 'R', long, required_unless_present = "recipient")]
    recipient_path: Option<Vec<PathBuf>>,

    /// Compression algorithm.
    #[arg(long, default_value = "zstandard")]
    compression_algorithm: CompressionAlgorithm,

    /// Compression level.
    ///
    /// Possible values depend on the selected compression
    /// algorithm:
    /// - zstandard: 1 (lowest compression, fastest) - 21 (highest compression,
    ///   slowest), default: 3.
    /// - gzip: 1 (lowest compression, fastest) - 9 (highest compression,
    ///   slowest), default: 6.
    /// - stored: not applicable.
    #[arg(long)]
    compression_level: Option<u8>,

    /// Data Transfer Request (DTR) number (required for s3-portal).
    #[arg(long)]
    dtr: Option<u32>,

    /// Perform an additional data package verification using Portal.
    #[arg(long, requires = "dtr")]
    verify: bool,

    /// File(s) and/or directory(ies) to encrypt and package.
    #[arg(required = true)]
    file: Vec<PathBuf>,

    /// Extra fields to include in the data package metadata.
    ///
    /// Repeat this argument to add multiple key-value pairs or use the
    /// delimiter `,` to separate multiple pairs.
    #[arg(short, long, env="SETT_METADATA_EXTRA", value_name="NAME=VALUE", value_parser = parse_key_val::<String, String>, value_delimiter = ',')]
    metadata: Vec<(String, String)>,
}

/// Converts a `CompressionAlgorithm` enum that has no associated compression
/// level, to a `sett::dpkg::CompressionAlgorithm` that has the specified
/// compression `level` associated to it.
fn compression_algo_with_level(
    algo: CompressionAlgorithm,
    level: Option<u8>,
) -> Result<sett::package::CompressionAlgorithm> {
    let assert_in_range = |level: Option<u8>, max: u8| {
        level
            .map(|l| {
                ensure!(
                    (1..max + 1).contains(&l),
                    format!("Compression level is outside of the allowed range (1-{max})")
                );
                Ok(l)
            })
            .transpose()
    };
    Ok(match algo {
        CompressionAlgorithm::Stored => sett::package::CompressionAlgorithm::Stored,
        CompressionAlgorithm::Gzip => {
            sett::package::CompressionAlgorithm::Gzip(assert_in_range(level, 9)?.map(|l| l as u32))
        }
        CompressionAlgorithm::Zstandard => sett::package::CompressionAlgorithm::Zstandard(
            assert_in_range(level, 21)?.map(|l| l as i32),
        ),
    })
}

impl Encrypt {
    pub(crate) async fn run(self) -> Result<()> {
        match self {
            Self::Local(c) => c.run().await,
            Self::S3(c) => c.run().await,
            #[cfg(feature = "auth")]
            Self::S3Portal(c) => c.run().await,
            Self::Sftp(c) => c.run().await,
        }
    }
}

trait EncryptSubcommand {
    async fn destination(&self) -> Result<Destination>;
    fn into_args(self) -> EncryptionArgs;
}

impl EncryptSubcommand for Local {
    async fn destination(&self) -> Result<Destination> {
        Ok(if let Some(output) = &self.output {
            {
                let (path, name) = parse_output_path(output)?;
                ensure!(
                    !self.encryption.verify || name.is_none(),
                    "Custom package name is not allowed with --verify"
                );
                sett::destination::Local::new(path, name)?.into()
            }
        } else {
            Destination::Stdout
        })
    }

    fn into_args(self) -> EncryptionArgs {
        self.encryption
    }
}

impl EncryptSubcommand for S3 {
    async fn destination(&self) -> Result<Destination> {
        Ok(sett::destination::S3::new(
            super::common::get_s3_client(&self.s3).await?,
            self.s3.bucket.clone(),
        )
        .into())
    }

    fn into_args(self) -> EncryptionArgs {
        self.encryption
    }
}

#[cfg(feature = "auth")]
impl EncryptSubcommand for S3Portal {
    async fn destination(&self) -> Result<Destination> {
        let dtr = self.encryption.dtr.context("missing DTR")?;
        let portal_client = get_portal_client()?;
        let oidc_client = crate::util::get_oidc_client().await?;
        let token = oidc_client
            .authenticate(super::common::auth_handler)
            .await?;
        let connection_details = portal_client
            .get_s3_connection_details(dtr, sett::portal::S3Permission::Write, &token.access_token)
            .await?;
        let client = connection_details.build_client().await?;
        Ok(sett::destination::S3::new(client, connection_details.bucket).into())
    }

    fn into_args(self) -> EncryptionArgs {
        self.encryption
    }
}

impl EncryptSubcommand for Sftp {
    async fn destination(&self) -> Result<Destination> {
        Ok(sett::destination::Sftp::new(
            super::common::get_sftp_client(&self.sftp),
            self.sftp.base_path.clone(),
        )
        .into())
    }

    fn into_args(self) -> EncryptionArgs {
        self.encryption
    }
}

/// Formats the password `hint` of an OpenPGP key as a string.
pub(super) fn hint_to_string(hint: sett::openpgp::crypto::PasswordHint) -> String {
    if let Some(fingerprint_primary) = hint.fingerprint_primary {
        let userid = hint.userid.as_deref().unwrap_or("--missing user ID--");
        if fingerprint_primary == hint.fingerprint {
            // Key is a primary key.
            format!("{userid} {fingerprint_primary}")
        } else {
            // Key is a subkey: both the primary and the subkey's fingerprints
            // are included in the string to help better identify the key.
            format!(
                "{} {} (subkey {})",
                userid, fingerprint_primary, hint.fingerprint
            )
        }
    } else {
        format!("{} (key is not part of a certificate)", hint.fingerprint)
    }
}

async fn prompt_password(hint: sett::openpgp::crypto::PasswordHint) -> sett::secret::Secret {
    if let Ok(password) = std::env::var("SETT_OPENPGP_KEY_PWD") {
        return password.into();
    } else if let Ok(password_file) = std::env::var("SETT_OPENPGP_KEY_PWD_FILE") {
        if let Ok(content) = tokio::fs::read_to_string(&password_file).await {
            return content.trim().into();
        } else {
            tracing::warn!("Unable to load password from file {password_file}");
        }
    }
    if let Ok(Ok(password)) = tokio::task::spawn_blocking(move || {
        rpassword::prompt_password(format!(
            "Please enter the password to unlock the signing key {}: ",
            hint_to_string(hint),
        ))
    })
    .await
    {
        password.into()
    } else {
        tracing::error!("Failed to read password");
        "".into()
    }
}

async fn run_encrypt_subcommand(subcommand: impl EncryptSubcommand) -> Result<()> {
    let dest = subcommand.destination().await?;
    let args = subcommand.into_args();
    let _log_guard = crate::log::init_file_and_console_log(args.display.verbosity)?;
    let timestamp = Utc::now();
    let compression_algorithm =
        compression_algo_with_level(args.compression_algorithm, args.compression_level)?;
    let mut key_store = super::common::open_key_store(args.signer_path.as_deref()).await?;
    let cert_store =
        super::common::open_cert_store(args.recipient_path.as_ref().map(|p| p.iter()))?;
    let recipients = if let Some(recipients) = &args.recipient {
        // Search for the recipient's public key in the local cert store.
        recipients
            .iter()
            .map(|r| {
                super::certstore::get_single_cert(&r.parse()?, &cert_store).map(|c| c.fingerprint())
            })
            .collect::<Result<Vec<_>>>()?
    } else {
        // Use all certificates loaded from the recipient's certificate files.
        cert_store
            .certs()
            .filter_map(|c| {
                c.map_or_else(
                    |e| {
                        tracing::warn!("Key parsing error: {e}");
                        None
                    },
                    |cert| Some(cert.fingerprint()),
                )
            })
            .collect()
    };

    let signer = if let Some(p) = &args.signer_path {
        // Import the signer's private keys from the provided TSK file.
        // Use the first certificate's fingerprint as the signer's fingerprint.
        let mut signer_fingerprint = None;
        for cert in sett::openpgp::cert::parse_certs(std::fs::File::open(p)?)? {
            if signer_fingerprint.is_none() {
                signer_fingerprint = Some(cert.fingerprint());
            }
            key_store.import(cert).await?;
        }
        // Use the first certificate's fingerprint as the signer's key.
        signer_fingerprint.context("No signing key found")?
    } else {
        super::certstore::get_single_cert(
            &args
                .signer
                .as_deref()
                .expect("signer identifier is provided")
                .parse()?,
            &cert_store,
        )?
        .fingerprint()
    };

    // If the user requested verification from Portal (this requires that a
    // DTR ID was provided), check the data package's metadata and use the
    // projects "code" as prefix for the name of the generated data package.
    let prefix = if args.verify {
        Some(
            get_portal_client()?
                .check_package(
                    &sett::package::Metadata {
                        sender: signer.to_hex(),
                        recipients: recipients.iter().map(|r| r.to_hex()).collect(),
                        timestamp,
                        compression_algorithm,
                        transfer_id: args.dtr,
                        ..Default::default()
                    },
                    "missing",
                )
                .await?
                .project_code,
        )
    } else {
        None
    };

    // Package, encrypt, and optionally transfer, the data.
    let encrypt_opts = sett::encrypt::EncryptOpts {
        files: args.file.clone(),
        recipients,
        signer,
        cert_store,
        key_store,
        mode: if args.mode.check {
            Mode::Check
        } else {
            Mode::Run
        },
        purpose: None,
        extra_metadata: args.metadata.into_iter().collect(),
        transfer_id: args.dtr,
        compression_algorithm,
        password: prompt_password,
        progress: (!args.display.quiet).then(crate::util::CliProgress::new),
        timestamp,
        prefix,
    };
    let status = sett::encrypt::encrypt(encrypt_opts, dest).await?;
    super::common::Task::Encrypt.print_status(&status);
    Ok(())
}

impl Local {
    async fn run(self) -> Result<()> {
        run_encrypt_subcommand(self).await
    }
}

impl S3 {
    async fn run(self) -> Result<()> {
        run_encrypt_subcommand(self).await
    }
}

#[cfg(feature = "auth")]
impl S3Portal {
    async fn run(self) -> Result<()> {
        run_encrypt_subcommand(self).await
    }
}

impl Sftp {
    async fn run(self) -> Result<()> {
        run_encrypt_subcommand(self).await
    }
}

fn to_absolute_path(path: &Path) -> Result<PathBuf> {
    Ok(if path.is_relative() {
        std::env::current_dir()?.join(path)
    } else {
        path.to_path_buf()
    })
}

fn parse_output_path(path: &Path) -> Result<(PathBuf, Option<String>)> {
    let path = to_absolute_path(path)?;
    Ok(if path.is_dir() {
        (path.clone(), None)
    } else {
        let d = path
            .parent()
            .context("Unable to find directory for the given output")?
            .to_path_buf();
        ensure!(d.exists(), "Output directory does not exist {:?})", d);
        let mut f = path
            .file_name()
            .expect("There is always a file at this point")
            .to_string_lossy()
            .to_string();
        if !f.ends_with(".zip") {
            f.push_str(".zip");
        }
        (d, Some(f))
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_output_path() -> Result<()> {
        assert!(parse_output_path(Path::new("does/not/exist")).is_err());
        let tmp = std::env::temp_dir();
        assert_eq!(parse_output_path(&tmp)?, (tmp.clone(), None));
        assert_eq!(
            parse_output_path(&tmp.join("package.zip"))?,
            (tmp.clone(), Some("package.zip".to_owned()))
        );
        assert_eq!(
            parse_output_path(&tmp.join("package.foo"))?,
            (tmp, Some("package.foo.zip".to_owned()))
        );
        Ok(())
    }
}
