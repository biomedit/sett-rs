use std::{io::Write as _, path::PathBuf};

use clap::{command, Args, Subcommand};
use sett::package::Package;

#[derive(Subcommand)]
pub(crate) enum Inspect {
    /// Inspect data package from the local filesystem.
    Local(Local),
    /// Inspect data package from an S3 object store.
    S3(S3),
}

#[derive(Args)]
pub(crate) struct Local {
    #[command(flatten)]
    inspect_args: InspectArgs,

    /// Data package to inspect.
    package: PathBuf,
}

#[derive(Args)]
pub(crate) struct S3 {
    #[command(flatten)]
    inspect_args: InspectArgs,

    #[command(flatten)]
    s3: super::common::S3Args,

    /// Data package to inspect (file name).
    package: String,
}

#[derive(clap::Args)]
pub(crate) struct InspectArgs {
    /// Show the raw, JSON-formatted metadata representation
    #[arg(long)]
    json: bool,

    /// OpenPGP key file path (optional).
    ///
    /// Path of file containing the OpenPGP public keys.
    /// It is used for verifying the authenticity of the data package.
    /// When missing, the default key store is used.
    #[arg(long)]
    keys: Option<PathBuf>,
}

impl Inspect {
    pub(crate) async fn run(self) -> anyhow::Result<()> {
        let _log_guard = crate::log::init_file_and_console_log(crate::log::Level::Warn)?;
        match self {
            Self::Local(args) => {
                print_metadata(
                    Package::open(args.package).await?,
                    args.inspect_args.json,
                    args.inspect_args.keys,
                )
                .await
            }
            Self::S3(args) => {
                print_metadata(
                    Package::open_s3(
                        &super::common::get_s3_client(&args.s3).await?,
                        args.s3.bucket,
                        args.package,
                    )
                    .await?,
                    args.inspect_args.json,
                    args.inspect_args.keys,
                )
                .await
            }
        }
    }
}

async fn print_metadata(package: Package, json: bool, keys: Option<PathBuf>) -> anyhow::Result<()> {
    let certstore = super::common::open_cert_store(keys.as_ref().map(std::iter::once))?;
    let metadata = package.verify(&certstore).await?.metadata().await?;
    if json {
        println!("{}", serde_json::to_string(&metadata)?);
    } else {
        use sett::package::CompressionAlgorithm::*;
        let get_uid = |fingerprint: &str| -> Option<_> {
            let uid = certstore
                .get_cert(&fingerprint.parse().ok()?)
                .ok()?
                .pop()?
                .userids()
                .pop()?;
            Some(format!("{} {}", uid, fingerprint))
        };
        let rows = vec![
            (
                "Sender",
                get_uid(&metadata.sender).unwrap_or(metadata.sender),
            ),
            (
                "Recipients",
                metadata
                    .recipients
                    .into_iter()
                    .map(|fingerprint| get_uid(&fingerprint).unwrap_or(fingerprint))
                    .collect::<Vec<_>>()
                    .join(", "),
            ),
            ("Date created", metadata.timestamp.to_string()),
            ("Checksum", metadata.checksum),
            (
                "Checksum algorithm",
                format!("{:?}", metadata.checksum_algorithm),
            ),
            (
                "Compression algorithm",
                match metadata.compression_algorithm {
                    Gzip(_) => "gzip".to_string(),
                    Stored => "uncompressed".to_string(),
                    Zstandard(_) => "zstandard".to_string(),
                },
            ),
            ("Metadata version", metadata.version),
            (
                "Data transfer ID",
                metadata
                    .transfer_id
                    .map_or_else(|| "-".to_string(), |dtr| format!("{dtr}")),
            ),
            (
                "Purpose",
                metadata
                    .purpose
                    .map_or_else(|| "-".to_string(), |purpose| format!("{purpose:?}")),
            ),
        ];
        let col_max_len = rows
            .iter()
            .fold(0, |acc, x| acc.max(x.0.len()))
            .max(metadata.extra.keys().fold(0, |acc, x| acc.max(x.len())))
            + 2;
        let mut std_lock = std::io::stdout();
        for row in rows {
            writeln!(
                &mut std_lock,
                "{:width$}{}",
                row.0,
                row.1,
                width = col_max_len
            )?;
        }
        if !metadata.extra.is_empty() {
            writeln!(&mut std_lock)?;
            writeln!(&mut std_lock, "Extra metadata")?;
            for (key, value) in metadata.extra.iter() {
                writeln!(
                    &mut std_lock,
                    "{:width$}{}",
                    key,
                    value,
                    width = col_max_len
                )?;
            }
        }
    }
    Ok(())
}
