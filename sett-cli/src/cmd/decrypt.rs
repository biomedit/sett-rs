use std::path::PathBuf;

use clap::{command, Args, Subcommand};
use sett::package::Package;

#[derive(Subcommand)]
pub(crate) enum Decrypt {
    /// Decrypt data package from the local filesystem
    Local(Local),
    /// Decrypt data package from an S3 object store
    S3(S3),
    #[cfg(feature = "auth")]
    /// Decrypt data package from an S3 object store with credentials fetched from Portal
    S3Portal(S3Portal),
}

#[derive(Args)]
pub(crate) struct Local {
    #[command(flatten)]
    decryption: DecryptArgs,

    /// Data package to decrypt
    package: PathBuf,
}

#[derive(Args)]
pub(crate) struct S3 {
    #[command(flatten)]
    decryption: DecryptArgs,

    #[command(flatten)]
    s3: super::common::S3Args,

    /// Data package to decrypt (file name)
    package: String,
}

#[cfg(feature = "auth")]
#[derive(Args)]
pub(crate) struct S3Portal {
    #[command(flatten)]
    decryption: DecryptArgs,

    /// Data Transfer Request (DTR) number
    #[arg(long)]
    dtr: u32,

    /// Data package to decrypt (file name)
    package: String,
}

#[derive(clap::Args)]
pub(crate) struct DecryptArgs {
    #[command(flatten)]
    display: super::common::DisplayArgs,

    #[command(flatten)]
    mode: super::common::ModeArgs,

    /// Signer OpenPGP key file path (Optional)
    ///
    /// Path of file containing the signer's public key, used to verify the
    /// authenticity of the data package to decrypt. Only needed when the
    /// signer's key is not present in the local keystore.
    #[arg(short = 'S', long)]
    pub(super) signer_path: Option<PathBuf>,

    /// Recipient OpenPGP key file path (Optional)
    ///
    /// Path of file containing the recipient's private key, used to decrypt
    /// the data package. Only needed when the recipient's key is not present
    /// in the local keystore.
    #[arg(short = 'R', long)]
    recipient_path: Option<PathBuf>,

    /// Output directory
    #[arg(short, long)]
    output: Option<PathBuf>,

    /// Decrypt the package without unpacking files
    #[arg(short, long)]
    decrypt_only: bool,
}

#[cfg(feature = "auth")]
async fn get_package_remote(dtr: u32) -> anyhow::Result<(sett::remote::s3::Client, String)> {
    let portal_client = crate::util::get_portal_client()?;
    let oidc_client = crate::util::get_oidc_client().await?;
    let token = oidc_client
        .authenticate(super::common::auth_handler)
        .await?;
    let connection_details = portal_client
        .get_s3_connection_details(dtr, sett::portal::S3Permission::Read, &token.access_token)
        .await?;
    Ok((
        connection_details.build_client().await?,
        connection_details.bucket,
    ))
}

fn prompt_password(hint: sett::openpgp::crypto::PasswordHint) -> sett::secret::Secret {
    if let Ok(password) = std::env::var("SETT_OPENPGP_KEY_PWD") {
        return password.into();
    } else if let Ok(password_file) = std::env::var("SETT_OPENPGP_KEY_PWD_FILE") {
        if let Ok(content) = std::fs::read_to_string(&password_file) {
            return content.trim().into();
        } else {
            tracing::warn!("Unable to load password from file {password_file}");
        }
    }
    if let Ok(password) = rpassword::prompt_password(format!(
        "Please enter the password to unlock the decryption key {}: ",
        super::encrypt::hint_to_string(hint)
    )) {
        password.into()
    } else {
        tracing::error!("Failed to read password");
        "".into()
    }
}

impl Decrypt {
    async fn open(self) -> anyhow::Result<(Package, DecryptArgs)> {
        match self {
            Self::Local(args) => Ok((Package::open(&args.package).await?, args.decryption)),
            Self::S3(args) => Ok((
                Package::open_s3(
                    &super::common::get_s3_client(&args.s3).await?,
                    args.s3.bucket,
                    args.package,
                )
                .await?,
                args.decryption,
            )),
            #[cfg(feature = "auth")]
            Self::S3Portal(args) => {
                let (client, bucket) = get_package_remote(args.dtr).await?;
                Ok((
                    Package::open_s3(&client, bucket, args.package).await?,
                    args.decryption,
                ))
            }
        }
    }

    pub(crate) async fn run(self) -> anyhow::Result<()> {
        let (package, args) = self.open().await?;
        let output = if let Some(path) = args.output {
            Some(path.canonicalize()?)
        } else {
            None
        };
        let _log_guard = crate::log::init_file_and_console_log(args.display.verbosity)?;
        let status = sett::decrypt::decrypt(sett::decrypt::DecryptOpts {
            package,
            key_store: super::common::open_key_store(args.recipient_path.as_deref()).await?,
            cert_store: super::common::open_cert_store(
                args.signer_path.as_ref().map(std::iter::once),
            )?,
            output,
            decrypt_only: args.decrypt_only,
            mode: if args.mode.check {
                sett::task::Mode::Check
            } else {
                sett::task::Mode::Run
            },
            password: prompt_password,
            progress: (!args.display.quiet).then(crate::util::CliProgress::new),
        })
        .await?;
        super::common::Task::Decrypt.print_status(&status);
        Ok(())
    }
}
