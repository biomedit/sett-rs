use std::str::FromStr;

use anyhow::Context as _;
use chrono::{DateTime, Utc};

const SECONDS_PER_DAY: u64 = 24 * 60 * 60;
const SECONDS_PER_YEAR: u64 = (365.2421897 * SECONDS_PER_DAY as f64) as u64;

#[derive(Clone, Debug, PartialEq, Eq)]
pub(super) enum Expiration {
    Never,
    At(DateTime<Utc>),
    Duration(Duration),
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub(super) enum Duration {
    Seconds(u64),
    Days(u64),
    Weeks(u64),
    Months(u64),
    Years(u64),
}

impl FromStr for Duration {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        anyhow::ensure!(s.len() > 1, "Invalid duration '{}'", s);
        let value = s[..s.len() - 1]
            .parse::<u64>()
            .with_context(|| format!("Invalid duration '{}'", s))?;
        match s.chars().last().expect("str with at least one char") {
            's' => Ok(Self::Seconds(value)),
            'd' => Ok(Self::Days(value)),
            'w' => Ok(Self::Weeks(value)),
            'm' => Ok(Self::Months(value)),
            'y' => Ok(Self::Years(value)),
            _ => Err(anyhow::anyhow!("Invalid duration '{}'", s)),
        }
    }
}

impl std::fmt::Display for Duration {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::Seconds(v) => write!(f, "{}s", v),
            Self::Days(v) => write!(f, "{}d", v),
            Self::Weeks(v) => write!(f, "{}w", v),
            Self::Months(v) => write!(f, "{}m", v),
            Self::Years(v) => write!(f, "{}y", v),
        }
    }
}

impl From<&Duration> for std::time::Duration {
    fn from(duration: &Duration) -> Self {
        match duration {
            Duration::Seconds(v) => Self::from_secs(*v),
            Duration::Days(v) => Self::from_secs(*v * SECONDS_PER_DAY),
            Duration::Weeks(v) => Self::from_secs(*v * 7 * SECONDS_PER_DAY),
            Duration::Months(v) => Self::from_secs(*v * SECONDS_PER_YEAR / 12),
            Duration::Years(v) => Self::from_secs(*v * SECONDS_PER_YEAR),
        }
    }
}

impl Default for Expiration {
    fn default() -> Self {
        Self::Duration(Duration::Years(3))
    }
}

impl FromStr for Expiration {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(if s == "never" {
            Self::Never
        } else if s.len() > 1 && s.ends_with(['y', 'm', 'w', 'd', 's']) {
            Self::Duration(s.parse()?)
        } else {
            let d = DateTime::parse_from_rfc3339(s).context("Invalid datetime format")?;
            anyhow::ensure!(d > Utc::now(), "Expiration date must be in the future");
            Self::At(d.into())
        })
    }
}

impl std::fmt::Display for Expiration {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::Never => write!(f, "never"),
            Self::At(date) => write!(f, "{date}"),
            Self::Duration(duration) => write!(f, "{duration}"),
        }
    }
}

impl Expiration {
    pub(super) fn try_to_duration(&self) -> anyhow::Result<Option<std::time::Duration>> {
        Ok(match self {
            Self::Never => None,
            Self::Duration(d) => Some(d.into()),
            Self::At(datetime) => {
                let duration = datetime.signed_duration_since(Utc::now());
                anyhow::ensure!(
                    duration > chrono::TimeDelta::zero(),
                    "Expiration date must be in the future"
                );
                Some(duration.to_std()?)
            }
        })
    }

    pub(super) fn try_to_system_time(&self) -> anyhow::Result<Option<std::time::SystemTime>> {
        Ok(self
            .try_to_duration()?
            .map(|duration| std::time::SystemTime::now() + duration))
    }
}

#[cfg(test)]
mod tests {
    use super::{Duration, Expiration, SECONDS_PER_DAY, SECONDS_PER_YEAR};

    #[test]
    fn duration_from_str() {
        assert_eq!("1s".parse::<Duration>().unwrap(), Duration::Seconds(1));
        assert_eq!("3d".parse::<Duration>().unwrap(), Duration::Days(3));
        assert_eq!("7w".parse::<Duration>().unwrap(), Duration::Weeks(7));
        assert_eq!("11m".parse::<Duration>().unwrap(), Duration::Months(11));
        assert_eq!("3y".parse::<Duration>().unwrap(), Duration::Years(3));
        assert!("".parse::<Duration>().is_err());
        assert!("1".parse::<Duration>().is_err());
        assert!("1x".parse::<Duration>().is_err());
    }

    #[test]
    fn duration_to_string() {
        assert_eq!(Duration::Seconds(1).to_string(), "1s");
        assert_eq!(Duration::Days(3).to_string(), "3d");
        assert_eq!(Duration::Weeks(7).to_string(), "7w");
        assert_eq!(Duration::Months(11).to_string(), "11m");
        assert_eq!(Duration::Years(3).to_string(), "3y");
    }

    #[test]
    fn duration_to_std_duration() {
        assert_eq!(
            std::time::Duration::from(&Duration::Seconds(1)),
            std::time::Duration::from_secs(1)
        );
        assert_eq!(
            std::time::Duration::from(&Duration::Days(3)),
            std::time::Duration::from_secs(3 * SECONDS_PER_DAY)
        );
        assert_eq!(
            std::time::Duration::from(&Duration::Weeks(7)),
            std::time::Duration::from_secs(7 * 7 * SECONDS_PER_DAY)
        );
        assert_eq!(
            std::time::Duration::from(&Duration::Months(11)),
            std::time::Duration::from_secs(11 * SECONDS_PER_YEAR / 12)
        );
        assert_eq!(
            std::time::Duration::from(&Duration::Years(3)),
            std::time::Duration::from_secs(3 * SECONDS_PER_YEAR)
        );
    }

    #[test]
    fn expiration_from_str() {
        assert_eq!("never".parse::<Expiration>().unwrap(), Expiration::Never);
        assert_eq!(
            "1s".parse::<Expiration>().unwrap(),
            Expiration::Duration(Duration::Seconds(1))
        );
        assert_eq!(
            "3d".parse::<Expiration>().unwrap(),
            Expiration::Duration(Duration::Days(3))
        );
        assert_eq!(
            "7w".parse::<Expiration>().unwrap(),
            Expiration::Duration(Duration::Weeks(7))
        );
        assert_eq!(
            "11m".parse::<Expiration>().unwrap(),
            Expiration::Duration(Duration::Months(11))
        );
        assert_eq!(
            "3y".parse::<Expiration>().unwrap(),
            Expiration::Duration(Duration::Years(3))
        );
        let datetime = chrono::Utc::now() + chrono::TimeDelta::days(1);
        assert_eq!(
            datetime.to_rfc3339().parse::<Expiration>().unwrap(),
            Expiration::At(datetime)
        );
        assert!("".parse::<Expiration>().is_err());
        assert!("1".parse::<Expiration>().is_err());
        assert!("1x".parse::<Expiration>().is_err());
        assert!((chrono::Utc::now() - chrono::TimeDelta::days(1))
            .to_rfc3339()
            .parse::<Expiration>()
            .is_err());
    }

    #[test]
    fn expiration_to_string() {
        assert_eq!(Expiration::Never.to_string(), "never");
        let datetime = chrono::Utc::now() + chrono::TimeDelta::days(1);
        assert_eq!(Expiration::At(datetime).to_string(), datetime.to_string());
        assert_eq!(Expiration::Duration(Duration::Years(3)).to_string(), "3y");
    }

    #[test]
    fn expiration_to_duration() {
        assert_eq!(Expiration::Never.try_to_duration().unwrap(), None);
        assert_eq!(
            Expiration::Duration(Duration::Years(3))
                .try_to_duration()
                .unwrap(),
            Some(std::time::Duration::from_secs(3 * SECONDS_PER_YEAR)),
        );
        let now = chrono::Utc::now();
        let now_plus_1d = now + chrono::TimeDelta::days(1);
        assert_eq!(Expiration::Never.try_to_duration().unwrap(), None,);
        // We can't compare the exact duration because the conversion uses Utc::now.
        let computed_duration = Expiration::At(now_plus_1d).try_to_duration().unwrap();
        let expected_duration = now_plus_1d.signed_duration_since(now).to_std().unwrap();
        assert!(
            computed_duration <= Some(expected_duration)
                && computed_duration
                    >= Some(expected_duration - std::time::Duration::from_millis(10))
        );
    }

    #[test]
    fn expiration_to_system_time() {
        assert_eq!(Expiration::Never.try_to_system_time().unwrap(), None);
        let now = chrono::Utc::now();
        let delta = chrono::TimeDelta::days(1);
        let computed_system_time = Expiration::Duration(Duration::Days(1))
            .try_to_system_time()
            .unwrap();
        let expected_system_time = std::time::SystemTime::now() + delta.to_std().unwrap();
        assert!(
            computed_system_time <= Some(expected_system_time)
                && computed_system_time
                    >= Some(expected_system_time - std::time::Duration::from_millis(10))
        );
        let now_plus_1d = now + delta;
        let computed_system_time = Expiration::At(now_plus_1d).try_to_system_time().unwrap();
        let expected_system_time =
            std::time::SystemTime::now() + now_plus_1d.signed_duration_since(now).to_std().unwrap();
        assert!(
            computed_system_time <= Some(expected_system_time)
                && computed_system_time
                    >= Some(expected_system_time - std::time::Duration::from_millis(10))
        );
    }
}
