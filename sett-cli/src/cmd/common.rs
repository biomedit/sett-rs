#[cfg(feature = "auth")]
use std::io::{stdout, Write};
use std::path::PathBuf;

use anyhow::Result;
use clap::Args;
use console::Emoji;
use indicatif::HumanBytes;
use sett::{
    openpgp::{
        certstore::{CertStore, CertStoreOpts},
        keystore::KeyStore,
    },
    task::Status,
};

#[derive(Args)]
pub(super) struct DisplayArgs {
    /// Don't show progress bar.
    #[arg(short, long)]
    pub(super) quiet: bool,

    /// Log verbosity level.
    #[arg(short, action=clap::ArgAction::Count)]
    pub(super) verbosity: u8,
}

#[derive(Args)]
pub(super) struct ModeArgs {
    /// Check the input without running the full task. No data will be modified
    /// or stored at the destination.
    #[arg(long)]
    pub(super) check: bool,
}

#[derive(Args, Clone)]
pub(super) struct SftpArgs {
    /// User name for authentication with the SFTP server.
    #[arg(short, long)]
    pub(super) username: String,

    /// Domain name (or IP address) of the SFTP server.
    #[arg(short = 'H', long)]
    pub(super) host: String,

    /// SFTP server port number.
    #[arg(short = 'P', long, default_value_t = 22)]
    pub(super) port: u16,

    /// Path on the remote SFTP host where encrypted files will be stored.
    #[arg(long)]
    pub(super) base_path: PathBuf,

    /// Private SSH key path.
    #[arg(long)]
    pub(super) key_path: Option<String>,

    /// Private SSH key password.
    #[arg(long)]
    pub(super) key_pwd: Option<String>,
}

#[derive(Args, Clone)]
pub(super) struct S3Args {
    /// S3 endpoint URL.
    ///
    /// The URL of the S3 server where the encrypted data should
    /// be uploaded. By default, the official AWS S3 endpoint
    /// is used.
    #[arg(short, long)]
    pub(super) endpoint: Option<String>,

    /// Name of the S3 bucket to which the data encrypted should be uploaded.
    #[arg(short, long)]
    pub(super) bucket: String,

    /// Region, e.g. `us-east-1`, `eu-central-1`, etc. Required in AWS world,
    /// but might be optional if using a S3-compatible server like MinIO.
    #[arg(long)]
    pub(super) region: Option<String>,

    /// S3 credential profile to use. This option can be used if multiple
    /// credential profiles are defined in the login credential file, and
    /// allows to select a profile that is not the default one.
    #[arg(long)]
    pub(super) profile: Option<String>,

    /// Access key ID for authentication with the S3 server.
    #[arg(long)]
    pub(super) access_key: Option<String>,

    /// Secret access key for authentication with the S3 server.
    #[arg(long)]
    pub(super) secret_key: Option<String>,

    /// Session token. Required when using temporary credentials.
    #[arg(long)]
    pub(super) session_token: Option<String>,
}

pub(super) enum Task {
    Encrypt,
    Decrypt,
    Transfer,
}

impl Task {
    pub(super) fn print_status(&self, status: &Status) {
        match status {
        Status::Checked {
            destination,
            source_size,
        } => {
            eprintln!(
                "{}Check completed successfully: {destination} (source: {})",
                Emoji("✅ ", ""),
                HumanBytes(*source_size)
            );
        }
        Status::Completed {
            destination,
            source_size,
            destination_size,
            ..
        } => {
            match self {
                Task::Encrypt => eprintln!(
                    "{}Completed data encryption: {destination} (source: {}, destination: {}, ratio: {:.2})",
                    Emoji("✅ ", ""),
                    HumanBytes(*source_size),
                    HumanBytes(*destination_size),
                    *source_size as f32 / *destination_size as f32
                ),
                Task::Decrypt => eprintln!(
                    "{}Completed data decryption: {destination} (source: {}, destination: {}, ratio: {:.2})",
                    Emoji("✅ ", ""),
                    HumanBytes(*source_size),
                    HumanBytes(*destination_size),
                    *destination_size as f32 / *source_size as f32
                ),
                Task::Transfer => eprintln!(
                    "{}Completed data transfer: {destination} (source: {})",
                    Emoji("✅ ", ""),
                    HumanBytes(*source_size),
                ),
            }
        }
    }
    }
}

#[cfg(feature = "auth")]
pub(super) fn auth_handler(details: sett::auth::VerificationDetails) -> Result<()> {
    use crate::util::shorten_oidc_issuer_url;

    let mut lock = stdout().lock();
    writeln!(lock, "To authorize sett, using a browser, visit:")?;
    if let Some(uri) = &details.verification_uri_complete {
        writeln!(lock, "{}", shorten_oidc_issuer_url(uri))?;
    } else {
        writeln!(
            lock,
            "{}",
            shorten_oidc_issuer_url(&details.verification_uri)
        )?;
        writeln!(lock)?;
        writeln!(lock, "And enter the code:")?;
        writeln!(lock, "{}", details.user_code)?;
    };
    Result::Ok(())
}

/// Opens a certificate store.
///
/// If a path is provided, the certificates will be loaded from the file into an in-memory store.
/// Otherwise, the default store will be opened.
pub(super) fn open_cert_store<'a>(
    path: Option<impl Iterator<Item = &'a std::path::PathBuf>>,
) -> Result<CertStore<'static>> {
    Ok(if let Some(p) = path {
        let mut cert_store = CertStore::open_ephemeral();
        for cert_file in p {
            for cert in sett::openpgp::cert::parse_certs(std::fs::File::open(cert_file)?)? {
                cert_store.import(&cert)?;
            }
        }
        cert_store
    } else {
        CertStore::open(&CertStoreOpts {
            read_only: true,
            ..Default::default()
        })?
    })
}

/// Opens a key store.
///
/// If a path is provided, the keys will be loaded from the file into a temporary store.
/// Otherwise, the default store will be opened.
pub(super) async fn open_key_store(path: Option<&std::path::Path>) -> Result<KeyStore> {
    Ok(if let Some(p) = path {
        let mut key_store = KeyStore::open_ephemeral().await?;
        for cert in sett::openpgp::cert::parse_certs(std::fs::File::open(p)?)? {
            key_store.import(cert).await?;
        }
        key_store
    } else {
        KeyStore::open(None).await?
    })
}

pub(super) async fn get_s3_client(
    args: &S3Args,
) -> std::result::Result<sett::remote::s3::Client, sett::remote::s3::error::ClientError> {
    sett::remote::s3::Client::builder()
        .endpoint(args.endpoint.as_ref())
        .region(args.region.as_ref())
        .profile(args.profile.as_ref())
        .access_key(args.access_key.as_deref())
        .secret_key(args.secret_key.as_deref())
        .session_token(args.session_token.as_deref())
        .build()
        .await
}

pub(super) fn get_sftp_client(args: &SftpArgs) -> sett::remote::sftp::Client {
    sett::remote::sftp::Client::builder()
        .host(&args.host)
        .port(args.port)
        .username(&args.username)
        .key_path(args.key_path.as_ref())
        .key_password(args.key_pwd.as_deref())
        .two_factor_callback(Some(crate::util::two_factor_prompt))
        .build()
}
