use std::path::{Path, PathBuf};

use anyhow::{ensure, Context, Result};
use clap::{Args, Subcommand};
use sett::{
    openpgp::certstore::{CertStore, CertStoreOpts},
    package::{Package, DATETIME_FORMAT},
    task::Mode,
};

#[cfg(feature = "auth")]
use crate::util::get_oidc_client;
use crate::util::get_portal_client;

#[derive(Subcommand)]
pub(crate) enum Transfer {
    Sftp(Sftp),
    S3(S3),
    #[cfg(feature = "auth")]
    S3Portal(S3Portal),
}

/// Transfer an encrypted package to an SFTP server
#[derive(Args)]
pub(crate) struct Sftp {
    #[command(flatten)]
    display: super::common::DisplayArgs,

    #[command(flatten)]
    mode: super::common::ModeArgs,

    /// Perform an additional data package verification using Portal.
    #[arg(long)]
    verify: bool,

    #[command(flatten)]
    sftp: super::common::SftpArgs,

    file: PathBuf,
}

/// Transfer an encrypted package to an S3 server
///
/// S3 authentication credentials can be provider by the following means
/// (in order of precedence): cli arguments, environmental variables, credentials
/// file. For more information about the last two options refer to the official
/// AWS documentation
/// <https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html>.
#[derive(Args)]
pub(crate) struct S3 {
    #[command(flatten)]
    display: super::common::DisplayArgs,

    #[command(flatten)]
    mode: super::common::ModeArgs,

    /// Perform an additional data package verification using Portal.
    #[arg(long)]
    verify: bool,

    #[command(flatten)]
    s3: super::common::S3Args,

    file: PathBuf,
}

#[cfg(feature = "auth")]
/// Transfer an encrypted package to an S3 server with credentials fetched from Portal.
#[derive(Args)]
pub(crate) struct S3Portal {
    #[command(flatten)]
    display: super::common::DisplayArgs,

    #[command(flatten)]
    mode: super::common::ModeArgs,

    file: PathBuf,
}

impl Transfer {
    pub(crate) async fn run(self) -> Result<()> {
        match self {
            Self::Sftp(c) => c.run().await,
            Self::S3(c) => c.run().await,
            #[cfg(feature = "auth")]
            Self::S3Portal(c) => c.run().await,
        }
    }
}

impl Sftp {
    async fn run(self) -> Result<()> {
        let _log_guard = crate::log::init_file_and_console_log(self.display.verbosity)?;
        let package = if self.verify {
            verify(&self.file, Some(&get_portal_client()?)).await?
        } else {
            verify(&self.file, None).await?
        };
        let status = sett::remote::sftp::upload(
            &package,
            &super::common::get_sftp_client(&self.sftp),
            &self.sftp.base_path,
            if self.mode.check {
                Mode::Check
            } else {
                Mode::Run
            },
            (!self.display.quiet).then(crate::util::CliProgress::new),
        )
        .await?;
        super::common::Task::Transfer.print_status(&status);
        Ok(())
    }
}

impl S3 {
    async fn run(self) -> Result<()> {
        let _log_guard = crate::log::init_file_and_console_log(self.display.verbosity)?;
        let package = if self.verify {
            verify(&self.file, Some(&get_portal_client()?)).await?
        } else {
            verify(&self.file, None).await?
        };
        let status = super::common::get_s3_client(&self.s3)
            .await?
            .upload(
                &package,
                &self.s3.bucket,
                if self.mode.check {
                    Mode::Check
                } else {
                    Mode::Run
                },
                (!self.display.quiet).then(crate::util::CliProgress::new),
            )
            .await?;
        super::common::Task::Transfer.print_status(&status);
        Ok(())
    }
}

#[cfg(feature = "auth")]
impl S3Portal {
    async fn run(self) -> Result<()> {
        let _log_guard = crate::log::init_file_and_console_log(self.display.verbosity)?;
        let portal_client = get_portal_client()?;
        let package = verify(&self.file, Some(&portal_client)).await?;
        let dtr = package
            .metadata()
            .await?
            .transfer_id
            .expect("missing transfer id");
        let token = get_oidc_client()
            .await?
            .authenticate(Box::new(super::common::auth_handler))
            .await?;
        let connection_details = portal_client
            .get_s3_connection_details(dtr, sett::portal::S3Permission::Write, &token.access_token)
            .await?;
        let client = connection_details.build_client().await?;
        let dest = sett::destination::S3::new(client, connection_details.bucket);
        let status = dest
            .client()
            .upload(
                &package,
                dest.bucket(),
                if self.mode.check {
                    Mode::Check
                } else {
                    Mode::Run
                },
                (!self.display.quiet).then(crate::util::CliProgress::new),
            )
            .await?;
        super::common::Task::Transfer.print_status(&status);
        Ok(())
    }
}

async fn verify(
    file: &Path,
    portal_client: Option<&sett::portal::Portal>,
) -> Result<Package<sett::package::state::Verified>> {
    ensure!(
        file.is_file(),
        "File '{}' does not exist or is not a regular file",
        file.display()
    );
    let file_name = file
        .file_name()
        .context("Failed to get file name")?
        .to_string_lossy();
    let certstore = CertStore::open(&CertStoreOpts {
        read_only: true,
        ..Default::default()
    })?;
    let package = Package::open(file).await?.verify(&certstore).await?;
    let metadata = package.metadata().await?;
    if let Some(portal_client) = portal_client {
        let response = portal_client.check_package(&metadata, &file_name).await?;
        let file_name_expected = format!(
            "{}_{}.zip",
            response.project_code,
            metadata.timestamp.format(DATETIME_FORMAT)
        );
        ensure!(
            file_name == file_name_expected,
            "Package verification failed: invalid file name '{}' (expected '{}')",
            &file_name,
            file_name_expected
        );
    }
    Ok(package)
}
