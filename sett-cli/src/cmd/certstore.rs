use std::{
    borrow::Cow,
    fmt::Write as _,
    fs::File,
    io::{self, Cursor, Read, Write},
    path::{Path, PathBuf},
};

use anyhow::{anyhow, bail, ensure, Context, Result};
use chrono::{DateTime, Local};
use clap::{ArgAction, Args, Subcommand, ValueEnum};
use console::Emoji;

use sett::openpgp::{
    cert::{AsciiArmored, Cert, CertType, Fingerprint, ReasonForRevocation, RevocationStatus},
    certstore::{CertStore, CertStoreOpts, QueryTerm},
    keyserver::{Keyserver, KeyserverEmailStatus},
    keystore::KeyStore,
};

use super::time::Expiration;

const TIME_FORMAT: &str = "%d.%m.%Y %H:%M";

enum CertExpirationTime {
    Valid(Option<DateTime<Local>>),
    Expired(DateTime<Local>),
}

/// Enum indicating whether OpenPGP data should be serialized (i.e.
/// exported as bytes) to a binary format or to ASCII-armored format.
#[derive(Clone, Copy, Default)]
pub enum SerializationFormat {
    /// Certificate to be serialized to binary format.
    #[default]
    Binary,
    /// Certificate to be serialized to ASCII-armored format.
    AsciiArmored,
}

impl std::str::FromStr for SerializationFormat {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "binary" => Ok(Self::Binary),
            "ascii" => Ok(Self::AsciiArmored),
            _ => Err(anyhow::anyhow!("Invalid SerializationFormat: {s}")),
        }
    }
}

/// Declaration of the sub-commands of the `sett keys` CLI command. The names
/// of the Enum's variants become the sub-commands of the `sett keys` CLI
/// command.
#[derive(Subcommand)]
pub(crate) enum Keys {
    /// Import public or private OpenPGP keys into the local keystore.
    /// SECURITY WARNING: only import private keys that you trust, such as
    /// your own key.
    #[command(subcommand)]
    Import(Import),

    /// Export an OpenPGP key from the local store to a file in binary
    /// or ASCII-armored (plain text) format.
    Export(Export),

    /// List the OpenPGP keys present in the local store.
    List(List),

    /// Generate a new public/private OpenPGP key pair.
    Generate(Generate),

    /// Revoke an OpenPGP key.
    ///
    /// A public/private key pair can be revoked using the corresponding
    /// private key and providing a `REASON` and `MESSAGE`, or by using an
    /// existing revocation signature (`--rev-sig`).
    Revoke(Revoke),

    /// Upload an OpenPGP public key to the keyserver.
    Upload(Upload),

    /// Delete private key
    ///
    /// WARNING: Deleted keys cannot be restored. Before deleting consider revoking the associated
    /// public key first.
    DeletePrivateKey(DeletePrivateKey),

    /// Change expiration date
    Expire(Expire),
}

#[derive(Args)]
pub struct PrivateFlagArgs {
    /// Import/export an OpenPGP key containing private material.
    ///
    /// This option must be added when importing or exporting keys that contain
    /// private material. Passing this flag when importing/exporting public
    /// keys will generate an error.
    #[arg(short = 'p', long = "private", action = ArgAction::SetTrue, value_parser = parse_cert_type_flag )]
    cert_type: CertType,
}

#[derive(Subcommand)]
#[allow(clippy::enum_variant_names)]
pub(crate) enum Import {
    /// Import an OpenPGP key from a file (or multiple files) on disk.
    FromFile(FromFile),
    /// Download public OpenPGP keys from the keys.openpgp.org keyserver.
    FromKeyserver(FromKeyserver),
    /// Import OpenPGP keys from a local GnuPG keyring.
    FromGpg(FromGpg),
    /// Import OpenPGP keys from a stream passed to standard input.
    FromStdin(FromStdin),
}

#[derive(Args)]
pub(crate) struct FromFile {
    #[command(flatten)]
    private: PrivateFlagArgs,

    /// Path(s) of the file(s) containing the OpenPGP key(s) to import.
    ///
    /// Multiple file paths can be passed at a time. Binary and ASCII-armored
    /// formats are supported.
    files: Vec<PathBuf>,
}

impl FromFile {
    async fn run(&self) -> Result<()> {
        if let CertType::Secret = self.private.cert_type {
            print_secrets_warning_msg()
        }
        let mut cert_store = CertStore::open(&Default::default())?;
        let mut key_store = KeyStore::open(None).await?;
        for file in &self.files {
            import_certs(
                File::open(file)?,
                &mut cert_store,
                &mut key_store,
                self.private.cert_type,
            )
            .await
        }
        Ok(())
    }
}

#[derive(Args)]
pub(crate) struct FromKeyserver {
    /// Identifier of OpenPGP keys to import.
    ///
    /// Fingerprint(s) or email(s) of the key(s) to download from the
    /// keyserver.
    identifiers: Vec<String>,
}

impl FromKeyserver {
    async fn run(&self) -> Result<()> {
        let mut store = CertStore::open(&Default::default())?;
        let keyserver = Keyserver::new().context("Failed to create keyserver client")?;
        for identifier in &self.identifiers {
            let query_term = match identifier.parse() {
                Ok(query_term) => query_term,
                Err(e) => {
                    eprintln!("{} Invalid identifier: '{e}'", Emoji("💥", "ERROR:"),);
                    continue;
                }
            };
            keyserver.get_cert(&query_term).await.map_or_else(
                |e| {
                    eprintln!(
                        "{} Import from keyserver failed: {e}",
                        Emoji("💥", "ERROR:")
                    );
                },
                |cert| {
                    store
                        .import(&cert)
                        .map_or_else(print_failed_import_msg, |imported| {
                            print_successful_import_msg(&imported)
                        })
                },
            );
        }
        Ok(())
    }
}

#[derive(Args)]
pub(crate) struct FromGpg {
    #[command(flatten)]
    private: PrivateFlagArgs,

    /// Import all OpenPGP keys (public OR private) present in the local GnuPG
    /// store.
    #[arg(short = 'a', long, action = ArgAction::SetTrue, required_unless_present = "identifiers", conflicts_with = "identifiers")]
    all: bool,

    /// GnuPG home directory of GnuPG (Optional).
    ///
    /// By default, the default GnuPG directory is used (e.g. ~/.gnupg on
    /// Linux). This option allows overriding the default value by passing a
    /// custom directory where to look for a GnuPG keyring.
    #[arg(long = "gpghome")]
    gpg_home: Option<PathBuf>,

    /// Identifiers of OpenPGP keys to import.
    ///
    /// Fingerprint or email of the key(s) to extract from the GnuPG store. If
    /// a key with the same fingerprint is already present in the local sett
    /// keystore, the content of the existing and imported keys is merged.
    identifiers: Vec<String>,
}

impl FromGpg {
    async fn run(&self) -> Result<()> {
        let mut cert_store = CertStore::open(&Default::default())?;
        let mut key_store = KeyStore::open(None).await?;
        let identifiers = if self.all {
            sett::gnupg::list_keys(self.private.cert_type, self.gpg_home.as_ref())?
                .into_iter()
                .map(|c| c.fingerprint)
                .collect()
        } else {
            self.identifiers.to_owned()
        };
        for identifier in identifiers {
            let password = match self.private.cert_type {
                CertType::Secret => Some(
                    rpassword::prompt_password(format!(
                        "{}Enter the password of the private key {identifier}: ",
                        Emoji("🔑 ", "")
                    ))?
                    .into_bytes(),
                ),
                CertType::Public => None,
            };
            match sett::gnupg::export_key(
                &identifier,
                self.private.cert_type,
                password.as_deref(),
                self.gpg_home.as_deref(),
            ) {
                Ok(exported) => {
                    import_certs(
                        Cursor::new(exported),
                        &mut cert_store,
                        &mut key_store,
                        self.private.cert_type,
                    )
                    .await
                }
                Err(error) => print_failed_import_msg(error),
            }
        }
        Ok(())
    }
}

#[derive(Args)]
pub(crate) struct FromStdin {
    #[command(flatten)]
    private: PrivateFlagArgs,
}

impl FromStdin {
    async fn run(&self) -> Result<()> {
        if let CertType::Secret = self.private.cert_type {
            print_secrets_warning_msg()
        }

        let mut store = CertStore::open(&Default::default())?;
        let mut key_store = KeyStore::open(None).await?;

        let mut data = Vec::new();
        io::stdin().lock().read_to_end(&mut data)?;
        import_certs(
            Cursor::new(data),
            &mut store,
            &mut key_store,
            self.private.cert_type,
        )
        .await;
        Ok(())
    }
}

#[derive(Args)]
pub(crate) struct Export {
    /// OpenPGP key identifier.
    ///
    /// Fingerprint or email of the key to export. Only a single key can be
    /// exported at a time.
    key_identifier: String,

    #[command(flatten)]
    private: PrivateFlagArgs,

    /// Export the OpenPGP key(s) in ASCII-armored format (i.e. plain text).
    ///
    /// By default keys are exported in binary format.
    #[arg(short = 'a', long = "armor", action = ArgAction::SetTrue, value_parser = parse_ascii_armor_flag )]
    output_format: SerializationFormat, // Is set to variant `AsciiArmored` if the flag is passed.

    /// Output path for the exported OpenPGP key. If omitted, key is exported
    /// to stdout.
    ///
    /// Export the OpenPGP key to a file on disk. If this option is passed but
    /// no file name is provided, the key's fingerprint is used as default
    /// name.
    /// A ".pub"/".sec" extension for public/private keys exported in binary
    /// format and a ".pub.asc"/".sec.asc" extension for public/private keys
    /// exported in ASCII-armored format is added.
    /// When passing this option without specifying a file name, it must be
    /// passed at the end of the command line.
    #[arg(short, long)]
    output_file: Option<Option<PathBuf>>,
}

#[derive(Args)]
pub(crate) struct List {
    /// Display the expiration date of OpenPGP keys.
    ///
    /// Expired keys always display this information, even if this option is
    /// not passed.
    #[arg(short = 'e', long)]
    show_expiration: bool,

    /// Display the path of OpenPGP keys (location on disk).
    #[arg(short = 'p', long)]
    show_path: bool,

    /// Filter the results.
    ///
    /// Only shows a certificate if a self-signed user ID contains
    /// PATTERN. The comparison is done in a case insensitive manner
    /// in the current locale.
    #[arg(value_name = "PATTERN")]
    filter: Option<String>,
}

#[derive(Debug, Clone, ValueEnum)]
enum CipherSuite {
    /// EdDSA and ECDH over Curve25519 with SHA512 and AES256
    Cv25519,
    /// 4096 bit RSA with SHA512 and AES256
    Rsa4k,
}

impl From<CipherSuite> for sett::openpgp::cert::CipherSuite {
    fn from(val: CipherSuite) -> Self {
        match val {
            CipherSuite::Cv25519 => Self::Cv25519,
            CipherSuite::Rsa4k => Self::RSA4k,
        }
    }
}

#[derive(Args)]
pub(crate) struct Generate {
    /// Add a name to the User ID.
    ///
    /// The name is placed at the beginning of the User ID, e.g.,
    /// "Alice Smith <alice@example.org>".
    #[arg(short, long, required_unless_present = "no_name")]
    name: Option<String>,

    /// Create a key without a name in User ID.
    #[arg(long, conflicts_with = "name")]
    no_name: bool,

    /// Add an email address to the User ID.
    ///
    /// The email address is enclosed in angle brackets and placed at the end
    /// of the User ID, e.g., "Alice Smith <alice@example.org>".
    #[arg(short, long, required_unless_present = "no_email")]
    email: Option<String>,

    /// Create a key without an email address in User ID.
    #[arg(long, conflicts_with = "email")]
    no_email: bool,

    /// Add a comment to the User ID (optional).
    ///
    /// A comment is an optional part of the User ID that can be used to
    /// provide additional information about the key owner.
    ///
    /// The comment is enclosed in parentheses and placed between the name
    /// and the email, e.g., "Alice Smith (comment) <alice@example.org>"
    #[arg(short, long, conflicts_with_all = ["no_name", "no_email"])]
    comment: Option<String>,

    /// The cryptographic algorithm to be used for the new key.
    #[arg(long, default_value_t = CipherSuite::Cv25519, value_enum)]
    cipher_suite: CipherSuite,

    /// Key expiration time.
    ///
    /// The expiration time can be set to a duration from the current time,
    /// a specific date, or to never expire.
    ///
    /// Duration must be specified in the format "NUMBER{y,m,w,d,s}" where
    /// "y" stands for years, "m" for months, "w" for weeks, "d" for days,
    /// and "s" for seconds.
    /// Date must be specified in RFC3339 format (e.g. "2030-12-31T23:59:59Z").
    /// Finally, "never" can be used to specify that the key should never expire.
    ///
    /// Examples: "3y", "2m", "1w", "5d", "3600s", "2030-12-31T23:59:59Z", "never".
    #[arg(long, default_value_t)]
    expiration: Expiration,

    /// Export the newly generated key to a file or the standard output ("-")
    #[arg(long, required_if_eq("no_store", "true"))]
    export: Option<PathBuf>,

    /// Revocation signature file path (optional).
    ///
    /// Export the revocation signature of the newly generated key to a file
    /// instead of standard output (default, "-").
    #[arg(long, default_value = "-")]
    rev_sig: PathBuf,

    /// Do not add the newly generate key to the local keystore.
    #[arg(long)]
    no_store: bool,
}

impl Generate {
    async fn run(self) -> Result<()> {
        let p1 = rpassword::prompt_password(format!(
            "{}Enter a password to protect the private key: ",
            Emoji("🔑 ", "")
        ))?;
        let p2 = rpassword::prompt_password(format!(
            "{}Repeat the password for the private key: ",
            Emoji("🔑 ", "")
        ))?;
        ensure!(p1 == p2, "{}Passwords do not match", Emoji("💥 ", ""));
        let expires_in = self.expiration.try_to_duration()?;
        let mut user_id = Vec::with_capacity(3);
        if let Some(name) = self.name {
            user_id.push(name);
        }
        if let Some(comment) = self.comment {
            user_id.push(format!("({})", comment));
        }
        if let Some(email) = self.email {
            user_id.push(format!("<{}>", email));
        }
        let mut cert_builder = sett::openpgp::cert::CertBuilder::new()
            .set_cipher_suite(self.cipher_suite.into())
            .set_password(Some(p1.into()))
            .set_validity_period(expires_in);
        if !user_id.is_empty() {
            cert_builder = cert_builder.add_userid(&user_id.join(" "));
        }
        let (cert, rev) = cert_builder.generate()?;
        if let Some(p) = self.export {
            create_file_or_std_writer(Some(p))?
                .write_all(&AsciiArmored::try_from(cert.secret()?)?)?;
        }
        let cert_info = cert.to_string();

        eprintln!(
            "{} This is a revocation signature for the newly created \
            OpenPGP key: {}. Revocation signatures should be kept in a \
            safe place, such as a password manager.",
            Emoji("ℹ️ ", "INFO:"),
            cert_info
        );
        create_file_or_std_writer(Some(&self.rev_sig))?.write_all(&rev)?;

        let expiration_notice = if let Some(d) = expires_in {
            format!("The key will expire on {}.", Local::now() + d)
        } else {
            "The key will never expire.".to_string()
        };

        if self.no_store {
            eprintln!(
                "{}The following public/private OpenPGP key pair has been \
                successfully created: {}. {}",
                Emoji("🔏 ", ""),
                cert_info,
                expiration_notice
            );
        } else {
            CertStore::open(&Default::default())?.import(&cert)?;
            KeyStore::open(None).await?.import(cert).await?;
            eprintln!(
                "{}The following public/private OpenPGP key pair has been \
                successfully created and added to the local keystore: {}. {}",
                Emoji("📥 ", ""),
                cert_info,
                expiration_notice
            );
        }

        Ok(())
    }
}

#[derive(Clone, Copy, ValueEnum)]
enum RevocationReason {
    /// The private key is (or may have been) compromised.
    Compromised,
    /// The key has been replaced by a new one. The message should contain
    /// the fingerprint of the new key.
    Superseded,
    /// The key should not be used anymore and there is no replacement key.
    /// This reason is appropriate e.g. when leaving an organization.
    Retired,
    /// None of the above reasons apply.
    /// Note: It is preferable to use a specific reason.
    Unspecified,
}

impl From<RevocationReason> for ReasonForRevocation {
    fn from(reason: RevocationReason) -> Self {
        match reason {
            RevocationReason::Compromised => ReasonForRevocation::KeyCompromised,
            RevocationReason::Superseded => ReasonForRevocation::KeySuperseded,
            RevocationReason::Retired => ReasonForRevocation::KeyRetired,
            RevocationReason::Unspecified => ReasonForRevocation::Unspecified,
        }
    }
}

#[derive(Args)]
pub(crate) struct Revoke {
    /// Identified of OpenPGP key to revoke.
    ///
    /// Fingerprint or email of the key to revoke. Only a single key can be
    /// revoked at a time.
    cert_identifier: String,

    /// Revocation reason.
    #[arg(value_enum, required_unless_present = "rev_sig")]
    reason: Option<RevocationReason>,

    /// Revocation reason details (Optional).
    ///
    /// A short text explaining the reason for the key revocation.
    #[arg(required_unless_present = "rev_sig")]
    message: Option<String>,

    /// Revocation signature file path.
    ///
    /// Use an existing revocation signature, instead of generating a new one
    /// from the private key. If access to the private key was lost or the
    /// password was forgotten, this is the only way to revoke a key.
    #[arg(long, conflicts_with_all = ["reason", "message"])]
    rev_sig: Option<PathBuf>,

    /// Generate revocation signature.
    ///
    /// Generate a revocation signature without revoking the original key in
    /// the keystore.
    #[arg(long, conflicts_with = "rev_sig")]
    gen_rev: bool,

    /// After revoking the key, print it to standard output.
    #[arg(long)]
    show_revoked_key: bool,
}

impl Revoke {
    async fn run(&self) -> Result<()> {
        let mut store = CertStore::open(&Default::default())?;
        let cert = get_single_cert(&self.cert_identifier.parse()?, &store).context(format!(
            "{} No key matching '{}' was found in the local keystore.",
            Emoji("💥", "ERROR:"),
            &self.cert_identifier,
        ))?;
        // Load the revocation signature from a file provided by the user or
        // generate a new one.
        let revocation_signature = if let Some(p) = &self.rev_sig {
            std::fs::read(p)?
        } else {
            let mut key_store = KeyStore::open(None).await?;
            let cert = cert
                .clone()
                .update_secret_material(&mut key_store)
                .await
                .inspect_err(|_| {
                    eprintln!(
                        "{} A private key matching '{}' must be present in \
                        the local keystore when no revocation signature is \
                        passed to this command. \
                        The reason for this is that creating a revocation \
                        signature requires the private key.",
                        Emoji("💥", "ERROR:"),
                        &self.cert_identifier
                    )
                })?;

            // Create a revocation signature using the secret key of
            // the certificate.
            cert.generate_rev_sig(
                self.reason
                    .expect("`reason` is required when `rev_sig` is absent")
                    .into(),
                self.message
                    .as_ref()
                    .expect("`message` is required when `rev_sig` is absent")
                    .as_bytes(),
                cert.is_encrypted()?.then_some(
                    rpassword::prompt_password(format!(
                        "{}Enter the password for \"{}\": ",
                        Emoji("🔑 ", ""),
                        cert
                    ))?
                    .as_bytes(),
                ),
            )?
        };

        // If the user requested to create a revocation signature but not to
        // revoke the certificate, print that signature to a file/stdout and
        // exit.
        if self.gen_rev {
            create_file_or_std_writer(None::<&Path>)?.write_all(&revocation_signature)?;
            return Ok(());
        }

        // Revoke public and secret certificates in the local store (on disk).
        let revoked = cert.revoke(std::io::Cursor::new(&revocation_signature))?;
        if self.show_revoked_key {
            eprintln!(
                "{} Below is your revoked public key:",
                Emoji("ℹ️ ", "INFO:")
            );
            io::stdout().write_all(&AsciiArmored::try_from(revoked.public())?)?;
        }
        store.import(&revoked)?;
        eprintln!("{}Successfully revoked key: {}", Emoji("✅ ", ""), revoked,);
        Ok(())
    }
}

fn create_file_or_std_writer(path: Option<impl AsRef<Path>>) -> Result<Box<dyn Write>> {
    match path {
        None => Ok(Box::new(io::stdout())),
        Some(p) if p.as_ref() == Path::new("-") => Ok(Box::new(io::stdout())),
        Some(p) => {
            if p.as_ref().exists() {
                bail!(
                    "File {:?} already exists. Remove the existing file or \
                    use a different path.",
                    p.as_ref()
                )
            }
            Ok(Box::new(File::create(p)?))
        }
    }
}

#[derive(Args)]
pub(crate) struct DeletePrivateKey {
    /// Identified of OpenPGP key to revoke.
    ///
    /// Fingerprint or email of the key to delete. Only a single key can be
    /// deleted at a time.
    cert_identifier: String,
}

impl DeletePrivateKey {
    async fn run(&self) -> Result<()> {
        use std::collections::BTreeSet;
        let cert_store = CertStore::open(&Default::default())?;
        let cert =
            get_single_cert(&self.cert_identifier.parse()?, &cert_store).context(format!(
                "{} No key matching '{}' was found in the local keystore.",
                Emoji("💥", "ERROR:"),
                &self.cert_identifier,
            ))?;
        let mut key_store = KeyStore::open(None).await?;
        let all_private_keys = key_store.list().await?;
        let cert_keys = cert.keys();
        let keys_for_deletion: Vec<_> =
            BTreeSet::from_iter(cert_keys.into_iter().map(|key| key.fingerprint()))
                .intersection(&BTreeSet::from_iter(
                    all_private_keys.iter().map(|key| key.fingerprint()),
                ))
                .cloned()
                .collect();
        anyhow::ensure!(
            !keys_for_deletion.is_empty(),
            "{} No private keys found for {}",
            Emoji("💥", "ERROR:"),
            cert
        );

        let stdin = std::io::stdin();
        let mut stdout = std::io::stdout();
        let mut buf = String::new();

        let questions = [
            "Are you sure you really want to delete the private key associated with {cert}? (yes, No) ",
            "Consider revoking the key first before deleting it. Do you want to continue anyway? (yes, No) ",
            "This is the final confirmation. Do you really want to permanently delete the selected private key? (yes, No) ",
        ];
        for question in questions {
            buf.clear();
            write!(stdout, "{}", question)?;
            stdout.flush()?;
            stdin.read_line(&mut buf)?;
            if buf.trim() != "yes" {
                writeln!(stdout, "Operation cancelled")?;
                return Ok(());
            }
        }

        for key in keys_for_deletion.into_iter() {
            key_store.delete_key(key).await?;
        }
        writeln!(stdout, "Private key has been successfully deleted")?;
        Ok(())
    }
}

#[derive(Args)]
pub(crate) struct Expire {
    /// OpenPGP key identifier.
    ///
    /// Fingerprint or email of the key to change the expiration date.
    key_identifier: String,

    /// Key expiration time.
    ///
    /// The expiration time can be set to a duration from the current time,
    /// a specific date, or to never expire.
    ///
    /// Duration must be specified in the format "NUMBER{y,m,w,d,s}" where
    /// "y" stands for years, "m" for months, "w" for weeks, "d" for days,
    /// and "s" for seconds.
    /// Date must be specified in RFC3339 format (e.g. "2030-12-31T23:59:59Z").
    /// Finally, "never" can be used to specify that the key should never expire.
    ///
    /// Examples: "3y", "2m", "1w", "5d", "3600s", "2030-12-31T23:59:59Z", "never".
    #[arg(long, default_value_t)]
    expiration: Expiration,
}

// TODO: Convert it into a closure once `async` closures are stabilized.
// With closure it wouldn't be necessary to fetch the certificate from the
// store again.
async fn prompt_password(fingerprint: sett::openpgp::cert::Fingerprint) -> sett::secret::Secret {
    let mut prompt = None;
    if let Ok(cert_store) = CertStore::open(&CertStoreOpts {
        read_only: true,
        ..Default::default()
    }) {
        if let Ok(cert) = get_single_cert(
            &QueryTerm::Fingerprint(Cow::Borrowed(&fingerprint)),
            &cert_store,
        ) {
            prompt = Some(cert.to_string());
        }
    }
    if let Ok(Ok(password)) = tokio::task::spawn_blocking(move || {
        rpassword::prompt_password(format!(
            "Please enter the password to unlock the key {}: ",
            prompt.unwrap_or_else(|| fingerprint.to_string())
        ))
    })
    .await
    {
        password.into()
    } else {
        tracing::error!("Failed to read password");
        "".into()
    }
}

impl Expire {
    async fn run(&self) -> Result<()> {
        let expires_in = self.expiration.try_to_system_time()?;
        let mut cert_store = CertStore::open(&Default::default())?;
        let cert = get_single_cert(&self.key_identifier.parse()?, &cert_store)?;
        let private_keys = KeyStore::open(None)
            .await?
            .find_key(cert.fingerprint())
            .await?;
        anyhow::ensure!(
            !private_keys.is_empty(),
            "No private keys found for {}",
            cert
        );
        let mut errors = Vec::new();
        for mut key in private_keys {
            if let Err(e) = key.unlock(prompt_password).await {
                errors.push(e.to_string());
                continue;
            }
            let cert = cert.set_expiration_time(key, expires_in).await?;
            cert_store.import(&cert)?;
            eprintln!(
                "{}Expiration date for key '{}' successfully updated.",
                Emoji("🔒 ", ""),
                cert
            );
            return Ok(());
        }
        Err(anyhow!(
            "Failed to update expiration date for key '{}': {}",
            cert,
            errors.join(", ")
        ))
    }
}

impl Keys {
    pub(crate) async fn run(self) -> Result<()> {
        let _log_guard = crate::log::init_file_and_console_log(crate::log::Level::Warn)?;
        match self {
            Self::Import(cmd) => cmd.run().await,
            Self::Export(cmd) => cmd.run().await,
            Self::List(cmd) => cmd.run().await,
            Self::Generate(cmd) => cmd.run().await,
            Self::Revoke(cmd) => cmd.run().await,
            Self::Upload(cmd) => cmd.run().await,
            Self::DeletePrivateKey(cmd) => cmd.run().await,
            Self::Expire(cmd) => cmd.run().await,
        }
    }
}

impl Import {
    async fn run(&self) -> Result<()> {
        match self {
            Self::FromFile(cmd) => cmd.run().await,
            Self::FromKeyserver(cmd) => cmd.run().await,
            Self::FromGpg(cmd) => cmd.run().await,
            Self::FromStdin(cmd) => cmd.run().await,
        }
    }
}

/// Imports OpenPGP certificate(s) into the local store.
///
/// If a certificate with the same fingerprint is already present in the
/// store, the content of the existing and imported certificates are
/// merged.
///
/// If a secret material is present it is imported into the key store,
/// as long as the `CertType` argument is `Secret`.
///
/// # Arguments
///
/// * `data`: the certificate(s) to import
/// * `cert_store`: certificate store into which the certificate(s) should
///    be imported.
/// * `key_store`: key store into which the private keys should be imported.
/// * `cert_type`: if `Secret` than both the public certificate and private
///    keys are imported, otherwise only the public certificate is imported.
async fn import_certs<R: io::Read + Send + Sync>(
    data: R,
    cert_store: &mut CertStore<'_>,
    key_store: &mut KeyStore,
    cert_type: CertType,
) {
    if let Ok(certs) = sett::openpgp::cert::parse_certs(data) {
        for cert in certs {
            if let CertType::Secret = cert_type {
                key_store
                    .import(cert.clone())
                    .await
                    .map_or_else(print_failed_import_msg, |_| {
                        print_successful_import_msg(&cert)
                    });
            }
            cert_store
                .import(&cert)
                .map_or_else(print_failed_import_msg, |imported| {
                    print_successful_import_msg(&imported)
                });
            // If a secret certificate was imported only as a public certificate,
            // display a warning to the user.
            if cert.cert_type() == CertType::Secret && cert_type == CertType::Public {
                eprintln!(
                    "{} The input data for '{}' contains both a private and a public \
                    key, but only the public key was imported. To also import \
                    the private key, pass the `-p/--private` option to this command.",
                    Emoji("⚠️ ", "WARNING:"),
                    cert
                );
            }
        }
    } else {
        eprintln!(
            "{} Import failed: input does not contain valid OpenPGP key data",
            Emoji("💥", "ERROR:")
        )
    }
}

/// Print a summary of the successfully imported certificate.
fn print_successful_import_msg(imported_cert: &Cert) {
    eprintln!(
        "{}Imported {}",
        Emoji("📥 ", ""),
        userid_as_string_with_type(imported_cert)
    );
}

/// Print an error message when importing an OpenPGP certificate fails.
fn print_failed_import_msg(error: impl std::fmt::Display) {
    eprintln!(
        "{} Failed to import OpenPGP key: {error}",
        Emoji("💥", "ERROR:")
    );
}

/// Print a warning message when importing certificates containing
/// secret material.
fn print_secrets_warning_msg() {
    eprintln!(
        "{} Only import private keys that you trust, such as your own key.",
        Emoji("⚠️ ", "SECURITY WARNING:")
    )
}

#[derive(Args)]
pub(crate) struct Upload {
    /// Fingerprint or email of the public OpenPGP key to upload.
    identifier: String,

    /// Request the keyserver to send a verification email for the
    /// uploaded public key.
    #[arg(long, short = 'v', action = ArgAction::SetTrue)]
    verify: bool,
}

fn handle_failed_verification(error: impl std::fmt::Display) {
    eprintln!(
        "{} Rejected verification request: {error}",
        Emoji("💥", "ERROR:")
    )
}

impl Upload {
    async fn run(&self) -> Result<()> {
        let imported_cert = get_single_cert(
            &self.identifier.parse()?,
            &CertStore::open(&CertStoreOpts {
                read_only: true,
                ..Default::default()
            })?,
        )?;
        if let Some(email) = imported_cert.get_primary_email()? {
            let keyserver = Keyserver::new().context("Failed to create keyserver client")?;
            match keyserver.upload_cert(&imported_cert).await {
                Ok(response) => {
                    eprintln!(
                        "{}Uploaded {}",
                        Emoji("📤 ", ""),
                        userid_as_string_with_type(&imported_cert)
                    );
                    if self.verify {
                        match response.status.get(&email) {
                            Some(KeyserverEmailStatus::Unpublished)
                            | Some(KeyserverEmailStatus::Pending) => {
                                match keyserver.verify_cert(&response.token, &email).await {
                                    Ok(_) => eprintln!(
                                        "{}Verification request submitted.",
                                        Emoji("🔎 ", ""),
                                    ),
                                    Err(error) => handle_failed_verification(error),
                                }
                            }
                            Some(KeyserverEmailStatus::Revoked) => handle_failed_verification(
                                anyhow!("This key is revoked and cannot be used."),
                            ),
                            Some(KeyserverEmailStatus::Published) => {
                                handle_failed_verification(anyhow!("This key is already verified."))
                            }
                            None => handle_failed_verification(anyhow!(format!(
                                "Couldn't find {} in keyserver's response.",
                                email
                            ))),
                        }
                    }
                }
                Err(error) => eprintln!("{} Rejected upload: {error}", Emoji("💥", "ERROR:")),
            };
        } else {
            eprintln!(
                "{} This key does not contain an email.",
                Emoji("💥", "ERROR:"),
            )
        }
        Ok(())
    }
}

impl Export {
    async fn run(&self) -> Result<()> {
        // Open the local certificate store and retrieve the requested
        // certificate based on the identifier provided by the user. This
        // can be either a fingerprint or an email address.
        let cert_store = CertStore::open(&CertStoreOpts {
            read_only: true,
            ..Default::default()
        })?;
        let mut cert = get_single_cert(&self.key_identifier.parse()?, &cert_store)?;
        if let CertType::Secret = self.private.cert_type {
            let mut key_store = KeyStore::open(None).await?;
            cert = cert
                .update_secret_material(&mut key_store)
                .await
                .inspect_err(|_| {
                    eprintln!(
                        "{} Unable export private key material for key '{}'. \
                        If you wish to export a public key, remove the \
                        '-p/--private' option from this command.",
                        Emoji("💥", "ERROR:"),
                        &self.key_identifier
                    )
                })?;
        }
        let serialized_cert = match (self.output_format, self.private.cert_type) {
            (SerializationFormat::AsciiArmored, CertType::Secret) => {
                AsciiArmored::try_from(cert.secret()?)?.into()
            }
            (SerializationFormat::Binary, CertType::Secret) => Vec::<u8>::try_from(cert.secret()?)?,
            (SerializationFormat::AsciiArmored, CertType::Public) => {
                AsciiArmored::try_from(cert.public())?.into()
            }
            (SerializationFormat::Binary, CertType::Public) => Vec::<u8>::try_from(cert.public())?,
        };

        // Write the serialized certificate to either a file or stdout.
        if let Some(optional_file_name) = &self.output_file {
            // Case 1: the certificate should be written to file.
            let file_name = optional_file_name.clone().unwrap_or_else(|| {
                generate_file_name(&cert, self.private.cert_type, self.output_format)
            });
            File::create(&file_name)?.write_all(&serialized_cert)?;
            eprintln!(
                "{}Exported {} to file '{}'.",
                Emoji("📤 ", ""),
                userid_as_string_with_type(&cert),
                file_name.to_string_lossy(),
            );
        } else {
            // Case 2: the certificate should be written to stdout.
            io::stdout().write_all(&serialized_cert)?;
        }
        Ok(())
    }
}

fn cert_expiration_time(cert: &Cert) -> Result<CertExpirationTime> {
    Ok(match cert.expiration_time()? {
        // Certificate has an expiration date.
        Some(expiration_time) => {
            if expiration_time > std::time::SystemTime::now() {
                CertExpirationTime::Valid(Some(DateTime::<Local>::from(expiration_time)))
            } else {
                CertExpirationTime::Expired(DateTime::<Local>::from(expiration_time))
            }
        }
        // Certificate never expires.
        None => CertExpirationTime::Valid(None),
    })
}

struct ListItem {
    name: String,
    cert_type: CertType,
    fingerprint: Fingerprint,
    revocation_status: Option<String>,
    expiration_time: Option<String>,
    location: Option<String>,
}

impl std::fmt::Display for ListItem {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}{}{}{}",
            self.name,
            self.revocation_status.as_deref().unwrap_or(""),
            self.expiration_time.as_deref().unwrap_or(""),
            self.location.as_deref().unwrap_or(""),
        )
    }
}

impl ListItem {
    fn new(cert: &Cert, keys: &[Fingerprint], always_show_expiration: bool) -> Result<Self> {
        let fingerprint = cert.fingerprint();
        let cert_type = if keys.contains(&fingerprint) {
            CertType::Secret
        } else {
            CertType::Public
        };
        Ok(Self {
            name: cert.to_string(),
            cert_type,
            fingerprint,
            revocation_status: Self::revocation_status(cert),
            expiration_time: Self::expiration_time(cert, always_show_expiration),
            location: None,
        })
    }

    fn revocation_status(cert: &Cert) -> Option<String> {
        if let RevocationStatus::Revoked(revs) = cert.revocation_status() {
            let mut output = String::new();
            for r in revs {
                let (code, msg) = r.reason_for_revocation().map_or(
                    (ReasonForRevocation::Unspecified, Cow::Borrowed("")),
                    |(code, msg)| (code, String::from_utf8_lossy(msg)),
                );
                let (emoji, style) = match code {
                    ReasonForRevocation::KeyRetired
                    | ReasonForRevocation::UIDRetired
                    | ReasonForRevocation::KeySuperseded => {
                        (Emoji("⚠️ ", ""), console::Style::new().yellow())
                    }
                    _ => (Emoji("⛔", ""), console::Style::new().red()),
                };
                write!(
                    &mut output,
                    " {}",
                    style.apply_to(format!(
                        "{} REVOKED! {}{}",
                        emoji,
                        code,
                        if msg.is_empty() {
                            String::from("")
                        } else {
                            format!(": \"{msg}\"")
                        }
                    ))
                )
                .unwrap();
            }
            return Some(output);
        };
        None
    }

    /// Returns certificate's expiry date.
    ///
    /// * Always, for certificates that have expired or whose expiry date could not be retrieved.
    /// * Only if requested by the user otherwise.
    fn expiration_time(cert: &Cert, always_show: bool) -> Option<String> {
        match cert_expiration_time(cert) {
            Ok(CertExpirationTime::Valid(Some(expiration_time))) => always_show.then(|| {
                format!(
                    " {}",
                    console::style(format!(
                        "[Expires on {}]",
                        expiration_time.format(TIME_FORMAT)
                    ))
                    .green()
                )
            }),
            Ok(CertExpirationTime::Valid(None)) => {
                always_show.then(|| format!(" {}", console::style("[Never expires]").green()))
            }
            Ok(CertExpirationTime::Expired(expiration_time)) => Some(format!(
                " {}{}",
                Emoji("⛔ ", ""),
                console::style(format!(
                    "Expired on {}",
                    expiration_time.format(TIME_FORMAT)
                ))
                .red()
            )),
            Err(e) => Some(format!(
                " {}{}",
                Emoji("⛔ ", ""),
                console::style(format!("Expiration date could not be retrieved: {}", e)).red()
            )),
        }
    }

    async fn with_path(&mut self, cert_store: &CertStore<'_>) {
        let output = cert_store
            .get_cert_path(&self.fingerprint)
            .map_or_else(
                |e| console::style(format!(" [{e}]")).red(),
                |p| console::style(format!(" [{}]", p.to_string_lossy())).dim(),
            )
            .to_string();
        self.location = Some(output);
    }
}

impl List {
    async fn run(&self) -> Result<()> {
        let filter = self.filter.as_ref().map(|filter| filter.to_lowercase());
        let cert_store = CertStore::open(&CertStoreOpts {
            read_only: true,
            ..Default::default()
        })?;
        let mut key_store = KeyStore::open(None).await?;
        let keys = key_store
            .list()
            .await?
            .into_iter()
            .map(|key| key.fingerprint())
            .collect::<Vec<_>>();
        let mut private = Vec::new();
        let mut public = Vec::new();
        for cert in cert_store.certs() {
            let cert = match cert {
                Ok(cert) => cert,
                Err(e) => {
                    tracing::warn!("Failed to read a key: {e}",);
                    continue;
                }
            };
            if !filter
                .as_ref()
                .map(|filter| {
                    cert.userids()
                        .into_iter()
                        .any(|userid| userid.to_lowercase().contains(filter))
                })
                .unwrap_or(true)
            {
                continue;
            }

            let mut item = ListItem::new(&cert, &keys, self.show_expiration)?;
            if self.show_path {
                item.with_path(&cert_store).await;
            }
            if item.cert_type == CertType::Secret {
                private.push(item);
            } else {
                public.push(item);
            }
        }
        let mut std_lock = io::stdout();
        for (list, label) in [
            (private, Emoji("🔒 Private", "Private")),
            (public, Emoji("📨 Public", "Public")),
        ] {
            writeln!(&mut std_lock, "{label} keys")?;
            if list.is_empty() {
                write!(&std_lock, "{}", console::style("Key store is empty.").red())?;
            }
            for line in list {
                writeln!(&mut std_lock, "{}", line)?;
            }
            writeln!(&mut std_lock)?;
        }
        Ok(())
    }
}

/// Return `CertType::Secret` if `with_secret` is `true`.
fn parse_cert_type_flag(with_secret: &str) -> Result<CertType> {
    if with_secret == "true" {
        Ok(CertType::Secret)
    } else {
        Ok(CertType::Public)
    }
}

/// Return `SerializationFormat::AsciiArmored` if `ascii_armor` is `true`.
fn parse_ascii_armor_flag(ascii_armor: &str) -> Result<SerializationFormat> {
    if ascii_armor == "true" {
        Ok(SerializationFormat::AsciiArmored)
    } else {
        Ok(SerializationFormat::Binary)
    }
}

/// Generate an output file name based on the fingerprint of the specified
/// OpenPGP certificate.
fn generate_file_name(cert: &Cert, cert_type: CertType, format: SerializationFormat) -> PathBuf {
    PathBuf::from(cert.fingerprint().to_hex()).with_extension(match (format, cert_type) {
        (SerializationFormat::Binary, CertType::Public) => "pub",
        (SerializationFormat::Binary, CertType::Secret) => "sec",
        (SerializationFormat::AsciiArmored, CertType::Public) => "pub.asc",
        (SerializationFormat::AsciiArmored, CertType::Secret) => "sec.asc",
    })
}

/// Formats the primary user ID of an OpenPGP certificate as a string of type:
///  -> public/private key: `<user ID> <email> [fingerprint]`
fn userid_as_string_with_type(cert: &Cert) -> String {
    format!("{} key: {}", cert.cert_type(), cert)
}

/// Retrieve an OpenPGP certificate matching the specified `query_term`.
/// If the query returns 0 or more than 1 certificates, an error is returned.
pub(super) fn get_single_cert(query_term: &QueryTerm, store: &CertStore) -> Result<Cert> {
    // Search for certificates matching the specified query term. If no
    // certificate is found, an error is returned.
    let mut certs = store.get_cert(query_term)?;

    // Return an error if more than one certificates matches the query.
    // Note that this can only happen if the query term is an email.
    if certs.len() > 1 {
        bail!(
            "More than one key is matching the specified email: {}",
            certs
                .iter()
                .map(std::string::ToString::to_string)
                .collect::<Vec<String>>()
                .join(", ")
        );
    }

    // Return the (single) matching certificate - at this point we are
    // in principle guaranteed that `certs` contains exactly 1 certificate.
    Ok(certs
        .pop()
        .expect("`certs` should contain exactly 1 element at this point."))
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Cursor;

    fn generate_tmp_certstore<'a>() -> CertStore<'a> {
        let tmp_dir = tempfile::tempdir().unwrap().into_path();
        CertStore::open(&CertStoreOpts {
            location: Some(&tmp_dir.join("pgp.cert.d")),
            ..Default::default()
        })
        .unwrap()
    }

    #[tokio::test]
    async fn import() -> Result<()> {
        let mut cert_store = generate_tmp_certstore();
        let mut key_store = KeyStore::open_ephemeral().await?;

        macro_rules! assert_import {
            ($file_name:expr, $cert_type:expr) => {
                // Import certificate.
                import_certs(
                    Cursor::new(include_bytes!(concat!(
                        "../../../sett/tests/data/",
                        $file_name
                    ))),
                    &mut cert_store,
                    &mut key_store,
                    $cert_type,
                )
                .await;
                // Make sure certificate is now present in the CertStore.
                let fingerprint = &$file_name[..40];
                assert_eq!(
                    get_single_cert(&fingerprint.parse()?, &cert_store)?
                        .fingerprint()
                        .to_string(),
                    fingerprint,
                )
            };
        }
        // We start with empty stores.
        assert_eq!(cert_store.certs().count(), 0);
        assert_eq!(key_store.list().await?.len(), 0);
        // Importing a public or secret certificate in ASCII format should work.
        assert_import!(
            "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub.asc",
            CertType::Public
        );
        assert_import!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF.pub.asc",
            CertType::Public
        );
        assert_import!(
            "C0621CB3669020CC31050A361956EC38A96CA852.pub.asc",
            CertType::Public
        );
        assert_eq!(cert_store.certs().count(), 3);
        assert_eq!(key_store.list().await?.len(), 0);
        assert_import!(
            "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.sec.asc",
            CertType::Secret
        );
        assert_import!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF.sec.asc",
            CertType::Secret
        );
        assert_import!(
            "C0621CB3669020CC31050A361956EC38A96CA852.sec.asc",
            CertType::Secret
        );
        assert_eq!(cert_store.certs().count(), 3);
        assert!(key_store.list().await?.len() >= 3);

        // Clear stores
        cert_store = generate_tmp_certstore();
        key_store = KeyStore::open_ephemeral().await?;
        assert_eq!(cert_store.certs().count(), 0);
        assert_eq!(key_store.list().await?.len(), 0);

        // Importing public or secret certificates in binary format should work.
        assert_import!(
            "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub",
            CertType::Public
        );
        assert_import!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF.pub",
            CertType::Public
        );
        assert_import!(
            "C0621CB3669020CC31050A361956EC38A96CA852.pub",
            CertType::Public
        );
        assert_eq!(cert_store.certs().count(), 3);
        assert_eq!(key_store.list().await?.len(), 0);
        assert_import!(
            "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.sec",
            CertType::Secret
        );
        assert_import!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF.sec",
            CertType::Secret
        );
        assert_import!(
            "C0621CB3669020CC31050A361956EC38A96CA852.sec",
            CertType::Secret
        );
        assert_eq!(cert_store.certs().count(), 3);
        assert!(key_store.list().await?.len() >= 3);

        // Clear stores
        cert_store = generate_tmp_certstore();
        key_store = KeyStore::open_ephemeral().await?;
        assert_eq!(cert_store.certs().count(), 0);
        assert_eq!(key_store.list().await?.len(), 0);

        // Importing secret certificates as public should add certificates to the public store, but
        // no keys should be added to the key store.
        assert_import!(
            "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.sec.asc",
            CertType::Public
        );
        assert_import!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF.sec.asc",
            CertType::Public
        );
        assert_import!(
            "C0621CB3669020CC31050A361956EC38A96CA852.sec.asc",
            CertType::Public
        );
        assert_eq!(cert_store.certs().count(), 3);
        assert_eq!(key_store.list().await?.len(), 0);

        Ok(())
    }

    // Importing a public certificate as `Secret` should not add keys to the key store
    #[tokio::test]
    async fn import_pub_as_private() -> anyhow::Result<()> {
        let mut cert_store = generate_tmp_certstore();
        let mut key_store = KeyStore::open_ephemeral().await?;

        macro_rules! assert_import {
            ($file_name:expr) => {
                // Import certificate.
                import_certs(
                    Cursor::new(include_bytes!(concat!(
                        "../../../sett/tests/data/",
                        $file_name
                    ))),
                    &mut cert_store,
                    &mut key_store,
                    CertType::Secret,
                ).await;
                // A certificate should be imported in to the cert store.
                assert!(get_single_cert(&($file_name[..40]).parse()?, &cert_store).is_ok());
                    // No keys should be added to the key store (the certificate doesn't contain secret material).
        assert_eq!(key_store.list().await?.len(), 0);
            };
        }

        // Importing a public certificate as secret should return an error.
        assert_import!("1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub");
        assert_import!("1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub.asc");
        assert_import!("B2E961753ECE0B345E718E74BA6F29C998DDD9BF.pub");
        assert_import!("B2E961753ECE0B345E718E74BA6F29C998DDD9BF.pub.asc");
        assert_import!("C0621CB3669020CC31050A361956EC38A96CA852.pub");
        assert_import!("C0621CB3669020CC31050A361956EC38A96CA852.pub.asc");
        Ok(())
    }

    #[test]
    fn test_get_single_cert() -> Result<()> {
        let mut cert_store = generate_tmp_certstore();
        let fingerprint_cert_1 = "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB";
        let fingerprint_cert_2 = "C0621CB3669020CC31050A361956EC38A96CA852";

        macro_rules! assert_pass {
            ($query_term:expr, $cert:expr) => {
                assert_eq!(get_single_cert(&$query_term.parse()?, &cert_store)?, $cert)
            };
        }

        macro_rules! assert_fails {
            ($query_term:expr) => {
                assert!(get_single_cert(&$query_term.parse()?, &cert_store).is_err());
            };
        }

        // cert_1 => contains only public material.
        let cert_1 = cert_store.import(&Cert::from_bytes(include_bytes!(
            "../../../sett/tests/data/1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub"
        ))?)?;

        // Retrieving certificate by email or fingerprint should work.
        assert_pass!(fingerprint_cert_1, cert_1);
        assert_pass!("chuck.norris@roundhouse.org", cert_1);

        // Trying to retrieve a non-existing certificate should fail.
        assert_fails!("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        assert_fails!("missing@example.org");

        // When adding a second certificate to the store, with the same email
        // address as the first one, the function should now return an error
        // if we query by email.
        let cert_2 = cert_store.import(&Cert::from_bytes(include_bytes!(
            "../../../sett/tests/data/C0621CB3669020CC31050A361956EC38A96CA852.sec"
        ))?)?;
        assert_fails!("chuck.norris@roundhouse.org");

        // Querying by fingerprint should still work, since fingerprints
        // are unique.
        assert_pass!(fingerprint_cert_1, cert_1);
        assert_pass!(fingerprint_cert_2, cert_2);

        Ok(())
    }

    // Test that importing multiple certificate from data is working.
    #[tokio::test]
    async fn import_multiple_certs() -> Result<()> {
        macro_rules! assert_import {
            ($file_name:expr, $cert_type:expr) => {
                let mut cert_store = generate_tmp_certstore();
                let mut key_store = KeyStore::open_ephemeral().await?;
                assert_eq!(cert_store.certs().count(), 0);
                assert_eq!(key_store.list().await?.len(), 0);
                import_certs(
                    Cursor::new(include_bytes!(concat!(
                        "../../../sett/tests/data/",
                        $file_name
                    ))),
                    &mut cert_store,
                    &mut key_store,
                    $cert_type,
                )
                .await;
                // Make sure the 3 certificates that are part of the keyring
                // file are now present in the CertStore.
                for fingerprint in [
                    "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB",
                    "B2E961753ECE0B345E718E74BA6F29C998DDD9BF",
                    "C0621CB3669020CC31050A361956EC38A96CA852",
                ] {
                    assert_eq!(
                        get_single_cert(&fingerprint.parse()?, &cert_store)
                            .unwrap()
                            .fingerprint()
                            .to_string(),
                        fingerprint,
                    );
                    assert_eq!(cert_store.certs().count(), 3);
                    assert!(match $cert_type {
                        CertType::Public => key_store.list().await?.len() == 0,
                        CertType::Secret => key_store.list().await?.len() >= 3,
                    });
                }
            };
        }

        // Importing multiple public or secret certificates should work.
        assert_import!("keyring.pub", CertType::Public);
        assert_import!("keyring.sec", CertType::Secret);

        Ok(())
    }
}
