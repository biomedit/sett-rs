use indicatif::{ProgressBar, ProgressDrawTarget, ProgressStyle};
use sett::progress::ProgressDisplay;

#[derive(Clone)]
pub(crate) struct CliProgress {
    pb: ProgressBar,
}

impl CliProgress {
    pub(crate) fn new() -> Self {
        let style = ProgressStyle::with_template("{spinner} [{elapsed_precise}] {wide_bar} {bytes}/{total_bytes} ({bytes_per_sec}) ETA: {eta}")
            .expect("Failed to set progress bar style");
        let pb = ProgressBar::with_draw_target(None, ProgressDrawTarget::stderr_with_hz(2))
            .with_style(style);
        Self { pb }
    }
}

impl ProgressDisplay for CliProgress {
    fn set_completion_value(&mut self, len: u64) {
        self.pb.set_length(len)
    }
    fn increment(&mut self, delta: u64) {
        self.pb.inc(delta)
    }
    fn finish(&mut self) {
        self.pb.finish();
        eprintln!();
    }
}

pub(crate) fn two_factor_prompt() -> String {
    let mut input = String::new();
    if std::io::stdin().read_line(&mut input).is_err() {
        eprintln!("Failed to read input");
        input.clear();
    }
    input.replace('\n', "")
}

pub(crate) fn get_portal_client(
) -> std::result::Result<sett::portal::Portal, sett::portal::error::CreateError> {
    sett::portal::Portal::new(
        std::env::var("SETT_PORTAL_URL")
            .as_deref()
            .unwrap_or("https://portal.dcc.sib.swiss"),
    )
}

#[cfg(feature = "auth")]
const BIOMEDIT_KEYCLOAK_URL: &str = "https://login.biomedit.ch/realms/biomedit";
#[cfg(feature = "auth")]
const BIOMEDIT_KEYCLOAK_SHORT_URL: &str = "https://login.biomedit.ch";
#[cfg(feature = "auth")]
const ENV_SETT_OIDC_ISSUER_URL: &str = "SETT_OIDC_ISSUER_URL";

#[cfg(feature = "auth")]
pub(crate) async fn get_oidc_client() -> std::result::Result<sett::auth::Oidc, anyhow::Error> {
    Ok(sett::auth::Oidc::new(
        std::env::var("SETT_OIDC_CLIENT_ID")
            .as_deref()
            .unwrap_or("sett"),
        std::env::var(ENV_SETT_OIDC_ISSUER_URL)
            .as_deref()
            .unwrap_or(BIOMEDIT_KEYCLOAK_URL)
            .parse()?,
    )
    .await?)
}

#[cfg(feature = "auth")]
/// Shorten OIDC issuer URL.
///
/// The URL is only shortened for the official BioMedIT issuer when the
/// corresponding environment variable is unset.
pub(crate) fn shorten_oidc_issuer_url(issuer: &str) -> std::borrow::Cow<'_, str> {
    if std::env::var_os(ENV_SETT_OIDC_ISSUER_URL).is_none()
        && issuer.contains(BIOMEDIT_KEYCLOAK_URL)
    {
        issuer
            .replace(BIOMEDIT_KEYCLOAK_URL, BIOMEDIT_KEYCLOAK_SHORT_URL)
            .into()
    } else {
        issuer.into()
    }
}

#[cfg(test)]
mod tests {

    #[cfg(feature = "auth")]
    #[test]
    fn shorten_oidc_issuer_url() {
        use super::shorten_oidc_issuer_url;
        use crate::util::ENV_SETT_OIDC_ISSUER_URL;

        assert_eq!(
            &shorten_oidc_issuer_url("https://login.biomedit.ch/realms/biomedit"),
            "https://login.biomedit.ch"
        );
        assert_eq!(
            &shorten_oidc_issuer_url("https://example.org"),
            "https://example.org"
        );
        std::env::set_var(
            ENV_SETT_OIDC_ISSUER_URL,
            "https://login.biomedit.ch/realms/biomedit",
        );
        assert_eq!(
            &shorten_oidc_issuer_url("https://login.biomedit.ch/realms/biomedit"),
            "https://login.biomedit.ch/realms/biomedit"
        );
    }
}
