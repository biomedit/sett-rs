use crossterm::event::KeyEvent;
use indicatif::HumanBytes;
use ratatui::{
    layout::{Constraint, Layout, Rect},
    style::{Color, Style},
    text::{Line, Span},
    widgets::{Block, Borders, Paragraph, Row, Table},
    Frame,
};
use sett::package::Metadata;
use tokio::sync::mpsc::UnboundedSender;

use crate::tui::{
    action::Action,
    event::{Event, EventHandlingStatus},
    state::State,
    widget::{
        component::{Component, Focus},
        popup_area,
        progress::Progress,
        CyclicIndex,
    },
};

pub(crate) mod about;
pub(crate) mod cert;
pub(crate) mod decrypt;
pub(crate) mod encrypt;
#[cfg(feature = "auth")]
pub(crate) mod user;

pub(crate) struct Tab {
    pub(crate) label: &'static str,
    pub(crate) render: fn(&mut State, &mut Frame, Rect),
    pub(crate) handle_key: fn(&mut State, KeyEvent, UnboundedSender<Event>) -> EventHandlingStatus,
}

pub(crate) const TABS: &[&Tab] = &[
    &encrypt::TAB,
    &decrypt::TAB,
    &cert::TAB,
    #[cfg(feature = "auth")]
    &user::TAB,
    &about::TAB,
];

struct ProgressUpdater<Cb: Fn(u64, u64)> {
    current: u64,
    total: u64,
    last_fraction: f64,
    precision: f64,
    cb: Cb,
}

impl<Cb: Fn(u64, u64)> ProgressUpdater<Cb> {
    fn new(cb: Cb) -> Self {
        Self {
            current: 0,
            total: 0,
            last_fraction: 0.0,
            precision: 0.01,
            cb,
        }
    }

    fn update(&mut self) {
        let fraction = (self.current as f64) / (self.total as f64);
        if fraction - self.last_fraction > self.precision {
            self.last_fraction = fraction;
            (self.cb)(self.current, self.total);
        }
    }
}

impl<Cb: Fn(u64, u64)> sett::progress::ProgressDisplay for ProgressUpdater<Cb> {
    fn set_completion_value(&mut self, len: u64) {
        self.total = len;
    }
    fn increment(&mut self, delta: u64) {
        self.current += delta;
        self.update();
    }
    fn finish(&mut self) {
        self.current = self.total;
        (self.cb)(self.current, self.total);
    }
}

#[derive(Debug, Clone, Default)]
pub(crate) enum Status {
    #[default]
    New,
    Running,
    Finished(sett::task::Status),
    Error(String),
}

impl Status {
    fn into_encrypt_event(self) -> Event {
        Event::Action(Action::Encrypt(encrypt::action::EncryptAction::Task(
            TaskAction::Status(self),
        )))
    }

    fn into_decrypt_event(self) -> Event {
        Event::Action(Action::Decrypt(decrypt::action::DecryptAction::Task(
            TaskAction::Status(self),
        )))
    }
}

#[derive(Debug, Clone)]
pub(crate) enum TaskAction {
    Run,
    Progress(Progress),
    Status(Status),
}

impl TaskAction {
    fn into_encrypt_event(self) -> Event {
        Event::Action(Action::Encrypt(encrypt::action::EncryptAction::Task(self)))
    }

    fn into_decrypt_event(self) -> Event {
        Event::Action(Action::Decrypt(decrypt::action::DecryptAction::Task(self)))
    }
}

enum TaskKind {
    Encrypt,
    Decrypt,
    Transfer,
}

#[derive(Debug, Default, Copy, Clone, PartialEq, Eq)]
pub(crate) enum UserMode {
    #[default]
    Anonymous,
    #[cfg(feature = "auth")]
    Authenticated,
}

fn render_status(status: &Status, task: TaskKind, frame: &mut Frame, area: Rect) {
    match status {
        Status::New => {}
        Status::Running => {}
        Status::Error(error) => frame.render_widget(
            Paragraph::new(error.as_str()).style(Style::new().fg(Color::Red)),
            area,
        ),
        Status::Finished(status) => {
            let constraints = [Constraint::Length(30), Constraint::Min(40)];
            frame.render_widget(
                match (status, task) {
                    (sett::task::Status::Checked { .. }, _) => unreachable!(),
                    (
                        sett::task::Status::Completed {
                            destination,
                            source_size,
                            destination_size,
                            ..
                        },
                        TaskKind::Encrypt,
                    ) => Table::new(
                        [
                            Row::new(["Completed data encryption", ""]),
                            Row::new(["Destination", destination]),
                            Row::new([
                                Span::from("Source size"),
                                Span::from(HumanBytes(*source_size).to_string()),
                            ]),
                            Row::new([
                                Span::from("Destination size"),
                                Span::from(HumanBytes(*destination_size).to_string()),
                            ]),
                            Row::new([
                                Span::from("Compression ratio"),
                                Span::from(format!(
                                    "{:.2}",
                                    *source_size as f32 / *destination_size as f32
                                )),
                            ]),
                        ],
                        constraints,
                    ),
                    (
                        sett::task::Status::Completed {
                            destination,
                            source_size,
                            destination_size,
                            ..
                        },
                        TaskKind::Decrypt,
                    ) => Table::new(
                        [
                            Row::new(["Completed data decryption", ""]),
                            Row::new(["Destination", destination]),
                            Row::new([
                                Span::from("Source size"),
                                Span::from(HumanBytes(*source_size).to_string()),
                            ]),
                            Row::new([
                                Span::from("Destination size"),
                                Span::from(HumanBytes(*destination_size).to_string()),
                            ]),
                            Row::new([
                                Span::from("Compression ratio"),
                                Span::from(format!(
                                    "{:.2}",
                                    *destination_size as f32 / *source_size as f32
                                )),
                            ]),
                        ],
                        constraints,
                    ),
                    (
                        sett::task::Status::Completed {
                            destination,
                            source_size,
                            ..
                        },
                        TaskKind::Transfer,
                    ) => Table::new(
                        [
                            Row::new(["Completed data transfer", ""]),
                            Row::new(["Destination", destination]),
                            Row::new([
                                Span::from("Source size"),
                                Span::from(HumanBytes(*source_size).to_string()),
                            ]),
                        ],
                        constraints,
                    ),
                },
                area,
            );
        }
    }
}

fn render_components<const N: usize>(
    components: &[&Component; N],
    constraints: &[Constraint; N],
    state: &mut State,
    current_component: CyclicIndex<N>,
    frame: &mut Frame,
    rect: Rect,
) {
    const KEY_BINDINGS: &str =
        " <tab> change field <number> change tab <c-x> clear form <esc> exit ";
    let block = Block::default()
        .borders(Borders::ALL)
        .title_bottom(Line::from(KEY_BINDINGS).centered());
    let inner_area = block.inner(rect);
    frame.render_widget(block, rect);
    let areas: [Rect; N] = Layout::vertical(constraints).areas(inner_area);
    for (i, (component, area)) in components.iter().zip(areas.into_iter()).enumerate() {
        if (component.show)(state) {
            let block = Block::default()
                .borders(Borders::ALL)
                .title_bottom(Line::from(component.key_bindings).centered())
                .border_style(Style::default().fg(Color::Green));
            let inner_area = block.inner(area);
            let focus = if *current_component == i {
                frame.render_widget(block, area);
                Focus::Focused
            } else {
                Focus::NotFocused
            };
            (component.render)(state, frame, inner_area, focus);
        }
    }
    let popup_area = popup_area(inner_area, 80, 80);
    for component in components {
        (component.render_popup)(state, frame, popup_area);
    }
}

const fn constraints<const N: usize>(components: &[&Component; N]) -> [Constraint; N] {
    let mut constraints: [Constraint; N] = [Constraint::Length(1); N];
    let mut i = 0;
    const BLOCK_LENGTH: u16 = 2;
    while i < N {
        constraints[i] = match components[i].constraint {
            Constraint::Length(length) => Constraint::Length(length + BLOCK_LENGTH),
            Constraint::Min(length) => Constraint::Min(length + BLOCK_LENGTH),
            Constraint::Max(length) => Constraint::Max(length + BLOCK_LENGTH),
            c => c,
        };
        i += 1;
    }
    constraints
}

fn render_metadata(metadata: &Metadata, frame: &mut Frame, area: Rect) {
    let table = Table::new(
        [
            Row::new([Span::from("Sender"), Span::from(metadata.sender.as_str())]),
            Row::new([
                Span::from("Recipients"),
                Span::from(metadata.recipients.join(", ")),
            ]),
            Row::new([
                Span::from("Date created"),
                Span::from(metadata.timestamp.to_string()),
            ]),
            Row::new([
                Span::from("Transfer ID"),
                Span::from(
                    metadata
                        .transfer_id
                        .map_or_else(|| "-".to_string(), |dtr| format!("{dtr}")),
                ),
            ]),
        ],
        [Constraint::Length(20), Constraint::Min(20)],
    );
    frame.render_widget(table, area);
}

#[cfg(feature = "auth")]
async fn load_data_transfers(
    sender: UnboundedSender<Event>,
) -> Result<Vec<sett::portal::response::DataTransfer>, String> {
    let (tx, rx) = tokio::sync::oneshot::channel();
    sender
        .send(crate::tui::tab::user::AuthAction::GetToken(tx).into())
        .expect("receiver is alive");
    let token = rx
        .await
        .expect("sender is alive")
        .ok_or_else(|| "Unable to get OAuth2 token".to_string())?;
    let portal_client = crate::util::get_portal_client().map_err(|e| e.to_string())?;
    let data_transfers = portal_client
        .get_data_transfers(&token.access_token)
        .await
        .map_err(|e| e.to_string())?;
    let mut data_transfers: Vec<_> = data_transfers
        .into_iter()
        .filter(|t| t.status == sett::portal::response::DataTransferStatus::Authorized)
        .collect();
    data_transfers.sort_by(|a, b| a.id.cmp(&b.id));
    Ok(data_transfers)
}
