use tokio::sync::mpsc::UnboundedSender;

use crate::tui::{
    event::Event,
    state::{PasswordPrompt, State},
    tab,
};

#[derive(Debug)]
pub(crate) enum Action {
    Encrypt(tab::encrypt::action::EncryptAction),
    Decrypt(tab::decrypt::action::DecryptAction),
    Cert(tab::cert::CertAction),
    #[cfg(feature = "auth")]
    Auth(tab::user::AuthAction),
    PromptPassword {
        hint: sett::openpgp::crypto::PasswordHint,
        result: tokio::sync::oneshot::Sender<sett::secret::Secret>,
    },
}

impl From<Action> for Event {
    fn from(action: Action) -> Self {
        Self::Action(action)
    }
}

impl Action {
    pub(crate) async fn handle(self, state: &mut State, sender: UnboundedSender<Event>) {
        match self {
            Self::Encrypt(action) => state.encrypt.handle_action(action, sender).await,
            Self::Decrypt(action) => state.decrypt.handle_action(action, sender).await,
            Self::Cert(action) => state.cert.handle_action(action, sender).await,
            #[cfg(feature = "auth")]
            Self::Auth(action) => action.handle(&mut state.auth_status, sender).await,
            Self::PromptPassword { hint, result } => {
                state.password_prompt = Some(PasswordPrompt {
                    hint,
                    input: String::new(),
                    result,
                });
            }
        }
    }
}
