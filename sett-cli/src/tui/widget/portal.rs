use crossterm::event::{KeyCode, KeyEvent};
use ratatui::{
    layout::{Constraint, Rect},
    style::{Modifier, Style},
    text::{Line, Span},
    widgets::{Block, TableState},
    Frame,
};
use sett::portal::response::DataTransfer;

use crate::tui::event::EventHandlingStatus;

#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) struct DataTransferSelector {
    show: bool,
    state: TableState,
    transfers: Result<Vec<DataTransfer>, String>,
}

impl Default for DataTransferSelector {
    fn default() -> Self {
        Self {
            show: false,
            state: Default::default(),
            transfers: Ok(Vec::new()),
        }
    }
}

impl DataTransferSelector {
    pub(crate) fn open(&mut self) {
        self.show = true;
    }

    pub(crate) fn is_open(&self) -> bool {
        self.show
    }

    fn close(&mut self) {
        self.show = false
    }

    pub(crate) fn clear_selection(&mut self) {
        self.state.select(None);
    }

    pub(crate) fn set_data_transfers(&mut self, transfers: Result<Vec<DataTransfer>, String>) {
        self.transfers = transfers;
    }

    pub(crate) fn handle_key(
        &mut self,
        key: KeyEvent,
    ) -> (EventHandlingStatus, Option<DataTransfer>) {
        match key.code {
            KeyCode::Char('j') | KeyCode::Down => {
                self.state.select_next();
                (EventHandlingStatus::Handled, None)
            }
            KeyCode::Char('k') | KeyCode::Up => {
                self.state.select_previous();
                (EventHandlingStatus::Handled, None)
            }
            KeyCode::Esc | KeyCode::Char('q') => {
                self.close();
                (EventHandlingStatus::Handled, None)
            }
            KeyCode::Enter | KeyCode::Char(' ') => {
                if let Some(i) = self.state.selected() {
                    self.close();
                    return (
                        EventHandlingStatus::Handled,
                        self.transfers.as_ref().ok().and_then(|t| t.get(i).cloned()),
                    );
                }
                (EventHandlingStatus::Handled, None)
            }
            _ => (EventHandlingStatus::Ignored, None),
        }
    }

    pub(crate) fn render(&mut self, frame: &mut Frame, area: Rect) {
        let block = Block::bordered()
            .title_top("Select data transfer request ID")
            .title_bottom(
                Line::raw(" <j/k/down/up> move <space/enter> select <esc/q> close ").centered(),
            );
        let inner_area = block.inner(area);
        frame.render_widget(block, area);
        match &self.transfers {
            Ok(transfers) => {
                let table = ratatui::widgets::Table::new(
                    transfers.iter().map(|t| {
                        ratatui::widgets::Row::new([
                            Span::from(format!("{}", t.id)),
                            Span::from(format!("{:?}", t.status)),
                            Span::from(t.project_name.as_str()),
                            Span::from(t.data_provider_name.as_str()),
                        ])
                    }),
                    [
                        Constraint::Length(12),
                        Constraint::Length(14),
                        Constraint::Fill(1),
                        Constraint::Fill(1),
                    ],
                )
                .header(
                    ratatui::widgets::Row::new([
                        "Transfer ID",
                        "Status",
                        "Project",
                        "Data Provider",
                    ])
                    .style(Style::default().add_modifier(Modifier::BOLD)),
                )
                .row_highlight_style(Style::default().add_modifier(Modifier::REVERSED));
                frame.render_stateful_widget(table, inner_area, &mut self.state);
            }
            Err(e) => frame.render_widget(
                Line::from(Span::styled(
                    e.as_str(),
                    Style::default().fg(ratatui::style::Color::LightRed),
                )),
                inner_area,
            ),
        }
    }
}
