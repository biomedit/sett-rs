use ratatui::{
    layout::{Constraint, Flex, Layout, Rect},
    style::Stylize as _,
    text::{Line, Masked},
    widgets::{Block, Borders, Clear, Padding, Paragraph, Row, Table},
    Frame,
};
use sett::openpgp::crypto::PasswordHint;

use crate::tui::state::PasswordPrompt;

pub(crate) fn render(prompt: &PasswordPrompt, frame: &mut Frame, area: Rect) {
    let [area] = Layout::vertical([Constraint::Length(12)])
        .flex(Flex::Center)
        .areas(area);
    let [area] = Layout::horizontal([Constraint::Length(80)])
        .flex(Flex::Center)
        .areas(area);
    let block = Block::default()
        .title_top(Line::from(" Unlock private key ").centered())
        .borders(Borders::ALL)
        .padding(Padding {
            left: 1,
            right: 1,
            top: 1,
            bottom: 0,
        });
    let [msg_area, details_area, input_area] = Layout::vertical([
        Constraint::Length(2),
        Constraint::Length(5),
        Constraint::Length(3),
    ])
    .areas(block.inner(area));
    let details = Table::new(
        password_details(&prompt.hint),
        [Constraint::Length(20), Constraint::Min(40)],
    );
    let input = Paragraph::new(Masked::new(prompt.input.as_str(), '*'))
        .style(ratatui::style::Style::default().fg(ratatui::style::Color::Yellow))
        .block(Block::bordered());
    frame.render_widget(Clear, area);
    frame.render_widget(block, area);
    frame.render_widget(
        Paragraph::new("Please enter the password to unlock the following OpenPGP key."),
        msg_area,
    );
    frame.render_widget(details, details_area);
    frame.render_widget(input, input_area);
    frame.set_cursor_position(ratatui::layout::Position::new(
        input_area.x + prompt.input.chars().count() as u16 + 1,
        input_area.y + 1,
    ));
}

fn password_details(hint: &PasswordHint) -> impl Iterator<Item = Row> {
    let subkey_is_primary = Some(&hint.fingerprint) == hint.fingerprint_primary.as_ref();
    [
        hint.userid
            .as_ref()
            .map(|userid| Row::new(["User ID".into(), userid.as_str().bold()])),
        (!subkey_is_primary).then_some(()).and_then(|_| {
            hint.fingerprint_primary
                .as_ref()
                .map(|f| Row::new(["Primary fingerprint".into(), f.to_string().bold()]))
        }),
        Some(Row::new([
            if subkey_is_primary || hint.fingerprint_primary.is_none() {
                "Fingerprint"
            } else {
                "Subkey fingerprint"
            }
            .into(),
            hint.fingerprint.to_string().bold(),
        ])),
    ]
    .into_iter()
    .flatten()
}

#[cfg(test)]
mod tests {
    use ratatui::style::Stylize as _;
    use sett::openpgp::crypto::PasswordHint;

    #[test]
    fn password_details() {
        assert_eq!(
            super::password_details(&PasswordHint {
                userid: Some("Alice".to_string()),
                fingerprint_primary: Some(
                    "1234567890ABCDEF1234567890ABCDEF12345678".parse().unwrap()
                ),
                fingerprint: "1234567890ABCDEF1234567890ABCDEF12345679".parse().unwrap(),
            })
            .collect::<Vec<_>>(),
            vec![
                super::Row::new(["User ID".into(), "Alice".bold()]),
                super::Row::new([
                    "Primary fingerprint".into(),
                    "1234567890ABCDEF1234567890ABCDEF12345678".bold()
                ]),
                super::Row::new([
                    "Subkey fingerprint".into(),
                    "1234567890ABCDEF1234567890ABCDEF12345679".bold()
                ]),
            ]
        );
        assert_eq!(
            super::password_details(&PasswordHint {
                userid: Some("Alice".to_string()),
                fingerprint_primary: Some(
                    "1234567890ABCDEF1234567890ABCDEF12345678".parse().unwrap()
                ),
                fingerprint: "1234567890ABCDEF1234567890ABCDEF12345678".parse().unwrap(),
            })
            .collect::<Vec<_>>(),
            vec![
                super::Row::new(["User ID".into(), "Alice".bold()]),
                super::Row::new([
                    "Fingerprint".into(),
                    "1234567890ABCDEF1234567890ABCDEF12345678".bold()
                ]),
            ]
        );
        assert_eq!(
            super::password_details(&PasswordHint {
                userid: None,
                fingerprint_primary: Some(
                    "1234567890ABCDEF1234567890ABCDEF12345678".parse().unwrap()
                ),
                fingerprint: "1234567890ABCDEF1234567890ABCDEF12345679".parse().unwrap(),
            })
            .collect::<Vec<_>>(),
            vec![
                super::Row::new([
                    "Primary fingerprint".into(),
                    "1234567890ABCDEF1234567890ABCDEF12345678".bold()
                ]),
                super::Row::new([
                    "Subkey fingerprint".into(),
                    "1234567890ABCDEF1234567890ABCDEF12345679".bold()
                ]),
            ]
        );
        assert_eq!(
            super::password_details(&PasswordHint {
                userid: None,
                fingerprint_primary: None,
                fingerprint: "1234567890ABCDEF1234567890ABCDEF12345679".parse().unwrap(),
            })
            .collect::<Vec<_>>(),
            vec![super::Row::new([
                "Fingerprint".into(),
                "1234567890ABCDEF1234567890ABCDEF12345679".bold()
            ]),]
        );
        assert_eq!(
            super::password_details(&PasswordHint {
                userid: Some("Alice".to_string()),
                fingerprint_primary: None,
                fingerprint: "1234567890ABCDEF1234567890ABCDEF12345679".parse().unwrap(),
            })
            .collect::<Vec<_>>(),
            vec![
                super::Row::new(["User ID".into(), "Alice".bold()]),
                super::Row::new([
                    "Fingerprint".into(),
                    "1234567890ABCDEF1234567890ABCDEF12345679".bold()
                ]),
            ]
        );
    }
}
