use crossterm::event::{KeyCode, KeyEvent, KeyModifiers};
use ratatui::{
    layout::{Constraint, Layout, Rect},
    style::{Color, Style},
    text::Text,
    Frame,
};

use crate::tui::{
    event::EventHandlingStatus,
    widget::field::{Field, Required},
};

const COLUMN_SPACING: u16 = 4;

pub(crate) trait Form: std::fmt::Debug {
    fn label(&self) -> &'static str;

    fn description(&self) -> Option<&'static str> {
        None
    }

    fn field_count(&self) -> usize;

    fn selected_field(&mut self) -> &mut Option<usize>;

    fn fields(&self) -> Box<(dyn Iterator<Item = &dyn Field> + '_)>;

    fn fields_mut(&mut self) -> Box<(dyn Iterator<Item = &mut dyn Field> + '_)>;

    fn max_label_len(&self) -> u16 {
        self.fields()
            .map(|field| field.label().len())
            .max()
            .unwrap_or(0) as u16
    }

    fn clear(&mut self) {
        for field in &mut self.fields_mut() {
            field.clear()
        }
    }

    fn is_valid(&self) -> bool {
        self.fields().all(|field| field.validate().is_ok())
    }

    fn handle_key(&mut self, key: KeyEvent) -> EventHandlingStatus {
        let field_count = self.fields().count();
        match key.code {
            KeyCode::Tab => {
                let selected_field = self.selected_field();
                *selected_field = selected_field.map_or(Some(0), |i| {
                    (i + 1 != field_count).then_some((i + 1) % field_count)
                });
                EventHandlingStatus::Handled
            }
            KeyCode::BackTab => {
                let selected_field = self.selected_field();
                *selected_field = selected_field.map_or(Some(field_count - 1), |i| {
                    (i != 0).then_some((i + field_count - 1) % field_count)
                });
                EventHandlingStatus::Handled
            }
            KeyCode::Char('x') if key.modifiers == KeyModifiers::CONTROL => {
                self.clear();
                EventHandlingStatus::Handled
            }
            _ => {
                if let Some(index) = self.selected_field() {
                    let index = *index;
                    self.fields_mut()
                        .nth(index)
                        .expect("valid index")
                        .handle_key(key)
                } else {
                    EventHandlingStatus::Ignored
                }
            }
        }
    }

    fn render_summary(&self, frame: &mut Frame, rect: Rect) {
        let col1_len = self.max_label_len() + COLUMN_SPACING;
        let rows =
            Layout::vertical(std::iter::repeat(Constraint::Length(1)).take(self.field_count()))
                .split(rect);
        for (field, row) in self.fields().zip(rows.iter()) {
            let [label_area, value_area, error_area] = Layout::horizontal([
                Constraint::Length(col1_len),
                Constraint::Fill(1),
                Constraint::Fill(1),
            ])
            .areas(*row);
            field.render_label(frame, label_area);
            field.render_value(frame, value_area, false);
            field.render_error(frame, error_area);
        }
    }

    fn render_form(&mut self, frame: &mut Frame, rect: Rect) {
        let col1_len = self.max_label_len() + COLUMN_SPACING;
        let field_count = self.field_count();
        let [form_area, message_area] = Layout::vertical([
            Constraint::Length(field_count as u16 * 2 + 2),
            Constraint::Fill(1),
        ])
        .areas(rect);
        let rows = Layout::vertical(std::iter::repeat(Constraint::Length(2)).take(field_count))
            .split(form_area);
        let selected_field = *self.selected_field();
        for (i, (field, row)) in self.fields().zip(rows.iter()).enumerate() {
            let [label_area, value_area] =
                Layout::horizontal([Constraint::Length(col1_len), Constraint::Fill(1)]).areas(*row);
            let [input_area, error_area] =
                Layout::vertical([Constraint::Length(1), Constraint::Length(1)]).areas(value_area);
            field.render_label(frame, label_area);
            field.render_value(frame, input_area, Some(i) == selected_field);
            field.render_error(frame, error_area);
        }
        if self.fields().any(|field| field.required() == Required::Yes) {
            frame.render_widget(
                Text::styled(
                    "Fields marked with * are required",
                    Style::default().fg(Color::DarkGray),
                ),
                message_area,
            )
        }
        if let Some(i) = selected_field {
            self.fields_mut()
                .nth(i)
                .expect("valid index")
                .render_popup(frame, rect);
        }
    }
}
