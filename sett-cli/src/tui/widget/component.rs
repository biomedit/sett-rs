use crossterm::event::KeyEvent;
use ratatui::{
    layout::{Constraint, Rect},
    Frame,
};
use tokio::sync::mpsc::UnboundedSender;

use crate::tui::{
    event::{Event, EventHandlingStatus},
    state::State,
};

pub(crate) struct Component {
    pub(crate) mode: Mode,
    pub(crate) constraint: Constraint,
    pub(crate) key_bindings: &'static str,
    pub(crate) show: fn(&State) -> bool,
    pub(crate) render: fn(&mut State, &mut Frame, Rect, Focus),
    pub(crate) render_popup: fn(&mut State, &mut Frame, Rect),
    pub(crate) handle_key: fn(&mut State, KeyEvent, UnboundedSender<Event>) -> EventHandlingStatus,
}

impl Component {
    pub(crate) fn is_interactive(&self, state: &State) -> bool {
        self.mode == Mode::ReadWrite && (self.show)(state)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub(crate) enum Mode {
    ReadOnly,
    ReadWrite,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub(crate) enum Focus {
    Focused,
    NotFocused,
}
