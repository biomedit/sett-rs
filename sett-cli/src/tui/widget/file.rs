use std::{
    ffi::OsString,
    path::{Path, PathBuf},
};

use crossterm::event::{KeyCode, KeyEvent};
use indicatif::HumanBytes;
use ratatui::{
    layout::{Alignment, Constraint, Layout, Rect},
    style::{Color, Modifier, Style, Stylize as _},
    text::Line,
    widgets::{Block, Paragraph, Row, Table, TableState},
    Frame,
};

use crate::tui::event::EventHandlingStatus;

#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub(crate) struct FileSelector {
    current_dir: PathBuf,
    content: Vec<FileInfo>,
    state: TableState,
    show: bool,
    show_hidden_files: bool,
    filter_extension: Option<Vec<u8>>,
    file_type: FileType,
    multi_selection: bool,
    error: Option<String>,
}

#[derive(Debug, Clone)]
struct FileInfo {
    name: OsString,
    metadata: std::fs::Metadata,
}

impl PartialEq for FileInfo {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Eq for FileInfo {}

impl Ord for FileInfo {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        if self.metadata.is_dir() == other.metadata.is_dir() {
            self.name.cmp(&other.name)
        } else {
            other.metadata.is_dir().cmp(&self.metadata.is_dir())
        }
    }
}

impl PartialOrd for FileInfo {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Debug, Default, Copy, Clone, PartialEq, Eq)]
pub(crate) enum FileType {
    #[default]
    Any,
    File,
    Directory,
}

impl FileType {
    fn key_bindings(&self) -> &'static str {
        match self {
            FileType::Any => {
                " <j/k/down/up> move <enter> open folder/select file <space> select file/folder \
                <esc/q> close <H> show/hide hidden files "
            }
            FileType::File => {
                " <j/k/down/up> move <enter> open folder/select file <space> select file \
                <esc/q> close <H> show/hide hidden files "
            }
            FileType::Directory => {
                " <j/k/down/up> move <enter> open folder <space> select folder \
                <esc/q> close <H> show/hide hidden files "
            }
        }
    }
}

impl FileSelector {
    pub(crate) fn new() -> Self {
        Default::default()
    }

    pub(crate) fn with_file_type(mut self, file_type: FileType) -> Self {
        self.file_type = file_type;
        self
    }

    pub(crate) fn with_extension_filter(mut self, extension: Option<Vec<u8>>) -> Self {
        self.filter_extension = extension;
        self
    }

    pub(crate) fn with_multi_selection(mut self, multi_selection: bool) -> Self {
        self.multi_selection = multi_selection;
        self
    }

    fn load(&mut self) {
        if self.current_dir.as_os_str().is_empty() {
            match std::env::current_dir().or_else(|err| dirs::home_dir().ok_or(err)) {
                Ok(current_dir) => self.current_dir = current_dir,
                Err(error) => {
                    self.error = Some(format!(
                    "Unable to get the current working directory ({error}) and the home directory"
                ));
                    return;
                }
            }
        }
        let mut paths = match std::fs::read_dir(&self.current_dir) {
            Ok(paths) => {
                self.error = None;
                paths
                    .filter_map(|entry| {
                        entry.ok().and_then(|entry| {
                            let metadata = entry.metadata().ok()?;
                            if self.file_type == FileType::Directory && !metadata.is_dir() {
                                None
                            } else if let Some(ext) = self.filter_extension.as_ref() {
                                let name = entry.file_name();
                                if metadata.is_dir() {
                                    Some(FileInfo { name, metadata })
                                } else {
                                    name.as_encoded_bytes()
                                        .ends_with(ext)
                                        .then_some(FileInfo { name, metadata })
                                }
                            } else {
                                Some(FileInfo {
                                    name: entry.file_name(),
                                    metadata,
                                })
                            }
                        })
                    })
                    // TODO: handle Windows-type hidden files
                    .filter(|f| {
                        self.show_hidden_files || !f.name.as_encoded_bytes().starts_with(b".")
                    })
                    .chain(self.current_dir.metadata().map(|metadata| FileInfo {
                        name: ".".into(),
                        metadata,
                    }))
                    .chain(parent_file_info(&self.current_dir))
                    .collect::<Vec<_>>()
            }
            Err(e) => {
                self.error = Some(format!("Unable to read directory content: {}", e));
                parent_file_info(&self.current_dir).into_iter().collect()
            }
        };
        paths.sort_unstable();
        self.content = paths;
        self.state.select(Some(0));
    }

    pub(crate) fn open(&mut self) {
        self.load();
        self.show = true;
    }

    pub(crate) fn is_open(&self) -> bool {
        self.show
    }

    pub(crate) fn close(&mut self) {
        self.show = false
    }

    pub(crate) fn handle_key(
        &mut self,
        key: KeyEvent,
        mut on_selected: impl FnMut(PathBuf),
    ) -> EventHandlingStatus {
        match key.code {
            KeyCode::Char('j') | KeyCode::Down => {
                self.state.select_next();
            }
            KeyCode::Char('k') | KeyCode::Up => {
                self.state.select_previous();
            }
            KeyCode::Enter => {
                if let Some(i) = self.state.selected() {
                    let selected = &self.content[i];
                    match selected.name.to_str() {
                        Some("..") => {
                            if self.current_dir.pop() {
                                self.load();
                            }
                        }
                        Some(".") => {}
                        _ => {
                            let selected = self.current_dir.join(&selected.name);
                            if selected.is_dir() {
                                self.current_dir = selected;
                                self.load();
                            } else if selected.is_file() {
                                on_selected(selected);
                                if !self.multi_selection {
                                    self.close();
                                }
                            }
                        }
                    }
                }
            }
            KeyCode::Char(' ') => {
                let Some(selected) = self.state.selected().map(|i| &self.content[i]) else {
                    return EventHandlingStatus::Ignored;
                };
                let mut skip_close = false;
                match (self.file_type, selected.name.to_str()) {
                    (FileType::Directory, Some("..")) => {
                        on_selected(
                            self.current_dir
                                .parent()
                                .map_or_else(|| self.current_dir.clone(), |p| p.to_path_buf()),
                        );
                    }
                    (FileType::Directory, Some(".")) => {
                        on_selected(self.current_dir.clone());
                    }
                    (FileType::Directory, _) => {
                        let path = self.current_dir.join(&selected.name);
                        if path.is_dir() {
                            on_selected(path);
                        } else {
                            skip_close = true;
                        }
                    }
                    (FileType::File, _) => {
                        if selected.metadata.is_file() {
                            on_selected(self.current_dir.join(&selected.name));
                        } else {
                            skip_close = true;
                        }
                    }
                    (FileType::Any, _) => {
                        on_selected(self.current_dir.join(&selected.name));
                    }
                };
                if !self.multi_selection && !skip_close {
                    self.close()
                }
            }
            KeyCode::Char('H') => {
                self.show_hidden_files = !self.show_hidden_files;
                self.load();
            }
            KeyCode::Esc | KeyCode::Char('q') => {
                self.close();
            }
            _ => {
                return EventHandlingStatus::Ignored;
            }
        }
        EventHandlingStatus::Handled
    }

    pub(crate) fn render(&mut self, frame: &mut Frame, area: Rect) {
        let [error_area, main_area] =
            Layout::vertical([Constraint::Length(1), Constraint::Min(10)]).areas(area);
        let list = Table::new(
            self.content.iter().map(|f| {
                Row::new([
                    Line::from(f.name.to_string_lossy()).style(if f.metadata.is_dir() {
                        Style::default().add_modifier(Modifier::BOLD)
                    } else {
                        Default::default()
                    }),
                    f.metadata
                        .modified()
                        .map(|t| {
                            Line::from(format!(
                                "{}",
                                chrono::DateTime::<chrono::offset::Local>::from(t)
                                    .format("%Y-%m-%d %H:%M:%S")
                            ))
                        })
                        .unwrap_or_default(),
                    if f.metadata.is_dir() {
                        Line::default()
                    } else {
                        Line::from(HumanBytes(f.metadata.len()).to_string())
                            .alignment(Alignment::Right)
                    },
                ])
            }),
            [
                Constraint::Min(30),
                Constraint::Length(25),
                Constraint::Length(10),
            ],
        )
        .header(
            Row::new(["Name", "Last modified", "Size"])
                .style(Style::default().add_modifier(Modifier::BOLD)),
        )
        .row_highlight_style(Style::new().reversed())
        .block(
            Block::bordered()
                .title(self.current_dir.to_string_lossy())
                .title_bottom(Line::from(self.file_type.key_bindings()).centered()),
        );
        if let Some(error) = &self.error {
            frame.render_widget(
                Paragraph::new(error.as_str()).style(Style::default().fg(Color::Red)),
                error_area,
            );
        }
        frame.render_stateful_widget(list, main_area, &mut self.state);
    }
}

fn parent_file_info(path: &Path) -> Option<FileInfo> {
    path.parent().and_then(|parent| {
        parent
            .metadata()
            .map(|metadata| FileInfo {
                name: "..".into(),
                metadata,
            })
            .ok()
    })
}
