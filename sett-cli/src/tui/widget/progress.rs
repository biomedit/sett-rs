use indicatif::HumanBytes;
use ratatui::{
    layout::Rect,
    style::{Color, Style, Stylize as _},
    text::Span,
    widgets::LineGauge,
    Frame,
};

#[derive(Debug, Clone, Copy, Default)]
pub(crate) struct Progress {
    pub(crate) current: u64,
    pub(crate) total: u64,
}

impl Progress {
    pub(crate) fn render(&self, frame: &mut Frame, area: Rect) {
        let percent = (100 * self.current).checked_div(self.total).unwrap_or(0) as f64;
        let label = Span::styled(
            format!(
                "{}% {}/{}",
                percent,
                HumanBytes(self.current),
                HumanBytes(self.total)
            ),
            Style::new().bold(),
        );
        let gauge = LineGauge::default()
            .ratio(percent / 100.0)
            .label(label)
            .filled_style(Style::new().fg(Color::Green))
            .unfilled_style(Style::new().fg(Color::Gray))
            .line_set(ratatui::symbols::line::THICK);
        frame.render_widget(gauge, area);
    }
}
