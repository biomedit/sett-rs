use std::path::{Path, PathBuf};

use crossterm::event::{KeyCode, KeyEvent, KeyModifiers};
use ratatui::{
    layout::Rect,
    style::{Color, Style},
    text::{Line, Masked, Span, Text},
    widgets::Clear,
    Frame,
};

use crate::tui::event::EventHandlingStatus;

use super::file::FileSelector;

pub(crate) trait Field: std::fmt::Debug {
    fn label(&self) -> &str;

    fn clear(&mut self);

    fn handle_key(&mut self, key: KeyEvent) -> EventHandlingStatus;

    fn render_label(&self, frame: &mut Frame, rect: Rect) {
        frame.render_widget(
            Line::from_iter([
                Span::raw(self.label()),
                Span::styled(
                    if let Required::Yes = self.required() {
                        "*"
                    } else {
                        ""
                    },
                    Style::default().fg(Color::LightRed),
                ),
            ]),
            rect,
        );
    }

    fn render_value(&self, frame: &mut Frame, rect: Rect, focused: bool);

    fn render_error(&self, frame: &mut Frame, rect: Rect) {
        if let Err(msg) = self.validate() {
            match msg {
                ValidationError::Required => {}
                ValidationError::Invalid(msg) => {
                    frame.render_widget(
                        Line::from(Span::styled(msg, Style::default().fg(Color::LightRed))),
                        rect,
                    );
                }
            }
        }
    }

    fn render_popup(&mut self, _: &mut Frame, _: Rect) {}

    fn validate(&self) -> Result<(), ValidationError>;

    fn required(&self) -> Required;
}

type TextInputValidator = fn(&str) -> Result<(), ValidationError>;

pub(crate) enum ValidationError<'a> {
    Required,
    Invalid(std::borrow::Cow<'a, str>),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub(crate) enum DisplayMode {
    Normal,
    Masked,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub(crate) enum Required {
    Yes,
    No,
}

#[derive(Clone, PartialEq, Eq)]
pub(crate) struct TextField {
    label: &'static str,
    required: Required,
    value: String,
    cursor_position: usize,
    validator: TextInputValidator,
    mode: DisplayMode,
    show_masked_value: bool,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) struct PositiveIntegerField {
    label: &'static str,
    required: Required,
    value: String,
    cursor_position: usize,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) struct PathField {
    label: &'static str,
    required: Required,
    value: (PathBuf, String),
    file_selector: FileSelector,
}

impl TextField {
    pub(crate) fn new(
        label: &'static str,
        required: Required,
        mode: DisplayMode,
        validator: Option<TextInputValidator>,
    ) -> Self {
        Self {
            label,
            required,
            mode,
            validator: validator.unwrap_or(|_| Ok(())),
            value: String::new(),
            cursor_position: 0,
            show_masked_value: false,
        }
    }

    pub(crate) fn value(&self) -> &str {
        self.value.as_str()
    }
}

impl PositiveIntegerField {
    pub(crate) fn new(label: &'static str, required: Required) -> Self {
        Self {
            label,
            required,
            value: String::new(),
            cursor_position: 0,
        }
    }

    pub(crate) fn value(&self) -> Option<u32> {
        self.value.parse().ok()
    }
}

impl PathField {
    pub(crate) fn new(
        label: &'static str,
        required: Required,
        file_selector: FileSelector,
    ) -> Self {
        Self {
            label,
            required,
            value: (PathBuf::new(), String::new()),
            file_selector,
        }
    }

    pub(crate) fn value(&self) -> &Path {
        &self.value.0
    }
}

impl Field for TextField {
    fn label(&self) -> &str {
        self.label
    }

    fn clear(&mut self) {
        self.value.clear();
        self.cursor_position = 0;
    }

    fn render_value(&self, frame: &mut Frame, rect: Rect, focused: bool) {
        frame.render_widget(
            if self.mode == DisplayMode::Masked && !self.show_masked_value {
                Masked::new(self.value.as_str(), '*').into()
            } else {
                Text::raw(self.value.as_str())
            },
            rect,
        );
        if focused {
            frame.set_cursor_position(ratatui::layout::Position::new(
                rect.x + self.cursor_position as u16,
                rect.y,
            ));
        }
    }

    fn handle_key(&mut self, key: KeyEvent) -> EventHandlingStatus {
        match key.code {
            KeyCode::Char('h') if key.modifiers == KeyModifiers::CONTROL => {
                self.show_masked_value = !self.show_masked_value;
                EventHandlingStatus::Handled
            }
            KeyCode::Char(c) => {
                let position = self
                    .value
                    .char_indices()
                    .nth(self.cursor_position.saturating_sub(1))
                    .map_or(0, |(i, c)| i + c.len_utf8());
                self.value.insert(position, c);
                self.cursor_position += 1;
                debug_assert!(self.cursor_position <= self.value.chars().count());
                EventHandlingStatus::Handled
            }
            KeyCode::Backspace => {
                if self.cursor_position > 0 {
                    self.cursor_position = (self.cursor_position).saturating_sub(1);
                    let (position, _) = self
                        .value
                        .char_indices()
                        .nth(self.cursor_position)
                        .expect("cursor position is smaller or equal to char count");
                    self.value.remove(position);
                }
                EventHandlingStatus::Handled
            }
            KeyCode::Left => {
                self.cursor_position = (self.cursor_position).saturating_sub(1);
                EventHandlingStatus::Handled
            }
            KeyCode::Right => {
                self.cursor_position = (self.cursor_position + 1).min(self.value.chars().count());
                EventHandlingStatus::Handled
            }
            _ => EventHandlingStatus::Ignored,
        }
    }

    fn validate(&self) -> Result<(), ValidationError> {
        if self.required == Required::Yes && self.value.is_empty() {
            return Err(ValidationError::Required);
        }
        (self.validator)(&self.value)
    }

    fn required(&self) -> Required {
        self.required
    }
}

impl std::fmt::Debug for TextField {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("TextInputField")
            .field("label", &self.label)
            .field(
                "value",
                if let DisplayMode::Masked = self.mode {
                    &"<masked>"
                } else {
                    &self.value
                },
            )
            .field("cursor_position", &self.cursor_position)
            .field("secret", &self.show_masked_value)
            .field("masked", &self.show_masked_value)
            .finish()
    }
}

impl Field for PositiveIntegerField {
    fn label(&self) -> &str {
        self.label
    }

    fn clear(&mut self) {
        self.value.clear();
        self.cursor_position = 0;
    }

    fn handle_key(&mut self, key: KeyEvent) -> EventHandlingStatus {
        match key.code {
            KeyCode::Char(c) if c.is_ascii_digit() => {
                self.value.insert(self.cursor_position, c);
                self.cursor_position += 1;
                EventHandlingStatus::Handled
            }
            KeyCode::Backspace => {
                if self.cursor_position > 0 {
                    self.cursor_position = (self.cursor_position).saturating_sub(1);
                    self.value.remove(self.cursor_position);
                }
                EventHandlingStatus::Handled
            }
            KeyCode::Left => {
                self.cursor_position = (self.cursor_position).saturating_sub(1);
                EventHandlingStatus::Handled
            }
            KeyCode::Right => {
                self.cursor_position = (self.cursor_position + 1).min(self.value.len());
                EventHandlingStatus::Handled
            }
            _ => EventHandlingStatus::Ignored,
        }
    }

    fn render_value(&self, frame: &mut Frame, rect: Rect, focused: bool) {
        frame.render_widget(Text::raw(self.value.as_str()), rect);
        if focused {
            frame.set_cursor_position(ratatui::layout::Position::new(
                rect.x + self.cursor_position as u16,
                rect.y,
            ));
        }
    }

    fn validate(&self) -> Result<(), ValidationError> {
        match (self.required, self.value.is_empty()) {
            (Required::Yes, true) => Err(ValidationError::Required),
            (Required::No, true) => Ok(()),
            (_, false) => self
                .value
                .parse::<u32>()
                .map(|_| ())
                .map_err(|e| ValidationError::Invalid(format!("Invalid number: {e}").into())),
        }
    }

    fn required(&self) -> Required {
        self.required
    }
}

impl Field for PathField {
    fn label(&self) -> &str {
        self.label
    }

    fn clear(&mut self) {
        self.value.0.clear();
        self.value.1.clear();
    }

    fn render_value(&self, frame: &mut Frame, rect: Rect, focused: bool) {
        frame.render_widget(Text::raw(self.value.1.as_str()), rect);
        if focused {
            let mut rect = rect;
            rect.x += self.value.1.len() as u16;
            frame.render_widget(
                Text::raw(if self.value.1.is_empty() {
                    "(press <space> to select path)"
                } else {
                    "  (press <space> to change path)"
                }),
                rect,
            );
        }
    }

    fn render_error(&self, _: &mut Frame, _: Rect) {}

    fn render_popup(&mut self, frame: &mut Frame, rect: Rect) {
        if self.file_selector.is_open() {
            frame.render_widget(Clear, rect);
            self.file_selector.render(frame, rect);
        }
    }

    fn handle_key(&mut self, key: KeyEvent) -> EventHandlingStatus {
        match key.code {
            KeyCode::Char(' ') if !self.file_selector.is_open() => {
                self.file_selector.open();
                EventHandlingStatus::Handled
            }
            _ => {
                if self.file_selector.is_open() {
                    self.file_selector.handle_key(key, |selected| {
                        let s = selected.to_string_lossy().to_string();
                        self.value = (selected, s);
                    })
                } else {
                    EventHandlingStatus::Ignored
                }
            }
        }
    }

    fn validate(&self) -> Result<(), ValidationError> {
        if self.required == Required::Yes && self.value.0.as_os_str().is_empty() {
            return Err(ValidationError::Required);
        }
        Ok(())
    }

    fn required(&self) -> Required {
        self.required
    }
}

pub(crate) fn url_validator(s: &str) -> Result<(), ValidationError> {
    if s.starts_with("http://") || s.starts_with("https://") {
        Ok(())
    } else {
        Err(ValidationError::Invalid(
            "Invalid URL: must start with http:// or https://".into(),
        ))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn validate_positive_integer_field() {
        assert!(PositiveIntegerField {
            label: "int",
            required: Required::No,
            value: String::from(""),
            cursor_position: 0
        }
        .validate()
        .is_ok());

        assert!(PositiveIntegerField {
            label: "int",
            required: Required::Yes,
            value: String::from(""),
            cursor_position: 1
        }
        .validate()
        .is_err());

        assert!(PositiveIntegerField {
            label: "int",
            required: Required::Yes,
            value: String::from("42"),
            cursor_position: 2
        }
        .validate()
        .is_ok());

        assert!(PositiveIntegerField {
            label: "int",
            required: Required::No,
            value: String::from("42"),
            cursor_position: 2
        }
        .validate()
        .is_ok());

        assert!(PositiveIntegerField {
            label: "int",
            required: Required::Yes,
            value: String::from("aa"),
            cursor_position: 2
        }
        .validate()
        .is_err());

        assert!(PositiveIntegerField {
            label: "int",
            required: Required::No,
            value: String::from("aa"),
            cursor_position: 2
        }
        .validate()
        .is_err());

        assert!(PositiveIntegerField {
            label: "int",
            required: Required::No,
            value: format!("{}", u32::MAX as u64 + 1),
            cursor_position: 2
        }
        .validate()
        .is_err());
    }

    #[test]
    fn text_field_handle_unicode() {
        let mut field = TextField::new("text", Required::No, DisplayMode::Normal, None);
        field.handle_key(KeyCode::Char('ą').into());
        assert_eq!(field.value, "ą");
        assert_eq!(field.cursor_position, 1);
        field.handle_key(KeyCode::Right.into());
        field.handle_key(KeyCode::Left.into());
        assert_eq!(field.cursor_position, 0);
        field.handle_key(KeyCode::Backspace.into());
        assert_eq!(field.value, "ą");
        field.handle_key(KeyCode::Right.into());
        field.handle_key(KeyCode::Backspace.into());
        assert!(field.value.is_empty());
        assert_eq!(field.cursor_position, 0);
    }
}
