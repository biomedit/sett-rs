use std::path::PathBuf;

use crossterm::event::{KeyCode, KeyEvent};
use ratatui::{
    layout::{Constraint, Layout, Rect},
    text::Line,
    widgets::{Block, Paragraph, Tabs},
    Frame,
};

use crate::tui::{
    event::EventHandlingStatus,
    widget::{
        field::{url_validator, DisplayMode, Field, PathField, Required, TextField},
        file::{FileSelector, FileType},
        form::Form,
    },
};

#[derive(Debug)]
pub(crate) struct DestinationSelector {
    choices: [Box<dyn Destination>; 3],
    selected: usize,
    show: bool,
    subset: DestinationSubset,
    #[cfg(test)]
    pub(crate) clear_count: usize,
}

#[derive(Debug)]
pub(crate) enum DestinationSubset {
    All,
    Remote,
}

impl Default for DestinationSelector {
    fn default() -> Self {
        Self {
            choices: [
                Box::new(Local::new()),
                Box::new(S3::new()),
                Box::new(Sftp::new()),
            ],
            subset: DestinationSubset::All,
            selected: 0,
            show: false,
            #[cfg(test)]
            clear_count: 0,
        }
    }
}

impl DestinationSelector {
    #[cfg(feature = "auth")]
    pub(crate) fn new_with_auth_mode() -> Self {
        Self {
            choices: [
                Box::new(Local::new()),
                Box::new(S3AuthMode::default()),
                Box::new(Sftp::new()),
            ],
            selected: 1,
            ..Default::default()
        }
    }

    pub(crate) fn selected(&self) -> &dyn Destination {
        self.choices[self.selected].as_ref()
    }

    pub(crate) fn builder(&self) -> DestinationBuilder {
        self.choices[self.selected].builder()
    }

    pub(crate) fn open(&mut self) {
        self.show = true;
    }

    pub(crate) fn is_open(&self) -> bool {
        self.show
    }

    pub(crate) fn close(&mut self) {
        self.show = false
    }

    pub(crate) fn change_subset(&mut self, subset: DestinationSubset) {
        self.subset = subset;
        if self.selected == 0 {
            self.selected = 1;
        }
    }

    pub(crate) fn clear(&mut self) {
        for choice in &mut self.choices {
            choice.clear();
        }
        #[cfg(test)]
        {
            self.clear_count += 1;
        }
    }

    pub(crate) fn render(&mut self, frame: &mut Frame, rect: Rect) {
        let [tabs_area, body_area] = Layout::vertical([Constraint::Length(3), Constraint::Fill(1)])
            .margin(1)
            .areas(rect);
        let (choices, selected) = match self.subset {
            DestinationSubset::All => (&self.choices[..], self.selected),
            DestinationSubset::Remote => (&self.choices[1..], self.selected.saturating_sub(1)),
        };
        let tabs = choices
            .iter()
            .enumerate()
            .map(|(i, choice)| format!("[{}] {}", i + 1, choice.label()))
            .collect::<Tabs>()
            .select(selected)
            .block(Block::bordered());
        frame.render_widget(tabs, tabs_area);
        let block = Block::bordered().title_bottom(
            Line::from(
                " <tab> change field <q> close <c-h> show/hide secret value <c-x> clear form ",
            )
            .centered(),
        );
        let inner_area = block.inner(body_area);
        let form = &mut self.choices[self.selected];
        if let Some(description) = form.description() {
            let [description_area, form_area] =
                Layout::vertical([Constraint::Length(2), Constraint::Fill(1)]).areas(inner_area);
            frame.render_widget(Paragraph::new(description), description_area);
            form.render_form(frame, form_area);
        } else {
            form.render_form(frame, inner_area);
        }
        frame.render_widget(block, body_area);
    }

    pub(crate) fn handle_key(&mut self, key: KeyEvent) -> EventHandlingStatus {
        let selected = &mut self.choices[self.selected];
        if selected.handle_key(key) == EventHandlingStatus::Handled {
            return EventHandlingStatus::Handled;
        }
        match key.code {
            // The pattern intentionally matches all digits.
            // This prevents accidental switching of the main app tabs
            // when the user presses a digit > self.choices.len().
            KeyCode::Char(n) if n.is_ascii_digit() => {
                let n = n.to_digit(10).expect("valid digit") as usize;
                self.selected = n.min(3);
                if let DestinationSubset::All = self.subset {
                    self.selected = self.selected.saturating_sub(1);
                }
                EventHandlingStatus::Handled
            }
            KeyCode::Esc | KeyCode::Char('q') => {
                self.close();
                EventHandlingStatus::Handled
            }
            KeyCode::Enter => {
                if selected.is_valid() {
                    self.close();
                }
                EventHandlingStatus::Handled
            }
            _ => EventHandlingStatus::Ignored,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub(crate) enum DestinationKind {
    Local,
    Remote,
}

#[derive(Debug)]
pub(crate) enum DestinationBuilder {
    Local(PathBuf),
    S3 {
        client_builder: sett::remote::s3::ClientBuilder,
        bucket: String,
    },
    Sftp {
        client_builder: sett::remote::sftp::ClientBuilder,
        base_path: PathBuf,
    },
}

impl DestinationBuilder {
    pub(crate) async fn build(self) -> Result<sett::destination::Destination, anyhow::Error> {
        match self {
            Self::Local(path) => Ok(sett::destination::Local::new(path, None::<String>)?.into()),
            Self::S3 {
                client_builder,
                bucket,
            } => Ok(sett::destination::Destination::S3(
                sett::destination::S3::new(client_builder.build().await?, bucket),
            )),
            Self::Sftp {
                client_builder,
                base_path,
            } => Ok(sett::destination::Sftp::new(client_builder.build(), base_path).into()),
        }
    }
}

pub(crate) trait Destination: Form {
    fn kind(&self) -> DestinationKind;
    fn builder(&self) -> DestinationBuilder;
}

#[derive(Debug)]
pub(crate) struct Local {
    selected_field: Option<usize>,
    destination_dir: PathField,
}

impl Local {
    fn new() -> Self {
        Self {
            selected_field: Some(0),
            destination_dir: PathField::new(
                "Destination directory",
                Required::Yes,
                FileSelector::new().with_file_type(FileType::Directory),
            ),
        }
    }
}

impl Destination for Local {
    fn kind(&self) -> DestinationKind {
        DestinationKind::Local
    }

    fn builder(&self) -> DestinationBuilder {
        DestinationBuilder::Local(self.destination_dir.value().to_path_buf())
    }
}

impl Form for Local {
    fn label(&self) -> &'static str {
        "Local"
    }

    fn field_count(&self) -> usize {
        1
    }

    fn selected_field(&mut self) -> &mut Option<usize> {
        &mut self.selected_field
    }

    fn fields(&self) -> Box<(dyn Iterator<Item = &dyn Field> + '_)> {
        Box::new(std::iter::once(&self.destination_dir as &dyn Field))
    }

    fn fields_mut(&mut self) -> Box<(dyn Iterator<Item = &mut dyn Field> + '_)> {
        Box::new(std::iter::once(&mut self.destination_dir as &mut dyn Field))
    }
}

#[derive(Debug)]
pub(crate) struct S3 {
    selected_field: Option<usize>,
    endpoint: TextField,
    bucket: TextField,
    access_key: TextField,
    secret_key: TextField,
    session_token: TextField,
}

impl S3 {
    const FIELD_COUNT: usize = 5;

    fn new() -> Self {
        Self {
            selected_field: None,
            endpoint: TextField::new(
                "Endpoint",
                Required::Yes,
                DisplayMode::Normal,
                Some(url_validator),
            ),
            bucket: TextField::new("Bucket", Required::Yes, DisplayMode::Normal, None),
            access_key: TextField::new("Access key", Required::Yes, DisplayMode::Normal, None),
            secret_key: TextField::new("Secret key", Required::Yes, DisplayMode::Masked, None),
            session_token: TextField::new("Session token", Required::No, DisplayMode::Masked, None),
        }
    }
}

impl Destination for S3 {
    fn kind(&self) -> DestinationKind {
        DestinationKind::Remote
    }

    fn builder(&self) -> DestinationBuilder {
        DestinationBuilder::S3 {
            client_builder: sett::remote::s3::ClientBuilder::default()
                .endpoint(Some(self.endpoint.value()))
                .access_key(empty_to_none(self.access_key.value()))
                .secret_key(empty_to_none(self.secret_key.value()))
                .session_token(empty_to_none(self.session_token.value())),
            bucket: self.bucket.value().to_string(),
        }
    }
}

impl Form for S3 {
    fn label(&self) -> &'static str {
        "S3"
    }

    fn field_count(&self) -> usize {
        Self::FIELD_COUNT
    }

    fn selected_field(&mut self) -> &mut Option<usize> {
        &mut self.selected_field
    }

    fn fields(&self) -> Box<(dyn Iterator<Item = &dyn Field> + '_)> {
        Box::new(
            ([
                &self.endpoint,
                &self.bucket,
                &self.access_key,
                &self.secret_key,
                &self.session_token,
            ] as [&dyn Field; Self::FIELD_COUNT])
                .into_iter(),
        )
    }

    fn fields_mut(&mut self) -> Box<(dyn Iterator<Item = &mut dyn Field> + '_)> {
        Box::new(
            ([
                &mut self.endpoint,
                &mut self.bucket,
                &mut self.access_key,
                &mut self.secret_key,
                &mut self.session_token,
            ] as [&mut dyn Field; Self::FIELD_COUNT])
                .into_iter(),
        )
    }
}

#[cfg(feature = "auth")]
#[derive(Debug, Default)]
pub(crate) struct S3AuthMode {
    selected_field: Option<usize>,
}

#[cfg(feature = "auth")]
impl S3AuthMode {
    const DESCRIPTION: &str =
        "Authenticated mode: connection details and credentials are automatically retrieved.";
}

#[cfg(feature = "auth")]
impl Destination for S3AuthMode {
    fn kind(&self) -> DestinationKind {
        DestinationKind::Remote
    }

    fn builder(&self) -> DestinationBuilder {
        DestinationBuilder::S3 {
            client_builder: Default::default(),
            bucket: Default::default(),
        }
    }
}

#[cfg(feature = "auth")]
impl Form for S3AuthMode {
    fn label(&self) -> &'static str {
        "S3"
    }

    fn description(&self) -> Option<&'static str> {
        Some(Self::DESCRIPTION)
    }

    fn field_count(&self) -> usize {
        0
    }

    fn selected_field(&mut self) -> &mut Option<usize> {
        &mut self.selected_field
    }

    fn fields(&self) -> Box<(dyn Iterator<Item = &dyn Field> + '_)> {
        Box::new(std::iter::empty())
    }

    fn fields_mut(&mut self) -> Box<(dyn Iterator<Item = &mut dyn Field> + '_)> {
        Box::new(std::iter::empty())
    }

    fn render_summary(&self, frame: &mut Frame, rect: Rect) {
        frame.render_widget(Paragraph::new(Self::DESCRIPTION), rect);
    }
}

#[derive(Debug)]
pub(crate) struct Sftp {
    selected_field: Option<usize>,
    host: TextField,
    username: TextField,
    base_path: TextField,
    key_path: PathField,
    key_password: TextField,
}

impl Sftp {
    const FIELD_COUNT: usize = 5;

    fn new() -> Self {
        Self {
            selected_field: None,
            host: TextField::new("Host", Required::Yes, DisplayMode::Normal, None),
            username: TextField::new("Username", Required::Yes, DisplayMode::Normal, None),
            base_path: TextField::new(
                "Destination directory",
                Required::Yes,
                DisplayMode::Normal,
                None,
            ),
            key_path: PathField::new("SSH key location", Required::No, FileSelector::new()),
            key_password: TextField::new(
                "SSH key password",
                Required::No,
                DisplayMode::Masked,
                None,
            ),
        }
    }
}

impl Destination for Sftp {
    fn kind(&self) -> DestinationKind {
        DestinationKind::Remote
    }

    fn builder(&self) -> DestinationBuilder {
        let mut host = self.host.value().split(':');
        DestinationBuilder::Sftp {
            client_builder: sett::remote::sftp::ClientBuilder::default()
                .host(host.next().expect("at least one element"))
                .port(host.next().and_then(|s| s.parse().ok()).unwrap_or(22))
                .username(self.username.value())
                .key_path(
                    (!self.key_path.value().as_os_str().is_empty())
                        .then_some(self.key_path.value()),
                )
                .key_password(empty_to_none(self.key_password.value())),
            base_path: PathBuf::from(self.base_path.value()),
        }
    }
}

impl Form for Sftp {
    fn label(&self) -> &'static str {
        "Sftp"
    }

    fn field_count(&self) -> usize {
        Self::FIELD_COUNT
    }

    fn selected_field(&mut self) -> &mut Option<usize> {
        &mut self.selected_field
    }

    fn fields(&self) -> Box<(dyn Iterator<Item = &dyn Field> + '_)> {
        Box::new(
            ([
                &self.host,
                &self.username,
                &self.base_path,
                &self.key_path,
                &self.key_password,
            ] as [&dyn Field; Self::FIELD_COUNT])
                .into_iter(),
        )
    }

    fn fields_mut(&mut self) -> Box<(dyn Iterator<Item = &mut dyn Field> + '_)> {
        Box::new(
            ([
                &mut self.host,
                &mut self.username,
                &mut self.base_path,
                &mut self.key_path,
                &mut self.key_password,
            ] as [&mut dyn Field; Self::FIELD_COUNT])
                .into_iter(),
        )
    }
}

fn empty_to_none(s: &str) -> Option<&str> {
    (!s.is_empty()).then_some(s)
}
