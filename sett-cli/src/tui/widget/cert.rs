use crossterm::event::{KeyCode, KeyEvent};
use ratatui::{
    layout::{Constraint, Layout, Rect},
    style::{Modifier, Style},
    text::Line,
    widgets::{Block, Row, Table, TableState},
    Frame,
};
use sett::openpgp::cert::{Cert, CertType};

use crate::tui::event::EventHandlingStatus;

#[derive(Debug, Clone, PartialEq)]
pub(crate) struct CertInfo {
    pub(crate) cert_type: CertType,
    pub(crate) fingerprint: String,
    pub(crate) cert: Cert,
    pub(crate) userid: Option<String>,
}

pub(crate) fn handle_key(state: &mut TableState, key: KeyEvent) -> EventHandlingStatus {
    match key.code {
        KeyCode::Char('j') | KeyCode::Down => {
            state.select_next();
            EventHandlingStatus::Handled
        }
        KeyCode::Char('k') | KeyCode::Up => {
            state.select_previous();
            EventHandlingStatus::Handled
        }
        _ => EventHandlingStatus::Ignored,
    }
}

const PRIVATE_LABEL: &str = "Private + Public";
const PUBLIC_LABEL: &str = "Public";

trait AsLabel {
    fn label(&self) -> &'static str;
}

impl AsLabel for CertType {
    fn label(&self) -> &'static str {
        match self {
            CertType::Secret => PRIVATE_LABEL,
            CertType::Public => PUBLIC_LABEL,
        }
    }
}

pub(crate) fn render_table<'a, T>(state: &mut TableState, certs: T, frame: &mut Frame, rect: Rect)
where
    T: IntoIterator<Item = &'a CertInfo>,
{
    let table = Table::new(
        certs.into_iter().map(|cert| {
            Row::new([
                cert.userid.as_deref().unwrap_or_default(),
                cert.fingerprint.as_str(),
                cert.cert_type.label(),
            ])
        }),
        [
            Constraint::Min(40),
            Constraint::Min(PRIVATE_LABEL.len() as u16),
            Constraint::Min(20),
        ],
    )
    .header(
        Row::new(["User ID", "Fingerprint", "Type"])
            .style(Style::default().add_modifier(Modifier::BOLD)),
    )
    .row_highlight_style(Style::default().add_modifier(Modifier::REVERSED));
    frame.render_stateful_widget(table, rect, state);
}

pub(crate) fn render_details(cert: &CertInfo, frame: &mut Frame, rect: Rect) {
    let details = Table::new(
        [
            Row::new(["User ID", cert.userid.as_deref().unwrap_or_default()]),
            Row::new(["Fingerprint", cert.fingerprint.as_str()]),
            Row::new(["Type", cert.cert_type.label()]),
        ],
        [Constraint::Length(12), Constraint::Min(40)],
    );
    frame.render_widget(details, rect);
}

#[derive(Debug, Default)]
pub(crate) struct CertSelectionDialog {
    state: TableState,
    show: bool,
}

impl CertSelectionDialog {
    pub(crate) fn open(&mut self) {
        self.show = true;
    }

    pub(crate) fn is_open(&self) -> bool {
        self.show
    }

    pub(crate) fn close(&mut self) {
        self.show = false
    }

    pub(crate) fn handle_key(&mut self, key: KeyEvent) -> (EventHandlingStatus, Option<usize>) {
        match key.code {
            KeyCode::Enter | KeyCode::Char(' ') => {
                if let Some(i) = self.state.selected() {
                    return (EventHandlingStatus::Handled, Some(i));
                }
                (EventHandlingStatus::Handled, None)
            }
            KeyCode::Esc | KeyCode::Char('q') => {
                self.close();
                (EventHandlingStatus::Handled, None)
            }
            _ => (handle_key(&mut self.state, key), None),
        }
    }

    pub(crate) fn render<'a, T>(&mut self, certs: T, frame: &mut Frame, rect: Rect)
    where
        T: IntoIterator<Item = &'a CertInfo>,
    {
        let [rect] = Layout::default()
            .constraints([Constraint::Min(0)])
            .margin(1)
            .areas(rect);
        let block = Block::bordered().title_bottom(
            Line::raw(" <j/k/down/up> move <space/enter> select <esc/q> close ").centered(),
        );
        render_table(&mut self.state, certs, frame, block.inner(rect));
        frame.render_widget(block, rect);
    }
}
