use crossterm::event::{KeyCode, KeyEvent, KeyModifiers};

use crate::tui::{
    event::{Event, EventHandlingStatus},
    tab,
};

#[derive(Debug, Default)]
pub(crate) struct State {
    mode: AppMode,
    selected_tab: usize,
    pub(crate) encrypt: tab::encrypt::Encrypt,
    pub(crate) decrypt: tab::decrypt::Decrypt,
    pub(crate) cert: tab::cert::CertSelector,
    #[cfg(feature = "auth")]
    pub(crate) auth_status: tab::user::AuthStatus,
    pub(crate) password_prompt: Option<PasswordPrompt>,
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
enum AppMode {
    #[default]
    Running,
    Quitting,
}

#[derive(Debug)]
pub(crate) struct PasswordPrompt {
    pub(crate) hint: sett::openpgp::crypto::PasswordHint,
    pub(crate) input: String,
    pub(crate) result: tokio::sync::oneshot::Sender<sett::secret::Secret>,
}

impl State {
    pub(crate) fn new() -> Self {
        Self {
            encrypt: tab::encrypt::Encrypt::new(),
            decrypt: tab::decrypt::Decrypt::new(),
            cert: tab::cert::CertSelector::new(),
            ..Default::default()
        }
    }

    pub(crate) fn current_tab(&self) -> &'static tab::Tab {
        tab::TABS[self.selected_tab]
    }

    pub(crate) fn current_tab_index(&self) -> usize {
        self.selected_tab
    }

    pub(crate) async fn send(
        &mut self,
        event: Event,
        sender: tokio::sync::mpsc::UnboundedSender<Event>,
    ) {
        match event {
            Event::Key(event) => {
                if let KeyCode::Char('c') | KeyCode::Char('C') = event.code {
                    if event.modifiers == KeyModifiers::CONTROL {
                        self.exit();
                    }
                }
                if self.password_prompt.is_some() {
                    self.handle_password_event(event);
                } else {
                    if let EventHandlingStatus::Handled =
                        (self.current_tab().handle_key)(self, event, sender)
                    {
                        return;
                    }
                    if let KeyCode::Esc = event.code {
                        self.exit();
                    }
                    self.switch_tab(event.code);
                }
            }
            Event::Action(action) => action.handle(self, sender).await,
            Event::Tick => {}
            Event::Mouse(_) => {}
            Event::Resize(_, _) => {}
        }
    }

    fn switch_tab(&mut self, code: KeyCode) -> EventHandlingStatus {
        const LAST_TAB_DIGIT: char =
            char::from_digit(tab::TABS.len() as u32 + 1, 10).expect("valid tab count");
        match code {
            KeyCode::Char(n) if n > '0' && n < LAST_TAB_DIGIT => {
                if let Some(n) = n.to_digit(10) {
                    self.selected_tab = n as usize - 1;
                    EventHandlingStatus::Handled
                } else {
                    EventHandlingStatus::Ignored
                }
            }
            _ => EventHandlingStatus::Ignored,
        }
    }

    pub(crate) fn running(&self) -> bool {
        self.mode == AppMode::Running
    }

    fn exit(&mut self) {
        self.mode = AppMode::Quitting;
    }

    fn handle_password_event(&mut self, event: KeyEvent) {
        if let Some(prompt) = &mut self.password_prompt {
            match event.code {
                KeyCode::Enter => {
                    let prompt = self.password_prompt.take().expect("some");
                    prompt
                        .result
                        .send(prompt.input.clone().into())
                        .expect("receiver is alive");
                    self.password_prompt = None;
                }
                KeyCode::Char(c) => {
                    prompt.input.push(c);
                }
                KeyCode::Backspace => {
                    prompt.input.pop();
                }
                _ => {}
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn switch_tab() {
        let mut state = State::default();
        #[cfg(feature = "auth")]
        const TABS: &[&str] = &["Encrypt/Send", "Decrypt", "Keys", "User", "About"];
        #[cfg(not(feature = "auth"))]
        const TABS: &[&str] = &["Encrypt/Send", "Decrypt", "Keys", "About"];
        for (i, label) in TABS.iter().enumerate() {
            assert_eq!(
                state.switch_tab(KeyCode::Char(char::from_digit(i as u32 + 1, 10).unwrap())),
                super::EventHandlingStatus::Handled
            );
            assert_eq!(state.current_tab().label, *label);
        }
        for n in std::iter::once('0').chain('6'..='9').chain('a'..='Z') {
            assert_eq!(
                state.switch_tab(KeyCode::Char(n)),
                super::EventHandlingStatus::Ignored
            );
        }
    }

    #[tokio::test]
    async fn exit() {
        let mut state = State::default();
        assert!(state.running());

        state.exit();
        assert!(!state.running());

        let (tx, _) = tokio::sync::mpsc::unbounded_channel();
        for (key, modifier) in &[
            (KeyCode::Char('c'), KeyModifiers::CONTROL),
            (KeyCode::Esc, KeyModifiers::NONE),
        ] {
            state.mode = super::AppMode::Running;
            state
                .send(KeyEvent::new(*key, *modifier).into(), tx.clone())
                .await;
            assert!(!state.running());
        }
    }
}
