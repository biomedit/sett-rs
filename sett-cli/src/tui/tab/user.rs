use crossterm::event::KeyCode;
use ratatui::{
    layout::{Constraint, Layout},
    style::Stylize as _,
    text::{Line, Span, Text},
    widgets::{Block, Borders, Cell, Padding, Paragraph, Row, Table},
};
use sett::auth::{Token, UserInfo, VerificationDetails};
use tokio::sync::mpsc::UnboundedSender;

use crate::{
    tui::{
        action::Action,
        event::{Event, EventHandlingStatus},
        tab::{decrypt::action::DecryptAction, encrypt::action::EncryptAction, Tab, UserMode},
    },
    util::shorten_oidc_issuer_url,
};

pub(super) const TAB: Tab = Tab {
    label: "User",
    render: |state, frame, rect| {
        const KEY_BINDINGS: &str = " <number> change tab <esc> exit ";
        let block = Block::default()
            .borders(Borders::ALL)
            .title_bottom(Line::from(KEY_BINDINGS).centered())
            .padding(Padding::uniform(1));
        let inner_area = block.inner(rect);
        frame.render_widget(block, rect);
        match &state.auth_status {
            AuthStatus::Authenticated(user) => {
                let [details_area, hint_area] =
                    Layout::vertical([Constraint::Length(6), Constraint::Length(1)])
                        .areas(inner_area);
                frame.render_widget(
                    Table::new(
                        [
                            Row::new([
                                "Username",
                                user.info.username.as_ref().map_or("-", String::as_str),
                            ]),
                            Row::new([
                                "Email",
                                user.info.email.as_ref().map_or("-", String::as_str),
                            ]),
                            Row::new([
                                Cell::from("Name"),
                                Cell::from(
                                    match (
                                        user.info.given_name.as_deref().unwrap_or_default(),
                                        user.info.family_name.as_deref().unwrap_or_default(),
                                    ) {
                                        ("", "") => Line::from("-"),
                                        (given_name, "") => Line::from(given_name),
                                        ("", last_name) => Line::from(last_name),
                                        (given_name, last_name) => {
                                            Line::from_iter([given_name, " ", last_name])
                                        }
                                    },
                                ),
                            ]),
                        ],
                        [Constraint::Length(10), Constraint::Min(40)],
                    ),
                    details_area,
                );
                frame.render_widget(Paragraph::new("Press 'x' to logout."), hint_area);
            }
            AuthStatus::Running => {
                frame.render_widget(Paragraph::new("Authenticating..."), inner_area)
            }
            AuthStatus::Waiting(details) => {
                frame.render_widget(
                    Paragraph::new(
                        if let Some(complete_url) = &details.verification_uri_complete {
                            Line::from_iter([
                                Span::from("Open "),
                                complete_url.as_str().bold(),
                                " in your browser to authenticate.".into(),
                            ])
                        } else {
                            Line::from_iter([
                                Span::from("Open "),
                                details.verification_uri.as_str().bold(),
                                " in your browser and enter the code: ".into(),
                                details.user_code.as_str().bold(),
                                " to authenticate.".into(),
                            ])
                        },
                    ),
                    inner_area,
                );
            }
            AuthStatus::Unauthenticated => {
                frame.render_widget(
                    Paragraph::new(Text::from_iter([
                        "Press 'a' to authenticate.",
                        "",
                        "Authenticated users can access extra features, e.g., automated S3 credential fetching.",
                    ])),
                    inner_area,
                );
            }
            AuthStatus::Error(e) => {
                frame.render_widget(
                    Paragraph::new(Text::from_iter([
                        Line::from_iter(["Error: ", e.as_str()]),
                        "Press 'a' to retry.".into(),
                    ])),
                    inner_area,
                );
            }
        }
    },
    handle_key: |state, key, sender| match key.code {
        KeyCode::Char('a') => match state.auth_status {
            AuthStatus::Unauthenticated | AuthStatus::Error(_) => {
                sender
                    .send(AuthAction::Login.into())
                    .expect("receiver is alive");
                EventHandlingStatus::Handled
            }
            _ => EventHandlingStatus::Ignored,
        },
        KeyCode::Char('x') => match state.auth_status {
            AuthStatus::Authenticated(_) => {
                sender
                    .send(AuthAction::Logout.into())
                    .expect("receiver is alive");
                EventHandlingStatus::Handled
            }
            _ => EventHandlingStatus::Ignored,
        },
        _ => EventHandlingStatus::Ignored,
    },
};

#[derive(Debug, Default)]
pub(crate) enum AuthStatus {
    Authenticated(User),
    Running,
    Waiting(VerificationDetails),
    Error(String),
    #[default]
    Unauthenticated,
}

#[derive(Debug)]
pub(crate) struct User {
    token: Token,
    pub(crate) info: UserInfo,
}

#[derive(Debug)]
pub(crate) enum AuthAction {
    Login,
    Logout,
    Processing(VerificationDetails),
    Authenticated(User),
    Error(String),
    GetToken(tokio::sync::oneshot::Sender<Option<Token>>),
    SetToken(Token),
}

impl From<AuthAction> for Event {
    fn from(action: AuthAction) -> Self {
        Event::Action(Action::Auth(action))
    }
}

impl AuthAction {
    pub(crate) async fn handle(self, status: &mut AuthStatus, sender: UnboundedSender<Event>) {
        match self {
            Self::Login => {
                *status = AuthStatus::Running;
                fn send_error(e: impl ToString, sender: UnboundedSender<Event>) {
                    sender
                        .send(AuthAction::Error(e.to_string()).into())
                        .expect("receiver is alive");
                }
                tokio::spawn(async move {
                    let client = match crate::util::get_oidc_client().await {
                        Ok(client) => client,
                        Err(e) => {
                            send_error(e, sender);
                            return;
                        }
                    };
                    let token = match client
                        .authenticate(|mut details| {
                            details.verification_uri =
                                shorten_oidc_issuer_url(&details.verification_uri).to_string();
                            if let Some(complete_uri) = &mut details.verification_uri_complete {
                                *complete_uri = shorten_oidc_issuer_url(complete_uri).to_string();
                            }
                            sender.send(AuthAction::Processing(details).into())
                        })
                        .await
                    {
                        Ok(token) => token,
                        Err(e) => {
                            send_error(e, sender);
                            return;
                        }
                    };
                    let info = match client.user_info(&token.access_token).await {
                        Ok(info) => info,
                        Err(e) => {
                            send_error(e, sender);
                            return;
                        }
                    };
                    if token.refresh_token.is_some() {
                        let sender = sender.clone();
                        tokio::spawn(async move {
                            let mut interval =
                                tokio::time::interval(std::time::Duration::from_secs(15 * 60));
                            // The first tick completes immediately.
                            // No need to refresh the token yet.
                            interval.tick().await;
                            loop {
                                interval.tick().await;
                                let (tx, rx) = tokio::sync::oneshot::channel();
                                sender
                                    .send(AuthAction::GetToken(tx).into())
                                    .expect("receiver is alive");
                                if let Some(token) = rx.await.expect("sender is alive") {
                                    if let Some(refresh_token) = &token.refresh_token {
                                        match client.refresh(refresh_token).await {
                                            Ok(new_token) => {
                                                tracing::trace!("Refreshed token");
                                                sender
                                                    .send(AuthAction::SetToken(new_token).into())
                                                    .expect("receiver is alive");
                                            }
                                            Err(e) => {
                                                tracing::error!("Unable to refresh token: {}", e);
                                                send_error(e, sender);
                                                break;
                                            }
                                        }
                                    } else {
                                        // This case shouldn't happen.
                                        // When only the access token is available, the refresh
                                        // task isn't started.
                                        tracing::error!(
                                            "Unable to refresh token: no refresh token available"
                                        );
                                        break;
                                    }
                                } else {
                                    // This case could happen if the user has already logged out.
                                    tracing::warn!("Unable to refresh token: no token available");
                                    break;
                                }
                            }
                        });
                    }
                    sender
                        .send(AuthAction::Authenticated(User { token, info }).into())
                        .expect("receiver is alive");
                });
            }
            Self::Logout => {
                *status = AuthStatus::Unauthenticated;
                sender
                    .send(Action::Encrypt(EncryptAction::SwitchMode(Default::default())).into())
                    .expect("receiver is alive");
                sender
                    .send(Action::Decrypt(DecryptAction::SwitchMode(Default::default())).into())
                    .expect("receiver is alive");
            }
            Self::Processing(details) => {
                *status = AuthStatus::Waiting(details);
            }
            Self::Authenticated(user) => {
                *status = AuthStatus::Authenticated(user);
                sender
                    .send(
                        Action::Encrypt(EncryptAction::SwitchMode(UserMode::Authenticated)).into(),
                    )
                    .expect("receiver is alive");
                sender
                    .send(
                        Action::Decrypt(DecryptAction::SwitchMode(UserMode::Authenticated)).into(),
                    )
                    .expect("receiver is alive");
            }
            Self::Error(e) => {
                *status = AuthStatus::Error(e);
                sender
                    .send(Action::Encrypt(EncryptAction::SwitchMode(Default::default())).into())
                    .expect("receiver is alive");
                sender
                    .send(Action::Decrypt(DecryptAction::SwitchMode(Default::default())).into())
                    .expect("receiver is alive");
            }
            Self::GetToken(sender) => {
                sender
                    .send(if let AuthStatus::Authenticated(user) = status {
                        Some(user.token.clone())
                    } else {
                        None
                    })
                    .expect("receiver is alive");
            }
            Self::SetToken(token) => {
                if let AuthStatus::Authenticated(user) = status {
                    user.token = token;
                }
            }
        }
    }
}
