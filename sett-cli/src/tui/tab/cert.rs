use ratatui::{
    layout::{Constraint, Layout, Rect},
    style::{Color, Style},
    text::{Line, Text},
    widgets::{Block, Borders, Padding, Paragraph},
    Frame,
};
use sett::openpgp::{
    cert::CertType,
    certstore::{CertStore, CertStoreOpts},
    keystore::KeyStore,
};
use tokio::sync::mpsc;

use crate::tui::{
    action::Action,
    event::Event,
    tab::Tab,
    widget::{self, cert::CertInfo},
};

pub(super) const TAB: Tab = Tab {
    label: "Keys",
    render: |state, frame, rect| state.cert.render(frame, rect),
    handle_key: |state, key, _| widget::cert::handle_key(&mut state.cert.table_state, key),
};

#[derive(Debug)]
pub(crate) struct CertSelector {
    pub(crate) cert_info_store: Result<CertInfoStore, RefreshError>,
    table_state: ratatui::widgets::TableState,
}

#[derive(Debug, Default)]
pub(crate) struct CertInfoStore {
    pub(crate) private: Vec<CertInfo>,
    pub(crate) public: Vec<CertInfo>,
}

impl CertInfoStore {
    async fn load() -> anyhow::Result<Self> {
        let cert_store = CertStore::open(&CertStoreOpts {
            read_only: true,
            ..Default::default()
        })?;
        let keys: Vec<_> = KeyStore::open(None)
            .await?
            .list()
            .await?
            .into_iter()
            .map(|key| key.fingerprint())
            .collect();
        let mut public = Vec::new();
        let mut private = Vec::new();
        for cert in cert_store.certs().filter_map(|cert| {
            cert.inspect_err(|e| tracing::warn!("Failed to read a key: {e}"))
                .ok()
        }) {
            let userid = cert.userids().into_iter().next();
            let cert_info = CertInfo {
                fingerprint: cert.fingerprint().to_string(),
                cert_type: if keys.contains(&cert.fingerprint()) {
                    CertType::Secret
                } else {
                    CertType::Public
                },
                cert,
                userid,
            };
            if cert_info.cert_type == CertType::Public {
                public.push(cert_info);
            } else {
                private.push(cert_info);
            }
        }
        public.sort_unstable_by_key(|cert| (cert.userid.as_ref().map(|x| x.to_lowercase()),));
        private.sort_unstable_by_key(|cert| (cert.userid.as_ref().map(|x| x.to_lowercase()),));
        Ok(Self { public, private })
    }

    pub(crate) fn is_empty(&self) -> bool {
        self.private.is_empty() && self.public.is_empty()
    }

    pub(crate) fn get(&self, index: usize) -> Option<&CertInfo> {
        if index < self.private.len() {
            self.private.get(index)
        } else {
            self.public.get(index - self.private.len())
        }
    }
}

#[derive(Debug)]
pub(crate) enum CertAction {
    Refresh,
    RefreshResult(Result<CertInfoStore, RefreshError>),
}

#[derive(Debug)]
pub(crate) struct RefreshError(anyhow::Error);

impl std::fmt::Display for RefreshError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl core::error::Error for RefreshError {}

impl From<anyhow::Error> for RefreshError {
    fn from(e: anyhow::Error) -> Self {
        Self(e)
    }
}

impl From<CertAction> for Action {
    fn from(action: CertAction) -> Self {
        Self::Cert(action)
    }
}

impl From<CertAction> for Event {
    fn from(action: CertAction) -> Self {
        Self::Action(action.into())
    }
}

impl Default for CertSelector {
    fn default() -> Self {
        Self {
            cert_info_store: Ok(Default::default()),
            table_state: Default::default(),
        }
    }
}

impl CertSelector {
    pub(crate) fn new() -> Self {
        Default::default()
    }

    fn render(&mut self, frame: &mut Frame, rect: Rect) {
        const KEY_BINDINGS: &str = " <j/k/down/up> move <number> change tab <esc> exit ";
        let block = Block::default()
            .borders(Borders::ALL)
            .title_bottom(Line::from(KEY_BINDINGS).centered())
            .padding(Padding::uniform(1));
        let inner_area = block.inner(rect);
        frame.render_widget(block, rect);
        match &self.cert_info_store {
            Ok(store) => {
                let vertical = Layout::vertical([Constraint::Ratio(1, 2); 2]);
                let [cert_list_area, cert_detail_area] = vertical.areas(inner_area);
                widget::cert::render_table(
                    &mut self.table_state,
                    store.private.iter().chain(store.public.iter()),
                    frame,
                    cert_list_area,
                );
                if let Some(selected) = self.table_state.selected() {
                    if let Some(cert_info) = store.get(selected) {
                        widget::cert::render_details(cert_info, frame, cert_detail_area);
                    }
                }
            }
            Err(e) => {
                frame.render_widget(
                    Paragraph::new(Text::styled(
                        format!("Failed to load certificates: {e}"),
                        Style::default().fg(Color::Red),
                    )),
                    inner_area,
                );
            }
        }
    }

    pub(crate) async fn handle_action(
        &mut self,
        action: CertAction,
        sender: mpsc::UnboundedSender<Event>,
    ) {
        match action {
            CertAction::Refresh => {
                tokio::spawn(async move {
                    let store = match CertInfoStore::load().await {
                        Ok(store) => store,
                        Err(e) => {
                            send_error(e, sender);
                            return;
                        }
                    };
                    sender
                        .send(CertAction::RefreshResult(Ok(store)).into())
                        .expect("receiver is not dropped");
                });
            }
            CertAction::RefreshResult(cert_info_store) => {
                self.cert_info_store = cert_info_store;
                self.table_state.select(
                    self.cert_info_store
                        .as_ref()
                        .ok()
                        .and_then(|store| (!store.is_empty()).then_some(0)),
                );
            }
        }
    }
}

fn send_error(error: impl Into<anyhow::Error>, sender: mpsc::UnboundedSender<Event>) {
    sender
        .send(CertAction::RefreshResult(Err(RefreshError(error.into()))).into())
        .expect("receiver is not dropped");
}
