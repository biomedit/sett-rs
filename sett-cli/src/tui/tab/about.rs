use ratatui::{
    layout::{Constraint, Layout, Rect},
    style::Stylize,
    text::{Line, Text},
    widgets::{Block, Borders, Padding, Paragraph, Wrap},
    Frame,
};

use crate::tui::{event::EventHandlingStatus, tab::Tab};

pub(super) const TAB: Tab = Tab {
    label: "About",
    render,
    handle_key: |_, _, _| EventHandlingStatus::Ignored,
};

fn render(_state: &mut crate::tui::state::State, frame: &mut Frame, rect: Rect) {
    const KEY_BINDINGS: &str = " <number> change tab <esc> exit ";
    struct AboutFrame<'a> {
        title: &'static str,
        content: &'a str,
        trim: bool,
    }
    impl AboutFrame<'_> {
        fn render(&self, frame: &mut Frame, rect: Rect) {
            let p = Paragraph::new(Text::from_iter([
                Line::from(self.title.bold()),
                Line::default(),
                Line::from(self.content),
            ]))
            .centered();
            let p = if self.trim {
                p.wrap(Wrap { trim: true })
            } else {
                p
            };
            frame.render_widget(p, rect);
        }
    }
    const REGIONS: &[AboutFrame] = &[
        AboutFrame {
            title: "What is sett?",
            content: "Transferring sensitive data over a possibly unsafe \
                        network in a secure way is not a trivial task. sett is an \
                        application that automates the encryption, compression, \
                        packaging, and transfer of data over the network, ensuring \
                        data remains safe at all times and can only be read by the \
                        intended data recipient.",
            trim: true,
        },
        AboutFrame {
            title: "Version",
            content: env!("CARGO_PKG_VERSION"),
            trim: false,
        },
        AboutFrame {
            title: "License",
            content: "sett is licensed under the GNU General Public License 3.0",
            trim: false,
        },
        AboutFrame {
            title: "Developers",
            content: "sett is developed as part of the BioMedIT project.",
            trim: false,
        },
        AboutFrame {
            title: "Download",
            content: "sett comes in two flavours: a CLI app (the one you're \
                looking at) and a GUI. Available for: Linux, Windows, and Mac OS.",
            trim: false,
        },
        AboutFrame {
            title: "Technologies",
            content: "sett encrypts data using the OpenPGP protocol as \
                implemented by the Sequoia-PGP library.",
            trim: true,
        },
    ];
    let block = Block::default()
        .borders(Borders::ALL)
        .title_bottom(Line::from(KEY_BINDINGS).centered())
        .padding(Padding::uniform(1));
    let layout = Layout::vertical([Constraint::Ratio(1, REGIONS.len() as u32); REGIONS.len()])
        .split(block.inner(rect));
    for (rect, about_frame) in layout.iter().zip(REGIONS) {
        about_frame.render(frame, *rect);
    }
    frame.render_widget(block, rect);
}
