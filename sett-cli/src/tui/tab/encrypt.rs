pub(crate) mod action;
mod component;

use std::path::PathBuf;

use crossterm::event::{KeyCode, KeyEvent, KeyModifiers};
use ratatui::{layout::Constraint, widgets::ListState};
use sett::package::{Metadata, Package};
use tokio::sync::mpsc::UnboundedSender;

use crate::tui::{
    event::{Event, EventHandlingStatus},
    state::State,
    tab::{encrypt::component::COMPONENTS, Status, Tab, UserMode},
    widget::{
        cert::{CertInfo, CertSelectionDialog},
        destination::DestinationSelector,
        file::FileSelector,
        progress::Progress,
        CyclicIndex,
    },
};

pub(super) const TAB: Tab = Tab {
    label: "Encrypt/Send",
    render: |state, frame, rect| {
        const CONSTRAINTS: [Constraint; COMPONENTS.len()] = super::constraints(&COMPONENTS);
        super::render_components(
            &COMPONENTS,
            &CONSTRAINTS,
            state,
            state.encrypt.current_component,
            frame,
            rect,
        );
    },
    handle_key,
};

#[derive(Debug, Default)]
pub(crate) struct Encrypt {
    source: Source,
    files_selector: FileSelector,
    sender_selector: CertSelectionDialog,
    signer: Option<CertInfo>,
    recipients: Vec<CertInfo>,
    recipients_selector: CertSelectionDialog,
    recipients_state: ListState,
    transfer_id_dialog: component::TransferIdDialog,
    destination_selector: DestinationSelector,
    progress: Progress,
    status: Status,
    current_component: CyclicIndex<{ COMPONENTS.len() }>,
    mode: UserMode,
    #[cfg(test)]
    clear_count: usize,
}

impl Encrypt {
    pub(crate) fn new() -> Self {
        Self {
            files_selector: FileSelector::new().with_multi_selection(true),
            ..Default::default()
        }
    }

    fn clear(&mut self) {
        action::set_source_to_files(self, Default::default());
        self.signer = Default::default();
        self.recipients = Default::default();
        self.transfer_id_dialog.clear();
        self.destination_selector.clear();
        #[cfg(test)]
        {
            self.clear_count += 1
        }
    }
}

#[derive(Debug)]
enum Source {
    Package {
        package: Package<sett::package::state::Verified>,
        path: PathBuf,
        metadata: Metadata,
    },
    Files {
        files: Vec<PathBuf>,
        state: ListState,
    },
}

impl Default for Source {
    fn default() -> Self {
        Self::Files {
            files: Vec::new(),
            state: ListState::default(),
        }
    }
}

fn handle_key(
    state: &mut State,
    key: KeyEvent,
    sender: UnboundedSender<Event>,
) -> EventHandlingStatus {
    if let Status::Running = state.encrypt.status {
        return EventHandlingStatus::Ignored;
    }
    if let EventHandlingStatus::Handled =
        (COMPONENTS[*state.encrypt.current_component].handle_key)(state, key, sender.clone())
    {
        return EventHandlingStatus::Handled;
    }
    match key.code {
        KeyCode::Tab => {
            let mut current_component = state.encrypt.current_component;
            state.encrypt.current_component = current_component
                .find(|&c| COMPONENTS[*c].is_interactive(state))
                .expect("component found");
            EventHandlingStatus::Handled
        }
        KeyCode::BackTab => {
            let mut current_component = state.encrypt.current_component;
            state.encrypt.current_component = current_component
                .rfind(|&c| COMPONENTS[*c].is_interactive(state))
                .expect("component found");
            EventHandlingStatus::Handled
        }
        KeyCode::Char('x') if key.modifiers == KeyModifiers::CONTROL => {
            state.encrypt.clear();
            EventHandlingStatus::Handled
        }
        _ => EventHandlingStatus::Ignored,
    }
}

#[cfg(test)]
mod tests {
    use sett::openpgp::cert::{Cert, CertType};

    use super::*;

    #[test]
    fn key_event() {
        let (tx, _) = tokio::sync::mpsc::unbounded_channel();
        let mut state = State::default();
        // Event captured by the first component.
        assert_eq!(
            handle_key(&mut state, KeyCode::Char('x').into(), tx.clone(),),
            EventHandlingStatus::Handled
        );
        assert_eq!(state.encrypt.clear_count, 0);
        // Clear form
        assert_eq!(
            handle_key(
                &mut state,
                KeyEvent::new(KeyCode::Char('x'), KeyModifiers::CONTROL),
                tx.clone(),
            ),
            EventHandlingStatus::Handled
        );
        assert_eq!(state.encrypt.clear_count, 1);
        // Switch to the next component
        let index = state.encrypt.current_component;
        assert_eq!(
            handle_key(&mut state, KeyCode::Tab.into(), tx.clone(),),
            EventHandlingStatus::Handled
        );
        assert!(state.encrypt.current_component > index);
        // Switch back to the previous component
        assert_eq!(
            handle_key(&mut state, KeyCode::BackTab.into(), tx.clone(),),
            EventHandlingStatus::Handled
        );
        assert_eq!(state.encrypt.current_component, index);
    }

    #[test]
    fn clear_form() {
        let cert = Cert::from_bytes(include_bytes!(
            "../../../../sett/tests/data/1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub"
        ))
        .unwrap();
        let cert_info = CertInfo {
            cert_type: CertType::Public,
            fingerprint: cert.fingerprint().to_string(),
            userid: cert.userids().into_iter().next(),
            cert,
        };
        for index in CyclicIndex::<{ COMPONENTS.len() }>::default().take(COMPONENTS.len()) {
            let mut state = Encrypt {
                source: Source::Files {
                    files: vec!["foo.json".into(), "/bar/config.toml".into()],
                    state: Default::default(),
                },
                signer: Some(cert_info.clone()),
                recipients: vec![cert_info.clone()],
                current_component: index,
                ..Default::default()
            };
            state.clear();
            match state.source {
                Source::Files { files, .. } => assert!(files.is_empty()),
                Source::Package { .. } => unreachable!(),
            }
            assert_eq!(state.signer, None);
            assert_eq!(state.recipients, Vec::new());
            assert_eq!(state.transfer_id_dialog.clear_count, 1);
            assert_eq!(state.destination_selector.clear_count, 1);
        }
    }
}
