use std::path::{Path, PathBuf};

use ratatui::widgets::ListState;
use sett::{
    openpgp::cert::Fingerprint,
    package::{Metadata, Package},
};
use tokio::sync::mpsc::UnboundedSender;

use crate::tui::{
    action::Action,
    event::Event,
    tab::{
        encrypt::{Encrypt, Source},
        ProgressUpdater, Status, TaskAction, UserMode,
    },
    widget::{
        destination::{DestinationBuilder, DestinationSubset},
        progress::Progress,
    },
};

#[derive(Debug)]
pub(crate) enum EncryptAction {
    Task(TaskAction),
    AddFile(PathBuf),
    #[cfg(feature = "auth")]
    SwitchMode(UserMode),
    #[cfg(feature = "auth")]
    LoadDataTransfers,
    #[cfg(feature = "auth")]
    SetDataTransfers(Result<Vec<sett::portal::response::DataTransfer>, String>),
}

impl Encrypt {
    pub(crate) async fn handle_action(
        &mut self,
        action: EncryptAction,
        sender: UnboundedSender<Event>,
    ) {
        match action {
            EncryptAction::AddFile(path) => {
                self.status = Status::New;
                if path.extension() != Some("zip".as_ref()) {
                    add_file(self, path);
                } else if let Ok((package, metadata)) = extract_metadata(&path).await {
                    self.source = Source::Package {
                        package,
                        path,
                        metadata,
                    };
                    self.destination_selector
                        .change_subset(DestinationSubset::Remote);
                    self.files_selector.close();
                } else {
                    add_file(self, path);
                }
            }
            EncryptAction::Task(task_action) => match task_action {
                TaskAction::Run => {
                    self.status = Status::Running;
                    self.progress = Default::default();
                    match &self.source {
                        Source::Package {
                            package,
                            #[cfg(feature = "auth")]
                            metadata,
                            #[cfg(feature = "auth")]
                            path,
                            ..
                        } => {
                            let package = package.clone();
                            let destination = self.destination_selector.builder();
                            let mode = self.mode;
                            #[cfg(feature = "auth")]
                            let metadata = metadata.clone();
                            #[cfg(feature = "auth")]
                            let path = path.clone();
                            tokio::spawn(async move {
                                match send(
                                    &package,
                                    destination,
                                    mode,
                                    sender.clone(),
                                    #[cfg(feature = "auth")]
                                    &path,
                                    #[cfg(feature = "auth")]
                                    &metadata,
                                )
                                .await
                                {
                                    Ok(status) => sender
                                        .send(Status::Finished(status).into_encrypt_event())
                                        .expect("receiver is alive"),
                                    Err(e) => send_error(e, sender),
                                }
                            });
                        }
                        Source::Files { files, .. } => {
                            match (self.signer.as_ref(), self.recipients.is_empty()) {
                                (None, _) => {
                                    self.status = Status::Error("No sender selected".into())
                                }
                                (_, true) => {
                                    self.status = Status::Error("No recipients selected".into())
                                }
                                (Some(signer), _) => {
                                    let files = files.clone();
                                    let signer = signer.cert.fingerprint();
                                    let recipients = self
                                        .recipients
                                        .iter()
                                        .map(|r| r.cert.fingerprint())
                                        .collect();
                                    let transfer_id = self.transfer_id_dialog.value(self.mode);
                                    let destination = self.destination_selector.builder();
                                    let mode = self.mode;
                                    let sender = sender.clone();
                                    tokio::spawn(async move {
                                        match encrypt(
                                            files,
                                            signer,
                                            recipients,
                                            transfer_id,
                                            destination,
                                            mode,
                                            sender.clone(),
                                        )
                                        .await
                                        {
                                            Ok(status) => sender
                                                .send(Status::Finished(status).into_encrypt_event())
                                                .expect("receiver is alive"),

                                            Err(e) => send_error(e, sender),
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
                TaskAction::Progress(progress) => {
                    self.progress = progress;
                }
                TaskAction::Status(status) => {
                    if let Status::Finished { .. } = status {
                        set_source_to_files(self, Vec::new());
                    }
                    self.status = status;
                }
            },
            #[cfg(feature = "auth")]
            EncryptAction::SwitchMode(mode) => {
                match mode {
                    UserMode::Anonymous => {
                        self.destination_selector = Default::default();
                    }
                    #[cfg(feature = "auth")]
                    UserMode::Authenticated => {
                        self.destination_selector =
                            super::DestinationSelector::new_with_auth_mode();
                        sender
                            .send(Action::Encrypt(EncryptAction::LoadDataTransfers).into())
                            .expect("receiver is alive");
                    }
                }
                self.mode = mode;
            }
            #[cfg(feature = "auth")]
            EncryptAction::LoadDataTransfers => {
                tokio::spawn(async move {
                    sender.send(
                        Action::Encrypt(EncryptAction::SetDataTransfers(
                            crate::tui::tab::load_data_transfers(sender.clone()).await,
                        ))
                        .into(),
                    )
                });
            }
            #[cfg(feature = "auth")]
            EncryptAction::SetDataTransfers(transfers) => {
                self.transfer_id_dialog
                    .portal
                    .selector
                    .set_data_transfers(transfers);
            }
        }
    }
}

async fn build_destination(
    builder: DestinationBuilder,
    mode: UserMode,
    #[cfg(feature = "auth")] sender: UnboundedSender<Event>,
    #[cfg(feature = "auth")] transfer_id: Option<u32>,
) -> Result<sett::destination::Destination, anyhow::Error> {
    match (mode, builder) {
        #[cfg(feature = "auth")]
        (UserMode::Authenticated, DestinationBuilder::S3 { .. }) => {
            let transfer_id = transfer_id.ok_or(anyhow::anyhow!("Transfer ID is None"))?;
            let portal_client = crate::util::get_portal_client()?;
            let (tx, rx) = tokio::sync::oneshot::channel();
            sender.send(crate::tui::tab::user::AuthAction::GetToken(tx).into())?;
            let token = rx
                .await?
                .ok_or(anyhow::anyhow!("Unable to get OAuth2 token"))?;
            let connection_details = portal_client
                .get_s3_connection_details(
                    transfer_id,
                    sett::portal::S3Permission::Write,
                    &token.access_token,
                )
                .await?;
            let client = connection_details.build_client().await?;
            Ok(sett::destination::S3::new(client, connection_details.bucket).into())
        }
        (_, b) => b.build().await,
    }
}

async fn encrypt(
    files: Vec<PathBuf>,
    signer: Fingerprint,
    recipients: Vec<Fingerprint>,
    transfer_id: Option<u32>,
    destination: DestinationBuilder,
    mode: UserMode,
    sender: UnboundedSender<Event>,
) -> Result<sett::task::Status, anyhow::Error> {
    let timestamp = chrono::Utc::now();
    let compression_algorithm = Default::default();
    let prefix = match mode {
        UserMode::Anonymous => None,
        #[cfg(feature = "auth")]
        UserMode::Authenticated => Some(
            crate::util::get_portal_client()?
                .check_package(
                    &sett::package::Metadata {
                        sender: signer.to_hex(),
                        recipients: recipients.iter().map(|r| r.to_hex()).collect(),
                        timestamp,
                        compression_algorithm,
                        transfer_id,
                        ..Default::default()
                    },
                    "missing",
                )
                .await?
                .project_code,
        ),
    };
    let destination = build_destination(
        destination,
        mode,
        #[cfg(feature = "auth")]
        sender.clone(),
        #[cfg(feature = "auth")]
        transfer_id,
    )
    .await?;
    let sender_clone = sender.clone();
    let password = move |hint: sett::openpgp::crypto::PasswordHint| {
        let sender_clone = sender_clone.clone();
        async move {
            let (tx, rx) = tokio::sync::oneshot::channel();
            sender_clone
                .send(Action::PromptPassword { hint, result: tx }.into())
                .expect("receiver is alive");
            rx.await.expect("received")
        }
    };
    let cert_store =
        sett::openpgp::certstore::CertStore::open(&sett::openpgp::certstore::CertStoreOpts {
            read_only: true,
            ..Default::default()
        })?;
    let key_store = sett::openpgp::keystore::KeyStore::open(None).await?;
    let encrypt_opts = sett::encrypt::EncryptOpts {
        files,
        recipients,
        signer,
        cert_store,
        key_store,
        mode: sett::task::Mode::Run,
        purpose: None,
        extra_metadata: Default::default(),
        transfer_id,
        compression_algorithm,
        password,
        progress: Some(build_progress_updater(sender)),
        timestamp,
        prefix,
    };
    Ok(sett::encrypt::encrypt(encrypt_opts, destination).await?)
}

async fn send(
    package: &Package<sett::package::state::Verified>,
    destination: DestinationBuilder,
    mode: UserMode,
    sender: UnboundedSender<Event>,
    #[cfg(feature = "auth")] path: &Path,
    #[cfg(feature = "auth")] metadata: &Metadata,
) -> Result<sett::task::Status, anyhow::Error> {
    #[cfg(feature = "auth")]
    if let UserMode::Authenticated = mode {
        let file_name = path
            .file_name()
            .ok_or_else(|| anyhow::anyhow!("Failed to get package file name"))?
            .to_string_lossy();
        let response = crate::util::get_portal_client()?
            .check_package(metadata, &file_name)
            .await?;
        let file_name_expected = format!(
            "{}_{}.zip",
            response.project_code,
            metadata.timestamp.format(sett::package::DATETIME_FORMAT)
        );
        anyhow::ensure!(
            file_name == file_name_expected,
            "Package verification failed: invalid file name '{}' (expected '{}')",
            &file_name,
            file_name_expected
        );
    }
    use sett::destination::Destination::*;
    Ok(
        match build_destination(
            destination,
            mode,
            #[cfg(feature = "auth")]
            sender.clone(),
            #[cfg(feature = "auth")]
            metadata.transfer_id,
        )
        .await?
        {
            S3(s3) => {
                s3.client()
                    .upload(
                        package,
                        s3.bucket(),
                        sett::task::Mode::Run,
                        Some(build_progress_updater(sender)),
                    )
                    .await?
            }
            Sftp(sftp) => {
                sett::remote::sftp::upload(
                    package,
                    sftp.client(),
                    sftp.base_path(),
                    sett::task::Mode::Run,
                    Some(build_progress_updater(sender)),
                )
                .await?
            }
            Stdout | Local(_) => {
                return Err(anyhow::anyhow!(
                    "Local and stdout destinations are not supported"
                ));
            }
        },
    )
}

fn build_progress_updater(sender: UnboundedSender<Event>) -> ProgressUpdater<impl Fn(u64, u64)> {
    ProgressUpdater::new(move |current, total| {
        sender
            .send(TaskAction::Progress(Progress { current, total }).into_encrypt_event())
            .expect("receiver is alive");
    })
}

fn send_error(error: anyhow::Error, sender: UnboundedSender<Event>) {
    sender
        .send(Status::Error(format!("{:#}", error)).into_encrypt_event())
        .expect("receiver is alive");
}

fn add_file(state: &mut Encrypt, path: PathBuf) {
    match &mut state.source {
        Source::Files { files, state } => {
            if !files.contains(&path) {
                files.push(path);
                files.sort();
                state.select(None)
            }
        }
        Source::Package { .. } => {
            set_source_to_files(state, vec![path]);
        }
    }
}

async fn extract_metadata(
    p: &Path,
) -> Result<(Package<sett::package::state::Verified>, Metadata), ()> {
    let package = Package::open(p).await.map_err(|_| ())?;
    let cert_store =
        sett::openpgp::certstore::CertStore::open(&sett::openpgp::certstore::CertStoreOpts {
            read_only: true,
            ..Default::default()
        })
        .map_err(|_| ())?;
    let package = package.verify(&cert_store).await.map_err(|_| ())?;
    let metadata = package.metadata().await.map_err(|_| ())?;
    Ok((package, metadata))
}

pub(super) fn set_source_to_files(state: &mut Encrypt, files: Vec<PathBuf>) {
    state.source = Source::Files {
        files,
        state: ListState::default(),
    };
    state
        .destination_selector
        .change_subset(DestinationSubset::All);
}
