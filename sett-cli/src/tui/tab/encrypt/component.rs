use crossterm::event::{KeyCode, KeyEvent, KeyModifiers};
use ratatui::{
    layout::{Constraint, Layout, Rect},
    style::{Modifier, Style},
    text::{Line, Span},
    widgets::{Block, Clear, List, ListState, Paragraph},
    Frame,
};

use crate::tui::{
    action::Action,
    event::EventHandlingStatus,
    tab::{
        encrypt::{action::EncryptAction, Source},
        render_metadata, render_status, Status, TaskAction, TaskKind, UserMode,
    },
    widget::{
        component::{Component, Focus, Mode},
        field::{Field, PositiveIntegerField, Required},
        form::Form,
    },
};

pub(super) const COMPONENTS: [&Component; 8] = [
    &SOURCE_SELECTOR,
    &SENDER_SELECTOR,
    &RECIPIENTS_SELECTOR,
    &TRANSFER_ID_SELECTOR,
    &DESTINATION_SELECTOR,
    &RUN,
    &PROGRESS,
    &STATUS,
];

pub(super) const SOURCE_SELECTOR: Component = Component {
    mode: Mode::ReadWrite,
    show: |_| true,
    constraint: Constraint::Min(3),
    key_bindings: " <enter> select file(s)/package <j/k/down/up> move <x> remove ",
    render: |state, frame, area, focus| match &mut state.encrypt.source {
        Source::Files { files, .. } if files.is_empty() => frame.render_widget(
            Paragraph::new("Select files/directories or an existing data package"),
            area,
        ),
        Source::Files { files, state } => {
            let [header_area, files_area] =
                Layout::vertical([Constraint::Length(2), Constraint::Fill(1)]).areas(area);
            frame.render_widget(Paragraph::new("Selected files/directories:"), header_area);
            frame.render_stateful_widget(
                List::new(files.iter().map(|p| p.to_string_lossy())).highlight_style(
                    if let Focus::Focused = focus {
                        Style::default().add_modifier(Modifier::REVERSED)
                    } else {
                        Style::default()
                    },
                ),
                files_area,
                state,
            );
        }
        Source::Package { path, metadata, .. } => {
            let [header_area, files_area] =
                Layout::vertical([Constraint::Length(2), Constraint::Fill(1)]).areas(area);
            frame.render_widget(
                Line::from_iter([
                    Span::from("Selected package ("),
                    Span::from(path.to_string_lossy()),
                    Span::from("):"),
                ]),
                header_area,
            );
            render_metadata(metadata, frame, files_area);
        }
    },
    render_popup: |state, frame, area| {
        if state.encrypt.files_selector.is_open() {
            frame.render_widget(Clear, area);
            state.encrypt.files_selector.render(frame, area);
        }
    },
    handle_key: |state, key, sender| match (state.encrypt.files_selector.is_open(), key.code) {
        (true, KeyCode::Tab | KeyCode::BackTab) => EventHandlingStatus::Handled,
        (true, _) => state.encrypt.files_selector.handle_key(key, |selected| {
            sender
                .send(Action::Encrypt(EncryptAction::AddFile(selected)).into())
                .expect("receiver is alive");
        }),
        (false, KeyCode::Enter) => {
            state.encrypt.files_selector.open();
            EventHandlingStatus::Handled
        }
        (false, KeyCode::Down | KeyCode::Char('j')) => {
            if let Source::Files { state, .. } = &mut state.encrypt.source {
                state.select_next();
                EventHandlingStatus::Handled
            } else {
                EventHandlingStatus::Ignored
            }
        }
        (false, KeyCode::Up | KeyCode::Char('k')) => {
            if let Source::Files { state, .. } = &mut state.encrypt.source {
                state.select_previous();
                EventHandlingStatus::Handled
            } else {
                EventHandlingStatus::Ignored
            }
        }
        (false, KeyCode::Char('x')) if key.modifiers == KeyModifiers::NONE => {
            match &mut state.encrypt.source {
                Source::Files { files, state } => {
                    if let Some(selected) = state.selected() {
                        files.remove(selected);
                    }
                }
                Source::Package { .. } => {
                    state.encrypt.source = Source::Files {
                        files: Vec::new(),
                        state: ListState::default(),
                    }
                }
            }
            EventHandlingStatus::Handled
        }
        _ => EventHandlingStatus::Ignored,
    },
};

pub(super) const SENDER_SELECTOR: Component = Component {
    mode: Mode::ReadWrite,
    show: |state| matches!(state.encrypt.source, Source::Files { .. }),
    constraint: Constraint::Length(1),
    key_bindings: " <enter> select sender <x> remove ",
    render: |state, frame, area, _| {
        if let Some(sender) = &state.encrypt.signer {
            frame.render_widget(
                Paragraph::new(Line::from_iter([
                    Span::from("Selected sender: "),
                    Span::from(sender.userid.as_ref().unwrap_or(&sender.fingerprint)),
                ])),
                area,
            );
        } else {
            frame.render_widget(Paragraph::new("Select sender"), area);
        }
    },
    render_popup: |state, frame, area| {
        if state.encrypt.sender_selector.is_open() {
            frame.render_widget(Clear, area);
            if let Ok(store) = state.cert.cert_info_store.as_ref() {
                state
                    .encrypt
                    .sender_selector
                    .render(&store.private, frame, area);
            }
        }
    },
    handle_key: |state, key, _| match (state.encrypt.sender_selector.is_open(), key.code) {
        (true, KeyCode::Tab | KeyCode::BackTab) => EventHandlingStatus::Handled,
        (true, _) => {
            if let Ok(store) = state.cert.cert_info_store.as_ref() {
                let (status, selected) = state.encrypt.sender_selector.handle_key(key);
                if let Some(selected) = selected {
                    state.encrypt.signer = store.private.get(selected).cloned();
                    state.encrypt.sender_selector.close();
                }
                status
            } else {
                EventHandlingStatus::Ignored
            }
        }
        (false, KeyCode::Enter) => {
            state.encrypt.sender_selector.open();
            EventHandlingStatus::Handled
        }
        (false, KeyCode::Char('x')) if key.modifiers == KeyModifiers::NONE => {
            state.encrypt.signer = None;
            EventHandlingStatus::Handled
        }
        _ => EventHandlingStatus::Ignored,
    },
};

pub(super) const RECIPIENTS_SELECTOR: Component = Component {
    mode: Mode::ReadWrite,
    show: |state| matches!(state.encrypt.source, Source::Files { .. }),
    constraint: Constraint::Min(3),
    key_bindings: " <enter> select recipient(s) <j/k/down/up> move <x> remove ",
    render: |state, frame, area, focus| {
        if state.encrypt.recipients.is_empty() {
            frame.render_widget(Paragraph::new("Select recipients"), area);
        } else {
            let [header_area, recipients_area] =
                Layout::vertical([Constraint::Length(2), Constraint::Fill(1)]).areas(area);
            frame.render_widget(Paragraph::new("Selected recipients:"), header_area);
            frame.render_stateful_widget(
                List::new(
                    state
                        .encrypt
                        .recipients
                        .iter()
                        .map(|r| r.userid.as_deref().unwrap_or(r.fingerprint.as_str())),
                )
                .highlight_style(if let Focus::Focused = focus {
                    Style::default().add_modifier(Modifier::REVERSED)
                } else {
                    Style::default()
                }),
                recipients_area,
                &mut state.encrypt.recipients_state,
            );
        }
    },
    render_popup: |state, frame, area| {
        if state.encrypt.recipients_selector.is_open() {
            frame.render_widget(Clear, area);
            if let Ok(store) = state.cert.cert_info_store.as_ref() {
                state.encrypt.recipients_selector.render(
                    store.private.iter().chain(store.public.iter()),
                    frame,
                    area,
                );
            }
        }
    },
    handle_key: |state, key, _| match (state.encrypt.recipients_selector.is_open(), key.code) {
        (true, KeyCode::Tab | KeyCode::BackTab) => EventHandlingStatus::Handled,
        (true, _) => {
            let Ok(store) = state.cert.cert_info_store.as_ref() else {
                return EventHandlingStatus::Ignored;
            };
            let (status, selected) = state.encrypt.recipients_selector.handle_key(key);
            if let Some(selected) = selected.and_then(|s| store.get(s)) {
                if !state
                    .encrypt
                    .recipients
                    .iter()
                    .any(|r| r.fingerprint == selected.fingerprint)
                {
                    state.encrypt.recipients.push(selected.clone());
                }
            }
            status
        }
        (false, KeyCode::Enter) => {
            state.encrypt.recipients_selector.open();
            EventHandlingStatus::Handled
        }
        (false, KeyCode::Down | KeyCode::Char('j')) => {
            state.encrypt.recipients_state.select_next();
            EventHandlingStatus::Handled
        }
        (false, KeyCode::Up | KeyCode::Char('k')) => {
            state.encrypt.recipients_state.select_previous();
            EventHandlingStatus::Handled
        }
        (false, KeyCode::Char('x')) if key.modifiers == KeyModifiers::NONE => {
            if let Some(selected) = state.encrypt.recipients_state.selected() {
                state.encrypt.recipients.remove(selected);
            }
            EventHandlingStatus::Handled
        }
        _ => EventHandlingStatus::Ignored,
    },
};

pub(super) const TRANSFER_ID_SELECTOR: Component = Component {
    mode: Mode::ReadWrite,
    show: |state| matches!(state.encrypt.source, Source::Files { .. }),
    constraint: Constraint::Length(1),
    key_bindings: " <enter> select transfer ID <x> remove ",
    render: |state, frame, area, _| {
        state
            .encrypt
            .transfer_id_dialog
            .render_summary(state.encrypt.mode, frame, area);
    },
    render_popup: |state, frame, area| {
        if state.encrypt.transfer_id_dialog.is_open() {
            frame.render_widget(Clear, area);
            state
                .encrypt
                .transfer_id_dialog
                .render(state.encrypt.mode, frame, area);
        }
    },
    handle_key: |state, key, _| match (state.encrypt.transfer_id_dialog.is_open(), key.code) {
        (true, _) => state
            .encrypt
            .transfer_id_dialog
            .handle_key(state.encrypt.mode, key),
        (false, KeyCode::Enter) => {
            state.encrypt.transfer_id_dialog.open();
            EventHandlingStatus::Handled
        }
        (false, KeyCode::Char('x')) if key.modifiers == KeyModifiers::NONE => {
            state.encrypt.transfer_id_dialog.clear();
            EventHandlingStatus::Handled
        }
        _ => EventHandlingStatus::Ignored,
    },
};

pub(super) const DESTINATION_SELECTOR: Component = Component {
    mode: Mode::ReadWrite,
    show: |_| true,
    constraint: Constraint::Min(3),
    key_bindings: " <enter> select destination ",
    render: |state, frame, area, _| {
        let [header_area, destination_area] =
            Layout::vertical([Constraint::Length(2), Constraint::Fill(1)]).areas(area);
        let selected = state.encrypt.destination_selector.selected();
        if selected.is_valid() {
            frame.render_widget(
                Paragraph::new(Line::from_iter(["Destination type: ", selected.label()])),
                header_area,
            );
            selected.render_summary(frame, destination_area);
        } else {
            frame.render_widget(Paragraph::new("Select destination"), area);
        }
    },
    render_popup: |state, frame, area| {
        let dialog = &mut state.encrypt.destination_selector;
        if dialog.is_open() {
            frame.render_widget(Clear, area);
            dialog.render(frame, area);
        }
    },
    handle_key: |state, key, _| {
        let dialog = &mut state.encrypt.destination_selector;
        if dialog.is_open() {
            match (dialog.handle_key(key), key.code) {
                (EventHandlingStatus::Ignored, KeyCode::Tab | KeyCode::BackTab) => {
                    EventHandlingStatus::Handled
                }
                (status, _) => status,
            }
        } else if key.code == KeyCode::Enter {
            dialog.open();
            EventHandlingStatus::Handled
        } else {
            EventHandlingStatus::Ignored
        }
    },
};

pub(super) const RUN: Component = Component {
    mode: Mode::ReadWrite,
    show: |state| {
        matches!(state.encrypt.status, Status::New | Status::Error(_))
            && match &state.encrypt.source {
                Source::Package { .. } => true,
                Source::Files { files, .. } => {
                    !files.is_empty()
                        && state.encrypt.signer.is_some()
                        && !state.encrypt.recipients.is_empty()
                }
            }
            && state.encrypt.destination_selector.selected().is_valid()
            && match state.encrypt.mode {
                UserMode::Anonymous => true,
                #[cfg(feature = "auth")]
                UserMode::Authenticated => {
                    state.encrypt.transfer_id_dialog.portal.selected.is_some()
                }
            }
    },
    constraint: Constraint::Length(1),
    key_bindings: "<enter> run",
    render: |state, frame, area, _| {
        use crate::tui::widget::destination::DestinationKind::*;
        frame.render_widget(
            Paragraph::new(
                match (
                    &state.encrypt.source,
                    state.encrypt.destination_selector.selected().kind(),
                ) {
                    (Source::Files { .. }, Local) => "Encrypt and save",
                    (Source::Files { .. }, Remote) => "Encrypt and send",
                    (Source::Package { .. }, Remote) => "Send",
                    (Source::Package { .. }, Local) => "Unreachable",
                },
            ),
            area,
        );
    },
    render_popup: |_, _, _| {},
    handle_key: |_, key, sender| match key.code {
        KeyCode::Enter => {
            sender
                .send(Action::Encrypt(EncryptAction::Task(TaskAction::Run)).into())
                .expect("receiver is alive");
            EventHandlingStatus::Handled
        }
        _ => EventHandlingStatus::Ignored,
    },
};

pub(super) const PROGRESS: Component = Component {
    mode: Mode::ReadOnly,
    show: |state| matches!(state.encrypt.status, Status::Running | Status::Finished(_)),
    constraint: Constraint::Length(1),
    key_bindings: "",
    render: |state, frame, area, _| {
        state.encrypt.progress.render(frame, area);
    },
    render_popup: |_, _, _| {},
    handle_key: |_, _, _| EventHandlingStatus::Ignored,
};

pub(super) const STATUS: Component = Component {
    mode: Mode::ReadOnly,
    show: |state| matches!(state.encrypt.status, Status::Error(_) | Status::Finished(_)),
    constraint: Constraint::Max(5),
    key_bindings: "",
    render: |state, frame, area, _| {
        render_status(
            &state.encrypt.status,
            match state.encrypt.source {
                Source::Package { .. } => TaskKind::Transfer,
                Source::Files { .. } => TaskKind::Encrypt,
            },
            frame,
            area,
        );
    },
    render_popup: |_, _, _| {},
    handle_key: |_, _, _| EventHandlingStatus::Ignored,
};

#[derive(Debug, Default)]
pub(super) struct TransferIdDialog {
    show: bool,
    manual: TransferIdManual,
    #[cfg(feature = "auth")]
    pub(super) portal: TransferIdPortal,
    #[cfg(test)]
    pub(super) clear_count: usize,
}

#[cfg(feature = "auth")]
#[derive(Debug, Default)]
pub(super) struct TransferIdPortal {
    pub(super) selector: crate::tui::widget::portal::DataTransferSelector,
    selected: Option<sett::portal::response::DataTransfer>,
}

impl TransferIdDialog {
    fn open(&mut self) {
        self.show = true;
    }

    fn is_open(&self) -> bool {
        self.show
    }

    fn close(&mut self) {
        self.show = false
    }

    fn render(&mut self, mode: UserMode, frame: &mut Frame, rect: Rect) {
        let [rect] = Layout::default()
            .constraints([Constraint::Min(0)])
            .margin(1)
            .areas(rect);
        match mode {
            UserMode::Anonymous => {
                let block = Block::bordered()
                    .title_top(" Select data transfer request ID ")
                    .title_bottom(Line::raw(" <enter/esc/q> close ").centered());
                let inner_area = block.inner(rect);
                frame.render_widget(block, rect);
                self.manual.render_form(frame, inner_area);
            }
            #[cfg(feature = "auth")]
            UserMode::Authenticated => {
                self.portal.selector.render(frame, rect);
            }
        }
    }

    fn render_summary(&mut self, mode: UserMode, frame: &mut Frame, rect: Rect) {
        match mode {
            UserMode::Anonymous => self.manual.render_summary(frame, rect),
            #[cfg(feature = "auth")]
            UserMode::Authenticated => {
                frame.render_widget(
                    Line::from_iter([
                        Span::from("Transfer ID: "),
                        if let Some(t) = &self.portal.selected {
                            Span::from(format!("{}, {}", t.id, t.project_name))
                        } else {
                            Span::styled(
                                "(missing)",
                                Style::default().fg(ratatui::style::Color::Yellow),
                            )
                        },
                    ]),
                    rect,
                );
            }
        }
    }

    fn handle_key(&mut self, mode: UserMode, key: KeyEvent) -> EventHandlingStatus {
        match key.code {
            KeyCode::Esc | KeyCode::Char('q') => {
                self.close();
                EventHandlingStatus::Handled
            }
            KeyCode::Tab | KeyCode::BackTab => EventHandlingStatus::Handled,
            _ => match mode {
                UserMode::Anonymous => match key.code {
                    KeyCode::Enter => {
                        self.close();
                        EventHandlingStatus::Handled
                    }
                    _ => self.manual.handle_key(key),
                },
                #[cfg(feature = "auth")]
                UserMode::Authenticated => {
                    let (status, selected) = self.portal.selector.handle_key(key);
                    if selected.is_some() {
                        self.portal.selected = selected;
                        self.close();
                    }
                    status
                }
            },
        }
    }

    pub(super) fn value(&self, mode: UserMode) -> Option<u32> {
        match mode {
            UserMode::Anonymous => self.manual.transfer_id.value(),
            #[cfg(feature = "auth")]
            UserMode::Authenticated => self.portal.selected.as_ref().map(|s| s.id),
        }
    }

    pub(super) fn clear(&mut self) {
        self.manual.clear();
        #[cfg(feature = "auth")]
        {
            self.portal.selected = None;
            self.portal.selector.clear_selection();
        }
        #[cfg(test)]
        {
            self.clear_count += 1;
        }
    }
}

#[derive(Debug)]
pub(super) struct TransferIdManual {
    selected_field: Option<usize>,
    transfer_id: PositiveIntegerField,
}

impl Default for TransferIdManual {
    fn default() -> Self {
        Self {
            selected_field: Some(0),
            transfer_id: PositiveIntegerField::new("Transfer ID (optional)", Required::No),
        }
    }
}

impl Form for TransferIdManual {
    fn label(&self) -> &'static str {
        "Transfer ID"
    }

    fn field_count(&self) -> usize {
        1
    }

    fn selected_field(&mut self) -> &mut Option<usize> {
        &mut self.selected_field
    }

    fn fields(&self) -> Box<(dyn Iterator<Item = &dyn Field> + '_)> {
        Box::new(std::iter::once(&self.transfer_id as &dyn Field))
    }

    fn fields_mut(&mut self) -> Box<(dyn Iterator<Item = &mut dyn Field> + '_)> {
        Box::new(std::iter::once(&mut self.transfer_id as &mut dyn Field))
    }
}
