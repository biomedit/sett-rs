pub(crate) mod action;
mod component;

use std::path::PathBuf;

use crossterm::event::{KeyCode, KeyModifiers};
use ratatui::layout::Constraint;
use sett::package::{Metadata, Package};

use crate::tui::{
    event::EventHandlingStatus,
    tab::{decrypt::component::COMPONENTS, Status, Tab, UserMode},
    widget::{
        file::{FileSelector, FileType},
        progress::Progress,
        CyclicIndex,
    },
};

pub(super) const TAB: Tab = Tab {
    label: "Decrypt",
    render: |state, frame, rect| {
        const CONSTRAINTS: [Constraint; COMPONENTS.len()] = super::constraints(&COMPONENTS);
        super::render_components(
            &COMPONENTS,
            &CONSTRAINTS,
            state,
            state.decrypt.current_component,
            frame,
            rect,
        );
    },
    handle_key: |state, key, sender| {
        if let Status::Running = state.decrypt.status {
            return EventHandlingStatus::Ignored;
        }
        if let EventHandlingStatus::Handled =
            (COMPONENTS[*state.decrypt.current_component].handle_key)(state, key, sender.clone())
        {
            return EventHandlingStatus::Handled;
        }
        match key.code {
            KeyCode::Tab => {
                let mut current_component = state.decrypt.current_component;
                state.decrypt.current_component = current_component
                    .find(|&c| COMPONENTS[*c].is_interactive(state))
                    .expect("component found");
                EventHandlingStatus::Handled
            }
            KeyCode::BackTab => {
                let mut current_component = state.decrypt.current_component;
                state.decrypt.current_component = current_component
                    .rfind(|&c| COMPONENTS[*c].is_interactive(state))
                    .expect("component found");
                EventHandlingStatus::Handled
            }
            KeyCode::Char('x') if key.modifiers == KeyModifiers::CONTROL => {
                state.decrypt.clear();
                EventHandlingStatus::Handled
            }
            _ => EventHandlingStatus::Ignored,
        }
    },
};

#[derive(Debug, Default)]
pub(crate) struct Decrypt {
    source_selector: component::SourceDialog,
    loaded_package: Option<LoadedPackage>,
    destination_selector: FileSelector,
    destination: Option<PathBuf>,
    progress: Progress,
    status: Status,
    current_component: CyclicIndex<{ COMPONENTS.len() }>,
    user_mode: UserMode,
    #[cfg(test)]
    clear_count: usize,
}

#[derive(Debug)]
pub(crate) struct LoadedPackage {
    package: Package<sett::package::state::Unverified>,
    metadata: Metadata,
    location: String,
}

impl Decrypt {
    pub(crate) fn new() -> Self {
        Self {
            destination_selector: FileSelector::new().with_file_type(FileType::Directory),
            ..Default::default()
        }
    }

    fn clear(&mut self) {
        self.source_selector.clear();
        self.loaded_package = Default::default();
        self.destination = Default::default();
        #[cfg(test)]
        {
            self.clear_count += 1
        }
    }
}

#[cfg(test)]
mod tests {
    use crossterm::event::KeyEvent;

    use super::*;
    use crate::tui::state::State;

    #[test]
    fn key_event() {
        let (tx, _) = tokio::sync::mpsc::unbounded_channel();
        let mut state = State::default();
        // Event captured by the first component.
        assert_eq!(
            (TAB.handle_key)(&mut state, KeyCode::Char('x').into(), tx.clone(),),
            EventHandlingStatus::Handled
        );
        assert_eq!(state.decrypt.clear_count, 0);
        // Clear form
        assert_eq!(
            (TAB.handle_key)(
                &mut state,
                KeyEvent::new(KeyCode::Char('x'), KeyModifiers::CONTROL),
                tx.clone(),
            ),
            EventHandlingStatus::Handled
        );
        assert_eq!(state.decrypt.clear_count, 1);
        // Switch to the next component
        let index = state.decrypt.current_component;
        assert_eq!(
            (TAB.handle_key)(&mut state, KeyCode::Tab.into(), tx.clone(),),
            EventHandlingStatus::Handled
        );
        assert!(state.decrypt.current_component > index);
        // Switch back to the previous component
        assert_eq!(
            (TAB.handle_key)(&mut state, KeyCode::BackTab.into(), tx.clone(),),
            EventHandlingStatus::Handled
        );
        assert_eq!(state.decrypt.current_component, index);
    }

    #[test]
    fn clear_form() {
        for index in CyclicIndex::<{ COMPONENTS.len() }>::default().take(COMPONENTS.len()) {
            let mut state = Decrypt {
                destination: Some(PathBuf::from("/tmp")),
                current_component: index,
                ..Decrypt::default()
            };
            state.clear();
            assert_eq!(state.destination, None);
            assert_eq!(state.source_selector.clear_counter, 1);
        }
    }
}
