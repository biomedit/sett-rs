use std::path::PathBuf;

use sett::{package::Package, remote::s3::ClientBuilder};
use tokio::sync::mpsc::UnboundedSender;

use crate::tui::{
    action::Action,
    event::Event,
    tab::{
        decrypt::{Decrypt, LoadedPackage},
        ProgressUpdater, Status, TaskAction,
    },
    widget::progress::Progress,
};

#[derive(Debug)]
pub(crate) enum DecryptAction {
    Task(TaskAction),
    LoadPackage(PackageSource),
    SetPackage(LoadedPackage),
    #[cfg(feature = "auth")]
    SwitchMode(crate::tui::tab::UserMode),
    #[cfg(feature = "auth")]
    LoadDataTransfers,
    #[cfg(feature = "auth")]
    SetDataTransfers(Result<Vec<sett::portal::response::DataTransfer>, String>),
}

impl From<DecryptAction> for Event {
    fn from(value: DecryptAction) -> Self {
        Event::Action(Action::Decrypt(value))
    }
}

#[derive(Debug)]
pub(crate) enum PackageSource {
    Local(PathBuf),
    S3 {
        client_builder: Box<ClientBuilder>,
        bucket: String,
        object: String,
        location: String,
    },
    #[cfg(feature = "auth")]
    S3Auth {
        transfer_id: u32,
        object: String,
    },
}

async fn load_package(
    source: PackageSource,
    #[cfg(feature = "auth")] sender: UnboundedSender<Event>,
) -> anyhow::Result<LoadedPackage> {
    let (package, location) = match source {
        PackageSource::Local(p) => {
            let location = p.to_string_lossy().to_string();
            (Package::open(p).await?, location)
        }
        PackageSource::S3 {
            client_builder,
            bucket,
            object,
            location,
        } => {
            let client = client_builder.build().await?;
            (Package::open_s3(&client, bucket, object).await?, location)
        }
        #[cfg(feature = "auth")]
        PackageSource::S3Auth {
            transfer_id: dtr,
            object,
        } => {
            let portal_client = crate::util::get_portal_client()?;
            let (tx, rx) = tokio::sync::oneshot::channel();
            sender.send(crate::tui::tab::user::AuthAction::GetToken(tx).into())?;
            let token = rx
                .await?
                .ok_or(anyhow::anyhow!("Unable to get OAuth2 token"))?;
            let connection_details = portal_client
                .get_s3_connection_details(
                    dtr,
                    sett::portal::S3Permission::Read,
                    &token.access_token,
                )
                .await?;
            let client = connection_details.build_client().await?;
            let location = format!(
                "{}/{}/{}",
                connection_details.endpoint, connection_details.bucket, object
            );
            (
                Package::open_s3(&client, connection_details.bucket, object).await?,
                location,
            )
        }
    };
    let cert_store =
        sett::openpgp::certstore::CertStore::open(&sett::openpgp::certstore::CertStoreOpts {
            read_only: true,
            ..Default::default()
        })?;
    let unverified = package.clone();
    let verified = package.verify(&cert_store).await?;
    let metadata = verified.metadata().await?;
    Ok(LoadedPackage {
        package: unverified,
        metadata,
        location,
    })
}

impl Decrypt {
    pub(crate) async fn handle_action(
        &mut self,
        action: DecryptAction,
        sender: UnboundedSender<Event>,
    ) {
        match action {
            DecryptAction::LoadPackage(source) => {
                self.status = Status::New;
                tokio::spawn(async move {
                    match load_package(
                        source,
                        #[cfg(feature = "auth")]
                        sender.clone(),
                    )
                    .await
                    {
                        Ok(package) => sender
                            .send(DecryptAction::SetPackage(package).into())
                            .expect("receiver is alive"),
                        Err(e) => send_error(e, sender),
                    }
                });
            }
            DecryptAction::SetPackage(package) => self.loaded_package = Some(package),
            DecryptAction::Task(task_action) => match task_action {
                TaskAction::Run => {
                    self.status = Status::Running;
                    self.progress = Default::default();
                    if let (Some(loaded_package), Some(destination)) =
                        (&self.loaded_package, &self.destination)
                    {
                        let package = loaded_package.package.clone();
                        let output = Some(destination.clone());
                        tokio::spawn(async move {
                            let sender_clone = sender.clone();
                            let progress = ProgressUpdater::new(move |current, total| {
                                sender_clone
                                    .send(
                                        TaskAction::Progress(Progress { current, total })
                                            .into_decrypt_event(),
                                    )
                                    .expect("receiver is alive");
                            });

                            let sender_clone = sender.clone();
                            let password = move |hint: sett::openpgp::crypto::PasswordHint| {
                                let (tx, rx) = tokio::sync::oneshot::channel();
                                sender_clone
                                    .send(Action::PromptPassword { hint, result: tx }.into())
                                    .expect("receiver is alive");
                                rx.blocking_recv().expect("received")
                            };

                            let cert_store = match sett::openpgp::certstore::CertStore::open(
                                &sett::openpgp::certstore::CertStoreOpts {
                                    read_only: true,
                                    ..Default::default()
                                },
                            ) {
                                Ok(cert_store) => cert_store,
                                Err(e) => {
                                    send_error(e.into(), sender);
                                    return;
                                }
                            };
                            let key_store =
                                match sett::openpgp::keystore::KeyStore::open(None).await {
                                    Ok(key_store) => key_store,
                                    Err(e) => {
                                        send_error(e.into(), sender);
                                        return;
                                    }
                                };
                            let result = sett::decrypt::decrypt(sett::decrypt::DecryptOpts {
                                package,
                                key_store,
                                cert_store,
                                output,
                                decrypt_only: false,
                                mode: sett::task::Mode::Run,
                                password,
                                progress: Some(progress),
                            })
                            .await;
                            match result {
                                Ok(status) => sender
                                    .send(Status::Finished(status).into_decrypt_event())
                                    .expect("receiver is alive"),

                                Err(e) => send_error(e.into(), sender),
                            }
                        });
                    }
                }
                TaskAction::Progress(progress) => {
                    self.progress = progress;
                }
                TaskAction::Status(status) => {
                    if let Status::Finished { .. } = status {
                        self.loaded_package = None;
                    }
                    self.status = status;
                }
            },
            #[cfg(feature = "auth")]
            DecryptAction::SwitchMode(mode) => {
                match mode {
                    crate::tui::tab::UserMode::Anonymous => {}
                    crate::tui::tab::UserMode::Authenticated => {
                        sender
                            .send(Action::Decrypt(DecryptAction::LoadDataTransfers).into())
                            .expect("receiver is alive");
                    }
                }
                self.user_mode = mode;
            }
            #[cfg(feature = "auth")]
            DecryptAction::LoadDataTransfers => {
                tokio::spawn(async move {
                    sender.send(
                        Action::Decrypt(DecryptAction::SetDataTransfers(
                            crate::tui::tab::load_data_transfers(sender.clone()).await,
                        ))
                        .into(),
                    )
                });
            }
            #[cfg(feature = "auth")]
            DecryptAction::SetDataTransfers(transfers) => {
                self.source_selector.set_data_transfers(transfers);
            }
        }
    }
}

fn send_error(error: anyhow::Error, sender: UnboundedSender<Event>) {
    sender
        .send(Status::Error(format!("{:#}", error)).into_decrypt_event())
        .expect("receiver is alive");
}
