use crossterm::event::{KeyCode, KeyEvent, KeyModifiers};
use ratatui::{
    layout::{Constraint, Layout, Rect},
    text::{Line, Span},
    widgets::{Block, Clear, Paragraph, Tabs},
    Frame,
};
use sett::remote::s3::ClientBuilder;
use tokio::sync::mpsc::UnboundedSender;

use crate::tui::{
    event::EventHandlingStatus,
    tab::{
        decrypt::action::{DecryptAction, PackageSource},
        render_metadata, render_status, Event, Status, TaskAction, TaskKind, UserMode,
    },
    widget::{
        component::{Component, Mode},
        field::{url_validator, DisplayMode, Field, PathField, Required, TextField},
        file::{FileSelector, FileType},
        form::Form,
    },
};

pub(super) const COMPONENTS: [&Component; 5] = [
    &SOURCE_SELECTOR,
    &DESTINATION_SELECTOR,
    &RUN,
    &PROGRESS,
    &STATUS,
];

pub(super) const SOURCE_SELECTOR: Component = Component {
    mode: Mode::ReadWrite,
    show: |_| true,
    constraint: Constraint::Length(6),
    key_bindings: " <enter> select package <x> remove ",
    render: |state, frame, area, _| match state.decrypt.loaded_package.as_ref() {
        Some(package) => {
            let [header_area, metadata_area] =
                Layout::vertical([Constraint::Length(2), Constraint::Fill(1)]).areas(area);
            frame.render_widget(
                Line::from_iter([
                    Span::from("Selected package ("),
                    Span::from(package.location.as_str()),
                    Span::from("):"),
                ]),
                header_area,
            );
            render_metadata(&package.metadata, frame, metadata_area);
        }
        None => {
            frame.render_widget(Paragraph::new("Select package"), area);
        }
    },
    render_popup: |state, frame, area| {
        if state.decrypt.source_selector.is_open() {
            frame.render_widget(Clear, area);
            state
                .decrypt
                .source_selector
                .render(state.decrypt.user_mode, frame, area);
        }
    },
    handle_key: |state, key, sender| match (state.decrypt.source_selector.is_open(), key.code) {
        (true, _) => state
            .decrypt
            .source_selector
            .handle_key(state.decrypt.user_mode, key, sender),
        (false, KeyCode::Enter) => {
            state.decrypt.source_selector.open();
            EventHandlingStatus::Handled
        }
        (false, KeyCode::Char('x')) if key.modifiers == KeyModifiers::NONE => {
            state.decrypt.loaded_package = None;
            EventHandlingStatus::Handled
        }
        _ => EventHandlingStatus::Ignored,
    },
};

pub(super) const DESTINATION_SELECTOR: Component = Component {
    mode: Mode::ReadWrite,
    show: |_| true,
    constraint: Constraint::Length(1),
    key_bindings: " <enter> select destination ",
    render: |state, frame, area, _| match state.decrypt.destination.as_ref() {
        Some(p) => {
            frame.render_widget(
                Line::from_iter([
                    Span::from("Selected destination: "),
                    Span::from(p.to_string_lossy()),
                ]),
                area,
            );
        }
        None => {
            frame.render_widget(Paragraph::new("Select destination directory"), area);
        }
    },
    render_popup: |state, frame, area| {
        if state.decrypt.destination_selector.is_open() {
            frame.render_widget(Clear, area);
            state.decrypt.destination_selector.render(frame, area);
        }
    },
    handle_key: |state, key, _| match (state.decrypt.destination_selector.is_open(), key.code) {
        (true, KeyCode::Tab | KeyCode::BackTab) => EventHandlingStatus::Handled,
        (true, _) => state
            .decrypt
            .destination_selector
            .handle_key(key, |selected| state.decrypt.destination = Some(selected)),
        (false, KeyCode::Enter) => {
            state.decrypt.destination_selector.open();
            EventHandlingStatus::Handled
        }
        _ => EventHandlingStatus::Ignored,
    },
};

pub(super) const RUN: Component = Component {
    mode: Mode::ReadWrite,
    show: |state| {
        matches!(state.decrypt.status, Status::New | Status::Error(_))
            && state.decrypt.loaded_package.is_some()
            && state.decrypt.destination.is_some()
    },
    constraint: Constraint::Length(1),
    key_bindings: "<enter> run",
    render: |_, frame, area, _| {
        frame.render_widget(Paragraph::new("Decrypt"), area);
    },
    render_popup: |_, _, _| {},
    handle_key: |_, key, sender| match key.code {
        KeyCode::Enter => {
            sender
                .send(TaskAction::Run.into_decrypt_event())
                .expect("receiver is alive");
            EventHandlingStatus::Handled
        }
        _ => EventHandlingStatus::Ignored,
    },
};

pub(super) const PROGRESS: Component = Component {
    mode: Mode::ReadOnly,
    show: |state| matches!(state.decrypt.status, Status::Running | Status::Finished(_)),
    constraint: Constraint::Length(1),
    key_bindings: "",
    render: |state, frame, area, _| {
        state.decrypt.progress.render(frame, area);
    },
    render_popup: |_, _, _| {},
    handle_key: |_, _, _| EventHandlingStatus::Ignored,
};

pub(super) const STATUS: Component = Component {
    mode: Mode::ReadOnly,
    show: |state| matches!(state.decrypt.status, Status::Error(_) | Status::Finished(_)),
    constraint: Constraint::Length(5),
    key_bindings: "",
    render: |state, frame, area, _| {
        render_status(&state.decrypt.status, TaskKind::Decrypt, frame, area);
    },
    render_popup: |_, _, _| {},
    handle_key: |_, _, _| EventHandlingStatus::Ignored,
};

enum FormValidationResult {
    Valid(PackageSource),
    Invalid,
}

trait Source: Form {
    fn get_package_source(&self) -> FormValidationResult;
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum SourceKind {
    Local,
    S3,
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Sources {
    selected: SourceKind,
    local: Local,
    s3_manual: S3Form,
    #[cfg(feature = "auth")]
    pub(super) s3_portal: S3Portal,
}

impl Default for Sources {
    fn default() -> Self {
        Self {
            selected: SourceKind::Local,
            local: Local::new(),
            s3_manual: S3Form::new(),
            #[cfg(feature = "auth")]
            s3_portal: S3Portal::new(),
        }
    }
}

impl Sources {
    fn index(&self) -> usize {
        match self.selected {
            SourceKind::Local => 0,
            SourceKind::S3 => 1,
        }
    }

    fn select(&mut self, n: usize) {
        match n.min(2).saturating_sub(1) {
            0 => self.selected = SourceKind::Local,
            1 => self.selected = SourceKind::S3,
            _ => unreachable!(),
        }
    }

    fn labels(&self, mode: UserMode) -> [&str; 2] {
        match mode {
            UserMode::Anonymous => [self.local.label(), self.s3_manual.label()],
            #[cfg(feature = "auth")]
            UserMode::Authenticated => [self.local.label(), self.s3_portal.label()],
        }
    }

    fn selected(&mut self, mode: UserMode) -> &mut dyn Source {
        match (self.selected, mode) {
            (SourceKind::Local, _) => &mut self.local,
            (SourceKind::S3, UserMode::Anonymous) => &mut self.s3_manual,
            #[cfg(feature = "auth")]
            (SourceKind::S3, UserMode::Authenticated) => &mut self.s3_portal,
        }
    }

    fn clear(&mut self) {
        self.local.clear();
        self.s3_manual.clear();
        #[cfg(feature = "auth")]
        self.s3_portal.clear();
    }
}

#[derive(Debug, Default, PartialEq, Eq)]
pub(super) struct SourceDialog {
    show: bool,
    sources: Sources,
    #[cfg(test)]
    pub(super) clear_counter: usize,
}

impl SourceDialog {
    pub(super) fn open(&mut self) {
        self.show = true
    }

    pub(super) fn is_open(&self) -> bool {
        self.show
    }

    fn close(&mut self) {
        self.show = false;
    }

    pub(super) fn clear(&mut self) {
        self.sources.clear();
        #[cfg(test)]
        {
            self.clear_counter += 1;
        }
    }

    fn render(&mut self, mode: UserMode, frame: &mut Frame, rect: Rect) {
        let [tabs_area, body_area] = Layout::vertical([Constraint::Length(3), Constraint::Fill(1)])
            .margin(1)
            .areas(rect);
        let tabs = self
            .sources
            .labels(mode)
            .iter()
            .enumerate()
            .map(|(i, label)| format!("[{}] {}", i + 1, label))
            .collect::<Tabs>()
            .select(self.sources.index())
            .block(Block::bordered());
        frame.render_widget(tabs, tabs_area);
        let block = Block::bordered().title_bottom(
            Line::from(
                " <tab> change field <enter> load package <q> close <c-h> show/hide secret value <c-x> clear form ",
            )
            .centered(),
        );
        self.sources
            .selected(mode)
            .render_form(frame, block.inner(body_area));
        frame.render_widget(block, body_area);
    }

    fn handle_key(
        &mut self,
        mode: UserMode,
        key: KeyEvent,
        sender: UnboundedSender<Event>,
    ) -> EventHandlingStatus {
        let source = self.sources.selected(mode);
        if let EventHandlingStatus::Handled = source.handle_key(key) {
            return EventHandlingStatus::Handled;
        }
        match key.code {
            KeyCode::Enter => {
                if let FormValidationResult::Valid(package_source) = source.get_package_source() {
                    sender
                        .send(DecryptAction::LoadPackage(package_source).into())
                        .expect("receiver is alive");
                    self.close()
                }
                EventHandlingStatus::Handled
            }
            KeyCode::Char(n) if n.is_ascii_digit() => {
                self.sources
                    .select(n.to_digit(10).expect("valid digit") as usize);
                EventHandlingStatus::Handled
            }
            KeyCode::Esc | KeyCode::Char('q') => {
                self.close();
                EventHandlingStatus::Handled
            }
            _ => EventHandlingStatus::Ignored,
        }
    }

    #[cfg(feature = "auth")]
    pub(crate) fn set_data_transfers(
        &mut self,
        transfers: Result<Vec<sett::portal::response::DataTransfer>, String>,
    ) {
        self.sources
            .s3_portal
            .data_transfer
            .selector
            .set_data_transfers(transfers);
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
struct Local {
    selected_field: Option<usize>,
    path: PathField,
}

impl Local {
    fn new() -> Self {
        Self {
            selected_field: Some(0),
            path: PathField::new(
                "Package path",
                Required::Yes,
                FileSelector::new()
                    .with_file_type(FileType::File)
                    .with_extension_filter(Some(b".zip".to_vec())),
            ),
        }
    }
}

impl Form for Local {
    fn label(&self) -> &'static str {
        "Local"
    }

    fn field_count(&self) -> usize {
        1
    }

    fn selected_field(&mut self) -> &mut Option<usize> {
        &mut self.selected_field
    }

    fn fields(&self) -> Box<(dyn Iterator<Item = &dyn Field> + '_)> {
        Box::new(std::iter::once(&self.path as &dyn Field))
    }

    fn fields_mut(&mut self) -> Box<(dyn Iterator<Item = &mut dyn Field> + '_)> {
        Box::new(std::iter::once(&mut self.path as &mut dyn Field))
    }
}

impl Source for Local {
    fn get_package_source(&self) -> FormValidationResult {
        if self.is_valid() {
            FormValidationResult::Valid(PackageSource::Local(self.path.value().to_path_buf()))
        } else {
            FormValidationResult::Invalid
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) struct S3Form {
    selected_field: Option<usize>,
    endpoint: TextField,
    object: TextField,
    bucket: TextField,
    access_key: TextField,
    secret_key: TextField,
    session_token: TextField,
}

impl S3Form {
    const FIELD_COUNT: usize = 6;

    fn new() -> Self {
        Self {
            selected_field: None,
            endpoint: TextField::new(
                "Endpoint",
                Required::Yes,
                DisplayMode::Normal,
                Some(url_validator),
            ),
            object: TextField::new("Object", Required::Yes, DisplayMode::Normal, None),
            bucket: TextField::new("Bucket", Required::Yes, DisplayMode::Normal, None),
            access_key: TextField::new("Access key", Required::Yes, DisplayMode::Normal, None),
            secret_key: TextField::new("Secret key", Required::Yes, DisplayMode::Masked, None),
            session_token: TextField::new("Session token", Required::No, DisplayMode::Masked, None),
        }
    }
}

impl Form for S3Form {
    fn label(&self) -> &'static str {
        "S3"
    }

    fn field_count(&self) -> usize {
        Self::FIELD_COUNT
    }

    fn selected_field(&mut self) -> &mut Option<usize> {
        &mut self.selected_field
    }

    fn fields(&self) -> Box<(dyn Iterator<Item = &dyn Field> + '_)> {
        Box::new(
            ([
                &self.endpoint,
                &self.object,
                &self.bucket,
                &self.access_key,
                &self.secret_key,
                &self.session_token,
            ] as [&dyn Field; Self::FIELD_COUNT])
                .into_iter(),
        )
    }

    fn fields_mut(&mut self) -> Box<(dyn Iterator<Item = &mut dyn Field> + '_)> {
        Box::new(
            ([
                &mut self.endpoint,
                &mut self.object,
                &mut self.bucket,
                &mut self.access_key,
                &mut self.secret_key,
                &mut self.session_token,
            ] as [&mut dyn Field; Self::FIELD_COUNT])
                .into_iter(),
        )
    }
}

impl Source for S3Form {
    fn get_package_source(&self) -> FormValidationResult {
        fn empty_to_none(s: &str) -> Option<&str> {
            (!s.is_empty()).then_some(s)
        }
        if self.is_valid() {
            FormValidationResult::Valid(PackageSource::S3 {
                client_builder: Box::new(
                    ClientBuilder::default()
                        .endpoint(Some(self.endpoint.value()))
                        .access_key(empty_to_none(self.access_key.value()))
                        .secret_key(empty_to_none(self.secret_key.value()))
                        .session_token(empty_to_none(self.session_token.value())),
                ),
                bucket: self.bucket.value().to_string(),
                object: self.object.value().to_string(),
                location: format!(
                    "{}/{}/{}",
                    self.endpoint.value(),
                    self.bucket.value(),
                    self.object.value()
                ),
            })
        } else {
            FormValidationResult::Invalid
        }
    }
}

#[cfg(feature = "auth")]
#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) struct S3Portal {
    selected_field: Option<usize>,
    data_transfer: DataTransferField,
    object: TextField,
}

#[cfg(feature = "auth")]
impl Source for S3Portal {
    fn get_package_source(&self) -> FormValidationResult {
        if self.is_valid() {
            FormValidationResult::Valid(PackageSource::S3Auth {
                transfer_id: self.data_transfer.value.as_ref().expect("valid field").0.id,
                object: self.object.value().to_string(),
            })
        } else {
            FormValidationResult::Invalid
        }
    }
}

#[cfg(feature = "auth")]
impl S3Portal {
    const FIELD_COUNT: usize = 2;

    fn new() -> Self {
        Self {
            selected_field: Some(0),
            data_transfer: Default::default(),
            object: TextField::new("Object", Required::Yes, DisplayMode::Normal, None),
        }
    }
}

#[cfg(feature = "auth")]
impl Form for S3Portal {
    fn label(&self) -> &'static str {
        "S3"
    }

    fn field_count(&self) -> usize {
        Self::FIELD_COUNT
    }

    fn selected_field(&mut self) -> &mut Option<usize> {
        &mut self.selected_field
    }

    fn fields(&self) -> Box<(dyn Iterator<Item = &dyn Field> + '_)> {
        Box::new(
            ([&self.data_transfer, &self.object] as [&dyn Field; Self::FIELD_COUNT]).into_iter(),
        )
    }

    fn fields_mut(&mut self) -> Box<(dyn Iterator<Item = &mut dyn Field> + '_)> {
        Box::new(
            ([&mut self.data_transfer, &mut self.object] as [&mut dyn Field; Self::FIELD_COUNT])
                .into_iter(),
        )
    }
}

#[cfg(feature = "auth")]
#[derive(Clone, Debug, Default, PartialEq, Eq)]
struct DataTransferField {
    value: Option<(sett::portal::response::DataTransfer, String)>,
    selector: crate::tui::widget::portal::DataTransferSelector,
}

#[cfg(feature = "auth")]
impl Field for DataTransferField {
    fn label(&self) -> &str {
        "Data transfer"
    }

    fn clear(&mut self) {
        self.value = None;
    }

    fn render_value(&self, frame: &mut Frame, rect: Rect, focused: bool) {
        let value = self
            .value
            .as_ref()
            .map(|(_, s)| s.as_str())
            .unwrap_or_default();
        frame.render_widget(ratatui::text::Text::raw(value), rect);
        if focused {
            let mut rect = rect;
            rect.x += value.len() as u16;
            frame.render_widget(
                ratatui::text::Text::raw(if value.is_empty() {
                    "(press <space/enter> to select data transfer)"
                } else {
                    "  (press <space/enter> to change data transfer)"
                }),
                rect,
            );
        }
    }

    fn render_error(&self, _: &mut Frame, _: Rect) {}

    fn render_popup(&mut self, frame: &mut Frame, rect: Rect) {
        if self.selector.is_open() {
            frame.render_widget(Clear, rect);
            self.selector.render(frame, rect);
        }
    }

    fn handle_key(&mut self, key: KeyEvent) -> EventHandlingStatus {
        match (self.selector.is_open(), key.code) {
            (false, KeyCode::Enter | KeyCode::Char(' ')) => {
                self.selector.open();
                EventHandlingStatus::Handled
            }
            (false, _) => EventHandlingStatus::Ignored,
            (true, _) => {
                let (status, selected) = self.selector.handle_key(key);
                if let Some(transfer) = selected {
                    let s = format!("{}, {}", transfer.id, transfer.project_name);
                    self.value = Some((transfer, s));
                }
                status
            }
        }
    }

    fn validate(&self) -> Result<(), crate::tui::widget::field::ValidationError> {
        if self.value.is_none() {
            return Err(crate::tui::widget::field::ValidationError::Required);
        }
        Ok(())
    }

    fn required(&self) -> Required {
        Required::Yes
    }
}
