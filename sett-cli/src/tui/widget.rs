use ratatui::layout::{Constraint, Flex, Layout, Rect};

pub(crate) mod cert;
pub(crate) mod component;
pub(crate) mod destination;
pub(crate) mod field;
pub(crate) mod file;
pub(crate) mod form;
pub(crate) mod password;
#[cfg(feature = "auth")]
pub(crate) mod portal;
pub(crate) mod progress;

/// Creates a centered rect using up certain percentage of the available rect
pub(crate) fn popup_area(area: Rect, percent_x: u16, percent_y: u16) -> Rect {
    let [area] = Layout::vertical([Constraint::Percentage(percent_y)])
        .flex(Flex::Center)
        .areas(area);
    let [area] = Layout::horizontal([Constraint::Percentage(percent_x)])
        .flex(Flex::Center)
        .areas(area);
    area
}

#[derive(Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub(crate) struct CyclicIndex<const N: usize>(usize);

impl<const N: usize> Iterator for CyclicIndex<N> {
    type Item = Self;

    fn next(&mut self) -> Option<Self::Item> {
        debug_assert_ne!(N, 0);
        self.0 = (self.0 + 1) % N;
        Some(CyclicIndex::<N>(self.0))
    }
}

impl<const N: usize> DoubleEndedIterator for CyclicIndex<N> {
    fn next_back(&mut self) -> Option<Self::Item> {
        debug_assert_ne!(N, 0);
        self.0 = (self.0 + N - 1) % N;
        Some(CyclicIndex::<N>(self.0))
    }
}

impl<const N: usize> std::ops::Deref for CyclicIndex<N> {
    type Target = usize;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[cfg(test)]
mod tests {
    use super::CyclicIndex;

    #[test]
    fn cyclic_index() {
        let mut idx = CyclicIndex::<3>::default();
        assert_eq!(idx.next(), Some(CyclicIndex(1)));
        assert_eq!(idx.next(), Some(CyclicIndex(2)));
        assert_eq!(idx.next(), Some(CyclicIndex(0)));
        assert_eq!(idx.next(), Some(CyclicIndex(1)));
        assert_eq!(idx.next_back(), Some(CyclicIndex(0)));
        assert_eq!(idx.next_back(), Some(CyclicIndex(2)));
        assert_eq!(idx.next_back(), Some(CyclicIndex(1)));
    }
}
