use anyhow::Context;
use tracing::{level_filters::LevelFilter, Event, Subscriber};
use tracing_appender::non_blocking::WorkerGuard;
use tracing_subscriber::{
    filter::filter_fn, fmt, layer::SubscriberExt as _, EnvFilter, Layer, Registry,
};

#[derive(Debug, Clone, Copy)]
pub(crate) enum Level {
    Warn,
    Info,
    Debug,
    Trace,
}

impl From<u8> for Level {
    fn from(level: u8) -> Self {
        match level {
            0 => Level::Warn,
            1 => Level::Info,
            2 => Level::Debug,
            _ => Level::Trace,
        }
    }
}

pub(crate) fn init_file_log() -> anyhow::Result<WorkerGuard> {
    init_log(None)
}

pub(crate) fn init_file_and_console_log(
    console_log_level: impl Into<Level>,
) -> anyhow::Result<WorkerGuard> {
    init_log(Some(console_log_level.into()))
}

fn init_log(console: Option<Level>) -> anyhow::Result<WorkerGuard> {
    let (non_blocking_file_writer, non_blocking_file_writer_guard) = tracing_appender::non_blocking(
        tracing_appender::rolling::RollingFileAppender::builder()
            .rotation(tracing_appender::rolling::Rotation::DAILY)
            .filename_suffix("sett.jsonl")
            .build(
                dirs::data_dir()
                    .context("failed to get data directory")?
                    .join("ch.biomedit.sett")
                    .join("log"),
            )?,
    );
    let file_layer = fmt::layer()
        .with_writer(non_blocking_file_writer)
        .json()
        .with_filter(EnvFilter::try_from_default_env().unwrap_or_else(|_| {
            EnvFilter::try_new("info,sett=debug,sett_cli=debug").expect("valid directives")
        }));
    let console_layer = console.map(|level| {
        fmt::layer()
            .with_writer(std::io::stderr)
            .event_format(ConsoleLogFormatter)
            .with_filter(match level {
                Level::Warn => LevelFilter::WARN,
                Level::Info => LevelFilter::INFO,
                Level::Debug => LevelFilter::DEBUG,
                Level::Trace => LevelFilter::TRACE,
            })
            .with_filter(filter_fn(move |m| {
                if let Some(path) = m.module_path() {
                    // The `aws_smithy_runtime` crate is too verbose at INFO level
                    return !(path.starts_with("aws_smithy_runtime")
                        && matches!(level, Level::Warn | Level::Info));
                }
                true
            }))
    });
    tracing::subscriber::set_global_default(
        Registry::default().with(file_layer).with(console_layer),
    )?;
    Ok(non_blocking_file_writer_guard)
}

// TODO: currently `tracing_subscriber::fmt::format::Format` formatters don't
// allow hiding event scope arguments. Once they do, we should be able to get
// rid of this custom formatter (see, e.g.,
// https://github.com/tokio-rs/tracing/issues/2435)
struct ConsoleLogFormatter;

impl<S, N> fmt::FormatEvent<S, N> for ConsoleLogFormatter
where
    S: Subscriber + for<'a> tracing_subscriber::registry::LookupSpan<'a>,
    N: for<'a> fmt::FormatFields<'a> + 'static,
{
    fn format_event(
        &self,
        ctx: &fmt::FmtContext<'_, S, N>,
        mut writer: fmt::format::Writer<'_>,
        event: &Event<'_>,
    ) -> std::fmt::Result {
        let level = event.metadata().level();
        let style = match *level {
            tracing::Level::ERROR => console::Style::new().red(),
            tracing::Level::WARN => console::Style::new().yellow(),
            tracing::Level::INFO => console::Style::new().green(),
            tracing::Level::DEBUG => console::Style::new().blue(),
            tracing::Level::TRACE => console::Style::new().cyan(),
        };
        write!(&mut writer, "{:>5} ", style.apply_to(level))?;
        if let Some(scope) = ctx.event_scope() {
            for span in scope.from_root() {
                write!(writer, "{}: ", span.name())?;
            }
        }
        ctx.field_format().format_fields(writer.by_ref(), event)?;
        writeln!(writer)
    }
}
