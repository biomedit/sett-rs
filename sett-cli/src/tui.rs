use ratatui::{
    layout::{Alignment, Constraint, Layout, Rect},
    widgets::{Block, Tabs},
    Frame,
};

pub(crate) mod action;
pub(crate) mod event;
pub(crate) mod state;
pub(crate) mod tab;
pub(crate) mod widget;

pub(crate) async fn run() -> anyhow::Result<()> {
    let _log_guard = crate::log::init_file_log()?;
    let mut state = state::State::new();
    let mut terminal = ratatui::init();
    let mut events = event::EventHandler::new(250);
    state
        .send(tab::cert::CertAction::Refresh.into(), events.sender())
        .await;
    while state.running() {
        terminal.draw(|frame| render(&mut state, frame))?;
        let Some(e) = events.next().await else {
            tracing::error!("event stream closed");
            break;
        };
        state.send(e, events.sender()).await;
    }
    ratatui::restore();
    Ok(())
}

fn render(state: &mut state::State, frame: &mut Frame) {
    let area = frame.area();
    let [header_area, tab_area] =
        Layout::vertical([Constraint::Length(3), Constraint::Fill(1)]).areas(area);
    render_header(state, frame, header_area);
    (state.current_tab().render)(state, frame, tab_area);
    if let Some(prompt) = &state.password_prompt {
        widget::password::render(prompt, frame, area);
    }
}

fn render_header(state: &state::State, frame: &mut Frame, rect: Rect) {
    let block = Block::bordered()
        .title(concat!("| sett ", env!("CARGO_PKG_VERSION"), " |",))
        .title_alignment(Alignment::Center);
    #[cfg(feature = "auth")]
    let tabs_area = {
        const TABS_WIDTH: u16 = {
            let mut width = tab::TABS.len() * "[ ]  | ".len();
            let mut i = 0;
            while i < tab::TABS.len() {
                width += tab::TABS[i].label.len();
                i += 1;
            }
            width as u16
        };
        let [tabs_area, user_area] =
            Layout::horizontal([Constraint::Length(TABS_WIDTH), Constraint::Fill(1)])
                .areas(block.inner(rect));
        if let tab::user::AuthStatus::Authenticated(user) = &state.auth_status {
            frame.render_widget(
                ratatui::widgets::Paragraph::new(user.info.email.as_ref().map_or_else(
                    || {
                        user.info
                            .username
                            .as_ref()
                            .map_or("Logged in", |s| s.as_str())
                    },
                    |s| s.as_str(),
                ))
                .alignment(Alignment::Right),
                user_area,
            );
        }
        tabs_area
    };
    #[cfg(not(feature = "auth"))]
    let tabs_area = block.inner(rect);
    frame.render_widget(block, rect);
    let tabs = tab::TABS
        .iter()
        .enumerate()
        .map(|(idx, tab)| format!("[{}] {}", idx + 1, tab.label))
        .collect::<Tabs>()
        .select(state.current_tab_index());
    frame.render_widget(tabs, tabs_area);
}
