use clap::Parser;

mod cmd;
mod log;
mod tui;
mod util;

#[derive(Parser)]
#[command(about, version, propagate_version = true)]
struct Cli {
    #[command(subcommand)]
    command: Option<Base>,
}

#[derive(Parser)]
enum Base {
    /// Encrypt data package
    #[command(subcommand)]
    Encrypt(cmd::encrypt::Encrypt),
    /// Decrypt data package
    #[command(subcommand)]
    Decrypt(cmd::decrypt::Decrypt),
    /// Transfer data package
    #[command(subcommand)]
    Transfer(cmd::transfer::Transfer),
    /// OpenPGP key management
    #[command(subcommand)]
    Keys(cmd::certstore::Keys),
    /// Inspect sett package
    #[command(subcommand)]
    Inspect(cmd::inspect::Inspect),
    /// Interaction with Portal
    #[cfg(feature = "auth")]
    #[command(subcommand)]
    Portal(cmd::portal::Portal),
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    if let Cli { command: Some(c) } = Cli::parse() {
        match c {
            Base::Encrypt(c) => c.run().await,
            Base::Decrypt(c) => c.run().await,
            Base::Transfer(c) => c.run().await,
            Base::Keys(c) => c.run().await,
            Base::Inspect(c) => c.run().await,
            #[cfg(feature = "auth")]
            Base::Portal(c) => c.run().await,
        }
    } else {
        tui::run().await
    }
}
