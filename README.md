# sett - secure encryption and transfer tool

**sett** stands for `Secure Encryption and Transfer Tool`.

Powered by the Rust programming language, **sett** is designed to streamline the
packaging, encryption, and seamless transfer of your critical data. At its core,
**sett** integrates the [Sequoia-PGP](https://sequoia-pgp.org/) library for
encryption, and uses _S3_ and _SFTP_ as transfer protocols, ensuring security
and efficiency in data handling.

Offering both a sleek GUI experience with [sett-gui](sett-gui) and a powerful
command-line interface with [sett-cli](sett-cli), we support users of all
backgrounds in transferring their valuable information with ease. Learn more
about how to use **sett** in our
[documentation](https://biomedit.gitlab.io/docs/sett/).

**sett** is developed as part of the [BioMedIT](https://www.biomedit.ch/)
project, and is backed by a team of passionate developers, committed to
excellence.

This repository is composed of three sub-projects, each in its own directory:

- [sett](sett) - the main crate containing the main library.
- [sett-cli](sett-cli) - a _command line interface_ to the main library.
- [sett-gui](sett-gui) - a _graphical user interface_ to the main library.
